# ** MyOffice **

## Environment Preparation

* **Ionic 3.20.0**
    - http://ionicframework.com/getting-started/

* **Cordova 8.0.0**
    - https://cordova.apache.org/#getstarted

* **Node 8.12.0**
    - https://nodejs.org/en/download/


## Installation of dependencies

After cloning the repository project, perform the following procedures:

        npm install -g cordova ionic
        npm install
        ionic cordova platform add android
        ionic cordova platform add ios
        cordova clean
        ionic build

**  NOTE: Run the build on the device/simulator through Android Studio or Xcode. **

### Alternate Build Guide
Austin Smitherman's [Build Guide](buildguide.md)
