interface BaseModel {
    __local__: boolean;
    __locally_created__: boolean;
    __locally_updated__: boolean;
    __locally_deleted__: boolean;
}
