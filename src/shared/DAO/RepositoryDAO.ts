import { RecordTypeModel } from '../../app/model/RecordTypeModel';
import { ClientsModel } from '../../app/model/ClientsModel';
import { EventModel } from '../../app/model/EventModel';
import { Visita__cModel } from '../../app/model/Visita__cModel';
import { Hibrido_de_Visita__cModel } from '../../app/model/Hibrido_de_Visita__cModel';
import { BR_ProductUsed__cModel } from '../../app/model/BR_ProductUsed__cModel';
import { AttachmentModel } from '../../app/model/AttachmentModel';
import { BR_Sales_Coaching__cModel } from '../../app/model/BR_Sales_Coaching__cModel';

// import {
//     ServiceProvider
// } from '../providers/service-salesforce';
// import {
//     IService
// } from '../interfaces/IService';
// import {
//     ServiceGroupingHelper,
//     ServiceModel,
//     Entity
// } from './Helpers/ServiceGroupingHelper';

// export const RepositoryOrderType = {
//     ascending: 'ascending',
//     descending: 'descending',
// }

// export class RepositoryDAO < T extends BaseModel > {

//     private models: BaseModel[] = []
//     private currentService: IService

//     private constructorName: string

//     private serviceModels: ServiceModel[] = []
//     public modelsCursor: ResponseDAO

//     constructor(public serviceProvider: ServiceProvider, constructorName: string) {
//         this.constructorName = constructorName
//         let entities: Entity[] = ServiceGroupingHelper.loadEntitiesFromTS()
//         this.serviceModels = ServiceGroupingHelper.loadServicesFromEntities(entities, serviceProvider)
//         this.currentService = this.loadCurrentService()
//     }

//     private removeSuffix(modelName: string): string {
//         let suffix = 'Model'
//         let entityName = modelName.replace(suffix, '')
//         return entityName
//     }

//     private loadCurrentService(): IService {
//         let service: IService = undefined

//         const modelName = this.constructorName
//         console.log('---| modelName: ', modelName)
//         const entityName = this.removeSuffix(modelName)
//         console.log('---| entityName: ', entityName)

//         for (const serviceModel of this.serviceModels) {
//             if (serviceModel.entityName === entityName) {
//                 console.log('---| serviceModel.entityName: ', serviceModel.entityName)
//                 service = serviceModel.service
//             }
//         }

//         return service
//     }

//     public onSearchInputEnter(searchTerm): Promise < T[] > {
//         return new Promise((resolve, reject) => {
//             this.models = [];

//             if (searchTerm.length === 0) {
//                 searchTerm = ''
//             }

//             this.queryLikeFromSoupByName('%' + searchTerm + '%').then((response) => {
//                 this.modelsCursor = response;
//                 console.log('---| Response: ', response);
//                 resolve(response)
//             }).catch((err) => {
//                 this.error(err);
//             });
//         })
//     }

//     /**
//      * Promise que contém os registros da base local.
//      */
//     public queryLikeFromSoupWith(fieldKey: string, value: string): Promise < T[] > {
//         return new Promise((resolve, reject) => {
//             this.serviceProvider.getClientsService().queryLikeFromSoup(fieldKey, value, 'ascending', 10, 'Name', (response) => {
//                 resolve(response);
//             }, (err) => {
//                 this.error(err);
//             });
//         });
//     }

//     /**
//      * Promise que contém os registros da base local.
//      */
//     public queryLikeFromSoupByName(likeKey: string): Promise < any > {
//         return this.queryLikeFromSoupWith('Name', likeKey)
//     }

//     /**
//      * Função de callback de erro genérica
//      * @param err
//      */
//     public error(err: string) {
//         console.log(err);
//     }

//     // public doInfinite(): Promise < any > {
//     //     return this.moveCursorToNextPage();
//     // }

//     /**
//      * Promise que contém os registros da base local.
//      */
//     public queryAllFromSoup(orderBy: string, limit: number, repositoryOrderType: string): Promise < BaseModel[] > {
//         return new Promise((resolve, reject) => {
//             console.log('---| queryAllFromSoup:this.currentService: ', this.currentService)
//             this.currentService.queryAllFromSoup(orderBy, repositoryOrderType, limit, (response) => {
//                 console.log('---| queryAllFromSoup:response: ', response)
//                 console.log('---| queryAllFromSoup:response: ', JSON.stringify(response))
//                 this.modelsCursor = response;
//                 console.log('---| queryAllFromSoup:this.modelsCursor: ', this.modelsCursor)
//                 let baseModels: T[] = []
//                 for (let baseModelTemp of this.modelsCursor.currentPageOrderedEntries) {
//                     console.log('---| BaseModel: ', baseModelTemp)
//                     baseModels.push(baseModelTemp)
//                 }
//                 resolve(baseModels);
//             }, (error) => {
//                 this.error(error);
//                 reject(error)
//             });
//         });
//     }

//     /**
//      * Promise que navega entre as paginas do cursor.
//      */
//     public moveCursorToNextPage(): Promise < T[] > {
//         return new Promise((resolve, reject) => {
//             if (this.modelsCursor.currentPageIndex + 1 !== this.modelsCursor.totalPages) {
//                 console.log('---| this.modelsCursor: ', this.modelsCursor)
//                 console.log('---| moveCursorToNextPage:this.currentService: ', this.currentService)
//                 this.currentService.moveCursorToNextPage(this.modelsCursor, (response) => {
//                     this.modelsCursor = response;
//                     console.log('---| moveCursorToNextPage:this.modelsCursor: ', this.modelsCursor)
//                     let baseModels: BaseModel[] = this.modelsCursor.currentPageOrderedEntries
//                     resolve(this.modelsCursor);
//                 }, (err) => {
//                     reject(err);
//                 });
//             } else {
//                 resolve();
//             }
//         });
//     }

// }
export interface ResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: BaseModel[];
}

export interface RecordTypeResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: RecordTypeModel[];
}

export interface ClientResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: ClientsModel[];
}

export interface EventResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: EventModel[];
}

export interface VisitHybridResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: Hibrido_de_Visita__cModel[];
}

export interface SalesCoachingResponseDAO {
  totalPages: number;
  cursorId: string;
  currentPageIndex: number;
  pageSize: number;
  totalEntries: number;
  currentPageOrderedEntries: BR_Sales_Coaching__cModel[];
}

export interface VisitaResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: Visita__cModel[];
}

export interface ProductUsedResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: BR_ProductUsed__cModel[];
}

export interface AttachmentResponseDAO {
    totalPages: number;
    cursorId: string;
    currentPageIndex: number;
    pageSize: number;
    totalEntries: number;
    currentPageOrderedEntries: AttachmentModel[];
}
