import { ServiceProvider } from '../../providers/service-salesforce';

export class UserDAO {

  constructor(private service: ServiceProvider) { }

  public getUserNameById(Id: string) {
    return new Promise((resolve, reject) => {
      this.service.getUserService().queryExactFromSoup('Id', Id, 1, 'ascending', 'Id', (response) => {
        resolve(response.currentPageOrderedEntries[0]);
      }, (err) => {
        reject(err);
      });
    });
  }

}
