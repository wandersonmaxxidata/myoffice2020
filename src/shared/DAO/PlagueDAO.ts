import { ServiceProvider } from '../../providers/service-salesforce';
import { Praga__cModel } from '../../app/model/Praga__cModel';

export class PlagueDAO {

    constructor(private service: ServiceProvider) {}

    public savePlague(plague: Praga__cModel): Promise < any > {

        return new Promise((resolve, reject) => {
            console.log('---| Plague Saving... : ', plague);
            this.service.getPraga__cService().upsertSoupEntries([plague], (success) => {
                console.log('---| Plague Saved: ', success);
                resolve(success[0]);
            }, (error) => {
                console.log('---| Plague Error: ', error);
                reject(error);
            });
        });
    }

    public loadPlagues(hybridId) {

        return new Promise((resolve, reject) => {
            // Campo para busca (target), termo de busca (source), quant de resultados, ordem, campo de ordem
            this.service.getPraga__cService().queryExactFromSoup('Hibrido_de_Visita__c', hybridId, 3, 'ascending', 'Hibrido_de_Visita__c', (response) => {

              const plagues = [];
              // tslint:disable-next-line:forin
              for (const i in response.currentPageOrderedEntries) {
                plagues.push(new Praga__cModel(response.currentPageOrderedEntries[i]));
              }
              resolve(plagues);

            }, (err) => {
              console.log('-*-| plagueDAO.queryExactFromSoup: ', err);
              reject(err);
            });
          });

    }

    public removePlague(plague: Praga__cModel) {
        return new Promise((resolve, reject) => {
            const soupId = plague['_soupEntryId'];
            console.log('---| removePlague:soupId: ', soupId);
            this.service.getPraga__cService().removeEntries(soupId, (success) => {
                console.log('---| removePlague:success: ', success);
                resolve(success);
            }, (error) => {
                console.log('---| removePlague:error: ', error);
                reject(error);
            });
        });
    }

}
