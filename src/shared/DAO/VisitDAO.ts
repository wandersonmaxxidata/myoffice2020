import { BR_ProductUsed__cModel } from '../../app/model/BR_ProductUsed__cModel';
import { BR_WeedHandling__cModel } from '../../app/model/BR_WeedHandling__cModel';
import { BR_CropProducts__cModel } from '../../app/model/BR_CropProducts__cModel';
import { ServiceProvider } from '../../providers/service-salesforce';
import { Visita__cModel } from '../../app/model/Visita__cModel';
import { Hibrido_de_Visita__cModel } from '../../app/model/Hibrido_de_Visita__cModel';
import { VisitaResponseDAO } from './RepositoryDAO';
import { VisitStatus } from '../BO/VisitBO';
import { SaveHelper } from '../Helpers/SaveHelper';
import { DatabaseHelper } from '../Helpers/DatabaseHelper';
import { Doenca__cModel } from '../../app/model/Doenca__cModel';
import { Praga__cModel } from '../../app/model/Praga__cModel';

export class VisitDAO {

  constructor(public serviceProvider: ServiceProvider) { }

  public saveVisit(visit: Visita__cModel) {
    return new Promise((resolve, reject) => {

      if (!visit) {
        resolve();
        return;
      }

      SaveHelper.update_Local_(visit);

      this.serviceProvider.getVisita__cService().upsertSoupEntries([visit],
        (savedVisits: Visita__cModel[]) => {
          const model = new Visita__cModel(savedVisits[0]);
          resolve(model);
        },
        (error) => {
          reject(error);
        });
    });
  }

  public loadVisitByEventId(eventId): Promise<Visita__cModel> {
    return new Promise((resolve, reject) => {
      console.log('---| EventId__c: ', eventId);

      this.serviceProvider.getVisita__cService().queryExactFromSoup('EventId__c', eventId, 1, 'ascending', 'Id', (response: VisitaResponseDAO) => {
        const visit = new Visita__cModel(response.currentPageOrderedEntries[0]);

        visit.setHibrido_de_Visita__c(this.serviceProvider.getHibrido_de_Visita__cService()).then(() => {
          visit.Hibrido_de_Visita__c = DatabaseHelper.filterNotDeleted<Hibrido_de_Visita__cModel>(visit.Hibrido_de_Visita__c);

          const promises: Array<Promise<any>> = this.loadPlaguesHybrids(visit.Hibrido_de_Visita__c);
          const promisesDisease: Array<Promise<any>> = this.loadDiseasesOnHybrids(visit.Hibrido_de_Visita__c);

          for (const diseasePromise of promisesDisease) {
            promises.push(diseasePromise);
          }

          promises.push(visit.setBR_WeedHandling__c(this.serviceProvider.getBR_WeedHandling__cService()));
          promises.push(visit.setProductUsed(this.serviceProvider.getBR_ProductUsed__cService()));
          promises.push(visit.setCropProducts(this.serviceProvider.getBR_CropProducts__cService()));

          return Promise.all(promises);
        }).then(() => {
          visit.BR_WeedHandling__c = DatabaseHelper.filterNotDeleted<BR_WeedHandling__cModel>(visit.BR_WeedHandling__c);
          visit.ProductUsed = DatabaseHelper.filterNotDeleted<BR_ProductUsed__cModel>(visit.ProductUsed);
          visit.CropProducts = DatabaseHelper.filterNotDeleted<BR_CropProducts__cModel>(visit.CropProducts);
          resolve(visit);
        }).catch((error) => {
          reject(error);
        });

      }, (error) => {
        console.log('-*-| Error:loadVisitByEventId: ', error);
        reject(error);
      });
    });
  }


  private loadPlaguesHybrids(hybrids: Hibrido_de_Visita__cModel[]): Array<Promise<any>> {
    const promises: Array<Promise<any>> = [];

    for (const hybrid of hybrids) {
      const promise = hybrid.setPlagues(this.serviceProvider.getPraga__cService()).then(() => {
        hybrid.Plagues = DatabaseHelper.filterNotDeleted<Praga__cModel>(hybrid.Plagues);
      });
      promises.push(promise);
    }
    console.log('---| loadPlaguesHybrids:Promises: ', promises);
    return promises;
  }

  private loadDiseasesOnHybrids(hybrids: Hibrido_de_Visita__cModel[]): Array<Promise<any>> {
    const promises: Array<Promise<any>> = [];

    for (const hybrid of hybrids) {
      const promise = hybrid.setDiseases(this.serviceProvider.getDoenca__cService()).then((disease) => {
        hybrid.Diseases = DatabaseHelper.filterNotDeleted<Doenca__cModel>(hybrid.Diseases);
      });
      promises.push(promise);
    }
    console.log('---| loadDiseasesOnHybrids:promises.length: ', promises.length);
    return promises;
  }

  public deleteByEventId(eventId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const visitaService = this.serviceProvider.getVisita__cService();

      visitaService.queryExactFromSoup('EventId__c', eventId, 1, 'ascending', null, (data: VisitaResponseDAO) => {
        const visit = data.currentPageOrderedEntries[0];

        if (visit) {
          visit.Status__c = VisitStatus.canceled;
          SaveHelper.update_Local_(visit);

          visitaService.upsertSoupEntries([visit], () => {
            resolve();
          }, (error) => {
            reject(error);
          });

        } else {
          resolve();
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  public findById(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.serviceProvider.getVisita__cService().queryExactFromSoup('EventId__c', id, 10, 'ascending', 'EventId__c',
        (data: VisitaResponseDAO) => {
          const founded: boolean = data.currentPageOrderedEntries.length > 0;
          console.log('---| VisitDAO.findById:data: ', data);
          console.log('---| VisitDAO.findById:founded: ', founded);
          resolve(founded);
        }, (error) => {
          console.log('-*-| findById:error: ', error);
          reject(error);
        });
    });
  }

}
