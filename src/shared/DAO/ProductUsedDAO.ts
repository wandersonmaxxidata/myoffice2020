import { ServiceProvider } from '../../providers/service-salesforce';
import { BR_ProductUsed__cModel } from '../../app/model/BR_ProductUsed__cModel';

export class ProductUsedDAO {

    constructor(private service: ServiceProvider) {}

    public saveProductUsed(productUsed: BR_ProductUsed__cModel): Promise < any > {
        return new Promise((resolve, reject) => {
            console.log('---| ProductUsed Saving... : ', productUsed);
            this.service.getBR_ProductUsed__cService().upsertSoupEntries([productUsed], (success) => {
                console.log('---| ProductUsed Saved: ', success);
                resolve(success[0]);
            }, (error) => {
                console.log('---| ProductUsed Error: ', error);
                reject(error);
            });
        });
    }

    public loadProductsUsed(visitId) {
        return new Promise((resolve, reject) => {
            const productUsedDAO = this.service.getBR_ProductUsed__cService();
            // Campo para busca (target), termo de busca (source), quant de resultados, ordem, campo de ordem
            productUsedDAO.queryExactFromSoup('BR_VisitUsedProduct__c', visitId, 100, 'ascending', 'BR_VisitUsedProduct__c', (response) => {

              const productsUsed = [];
              // tslint:disable-next-line:forin
              for (const i in response.currentPageOrderedEntries) {
                productsUsed.push(new BR_ProductUsed__cModel(response.currentPageOrderedEntries[i]));
              }
              resolve(productsUsed);

            }, (err) => {
              console.log('-*-| productUsedDAO.queryExactFromSoup: ', err);
              reject(err);
            });
          });

    }

    public removeProductUsed(productUsed: BR_ProductUsed__cModel) {
      return new Promise((resolve, reject) => {
          const soupId = productUsed['_soupEntryId'];
          console.log('---| removeProductUsed:soupId: ', soupId);
          this.service.getBR_ProductUsed__cService().removeEntries(soupId, (success) => {
              console.log('---| removeProductUsed:success: ', success);
              resolve(success);
          }, (error) => {
              console.log('---| removeProductUsed:error: ', error);
              reject(error);
          });
      });
    }

}
