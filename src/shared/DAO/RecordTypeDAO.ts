import { ServiceProvider } from '../../providers/service-salesforce';
import { RecordTypeModel } from '../../app/model/RecordTypeModel';
import { RecordTypeResponseDAO } from './RepositoryDAO';
import { RecordTypeService } from '../../providers/recordtype/recordtype-service-salesforce';

export class RecordTypeDAO {

  private service: RecordTypeService;

  constructor(private serviceProvider: ServiceProvider) {
    this.service = this.serviceProvider.getRecordTypeService();
  }

  public loadRecordTypeForClient(developerName: string): Promise<string> {
    console.log('---| loadRecordTypeForVisit:developerName on Start: ', developerName);
    const sobjectType = 'Account';
    return this.loadRecordType(developerName, sobjectType);
  }

  public loadRecordTypeForEvent(developerName: string): Promise<string> {
    console.log('---| loadRecordTypeForEvents:developerName on Start: ', developerName);
    const sobjectType = 'Event';
    return this.loadRecordType(developerName, sobjectType);
  }

  public loadRecordTypeForVisit(developerName: string): Promise<string> {
    console.log('---| loadRecordTypeForVisit:developerName on Start: ', developerName);
    const sobjectType = 'Visita__c';
    return this.loadRecordType(developerName, sobjectType);
  }

  public loadRecordTypeForSalesCoaching(developerName: string): Promise<string> {
    console.log('---| loadRecordTypeForSalesCoaching:developerName on Start: ', developerName);
    const sobjectType = 'BR_Sales_Coaching__c';
    return this.loadRecordType(developerName, sobjectType);
  }

  public loadRecordTypeForVisitHybrid(developerName: string): Promise<string> {
    console.log('---| loadRecordTypeForVisit:developerName on Start: ', developerName);
    const sobjectType = 'Hibrido_de_Visita__c';
    return this.loadRecordType(developerName, sobjectType);
  }

  public loadRecordTypeForProfileMapping(developerName: string): Promise<string> {
    console.log('---| loadRecordTypeForVisit:developerName on Start: ', developerName);
    const sobjectType = 'BR_ProfileMapping__c';
    return this.loadRecordType(developerName, sobjectType);
  }

  public loadRecordType(developerName: string, sobjectType: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.service.queryExactFromSoup('DeveloperName',
        developerName, 100, 'ascending', 'Name', (data: RecordTypeResponseDAO) => {

          let recordType: RecordTypeModel;

          console.log('---| loadRecordType:data.currentPageOrderedEntries: ', data.currentPageOrderedEntries);
          for (const entry of data.currentPageOrderedEntries) {
            if (entry.SobjectType === sobjectType) {
              recordType = new RecordTypeModel(entry);
            }
          }

          const recordTypeId = recordType.getId();
          console.log('---| loadRecordTypeForVisit:recordTypeId: ', recordTypeId);
          console.log('---| loadRecordTypeForVisit:developerName: ', developerName);
          resolve(recordTypeId);
        }, (error) => {
          console.log('-*-| loadRecordTypeForVisit:recordType:error: ', error);
          reject(error);
        });
    });
  }

  public loadRecordTypeId(recordTypeId: string): Promise<string> {
    return new Promise((resolve, reject) => {
      this.service.queryExactFromSoup('Id', recordTypeId, 1, 'ascending', null, (res) => {
          resolve(res.currentPageOrderedEntries[0].DeveloperName);
      }, (err) => {
        console.log(err);
        reject(err);
      });
    });
  }

}
