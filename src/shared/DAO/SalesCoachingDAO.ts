import { BR_Sales_Coaching__cModel } from '../../app/model/BR_Sales_Coaching__cModel';
import { DataHelper } from '../Helpers/DataHelper';
import { SalesCoachingResponseDAO } from './RepositoryDAO';
import { SalesCoachingStatus } from '../BO/SalesCoachingBO';
import { SaveHelper } from '../Helpers/SaveHelper';
import { ServiceProvider } from '../../providers/service-salesforce';

export class SalesCoachingDAO {

  constructor(public serviceProvider: ServiceProvider) { }

  public saveSalesCoaching(salesCoaching: BR_Sales_Coaching__cModel) {
    return new Promise((resolve, reject) => {

      if (!salesCoaching) {
        resolve();
        return;
      }

      SaveHelper.update_Local_(salesCoaching);

      this.serviceProvider.getBR_Sales_Coaching__cService().upsertSoupEntries([salesCoaching],
        (savedSalesCoaching: BR_Sales_Coaching__cModel[]) => {
          const model = new BR_Sales_Coaching__cModel(savedSalesCoaching[0]);
          resolve(model);
        },
        (error) => {
          reject(error);
        });
    });
  }

  public loadSalesCoachingByEventId(Id: string): Promise<BR_Sales_Coaching__cModel> {
    return new Promise((resolve, reject) => {

      const salesCoachingId = Id ? Id : '';
      console.log('---| WhatId:SalesCoachingId: ', salesCoachingId);

      this.serviceProvider.getBR_Sales_Coaching__cService().queryExactFromSoup('Id', salesCoachingId, 1, 'ascending', 'Id', (response: SalesCoachingResponseDAO) => {
        const salesCoaching = new BR_Sales_Coaching__cModel(response.currentPageOrderedEntries[0]);

        salesCoaching.BR_Notes_Share_1__c = salesCoaching.BR_Notes_Share_1__c
          ? DataHelper.removeHTMLTags(salesCoaching.BR_Notes_Share_1__c)
          : undefined;

        resolve(salesCoaching);

      }, (error) => {
        console.log('-*-| Error:loadSalesCoachingBySalesCoachingId: ', error);
        reject(error);
      });
    });
  }

  public deleteByEventId(Id: string, motivation: string): Promise<any> {
    return new Promise((resolve, reject) => {

      const salesCoachingId = Id ? Id : '';
      console.log('---| WhatId:SalesCoachingId: ', salesCoachingId);

      this.serviceProvider.getBR_Sales_Coaching__cService().queryExactFromSoup('Id', salesCoachingId, 1, 'ascending', null, (data: SalesCoachingResponseDAO) => {
        const salesCoaching = data.currentPageOrderedEntries[0];

        if (salesCoaching) {
          salesCoaching.BR_Cancel_Motivation__c = motivation;
          salesCoaching.BR_Status__c = SalesCoachingStatus.canceled;
          SaveHelper.update_Local_(salesCoaching);

          this.serviceProvider.getBR_Sales_Coaching__cService().upsertSoupEntries([salesCoaching], () => {
            resolve();
          }, (error) => {
            reject(error);
          });

        } else {
          resolve();
        }
      }, (error) => {
        reject(error);
      });
    });
  }

  public findById(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.serviceProvider.getBR_Sales_Coaching__cService().queryExactFromSoup('Id', id, 10, 'ascending', 'Id',
        (data: SalesCoachingResponseDAO) => {
          const founded: boolean = (data.currentPageOrderedEntries.length > 0);
          resolve(founded);
        }, (error) => {
          console.log('-*-| findById:error: ', error);
          reject(error);
        });
    });
  }

}
