import { ServiceProvider } from '../../providers/service-salesforce';
import { BR_WeedHandling__cModel } from '../../app/model/BR_WeedHandling__cModel';

export class WeedHandlingDAO {

    constructor(private service: ServiceProvider) {}

    public saveWeedHandling(weedHandling: BR_WeedHandling__cModel): Promise < any > {
        return new Promise((resolve, reject) => {
            console.log('---| WeedHandling Saving... : ', weedHandling);
            this.service.getBR_WeedHandling__cService().upsertSoupEntries([weedHandling], (success) => {
                console.log('---| WeedHandling Saved: ', success);
                resolve(success[0]);
            }, (error) => {
                console.log('---| WeedHandling Error: ', error);
                reject(error);
            });
        });
    }

    public loadWeedHandlings(visitId) {
        return new Promise((resolve, reject) => {
            const weedHandlingDAO = this.service.getBR_WeedHandling__cService();
            // Campo para busca (target), termo de busca (source), quant de resultados, ordem, campo de ordem
            weedHandlingDAO.queryExactFromSoup('BR_Visit__c', visitId, 3, 'ascending', 'BR_Visit__c', (response) => {

              const weedHandlings = [];
              // tslint:disable-next-line:forin
              for (const i in response.currentPageOrderedEntries) {
                weedHandlings.push(new BR_WeedHandling__cModel(response.currentPageOrderedEntries[i]));
              }
              resolve(weedHandlings);

            }, (err) => {
              console.log('-*-| weedHandlingDAO.queryExactFromSoup: ', err);
              reject(err);
            });
          });

    }

    public removeWeedHandling(weedHandling: BR_WeedHandling__cModel) {
      return new Promise((resolve, reject) => {
          const soupId = weedHandling['_soupEntryId'];
          console.log('---| removeWeedHandling:soupId: ', soupId);
          this.service.getBR_WeedHandling__cService().removeEntries(soupId, (success) => {
              console.log('---| removeWeedHandling:success: ', success);
              resolve(success);
          }, (error) => {
              console.log('---| removeWeedHandling:error: ', error);
              reject(error);
          });
      });
    }

}
