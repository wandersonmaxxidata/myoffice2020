import { ServiceProvider } from './../../providers/service-salesforce';

export class HybridDAO {

  constructor(private serviceProvider: ServiceProvider) {}

  public getHybridList(culture: string, limit: number, filter?: string) {
    return new Promise((resolve, reject) => {
      let pageSize = 1;
      let smartSQL = 'SELECT COUNT(*) FROM {Hibrido__c}';

      this.querySmartFromSoup(smartSQL, pageSize).then((success) => {
        success.currentPageOrderedEntries.forEach((res) => {
          pageSize = res[0];
        });

        const list = [];
        smartSQL = `SELECT
                      {Hibrido__c:Id}, {Hibrido__c:Name}
                    FROM
                      {Hibrido__c}
                    WHERE
                      {Hibrido__c:Name} LIKE "%${filter}%"
                      AND {Hibrido__c:Name} NOT LIKE "%INATIVO%"
                      AND ({Hibrido__c:Tipo_de_Produto__c} = "${culture}"
                        OR {Hibrido__c:BR_ProductGroup__c} = "${culture}")
                    ORDER BY
                      {Hibrido__c:Name}
                    LIMIT ${limit}`;
        // smartSQL = `SELECT
        //               {Hibrido__c:Id}, {Hibrido__c:Name}
        //             FROM
        //               {Hibrido__c}
        //             WHERE
        //               {Hibrido__c:Name} LIKE "%${filter}%"
        //               AND {Hibrido__c:Name} NOT LIKE "%INATIVO%"
        //               AND ({Hibrido__c:Tipo_de_Produto__c} = "${culture}"
        //                 OR {Hibrido__c:BR_ProductGroup__c} = "${culture}")
        //             ORDER BY
        //               {Hibrido__c:Name}`;

        console.log('------| getHybridList:smartSQL: ', smartSQL);

        this.querySmartFromSoup(smartSQL, pageSize).then((response) => {
          response.currentPageOrderedEntries.forEach((item) => {
            list.push({ value: item[0], label: item[1] });
          });
          resolve(list);

        }, (err) => {
          reject(err);
        });
      });
    });
  }

  public getRecordTypeId(recordType: string) {
    return new Promise((resolve, reject) => {
      let pageSize = 1;
      let smartSQL = 'SELECT COUNT(*) FROM {Hibrido__c}';

      this.querySmartFromSoup(smartSQL, pageSize).then((success) => {
        success.currentPageOrderedEntries.forEach((res) => {
          pageSize = res[0];
        });

        smartSQL = `SELECT {RecordType:Id} FROM {RecordType} WHERE {RecordType:DeveloperName} = "${recordType}"`;

        this.querySmartFromSoup(smartSQL, pageSize).then((response) => {
          resolve(response);
        }, (err) => {
          reject(err);
        });
      });
    });
  }

  private querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.serviceProvider.getHibrido__cService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

}
