import { ServiceProvider } from '../../providers/service-salesforce';

export class ContactDAO {

  constructor(private service: ServiceProvider) { }

  public getContactList(id: string, brand: string): Promise<any> {
    return new Promise((resolve) => {
      const list = [];
      const queryRecordType = `SELECT {RecordType:Id} FROM {RecordType} WHERE {RecordType:DeveloperName} = "BR_Contact"`;

      this.querySmartFromSoup(queryRecordType, 1).then((result: any) => {
        const recordType = result.currentPageOrderedEntries[0];
        const queryCount = `SELECT count(*) FROM {Contact}`;

        this.querySmartFromSoup(queryCount, 1).then((count: any) => {
          const pageSize = count.currentPageOrderedEntries[0][0];
          const smartSql = `SELECT {Contact:Id}, {Contact:FirstName}, {Contact:LastName}, {Contact:Email}
                            FROM {Contact}
                            WHERE {Contact:RecordTypeId} = "${recordType[0]}"
                              AND {Contact:BR_Marca__c} = "${brand}"
                              AND {Contact:AccountId} = "${id}"
                            ORDER BY {Contact:FirstName}`;

          console.log('------| smartSql: ', smartSql);

          this.querySmartFromSoup(smartSql, pageSize).then((constacts: any) => {
            constacts.currentPageOrderedEntries.forEach((item) => {
              list.push(item);
            });

            resolve(list);

          }, (err) => {
            console.log('------| Erro getContactList: ', err);
            resolve(list);
          });
        });
      });
    });
  }

  private querySmartFromSoup(smartSql, pageSize) {
    return new Promise((resolve, reject) => {
      this.service.getContactService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

}
