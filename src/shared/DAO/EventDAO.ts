import { DataHelper } from '../Helpers/DataHelper';
import { DateHelper } from '../Helpers/DateHelper';
import { EventModel } from './../../app/model/EventModel';
import { EventResponseDAO } from './RepositoryDAO';
import { ServiceProvider } from '../../providers/service-salesforce';

export class EventDAO {

  constructor(private service: ServiceProvider) {}

  public create(event: EventModel): Promise<any> {
    event.__local__ = event.__locally_created__ = true;
    event.__locally_updated__ = event.__locally_deleted__ = false;

    return this.upsertSoupEntries(event);
  }

  public update(event: EventModel): Promise<any> {
    event.__local__ = event.__locally_updated__ = true;
    event.__locally_created__ = event.__locally_deleted__ = false;

    return this.upsertSoupEntries(event);
  }

  public upsertSoupEntries(event: EventModel): Promise<any> {
    return new Promise((resolve, reject) => {
      if (!event) {
        reject();
        return;
      }

      event.BR_MobileAppFlag__c = true;

      this.service.getEventService().upsertSoupEntries([event], (response) => {
        const model = new EventModel(response[0]);
        resolve(model);
      }, (error) => {
        reject(error);
      });
    });
  }

  public findById(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.service.getEventService().queryExactFromSoup('ExternalID__c', id, 10, 'ascending', 'ExternalID__c',
        (data: EventResponseDAO) => {
          const founded: boolean = data.currentPageOrderedEntries.length > 0;
          console.log('---| findById:data: ', data);
          console.log('---| findById:founded: ', founded);
          resolve(founded);
        }, (error) => {
          console.log('-*-| findById:error: ', error);
          reject(error);
        });
    });
  }

  public getEventDetail(event) {
    return new Promise((resolve, reject) => {
      this.queryExactFromSoup(event).then(async (response) => {
        response.currentPageOrderedEntries.forEach((item) => {
          const push = new EventModel(item);
          if (push.WhatId != null) {
            push.setAccount(this.service.getClientsService());
          }
          const data = {
            event: push,
            isCreateMode: false,
          };
          resolve(data);
        });
      }, (err) => {
        reject(err);
      });
    });
  }

  public searchEvent(filter: string, filterByOwnerId?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let smartSQL = 'SELECT COUNT(*) FROM {Event}';
      let pageSize = 1;
      const list = [];

      this.querySmartFromSoup(smartSQL, pageSize).then((success: any) => {
        success.currentPageOrderedEntries.forEach((res) => {
          pageSize = res[0];
        });

        const filterOwnerId = filterByOwnerId ? `AND {Event:OwnerId} LIKE '${filterByOwnerId}'` : ``;

        smartSQL = `
          SELECT
            {Event:Id},
            {Event:StartDateTime},
            {Event:EndDateTime},
            {Event:IsOutlook__c},
            {Event:Status__c},
            {Event:BR_Tipo_de_Visita__c},
            {Event:Location},
            {Event:ExternalID__c},
            {Event:IsAllDayEvent},
            {Event:Subject}
          FROM
            {Event}
          WHERE
            {Event:WhatId} IS NOT NULL
            ${filterOwnerId}
            AND ({Event:BR_Tipo_de_Visita__c} LIKE '%${filter}%'
              OR {Event:Status__c} LIKE '%${filter}%'
              OR {Event:Subject} LIKE '%${filter}%'
              OR {Event:Location} LIKE '%${filter}%')
            AND ({Event:IsOutlook__c} = 0
              OR {Event:IsOutlook__c} = "false")
          ORDER BY
            {Event:StartDateTime} DESC,
            {Event:EndDateTime} DESC
          LIMIT 40
        `;
        console.log('------| smartSQL:searchEvent: ', smartSQL);

        this.querySmartFromSoup(smartSQL, pageSize).then((response: any) => {
          response.currentPageOrderedEntries.forEach((item) => {
            const Status__c = item[4];
            const IsOutlook__c = DataHelper.getBoolean(item[3]);

            const event = {
              Id: item[0],
              StartDateTime: item[1],
              EndDateTime: item[2],
              IsOutlook__c,
              Status__c,
              BR_Tipo_de_Visita__c: item[5],
              Location: item[6],
              ExternalID__c: item[7],
              IsAllDayEvent: DataHelper.getBoolean(item[8]),
              Subject: item[9],
              cssSuffix: DataHelper.cssForStatus(Status__c, IsOutlook__c),
            };

            if (DateHelper.isAllDayEventNotAdjusted(event)) {
              event.StartDateTime = DateHelper.adjustAllDayEventTimezone(event.StartDateTime);
              event.EndDateTime = DateHelper.adjustAllDayEventTimezone(event.EndDateTime);
            }

            event['StartDateTimeToShow'] = DateHelper.formattedDateToEventCard(event.StartDateTime);
            event['EndDateTimeToShow'] = DateHelper.formattedDateToEventCard(event.EndDateTime);

            list.push(event);
          });
          resolve(list);
        }, (err) => {
          reject(err);
        });
      });
    });
  }

  private querySmartFromSoup(smartSql, pageSize) {
    return new Promise((resolve, reject) => {
      this.service.getEventService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (err) => {
        reject(err);
      });
    });
  }

  private queryExactFromSoup(event): Promise<any> {
    const matchKey = event.Id || event.ExternalID__c;
    const indexPath = event.Id ? 'Id' : 'ExternalID__c';

    return new Promise((resolve, reject) => {
      this.service.getEventService().queryExactFromSoup(indexPath, matchKey, 1, 'ascending', indexPath, (response) => {
        resolve(response);
      }, (err) => {
        reject(err);
      });
    });
  }

}
