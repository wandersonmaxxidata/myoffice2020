import { AttachmentModel } from '../../app/model/AttachmentModel';
import { AttachmentResponseDAO } from './RepositoryDAO';
import { AttachmentService } from '../../providers/attachment/attachment-service-salesforce';
import { DatabaseHelper } from '../Helpers/DatabaseHelper';
import { SaveHelper } from '../Helpers/SaveHelper';
import { ServiceProvider } from '../../providers/service-salesforce';

export class AttachmentDAO {

  public service: AttachmentService;

  constructor(private serviceProvider: ServiceProvider) {
    this.service = this.serviceProvider.getAttachmentService();
  }

  public save(attachment: AttachmentModel): Promise<AttachmentModel> {
    return new Promise((resolve, reject) => {
      SaveHelper.not_sync(attachment);
      this.service.upsertSoupEntries([attachment], (attach: AttachmentModel[]) => {
        resolve(attach[0]);
      }, (error) => {
        reject(error);
      });
    });
  }

  public saveAll(attachments: AttachmentModel[]): Promise<AttachmentModel[]> {
    for (const attach of attachments) {
      SaveHelper.not_sync(attach);
      attach.IsUploaded = false;
    }

    return new Promise((resolve, reject) => {
      this.service.upsertSoupEntries(attachments, (savedAttachments: AttachmentModel[]) => {
        resolve(savedAttachments);
      }, (error) => {
        reject(error);
      });
    });
  }

  public load(visitId: string): Promise<AttachmentModel[]> {
    return new Promise((resolve, reject) => {
      this.service.queryExactFromSoup('ParentId', visitId, 10, 'ascending', null, (data: AttachmentResponseDAO) => {
        let attachments: AttachmentModel[] = [];
        if (data.currentPageOrderedEntries.length > 0) {
          attachments = data.currentPageOrderedEntries;
        }
        attachments = DatabaseHelper.filterNotDeleted<AttachmentModel>(attachments);
        resolve(attachments);
      }, (error) => {
        reject(error);
      });
    });
  }

  public loadNotUploaded(visitId: string): Promise<AttachmentModel[]> {
    return new Promise((resolve, reject) => {
      this.service.queryExactFromSoup('ParentId', visitId, 10, 'ascending', null, (data: AttachmentResponseDAO) => {
        const attachments: AttachmentModel[] = [];

        console.log('---| before:loadNotUploaded:data: ', data);
        for (const attachment of data.currentPageOrderedEntries) {
          if (!attachment.IsUploaded && attachment.ParentId && attachment.ParentId.indexOf('local') > -1) {
            attachments.push(attachment);
          }
        }
        console.log('---| after:loadNotUploaded:attachments: ', attachments);
        resolve(attachments);
      }, (error) => {
        reject(error);
      });
    });
  }

  public loadAll(): Promise<AttachmentModel[]> {
    return new Promise((resolve, reject) => {
      this.service.queryAllFromSoup('ParentId', 'ascending', 1000, (data: AttachmentResponseDAO) => {
        let attachments: AttachmentModel[] = [];
        if (data.currentPageOrderedEntries.length > 0) {
          attachments = data.currentPageOrderedEntries;
        }
        resolve(attachments);
      }, (error) => {
        reject(error);
      });
    });
  }

  public loadAllNotUpdated(): Promise<AttachmentModel[]> {
    return new Promise((resolve, reject) => {
      this.service.queryAllFromSoup('ParentId', 'ascending', 1000, (data: AttachmentResponseDAO) => {
        const attachments: AttachmentModel[] = [];
        console.log('---| before:loadAllNotUpdated:data: ', data);

        for (const attachment of data.currentPageOrderedEntries) {
          if (!attachment.IsUploaded &&
            attachment.ParentId &&
            attachment.ParentId.indexOf('local') > -1) {
            attachments.push(attachment);
          }
        }
        console.log('---| after:loadAllNotUpdated:attachments: ', attachments);
        resolve(attachments);
      }, (error) => {
        reject(error);
      });
    });
  }

  public loadAllNotUploaded(): Promise<AttachmentModel[]> {
    return new Promise((resolve, reject) => {
      this.service.queryAllFromSoup('ParentId', 'ascending', 1000, (data: AttachmentResponseDAO) => {
        const attachments: AttachmentModel[] = [];
        console.log('---| before:loadAllNotUploaded:data: ', data);

        for (const attachment of data.currentPageOrderedEntries) {
          if (!attachment.IsUploaded &&
            attachment.ParentId &&
            attachment.ParentId.indexOf('local') === -1) {
            attachments.push(attachment);
          }
        }
        console.log('---| after:loadAllNotUploaded:attachments: ', attachments);
        resolve(attachments);
      }, (error) => {
        reject(error);
      });
    });
  }

  public remove(attachment: AttachmentModel): Promise<any> {
    return new Promise((resolve, reject) => {
      const soupEntryId = attachment['_soupEntryId'];
      this.service.removeEntries(soupEntryId, () => {
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }

}
