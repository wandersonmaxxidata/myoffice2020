import { ServiceProvider } from '../../providers/service-salesforce';
import { AppPreferences } from '@ionic-native/app-preferences';
import { Hibrido_de_Visita__cModel } from '../../app/model/Hibrido_de_Visita__cModel';

export class ReportDAO {

  public appPreferences = new AppPreferences();

  constructor(public service: ServiceProvider) {}

  public getUserBrand() {
    return new Promise((resolve, reject) => {
      this.appPreferences.fetch('user_brand').then((data: string) => {
        data = data ? data : 'agroceres'; // default
        resolve(data.toLowerCase());
      });
    });
  }

  public getUserName() {
    return new Promise((resolve, reject) => {
      this.appPreferences.fetch('user_name').then((data) => {
        resolve(data);
      });
    });
  }

  public getClientFarmName(client: string, farm: string) {
    return new Promise((resolve, reject) => {

      const smartSQL = `SELECT
                          {Clients:Name}
                        FROM
                          {Clients}
                        WHERE
                          {Clients:Id} = "${client}" OR
                          {Clients:Id} = "${farm}"
                        LIMIT 2`;

      console.log('------| smartSQL (getClientFarmName): ', smartSQL);

      this.service.getClientsService().querySmartFromSoup(smartSQL, 2, (data) => {
        resolve(data.currentPageOrderedEntries);
      }, (err) => {
        reject(err);
      });
    });
  }

  public hybridsLaunch(visit: string, hybridArray: Hibrido_de_Visita__cModel[]): any {
    return new Promise((resolve, reject) => {
      const hybridResult = [];

      for (let i = 0; i < hybridArray.length; i++) {
        const smartSQL = `SELECT
                            {MobileTexts:BR_Title__c},
                            {MobileTexts:BR_Text__c}
                          FROM
                            {MobileTexts}
                          WHERE
                            {MobileTexts:BR_Hibrido__c} = "${hybridArray[i].BR_HybridName__c ? hybridArray[i].BR_HybridName__c : hybridArray[i].Hibrido__c}" AND
                            {MobileTexts:BR_Visit__c} = "${visit}"
                          ORDER BY
                            {MobileTexts:BR_Position__c} ASC`;

        console.log('------| smartSQL (hybridsLaunch): ', smartSQL);

        this.querySmartFromSoup(smartSQL, 5).then((data) => {
          if (data.currentPageOrderedEntries.length > 0 ) {
            hybridResult.push({ Name: hybridArray[i].BR_HybridName__c, Info: data.currentPageOrderedEntries });
          }

          if ((i + 1) === hybridArray.length) {
            resolve(hybridResult);
          }
        }, (err) => {
          console.log('------| Error - ReportDAO hybridsLaunch (Modal): ', err);
        });
      }
    });
  }

  public getMsg(visit: string, brand: string, session: string): any {
    return new Promise((resolve, reject) => {

      const msgResult = [];
      const smartSQL = `SELECT
                          {MobileTexts:BR_Title__c},
                          {MobileTexts:BR_Text__c}
                        FROM
                          {MobileTexts}
                        WHERE
                          {MobileTexts:BR_Visit__c} = "${visit}" AND
                          {MobileTexts:BR_Brand__c} = "${brand}" AND
                          {MobileTexts:BR_Session__c} = "${session}"
                        ORDER BY
                          {MobileTexts:BR_Position__c} ASC`;

        console.log('------| smartSQL (getMsg): ', smartSQL);

        this.querySmartFromSoup(smartSQL, 10).then((data) => {
          if (data.currentPageOrderedEntries.length > 0) {
            msgResult.push({ Name: session, Info: data.currentPageOrderedEntries });
          }

          resolve(msgResult);

        }, (err) => {
          console.log('------| Error - ReportDAO getMsg: ', err);
        });
    });
  }

  private querySmartFromSoup(smartSQL: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getMobileTextsService().querySmartFromSoup(smartSQL, pageSize, (data) => {
        resolve(data);
      }, (err) => {
        reject(err);
      });
    });
  }

}
