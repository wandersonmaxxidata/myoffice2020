import { ClientsModel } from '../../app/model/ClientsModel';
import { ServiceProvider } from '../../providers/service-salesforce';
import { DataHelper } from '../Helpers/DataHelper';

export class ClientDAO {

  constructor(private service: ServiceProvider) { }

  public syncMountClients(clientObject, callback: (error: Error, clientModel) => void) {
    this.fillClientRelationShip(clientObject).then((clientModel) => {
      callback(null, clientModel);
    }).catch((error) => {
      callback(error, null);
    });
  }

  private fillClientRelationShip(clientObject): Promise<any> {
    return new Promise((resolve, reject) => {
      const model = new ClientsModel(clientObject);
      if (model.RecordTypeId !== null && model.RecordTypeId !== undefined) {
        model.setRecordType(this.service.getRecordTypeService()).then(() => {
          model.setMunicipio(this.service.getMunicipioService()).then(() => {
            model.setBR_ProfileMapping__c(this.service.getBR_ProfileMapping__cService())
              .then(() => {
                model.setContact(this.service.getContactService()).then(() => {
                  resolve(model);
                }).catch((err) => {
                  reject(err);
                });
              }).catch((err) => {
                reject(err);
              });
          }).catch((err) => {
            reject(err);
          });
        }).catch((err) => {
          reject(err);
        });
      } else {
        resolve(model);
      }
    });
  }

  public getFarmList(Id: string): Promise<any> {
    return new Promise((resolve) => {
      const list = [];
      const queryRecordType = `SELECT {RecordType:Id} FROM {RecordType} WHERE {RecordType:DeveloperName} = "GC_ACC_Farm"`;

      this.querySmartFromSoup(queryRecordType, 1).then((result: any) => {
        const recordType = result.currentPageOrderedEntries[0];
        const queryCount = `SELECT count(*) FROM {Contact}`;

        this.querySmartFromSoup(queryCount, 1).then((count: any) => {
          const pageSize = count.currentPageOrderedEntries[0][0];
          const smartSql = `SELECT {Clients:Id}, {Clients:Name}
                            FROM {Clients}
                            WHERE {Clients:RecordTypeId} = "${recordType[0]}"
                              AND {Clients:ParentId} = "${Id}"
                            ORDER BY {Clients:Name}`;

          console.log('------| smartSql: ', smartSql);

          this.querySmartFromSoup(smartSql, pageSize).then((clients: any) => {
            clients.currentPageOrderedEntries.forEach((item) => {
              list.push(item);
            });

            resolve(list);

          }, (err) => {
            console.log('------| Erro getFarmList: ', err);
            resolve(list);
          });
        });
      });
    });
  }

  public getFarmById(farmId) {
    return new Promise((resolve) => {
      this.service.getClientsService().queryExactFromSoup('Id', farmId, 1, 'ascending', 'Id', (response) => {
        resolve(response.currentPageOrderedEntries[0]);
      }, (err) => {
        console.log(err);
      });
    });
  }

  public getFarmerByParentId(parentId: string) {
    return new Promise((resolve, reject) => {
      const smartSql = `SELECT {Clients:Id} FROM {Clients} WHERE {Clients:Id} = '${parentId}'`;
      const list = [];

      this.querySmartFromSoup(smartSql, 1).then((result: any) => {
        result.currentPageOrderedEntries.forEach((item) => {
          list.push(item);
        });
        resolve(list);
      });
    });
  }

  public getClientById(model: ClientsModel): Promise<any> {
    return this.queryExactFromSoup(model, 1);
  }

  public getClientsList(developerName: string, filter: string, where?: any) {
    return new Promise((resolve, reject) => {
      const list = [];
      const queryRecordTypeAll = `SELECT * FROM {Clients}`;
      this.querySmartFromSoup(queryRecordTypeAll, 10000).then((result: any) => {
        console.log('TODOS CLIENTES')
        console.log(result)

      });

      const queryRecordTypeAllTipos = `SELECT * FROM {RecordType}`;
      this.querySmartFromSoup(queryRecordTypeAll, 10000).then((result: any) => {
        console.log('TODOS Tipos')
        console.log(result)

      });
      // const queryRecordType = `SELECT {RecordType:Id} FROM {RecordType}`;
      const queryRecordType = `SELECT {RecordType:Id} FROM {RecordType} WHERE {RecordType:DeveloperName} = "${developerName}"`;
      console.log('------| smartSql: ', queryRecordType);
      this.querySmartFromSoup(queryRecordType, 1).then((result: any) => {
        const recordTypeId = result.currentPageOrderedEntries[0][0];
        const queryCount = `SELECT count(*) FROM {Clients}`;

        this.querySmartFromSoup(queryCount, 1).then((countClients: any) => {
          const pageSize = countClients.currentPageOrderedEntries[0][0];
          let queryFilter = ` AND {Clients:BR_MobileSearchFilter__c} LIKE "%${filter}%" `;
          let tableFilter = '';

          if (where && where.length > 0) {
            tableFilter += ', {BR_ProfileMapping__c}';
            queryFilter += ` AND {Clients:Id} = {BR_ProfileMapping__c:BR_Account__c} `;

            let whereCondition = 'AND (';
            where.forEach((item, i) => {
              whereCondition += `{Clients:${item.value}} LIKE "%${item.label}%"`;
              whereCondition += (i < where.length - 1) ? ' OR ' : '';
            });
            whereCondition += ')';
            queryFilter += whereCondition;
          }

          const smartSql = `
            SELECT
              {Clients:Id},
              {Clients:ExternalID__c},
              {Clients:Name},
              {Clients:GC_VATIN__c},
              {Clients:GC_SAP_ID__c},
              {Clients:BR_MobileSearchFilter__c}
              {Clients:RADL__c}
            FROM
              {Clients} ${tableFilter}
            WHERE
              {Clients:Name} IS NOT NULL
              AND {Clients:RecordTypeId} = "${recordTypeId}"
              ${queryFilter}
            ORDER BY
              {Clients:BR_MobileSearchFilter__c}
          `;

          //   const smartSql = `
          //   SELECT
          //     {Clients:Id},
          //     {Clients:ExternalID__c},
          //     {Clients:Name},
          //     {Clients:GC_VATIN__c},
          //     {Clients:GC_SAP_ID__c},
          //     {Clients:BR_MobileSearchFilter__c}
          //   FROM
          //     {Clients} ${tableFilter}
          //   WHERE
          //     {Clients:Name} IS NOT NULL
          //     ${queryFilter}
          //   ORDER BY
          //     {Clients:BR_MobileSearchFilter__c}
          // `;

          console.log('------| smartSql: ', smartSql);

          this.querySmartFromSoup(smartSql, pageSize).then((clients: any) => {
            clients.currentPageOrderedEntries.forEach((item) => {

              const cpf_cnpj = (item[3] && item[3].length <= 11)
                ? DataHelper.mascaraCpf(item[3])
                : DataHelper.mascaraCnpj(item[3]);

              list.push({
                Id: item[0],
                ExternalID__c: item[1],
                Name: item[2],
                GC_VATIN__c: cpf_cnpj,
                GC_SAP_ID__c: item[4],
                BR_MobileSearchFilter__c: item[5],
              });
            });

            resolve(list);
          }, (err) => {
            reject(list);
          });
        });
      });
    });
  }

  //MAXXIDATA


  public returnOnebyOneOptions() {
    return new Promise((resolve, reject) => {
      const smartSql = `
    SELECT
      {BR_ProfileMapping__c:BR_Rating__c}
    FROM
      {BR_ProfileMapping__c}
    WHERE 
    {BR_ProfileMapping__c:BR_Rating__c} LIKE "%1:1%"
    GROUP BY
    {BR_ProfileMapping__c:BR_Rating__c}
  `;
      this.querySmartFromSoup(smartSql, 100).then((list: any) => {
        resolve(list.currentPageOrderedEntries)
      });
    });


  }


  public getClientsListRadla(radl: any, developerName: string, filter: string, where?: any) {
    return new Promise((resolve, reject) => {
      const list = [];
      const queryRecordTypeAll = `SELECT * FROM {Clients}`;
      this.querySmartFromSoup(queryRecordTypeAll, 10000).then((result: any) => {
        console.log('TODOS CLIENTES')
        console.log(result)

      });

      const queryRecordTypeAllTipos = `SELECT * FROM {RecordType}`;
      this.querySmartFromSoup(queryRecordTypeAll, 10000).then((result: any) => {
        console.log('TODOS Tipos')
        console.log(result)

      });
      // const queryRecordType = `SELECT {RecordType:Id} FROM {RecordType}`;
      const queryRecordType = `SELECT {RecordType:Id} FROM {RecordType} WHERE {RecordType:DeveloperName} = "${developerName}"`;
      console.log('------| smartSql: ', queryRecordType);
      this.querySmartFromSoup(queryRecordType, 1).then((result: any) => {
        const recordTypeId = result.currentPageOrderedEntries[0][0];
        const queryCount = `SELECT count(*) FROM {Clients}`;

        this.querySmartFromSoup(queryCount, 1).then(async (countClients: any) => {
          const pageSize = countClients.currentPageOrderedEntries[0][0];
          let queryFilter = ` AND {Clients:BR_MobileSearchFilter__c} LIKE "%${filter}%" `;
          let tableFilter = '';

          if (where && where.length > 0) {
            tableFilter += ', {BR_ProfileMapping__c}';
            queryFilter += ` AND {Clients:Id} = {BR_ProfileMapping__c:BR_Account__c} `;

            let whereCondition = 'AND (';
            where.forEach((item, i) => {
              whereCondition += `{Clients:${item.value}} LIKE "%${item.label}%"`;
              whereCondition += (i < where.length - 1) ? ' OR ' : '';
            });
            whereCondition += ')';
            queryFilter += whereCondition;
          }


          if (sessionStorage.getItem('fone')) {
            let currentSelect = JSON.parse(sessionStorage.getItem('fone'));
            if (currentSelect.length > 0) {
              let cont = 1;
              await currentSelect.forEach(function (value) {
                if (currentSelect.length == 1) {
                  queryFilter += `AND {BR_ProfileMapping__c:BR_Rating__c} LIKE "%${value}%" `;
                } else {
                  if (cont == 1) {
                    queryFilter += `AND ( {BR_ProfileMapping__c:BR_Rating__c} LIKE "%${value}%"`;
                  } else if (cont < currentSelect.length) {
                    queryFilter += ` OR {BR_ProfileMapping__c:BR_Rating__c} LIKE "%${value}%"`;
                  } else if (cont == currentSelect.length) {
                    queryFilter += ` OR {BR_ProfileMapping__c:BR_Rating__c} LIKE "%${value}%")`;
                  }
                  cont++;
                }
              });
            }
          }
          console.log(radl)
          if (radl != undefined) {
            if (radl.length > 0) {
              let cont = 1;
              await radl.forEach(function (value) {
                if(value.length == 1){
                  if (radl.length == 1) {
                    queryFilter += `AND {Clients:RADL__c} LIKE "%${value}%" `;
                  } else {
                    if (cont == 1) {
                      queryFilter += `AND ( {Clients:RADL__c} LIKE "%${value}%"`;
                    } else if (cont < radl.length) {
                      queryFilter += ` OR {Clients:RADL__c} LIKE "%${value}%"`;
                    } else if (cont == radl.length) {
                      queryFilter += ` OR {Clients:RADL__c} LIKE "%${value}%")`;
                    }
                  }
                  cont++;
                }
              });
            }
          }


          const smartSql = `
            SELECT
              {Clients:Id},
              {Clients:ExternalID__c},
              {Clients:Name},
              {Clients:GC_VATIN__c},
              {Clients:GC_SAP_ID__c},
              {Clients:BR_MobileSearchFilter__c},
              {Clients:RADL__c}
            FROM
              {Clients} ${tableFilter}
            LEFT JOIN 
            {BR_ProfileMapping__c}
            ON {Clients:Id} = {BR_ProfileMapping__c:BR_Account__c}
            WHERE
              {Clients:Name} IS NOT NULL
              AND {Clients:RecordTypeId} = "${recordTypeId}"
              ${queryFilter}
            ORDER BY
              {Clients:BR_MobileSearchFilter__c}
          `;

          //   const smartSql = `
          //   SELECT
          //     {Clients:Id},
          //     {Clients:ExternalID__c},
          //     {Clients:Name},
          //     {Clients:GC_VATIN__c},
          //     {Clients:GC_SAP_ID__c},
          //     {Clients:BR_MobileSearchFilter__c}
          //   FROM
          //     {Clients} ${tableFilter}
          //   WHERE
          //     {Clients:Name} IS NOT NULL
          //     ${queryFilter}
          //   ORDER BY
          //     {Clients:BR_MobileSearchFilter__c}
          // `;

          console.log('------| smartSql: ', smartSql);

          this.querySmartFromSoup(smartSql, pageSize).then((clients: any) => {
            clients.currentPageOrderedEntries.forEach((item) => {

              const cpf_cnpj = (item[3] && item[3].length <= 11)
                ? DataHelper.mascaraCpf(item[3])
                : DataHelper.mascaraCnpj(item[3]);

              list.push({
                Id: item[0],
                ExternalID__c: item[1],
                Name: item[2],
                GC_VATIN__c: cpf_cnpj,
                GC_SAP_ID__c: item[4],
                BR_MobileSearchFilter__c: item[5],
              });
            });
            console.log('------| smartSql: ', list);
            resolve(list);
          }, (err) => {
            reject(list);
          });
        });
      });
    });
  }

  private querySmartFromSoup(smartSql, pageSize) {
    return new Promise((resolve, reject) => {
      this.service.getClientsService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  private queryExactFromSoup(model: ClientsModel, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getClientsService().queryExactFromSoup(model.getIdFieldName(), model.getId(),
        pageSize, 'ascending', 'BR_MobileSearchFilter__c', (response) => {
          resolve(response);
        }, (error) => {
          console.log(error);
          reject(error);
        });
    });
  }

  public getFamilyGroupNameById(id: string) {
    return new Promise((resolve, reject) => {
      const smartSql = 'SELECT {Clients:Name} from {Clients} WHERE {Clients:Id} = "' + id + '"';
      const list = [];

      this.querySmartFromSoup(smartSql, 1).then((result: any) => {
        result.currentPageOrderedEntries.forEach((item) => {
          list.push(item[0]);
        });
        resolve(list);
      }).catch(reject);
    });
  }

}
