import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from './../../providers/database/database';
import { DataErrorModel } from '../../app/model/DataErrorModel';

export class ErrorDAO {

    constructor(
        public db?: DatabaseProvider
        ) {
      }

      public insert(dataErrorModel: DataErrorModel){
        return new Promise(resolve => {
          this.db.getDb().then((db: SQLiteObject) =>{
            let sql = 'insert into error ( id,dat_data_type, dat_sop_id) values (?,?,?)';
            let values = [dataErrorModel.getId(), dataErrorModel.getDataId(),dataErrorModel.getErrorId()];
            db.executeSql(sql,values).then(()=>{
              resolve(1);
            }).catch(e => {
              //console.log(e)
              resolve(0);
            });
          }).catch(e => {
           // console.log(e)
            resolve(0);
          });
    
         });
       }

       public delete(id: number){
        return new Promise(resolve => {
          this.db.getDb().then((db: SQLiteObject) =>{
            let sql = 'delet error where id = ?';
            let values = [id];
            resolve(1);
            db.executeSql(sql,values).then(()=>{
            }).catch(e => {
              //console.log(e)
              resolve(0);
            });
          }).catch(e => {
              //console.log(e)
              resolve(0);
            });
        });
      }


}