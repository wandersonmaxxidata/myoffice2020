import { Hibrido_de_Visita__cModel } from '../../app/model/Hibrido_de_Visita__cModel';
import { ServiceProvider } from '../../providers/service-salesforce';
import { SaveHelper } from '../Helpers/SaveHelper';

export class VisitHybridDAO {

  constructor(private service: ServiceProvider) { }

  public saveHybrid(hybrid: Hibrido_de_Visita__cModel): Promise<any> {

    SaveHelper.update_Local_(hybrid);

    return new Promise((resolve, reject) => {
      console.log('---| Hybrid Saving... : ', hybrid);
      this.service.getHibrido_de_Visita__cService().upsertSoupEntries([hybrid], (success) => {
        console.log('---| Hybrid Saved: ', success);
        resolve(success[0]);
      }, (error) => {
        console.log('---| Hybrid Error: ', error);
        reject(error);
      });
    });

  }

  public loadHybrids(visitId) {

    return new Promise((resolve, reject) => {
      const visitHybridDAO = this.service.getHibrido_de_Visita__cService();
      // Campo para busca (target), termo de busca (source), quant de resultados, ordem, campo de ordem
      visitHybridDAO.queryExactFromSoup('BR_VisitMobileId__c', visitId, 100, 'ascending', 'Id', (response) => {

        const hybrids = [];
        // tslint:disable-next-line:forin
        for (const i in response.currentPageOrderedEntries) {
          hybrids.push(new Hibrido_de_Visita__cModel(response.currentPageOrderedEntries[i]));
        }

        console.log('---| loadHybrids:hybrids: ', hybrids);
        resolve(hybrids);

      }, (err) => {
        console.log('-*-| VisitHybridDAO.queryExactFromSoup: ', err);
        reject(err);
      });
    });

  }

  public removeHybrid(hybrid: Hibrido_de_Visita__cModel) {
    return new Promise((resolve, reject) => {
      const soupId = hybrid['_soupEntryId'];
      console.log('---| removeHybrid:soupId: ', soupId);
      this.service.getHibrido_de_Visita__cService().removeEntries(soupId, (success) => {
        console.log('---| removeHybrid:success: ', success);
        resolve(success);
      }, (error) => {
        console.log('---| removeHybrid:error: ', error);
        reject(error);
      });
    });
  }

}
