import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from './../../providers/database/database';
import { ShildDataModel } from '../../app/model/ShildDataModel';

export class ShildDataDAO {

    constructor(
        public db: DatabaseProvider
        ) {
      }

      public insert(shildDataModel: ShildDataModel){
        return new Promise(resolve => {
          this.db.getDb().then((db: SQLiteObject) =>{
            let sql = 'insert into shild_data ( id, sda_type, sda_sop_id,sda_id_data) values (?,?,?,?)';
            let values = [shildDataModel.getId(),shildDataModel.getType(),shildDataModel.getSopId(),shildDataModel.getDataId()];
            db.executeSql(sql,values).then(()=>{
              resolve(1);
            }).catch(e => {
              //console.log(e)
              resolve(0);
            });
          }).catch(e => {
            //console.log(e)
            resolve(0);
          });
         });
       }

       public delete(id: number){
        return new Promise(resolve => {
          this.db.getDb().then((db: SQLiteObject) =>{
            let sql = 'delet shild_data where id = ?';
            let values = [id];
            resolve(1);
            db.executeSql(sql,values).then(()=>{
            }).catch(e => {
             // console.log(e)
              resolve(0);
            });
          }).catch(e => {
              //console.log(e)
              resolve(0);
            });
        });
      }


}