import { EventStatus } from './../BO/EventBO';
import { EventModel } from './../../app/model/EventModel';
import { ServiceProvider } from '../../providers/service-salesforce';
import * as moment from 'moment';
import { DateHelper } from '../Helpers/DateHelper';
import { DataHelper } from '../Helpers/DataHelper';

export class NotificationDAO {

  constructor(private service: ServiceProvider) {}

  public getEventsToNotifications(user_id): Promise<any> {
    return new Promise((resolve, reject) => {
      let pageSize = 1;
      let smartSQL = 'SELECT COUNT(*) FROM {Event}';

      this.querySmartFromSoup(smartSQL, pageSize).then((success: any) => {
        success.currentPageOrderedEntries.forEach((res) => {
          pageSize = res[0];
        });

        const notifications = [];
        const endKey = moment().add(-2, 'day').endOf('day').set({ minute: 0, second: 0 }).utcOffset(0).format();

        smartSQL = `SELECT
                      {Event:Id}, {Event:StartDateTime}, {Event:EndDateTime},
                      {Event:IsOutlook__c}, {Event:Status__c}, {Event:BR_Tipo_de_Visita__c},
                      {Event:Location}, {Event:ExternalID__c}, {Event:IsAllDayEvent}
                    FROM
                      {Event}
                    WHERE
                      {Event:OwnerId} = "${user_id}"
                      AND {Event:EndDateTime} <= "${endKey}"
                      AND ({Event:IsOutlook__c} = 0
                        OR {Event:IsOutlook__c} = "false")
                      AND ({Event:Status__c} = "${EventStatus.open}"
                        OR {Event:Status__c} = "${EventStatus.inProgress}")
                    ORDER BY
                      {Event:StartDateTime} DESC, {Event:EndDateTime}, {Event:CreatedDate}`;

        console.log('------| smartSQL:getEventsToNotifications: ', smartSQL);

        this.querySmartFromSoup(smartSQL, pageSize).then((response: any) => {
          response.currentPageOrderedEntries.forEach((item) => {
            const event = {
              Id: item[0],
              StartDateTime: item[1],
              StartDateTimeToShow: DateHelper.formattedLastSyncDate(item[1]),
              EndDateTime: item[2],
              IsOutlook__c: DataHelper.getBoolean(item[3]),
              Status__c: item[4],
              BR_Tipo_de_Visita__c: item[5],
              Location: item[6],
              ExternalID__c: item[7],
              IsAllDayEvent: DataHelper.getBoolean(item[8]),
            };

            if (DateHelper.isAllDayEventNotAdjusted(event)) {
              event.StartDateTime = DateHelper.adjustAllDayEventTimezone(event.StartDateTime);
              event.EndDateTime = DateHelper.adjustAllDayEventTimezone(event.EndDateTime);
            }

            notifications.push(event);
          });
          resolve(notifications);
        }, (err) => {
          reject(err);
        });
      });
    });
  }

  public getTotalEventsToNotifications(user_id): Promise<any> {
    return new Promise((resolve, reject) => {
      const endKey = moment().add(-2, 'day').endOf('day').format();
      const smartSQL = `SELECT
                          COUNT(*)
                        FROM
                          {Event}
                        WHERE
                          {Event:OwnerId} = "${user_id}"
                          AND {Event:EndDateTime} <= "${endKey}"
                          AND ({Event:IsOutlook__c} = 0
                            OR {Event:IsOutlook__c} = "false")
                          AND ({Event:Status__c} = "${EventStatus.open}"
                            OR {Event:Status__c} = "${EventStatus.inProgress}")
                          `;

      console.log('------| smartSQL:getTotalEventsToNotifications: ', smartSQL);

      this.querySmartFromSoup(smartSQL, 1).then((success: any) => {
        success.currentPageOrderedEntries.forEach((res) => {
          resolve(res[0]);
        });
      });
    });
  }

  public getCountSyncBadge(table): Promise<any> {
    return new Promise((resolve, reject) => {
      const smartSQL = `SELECT
                          count()
                        FROM
                          {${table}}
                        WHERE
                          ({${table}:__local__} = 1
                          OR {${table}:__local__} = "true")
                        `;

      this.querySmartFromSoup(smartSQL, 1).then((success: any) => {
        success.currentPageOrderedEntries.forEach((res) => {
          resolve(res[0]);
        });
      });
    });
  }

  //MAXXIDATA
  public getCountSyncVisitsSnycy(table): Promise<any> {
    return new Promise((resolve, reject) => {
      const smartSQL = `SELECT
                          count()
                        FROM
                          {${table}}
                        WHERE
                          ({${table}:__local__} = 1
                          OR {${table}:__local__} = "true")
                        `;

      this.querySmartFromSoup(smartSQL, 1).then((success: any) => {
        success.currentPageOrderedEntries.forEach((res) => {
          resolve(res[0]);
        });
      });
    });
  }
  //FIM MAXXIDATA

  private querySmartFromSoup(smartSql, pageSize) {
    return new Promise((resolve, reject) => {
      this.service.getEventService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (err) => {
        reject(err);
      });
    });
  }

}
