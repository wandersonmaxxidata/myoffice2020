import { IService } from '../../interfaces/IService';
import { DatabaseHelper } from '../Helpers/DatabaseHelper';
import { SaveHelper } from '../Helpers/SaveHelper';

export class GenericDAO {

  private service: IService;

  constructor(private serviceParam: IService) {
    this.service = serviceParam;
  }

  public saveAll(entities: BaseModel[]): Promise<BaseModel[]> {
    return new Promise<BaseModel[]>((resolve, reject) => {

      if (entities.length === 0) {
        resolve();
        return;
      }

      entities.map((model) => {
        SaveHelper.update_Local_(model);
      });

      const entityName = entities[0].constructor.name;
      console.log('---| entityName: ', entityName, ' entities: ', entities);

      this.service.upsertSoupEntries(entities, (savedEntities) => {
        resolve(savedEntities);
      }, (error) => {
        reject(error);
      });
    });
  }

  public deleteAll(entities: BaseModel[]): Promise<any> {

    const promises: Array<Promise<any>> = [];
    let removeFromDatabase: BaseModel[] = [];
    let updateWithDeletedFlag: BaseModel[] = [];

    removeFromDatabase = entities.filter((model) => {
      return DatabaseHelper.isOnlyInLocalDatabase(model);
    });

    updateWithDeletedFlag = entities.filter((model) => {
      return DatabaseHelper.isAlreadyInSalesforce(model);
    });

    promises.push(this.removeAllFromDatabase(removeFromDatabase));
    promises.push(this.deleteAllFromDatabase(updateWithDeletedFlag));

    return Promise.all(promises);
  }

  private removeAllFromDatabase(entities: BaseModel[]): Promise<any> {
    return new Promise((resolve, reject) => {

      if (entities.length === 0) {
        resolve();
        return;
      }

      const ids: string[] = entities.map((model) => {
        return DatabaseHelper.soupEntryId(model);
      });

      console.log('---| removeAllFromDatabase:entities: ', entities);
      this.service.removeAllEntries(ids, () => {
        resolve();
      }, (error) => {
        reject(error);
      });

    });
  }

  private deleteAllFromDatabase(entities: BaseModel[]): Promise<any> {
    return new Promise((resolve, reject) => {

      if (entities.length === 0) {
        resolve();
        return;
      }

      entities.map((model) => {
        SaveHelper.deleted_Local_(model);
      });

      console.log('---| deleteAllFromDatabase:entities: ', entities);
      this.service.upsertSoupEntries(entities, () => {
        resolve();
      }, (error) => {
        reject(error);
      });

    });
  }

}
