import { BR_ProfileMapping__cService } from '../../providers/br_profilemapping__c/br_profilemapping__c-service-salesforce';
import { ServiceProvider } from '../../providers/service-salesforce';

export class ProfileMappingDAO {

  private service: BR_ProfileMapping__cService;

  constructor(public serviceProvider: ServiceProvider) {
    this.service = this.serviceProvider.getBR_ProfileMapping__cService();
  }

  public getRADL(clientID) {
    return new Promise((resolve, reject) => {
      const smartSQL = `SELECT {BR_ProfileMapping__c:RADL__c}
                          FROM {BR_ProfileMapping__c}
                          WHERE {BR_ProfileMapping__c:BR_Account__c} = '${clientID}'`;

      this.querySmartFromSoup(smartSQL, 1).then((result) => {
        resolve(result.currentPageOrderedEntries[0]);
      });
    });
  }

  public getRecordType(recordTypeId) {
    return new Promise((resolve, reject) => {
      const smartSQL = `SELECT {RecordType:DeveloperName} FROM {RecordType} WHERE {RecordType:Id} = "${recordTypeId}"`;
      console.log('------| SQL:getRecordType: ', smartSQL);


      this.querySmartFromSoup(smartSQL, 1).then((result) => {
        resolve(result.currentPageOrderedEntries[0]);
      });
    });
  }

  public getBrandList(OwnerId) {
    return new Promise((resolve, reject) => {
      const smartSQL = `SELECT {User:Marca__c} FROM {User} WHERE {User:Id} = "${OwnerId}"`;
      console.log('------| SQL:getBrandList: ', smartSQL);

      this.querySmartFromSoup(smartSQL, 1).then((result) => {
        resolve(result.currentPageOrderedEntries[0]);
      });
    });
  }

  public querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

}
