import { SalesCoachingRecordTypes } from './Types';

export const SalesCoachingTexts = {
  [SalesCoachingRecordTypes.needs]: {
    header: `Demonstração de conhecimento técnico e comercial e adequação para passar confiança aos clientes; entenda suas principais necessidades, pontos problemáticos e interesses para esta próxima safra.`,
    prepare0: `Reveja o DPR/PDI 
    Reveja as notas e o Plano de Ação da última Sessão de Coaching 
    Reveja o dashboard/KPI 
    Reveja as análises para a região 
    Reveja os planos `,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching? 
    Como você está progredindo com relação ás ações que acordamos ? 
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados 
    Há alguma dúvida sobre essa Sessão de Coaching?`,
    execute0: `Qual é a sua conclusão sobre a análise retrospectiva de suas contas? O que o/a surpreendeu?
    Quais foram os principais problemas enfrentados por suas contas na última safra?
    Quais são as necessidades mais críticas identificadas para suas contas? Como você os identificou? 
    Quais são as consequências esperadas de não atender a essas necessidades?
    O que você aprendeu com os resultados da pesquisa NPS?
    Como você espera que seja a abordagem da concorrência nesta safra?
    Como você avalia a eficácia dos programas de marketing?
    Com base em sua análise RADL, quais devem ser suas prioridades? Isso é razoável?
    Como você pretende influenciar sua conta para que alcance seu potencial?
    Quais são os principais riscos para o atingimento de sua previsão?
    Como vê que sua previsão é desafiadora o suficiente?`,
    observe0: ``,
    observe1: ``,
    observe2: ``,
    observe3: ``,
    observe4: ``,
    posvisit0: ``,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.planArticulate]: {
    header: `Criar uma solução personalizada com base nas necessidades do cliente e oferecer um plano sobre como cumprir essas promessas, técnica e comercialmente`,
    prepare0: `Reveja o DPR/PDI 
    Reveja as notas e o Plano de Ação da última Sessão de Coaching 
    Reveja o dashboard/KPI 
    Rever as necessidades de identificação para o distrito e as principais contas
    Revisar planos de conta`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching? 
    Como você está progredindo com relação ás ações que acordamos ? 
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados 
    Há alguma dúvida sobre essa Sessão de Coaching?`,
    execute0: `Como você planeja posicionar a Monsanto como o melhor parceiro? Como isso muda para contas insatisfeitas?
    Como você está decidindo a alocação de seu tempo e recursos?
    Como você pretende influenciar sua conta para alcançar seu potencial?
    Quais programas / touchpoints o ajudarão mais a atingir suas metas? Por quê?
    Quais são os benefícios dos programas de marketing que você considera mais relevantes para suas contas? E menos?
    Como você compartilha com suas contas o nível de serviço fornecido e suas recomendações?
    Quais serviços eles consideram mais impactantes?
    Qual material/ caminho você considera mais eficaz para enfatizar os benefícios de cada híbrido? Por quê?
    Como você reforça a importância da área de refúgio?
    Quais fontes de informação você usa para fornecer recomendações sobre fertilizantes e inseticidas? Você acha relevante para suas contas?`,
    observe0: ``,
    observe1: ``,
    observe2: ``,
    observe3: ``,
    observe4: ``,
    posvisit0: ``,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.negotiateCloseOnline]: {
    header: `Garantir que o negócio / acordo seja concluído, gerenciando a disponibilidade do produto, o posicionamento do produto, as opções de crédito e as condições de preço para a campanha; Se a venda ainda não estiver concluída, alinhe e promova um plano de geração de demanda para Y + 1.`,
    prepare0: `Reveja o DPR/PDI 
    Reveja as notas e o Plano de Ação da última Sessão de Coaching 
    Reveja o dashboard/KPI 
    Reveja as análises para o distrito 
    Revise os planos e as necessidades de conta 
    Quanto o Levantamentos das Necessidades e o Planejamento foram úteis para entender as metas do produtor para a safra atual?`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que combinamos acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados?
    Há alguma dúvida sobre essa Sessão?
    Por que estamos visitando essa conta hoje?
    Qual é o seu objetivo para esta visita?`,
    execute0: `Como você apresentará as recomendações/serviços de maior impacto a serem fornecidos? Os materiais/apresentações são eficazes para enfatizá-los e adicionalmente mostram o valor agregado? Por quê?
    Como você pretende apresentar as exigências de refúgio e BMPs para a conta?
    A proposta da conta (detalhes da campanha, opções de crédito / financiamento, pacote de serviços) está alinhada com a priorização de RADL e com a Jornada do Cliente?
    Como você está monitorando o preço da concorrência?
    Você considerou diferentes cenários e possíveis objeções?
    Quais são as condições de crédito disponíveis para esta conta?
    Quais são os prazos de entrega e o volume disponível de cada produto/solução oferecido?
    Você analisou o plano de treinamento e execução de ATs? Ele está alinhado com a proposta de valor e com a priorização de conta?`,
    observe0: ``,
    observe1: ``,
    observe2: ``,
    observe3: ``,
    observe4: ``,
    posvisit0: ``,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.negotiateCloseRideAlong]: {
    header: `Garantir que o negócio / acordo seja concluído, gerenciando a disponibilidade do produto, o posicionamento do produto, as opções de crédito e as condições de preço para a campanha; Se a venda ainda não estiver concluída, alinhe e promova um plano de geração de demanda para Y + 1`,
    prepare0: `Reveja o DPR/PDI 
    Reveja as notas e o Plano de Ação da última Sessão de Coaching 
    Reveja o dashboard/KPI 
    Reveja as análises para o distrito 
    Revise os planos e as necessidades de conta 
    Quanto o Levantamentos das Necessidades e o Planejamento foram úteis para entender as metas do produtor para a safra atual?`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que combinamos acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados?
    Há alguma dúvida sobre essa Sessão?
    Por que estamos visitando essa conta hoje?
    Qual é o seu objetivo para esta visita?`,
    execute0: `Como você apresentará as recomendações/serviços de maior impacto a serem fornecidos? Os materiais/apresentações são eficazes para enfatizá-los e adicionalmente mostram o valor agregado? Por quê?
    Como você pretende apresentar as exigências de refúgio e BMPs para a conta?
    A proposta da conta (detalhes da campanha, opções de crédito / financiamento, pacote de serviços) está alinhada com a priorização de RADL e com a Jornada do Cliente?
    Como você está monitorando o preço da concorrência?
    Você considerou diferentes cenários e possíveis objeções?
    Quais são as condições de crédito disponíveis para esta conta?
    Quais são os prazos de entrega e o volume disponível de cada produto/solução oferecido?
    Você analisou o plano de treinamento e execução de ATs? Ele está alinhado com a proposta de valor e com a priorização de conta?`,
    observe0: `Defina a agenda
    Enfatize o que é importante para o cliente (ou seja, resultados técnicos, rendimento, etc.)`,
    observe1: `Pergunte "O quê, Como, Por quê, Quando"
    Ouça e faça perguntas para confirmar a necessidade ou preocupação`,
    observe2: `Converta as características específicas em benefícios (resultados importantes para o cliente)
    Afaste a conversas sobre preço e concentre-se no valor
    Use anúncios visuais como suporte`,
    observe3: `Reconheça quando há uma objeção
    Esclareça / Responda / Verifique
    Encerre ou peça que eles tomem ações`,
    observe4: `Resuma as conclusões e os próximos passos
    Programe a próxima reunião`,
    posvisit0: `O que você aprendeu com a visita?
    O que você faria de diferente?
    Sua meta foi cumprida? Por quê, por que não?
    Quais são os itens de ação para o cliente e para você?`,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.assistPurchaseOnline]: {
    header: `Permitir o cumprimento do que foi prometido ao produtor; fornecer informações atualizadas e confiáveis sobre sua entrega e facilitar a interação entre o produtor e o parceiro.`,
    prepare0: `Reveja o DPR / PDI
    Reveja as notas e o Plano de Ação da última Sessão de Coaching
    Reveja o dashboard/ KPI
    Reveja a agenda para a Sessão de Coaching 
    Avaliar as análises para o distrito
    Revisar planos e necessidades da conta`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que combinamos acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados?
    Há alguma dúvida sobre essa Sessão?
    Por que estamos visitando essa conta hoje?
    Qual é o seu objetivo para esta visita?`,
    execute0: `Como você apresentará as recomendações/serviços de maior impacto a serem fornecidos? Os materiais/apresentações são eficazes para enfatizá-los e a adicionalmente mostram o valor agregado? Por quê?
    Qual é a intenção de compra para essa conta e o que é surpreendente para você?
    Você considerou diferentes cenários e possíveis objeções?
    Você consegue prever algum problema com os multiplicadores e com a disponibilidade de sementes? Se sim, como você planeja minimizar o impacto para o produtor?
    Quais são os melhores parceiros para essa conta? Quais são seus planos para conectar o produtor ao melhor parceiro?
    Final da Safra - como você está lidando com a coleta de todos os documentos e faturas necessários?
    Final da Safra - Programas de Marketing: quais são as vantagens e benefícios mais reconhecidos? Onde os produtores / parceiros percebem menos valor? Como você lida com isso?`,
    observe0: ``,
    observe1: ``,
    observe2: ``,
    observe3: ``,
    observe4: ``,
    posvisit0: ``,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.assistPurchaseRideAlong]: {
    header: `Permitir o cumprimento do que foi prometido ao produtor; fornecer informações atualizadas e confiáveis sobre sua entrega e facilitar a interação entre o produtor e o parceiro.`,
    prepare0: `Reveja o DPR / PDI
    Reveja as notas e o Plano de Ação da última Sessão de Coaching
    Reveja o dashboard/ KPI
    Reveja a agenda para a Sessão de Coaching 
    Avaliar as análises para o distrito
    Revisar planos e necessidades da conta`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que combinamos acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados?
    Há alguma dúvida sobre essa Sessão?
    Por que estamos visitando essa conta hoje?
    Qual é o seu objetivo para esta visita?`,
    execute0: `Como você apresentará as recomendações/serviços de maior impacto a serem fornecidos? Os materiais/apresentações são eficazes para enfatizá-los e a adicionalmente mostram o valor agregado? Por quê?
    Qual é a intenção de compra para essa conta e o que é surpreendente para você?
    Você considerou diferentes cenários e possíveis objeções?
    Você consegue prever algum problema com os multiplicadores e com a disponibilidade de sementes? Se sim, como você planeja minimizar o impacto para o produtor?
    Quais são os melhores parceiros para essa conta? Quais são seus planos para conectar o produtor ao melhor parceiro?
    Final da Safra - como você está lidando com a coleta de todos os documentos e faturas necessários?
    Final da Safra - Programas de Marketing: quais são as vantagens e benefícios mais reconhecidos? Onde os produtores / parceiros percebem menos valor? Como você lida com isso?`,
    observe0: `Defina a agenda
    Enfatize o que é importante para o cliente (ou seja, resultados técnicos, rendimento, etc.)`,
    observe1: `Pergunte "O quê, Como, Por quê, Quando"
    Ouça e faça perguntas para confirmar a necessidade ou preocupação`,
    observe2: `Converta as características específicas em benefícios (resultados importantes para o cliente)
    Afaste a conversas sobre preço e concentre-se no valor
    Use anúncios visuais como suporte`,
    observe3: `Reconheça quando há uma objeção
    Esclareça / Responda / Verifique
    Encerre ou peça que eles tomem ações`,
    observe4: `Resuma as conclusões e os próximos passos
    Programe a próxima reunião`,
    posvisit0: `O que você aprendeu com a visita? 
    O que você faria de diferente? 
    Sua meta foi cumprida? Por quê, por que não? 
    Quais são os itens de ação para o cliente e para você`,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.trackDeliveryOTC]: {
    header: `Permitir o cumprimento do que foi prometido ao produtor; fornecer informações atualizadas e confiáveis sobre sua entrega e começar a compartilhar dicas sobre como posicionar corretamente o produto para um bom estabelecimento da safra.`,
    prepare0: `Reveja o DPR / PDI 
    Reveja as notas e o Plano de Ação da última Sessão de Coaching 
    Reveja o dashboard/ KPI 
    Reveja as anotações e os resultados de Negociação e Fechamento
    Reveja as necessidades recentes de disponibilidade e ajustes de preços que afetam o território`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching? 
    Como você está progredindo com relação ás ações que acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados
    Há alguma dúvida sobre essa Sessão?`,
    execute0: `Quais são as mudanças inesperadas em relação à disponibilidade do produto? E status de entrega?
    Como você planeja abordar alterações na disponibilidade/entrega com suas contas?
    Quais são as consequências esperadas desta situação? Como você poderia mitigar isso?
    Qual é o seu plano para compartilhar as melhores práticas de armazenamento / pré-plantio / plantio?
    Quais as recomendações / conhecimento você considera mais relevantes para suas contas?
    Como você enfatiza com suas contas o nível de serviço prestado e o benefício de cada híbrido?
    Quais fontes de informação você usa para fornecer recomendações sobre fertilizantes e inseticidas? Você as acha relevante para suas contas?
    Como você planeja abordar os ativos de geração de demanda este ano? Quais são seus objetivos e prioridades?`,
    observe0: ``,
    observe1: ``,
    observe2: ``,
    observe3: ``,
    observe4: ``,
    posvisit0: ``,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.demandGenerationOnline]: {
    header: `Garantir a implementação de testes bem-sucedidos, posicionar o produto para o sucesso, orientar os produtores por meio do pré-plantio e plantio do BMP e fornecer respostas rápidas e precisas às reclamações.`,
    prepare0: `Reveja o DPR / PDI 
    Reveja as notas e o Plano de Ação da última Sessão de coaching 
    Reveja o dashboard/ KPI 
    Reveja a agenda para a Sessão 
     Analise os resultados da etapa anterior (entrega, reclamações, etc.)
    Analise as atualizações de dados recentes ( qualidade, tempo)
    Revise os planos para esta visita e a conexão com o RADL`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados?
    Há alguma dúvida sobre essa Sessão?`,
    execute0: `Como a entrega de sementes (Híbridas + Refúgio) atende às expectativas dos clientes? Você prevê problemas? Como você está planejando atenuar o impacto? 
    Como você consegue documentar suas observações e recomendações das visitas no CRM? Como isso o/a está ajudando em futuras visitas? 
    Como você está influenciando a área do Refúgio a se manter em conformidade? 
    Como os híbridos são posicionados de acordo com a proposta de valor (atual & Y + 1)? 
    Quais são as tendências mais comuns para esta região (por exemplo, pragas, ervas daninhas, desafios climáticos)?
    Ao monitorar sua área para verificar o BMP, quais são as oportunidades mais críticas? Como você pode capturá-las?
    Existem recomendações ou atividades que precisam ser conduzidas para garantir o sucesso híbrido? Como você está capturando e compartilhando?
    Como você planeja acompanhar a conta sobre com relação às demandas da última visita?`,
    observe0: ``,
    observe1: ``,
    observe2: ``,
    observe3: ``,
    observe4: ``,
    posvisit0: ``,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.demandGenerationRideAlong]: {
    header: `Garantir a implementação de testes bem-sucedidos, posicionar o produto para o sucesso, orientar os produtores por meio do pré-plantio e plantio do BMP e fornecer respostas rápidas e precisas às reclamações.`,
    prepare0: `Reveja o DPR / PDI 
    Reveja as notas e o Plano de Ação da última Sessão de coaching 
    Reveja o dashboard/ KPI 
    Reveja a agenda para a Sessão 
     Analise os resultados da etapa anterior (entrega, reclamações, etc.)
    Analise as atualizações de dados recentes ( qualidade, tempo)
    Revise os planos para esta visita e a conexão com o RADL`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados?
    Há alguma dúvida sobre essa Sessão?`,
    execute0: `Como a entrega de sementes (Híbridas + Refúgio) atende às expectativas dos clientes? Você prevê problemas? Como você está planejando atenuar o impacto? 
    Como você consegue documentar suas observações e recomendações das visitas no CRM? Como isso o/a está ajudando em futuras visitas? 
    Como você está influenciando a área do Refúgio a se manter em conformidade? 
    Como os híbridos são posicionados de acordo com a proposta de valor (atual & Y + 1)? 
    Quais são as tendências mais comuns para esta região (por exemplo, pragas, ervas daninhas, desafios climáticos)?
    Ao monitorar sua área para verificar o BMP, quais são as oportunidades mais críticas? Como você pode capturá-las?
    Existem recomendações ou atividades que precisam ser conduzidas para garantir o sucesso híbrido? Como você está capturando e compartilhando?
    Como você planeja acompanhar a conta sobre com relação às demandas da última visita?`,
    observe0: `Defina a agenda
    Enfatize o que é importante para o cliente (ou seja, resultados técnicos, rendimento, etc.)`,
    observe1: `Pergunte "O quê, Como, Por quê, Quando"
    Ouça e faça perguntas para confirmar a necessidade ou preocupação`,
    observe2: `Converta as características específicas em benefícios (resultados importantes para o cliente)
    Afaste a conversas sobre preço e concentre-se no valor
    Use anúncios visuais como suporte`,
    observe3: `Reconheça quando há uma objeção
    Esclareça / Responda / Verifique
    Encerre ou peça que eles tomem ações`,
    observe4: `Resuma as conclusões e os próximos passos
    Programe a próxima reunião`,
    posvisit0: `O que você aprendeu com a visita? 
    O que você faria de diferente? 
    Sua meta foi cumprida? Por quê, por que não? 
    Quais são os itens de ação para o cliente e para você? 
    Quais relatórios precisam ser atualizados? `,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.endSeasonOnline]: {
    header: `Participar e apoiar na colheita para coletar informações sobre cada produtor (e região) para: ajudar os produtores a avaliar os resultados e benefícios do produto, determinar pontos fortes ou fracos durante a safra e começar a identificar um plano. para a próxima safra.`,
    prepare0: `Reveja o DPR / PDI
    Reveja as notas e o Plano de Ação da última Sessão de Coaching
    Reveja o dashboard/ KPI
    Reveja a agenda para a Sessão de Coaching
    Analise os resultados da etapa anterior (conclusão do touchpoint; suporte solicitado/fornecido; reclamações, etc.)
    Analise as atualizações de dados recentes (qualidade, tempo)`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados? 
    Há alguma dúvida sobre essa Sessão?`,
    execute0: `Quais são os resultados mais relevantes sobre a colheita? Como você planeja comunicá-los? Qual material de apoio você está usando?
    Quais observações você está documentando na Ferramenta de CRM?
    Quais são os comentários mais relevantes da conta durante a safra? Como isso afeta a satisfação do cliente?
    Reclamações durante a safra: Qual é a sua estratégia e plano de ação para abordá-las para a próxima safra?
    Reclamações durante a safra: Quais são as causas? Como você pode usá-las para criar a satisfação do cliente?
    Analisando a análise dos resultados de produtividade, quais alternativas estão sendo discutidas com os produtores?
    Como você planeja conduzir os Dias D? O que você está destacando? Como você planeja influenciar as decisões da próxima safra?
    Como os produtores alavancaram as soluções da Climate?
    O que você aprendeu sobre as perspectivas? Como você está ajustando sua estratégia para a próxima safra?`,
    observe0: ``,
    observe1: ``,
    observe2: ``,
    observe3: ``,
    observe4: ``,
    posvisit0: ``,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
  [SalesCoachingRecordTypes.endSeasonRideAlong]: {
    header: `Participar e apoiar na colheita para coletar informações sobre cada produtor (e região) para: ajudar os produtores a avaliar os resultados e benefícios do produto, determinar pontos fortes ou fracos durante a safra e começar a identificar um plano. para a próxima safra.`,
    prepare0: `Reveja o DPR / PDI
    Reveja as notas e o Plano de Ação da última Sessão de Coaching
    Reveja o dashboard/ KPI
    Reveja a agenda para a Sessão de Coaching
    Analise os resultados da etapa anterior (conclusão do touchpoint; suporte solicitado/fornecido; reclamações, etc.)
    Analise as atualizações de dados recentes (qualidade, tempo)`,
    prepare1: `Como você se sente com relação ao plano de ação que acordamos na última Sessão de Coaching?
    Como você está progredindo com relação ás ações que acordamos?
    Quais novos comportamentos / técnicas você já experimentou? Quais foram os resultados? 
    Há alguma dúvida sobre essa Sessão?`,
    execute0: `Quais são os resultados mais relevantes sobre a colheita? Como você planeja comunicá-los? Qual material de apoio você está usando?
    Quais observações você está documentando na Ferramenta de CRM?
    Quais são os comentários mais relevantes da conta durante a safra? Como isso afeta a satisfação do cliente?
    Reclamações durante a safra: Qual é a sua estratégia e plano de ação para abordá-las para a próxima safra?
    Reclamações durante a safra: Quais são as causas? Como você pode usá-las para criar a satisfação do cliente?
    Analisando a análise dos resultados de produtividade, quais alternativas estão sendo discutidas com os produtores?
    Como você planeja conduzir os Dias D? O que você está destacando? Como você planeja influenciar as decisões da próxima safra?
    Como os produtores alavancaram as soluções da Climate?
    O que você aprendeu sobre as perspectivas? Como você está ajustando sua estratégia para a próxima safra?`,
    observe0: `Defina a agenda
    Enfatize o que é importante para o cliente (ou seja, resultados técnicos, rendimento, etc.)`,
    observe1: `Pergunte "O quê, Como, Por quê, Quando"
    Ouça e faça perguntas para confirmar a necessidade ou preocupação`,
    observe2: `Converta as características específicas em benefícios (resultados importantes para o cliente)
    Afaste a conversas sobre preço e concentre-se no valor
    Use anúncios visuais como suporte`,
    observe3: `Reconheça quando há uma objeção
    Esclareça / Responda / Verifique
    Encerre ou peça que eles tomem ações`,
    observe4: `Resuma as conclusões e os próximos passos
    Programe a próxima reunião`,
    posvisit0: `O que você aprendeu com a visita? 
    O que você faria de diferente? 
    Sua meta foi cumprida? Por quê, por que não? 
    Quais são os itens de ação para o cliente e para você? 
    Quais relatórios precisam ser atualizados? `,
    share0: `Reforce os pontos fortes e utilize exemplos específicos desta Sessão.`,
    future0: `Reforce áreas de melhoria e utilize exemplos específicos desta Sessão.`,
  },
};
