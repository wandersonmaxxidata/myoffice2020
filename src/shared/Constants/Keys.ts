export const NotificationKeys = {
  afterEndSync            : 'afterEndSync',
  changeTab               : 'changeTab',
  cleanLoockup            : 'cleanLoockup',
  closeSync               : 'closeSync',
  contacsUpdate           : 'contacsUpdate',
  endSync                 : 'endSync',
  errorSync               : 'errorSync',
  errorSyncFromProgressBar: 'errorSyncFromProgressBar',
  filterAgenda            : 'filterAgenda',
  firstSync               : 'firstSync',
  loadClientVisits        : 'loadClientVisits',
  openHome                : 'openHome',
  openToday               : 'openToday',
  selectedDate            : 'selectedDate',
  startSync               : 'startSync',
  startSyncFromHome       : 'startSyncFromHome',
  startSyncFromProgressBar: 'startSyncFromProgressBar',
  statusEventSave         : 'statusEventSave',
  totalNotifications      : 'totalNotifications',
  openSyncError           : 'openSyncError',
  closeSyncError          : 'openSyncError',
};

export const LocalStorageKeys = {
  compareSyncDateKey: 'compareSyncDate',
  lastSyncDateKey   : 'lastSyncDate',
  milliNextSyncKey  : 'milliNextSyncKey',
};

export const LocalStorageUser = {
  userCultureKey: 'userCulture',
  userId        : 'userId',
  userProfile   : 'userProfile',
};
