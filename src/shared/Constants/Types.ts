export const AttachmentSuffix = {
  portrait : '__portrait',
  landscape: '__landscape',
};

export const EventDivision = [
  { label: 'Milho', value: 'Milho' },
  { label: 'Soja', value: 'Soja' },
  { label: 'Crop', value: 'Crop' },
];

export const EventVisitType = {
  Crop: [
    'Visita Crop - Comercial',
    'Visita Crop - Climate',
    'Visita Crop - Evento',
  ],
  Soja: [
    'Plantio - Soja',
    'Colheita - Soja',
    'Visita Verde - Soja',
  ],
  Milho: [
    'Pré-Plantio',
    'Plantio',
    'Germinação e Emergência',
    'Desenvolvimento Vegetativo',
    'Desenvolvimento Reprodutivo',
    'Colheita',
    'Dia D',
    'Evento',
  ],
  COACHING_AGROCERES_DEKALB: [
    '1. Identificação das Necessidades',
    '2. Desenvolver o Plano e Articular Proposta de Valor',
    '3. Negociar e Fechar',
    '4. Acompanhar a Entrega & Order-To-Cash',
    '5. Touchpoints de Geração de Demanda',
    '6. Revisão no Final da Safra',
  ],
  COACHING_AGROESTE: [
    '1. Identificação das Necessidades',
    '2. Desenvolver o Plano e Articular Proposta de Valor',
    '3. Negociar e Fechar (MILHO)',
    '3. Assistência com a Compra (SOJA)',
    '4. Acompanhar a Entrega & Order-To-Cash',
    '5. Touchpoints de Geração de Demanda',
    '6. Revisão no Final da Safra',
  ],
};

export const Profile = {
  admin: ['Admin', 'PT1'],
  gr   : ['GR', 'Gerente'],
  rtv  : ['RTV', 'ATC', 'ATS'],
  rc   : ['RC'],
  kam  : ['Key Account Manager', 'KAM'],
};

export const FilterList = {
  profileMapping: [
    { label: 'R', value: 'RADL__c' },
    { label: 'A', value: 'RADL__c' },
    { label: 'D', value: 'RADL__c' },
    { label: 'L', value: 'RADL__c' },
  ],
};

export const RecordTypeDeveloperName = {
  channel: 'Parceiros',
  farm   : 'GC_ACC_Farm',
  farmer : 'Agricultores',
};

export const EventDeveloperName = {
  default : 'Evento_Default',
  coaching: 'Evento_Sales_Coaching',
};

export const VisitRecordTypes = {
  prePlanting            : 'Pre_Plantio',
  planting               : 'Plantio',
  germinationAndEmergency: 'Germinacao_Emergencia',
  reproductiveDevelopment: 'Desenvolvimento_Reprodutivo',
  vegetativeDevelopment  : 'Desenvolvimento_Vegetativo',
  harvest                : 'Colheita',
  comercial              : 'BR_CropVisitCommercial',
  eventCrop              : 'BR_CropVisitEvent',
  climate                : 'BR_CropVisitClimate',
  dDay                   : 'BR_Dia_D',
  cornEvent              : 'BR_EventoVisit',
  soyPlanting            : 'BR_SoyPlanting',
  soyGreenVisit          : 'BR_SoyGreenVisit',
  cropSoy                : 'BR_CropSoy',
};

export const VisitHybridRecordTypes = {
  prePlanting            : 'BR_HybridPrePlanting',
  planting               : 'Plantio',
  germinationAndEmergency: 'Germinacao_Emergencia',
  reproductiveDevelopment: 'Desenvolvimento_Reprodutivo',
  vegetativeDevelopment  : 'Desenvolvimento_Vegetativo',
  harvest                : 'Colheita',
  dDay                   : 'BR_DayDVisitHy',
  soyPlanting            : 'BR_HybridPlantingSoy',
  soyGreenVisit          : 'BR_GreenVisitSoy',
  cropSoy                : 'BR_HybridCropSoy',
};

export const ProfileMappingTypes = {
  channelCorn          : 'BR_ChannelCorn',
  channelCotton        : 'BR_ChannelCotton',
  channelCropProtection: 'BR_ChannelCropProtection',
  dealerCornAgroeste   : 'BR_DealerCornAgroeste',
  dealerSoy            : 'BR_DealerSoy',
  farmCornAgroceres    : 'BR_FarmCornAgroceres',
  farmCornAgroeste     : 'BR_FarmCornAgroeste',
  farmCornDekalb       : 'BR_FarmCornDekalb',
  farmCotton           : 'BR_FarmCotton',
  farmCrop             : 'BR_FarmCrop',
  farmSoy              : 'BR_FarmSoy',
  growerCornAgroceres  : 'BR_GrowerCornAgroceres',
  growerCornAgroeste   : 'BR_GrowerCornAgroeste',
  growerCornDekalb     : 'BR_GrowerCornDekalb',
  growerCotton         : 'BR_GrowerCotton',
  growerCropProtection : 'BR_GrowerCropProtection',
  growerSoy            : 'BR_GrowerSoy',
};

export const SalesCoachingRecordTypes = {
  needs                    : 'BR_Needs_ID',
  planArticulate           : 'BR_Dev_Plan_Articulate_Value_Prop',
  negotiateCloseOnline     : 'BR_Negotiate_Close_Online',
  negotiateCloseRideAlong  : 'BR_Negotiate_Close_Ride_Along',
  assistPurchaseOnline     : 'BR_Assist_Purchase_Online',
  assistPurchaseRideAlong  : 'BR_Assist_Purchase_Ride_Along',
  trackDeliveryOTC         : 'BR_Track_Delivery_OTC',
  demandGenerationOnline   : 'BR_Demand_Generation_Online',
  demandGenerationRideAlong: 'BR_Demand_Generation_Ride_Along',
  endSeasonOnline          : 'BR_End_Season_Online',
  endSeasonRideAlong       : 'BR_End_Season_Ride_Along',
};
