import { IPicklistItem } from '../../interfaces/IPicklistItem';

export const WindowStorageType = {
  config: ':config',
  data: ':data',
  order: ':order',
};

export abstract class WindowStorageHelper {

  public static savePicklistsToLocalStorage(id: string, picklists: IPicklistItem[]) {
    const jsonString = JSON.stringify(picklists);
    const key = id + ':picklist';
    window.localStorage.setItem(key, jsonString);
  }

  public static erasePicklistsFromLocalStorage(id: string) {
    const key = id + ':picklist';
    window.localStorage.removeItem(key);
  }

  public static retrievePicklistsFromLocalStorage(id: string) {
    const key = id + ':picklist';
    const jsonString = window.localStorage.getItem(key);
    const picklists: IPicklistItem[] = JSON.parse(jsonString);
    let temp = picklists.filter(e => e.label !== 'Foreman' && e.label !== 'Capataz');
    return temp;
  }

  public static retrievePicklistsWithFilter(id: string, total: number, filter: string) {
    return new Promise((resolve) => {
      const picklists = WindowStorageHelper.retrievePicklistsFromLocalStorage(id);
      const list = [];

      for (const picklist of picklists) {
        if (picklist.label.toLowerCase().startsWith(filter.toLowerCase())) {
          list.push(picklist);
          if (list.length >= total) {
            break;
          }
        }
      }

      resolve(list);
    });
  }

}
