
export abstract class SaveHelper {

    public static update_Local_(entity: any) {
        entity.__local__ = true;
        entity.__locally_updated__ = true;
    }

    public static not_sync(entity: any) {
        entity.__local__ = true;
        entity.__locally_created__ = true;
        entity.__locally_updated__ = false;
        entity.__locally_deleted__ = false;
    }

    public static deleted_Local_(entity: any) {
        entity.__local__ = true;
        entity.__locally_deleted__ = true;
    }

}
