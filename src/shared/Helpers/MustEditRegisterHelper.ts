export abstract class MustEditRegisterHelper {

  private static ErrorObjectNameKey = 'ErrorObjectNameKey:';

  public static setObjectName(objectName: string) {
    window.localStorage.setItem(this.ErrorObjectNameKey, objectName);
  }

  public static cleanObjectName() {
    window.localStorage.removeItem(this.ErrorObjectNameKey);
  }

  public static isObjectTreeWithError(model: BaseModel): boolean {

    const errorObjectName = this.retrieveErrorObjectName() + 'Model';
    const keys = Object.keys(model);

    // console.log('---| isObjectTreeWithError:model: ', model);
    // console.log('---| isObjectTreeWithError:objectName: ', errorObjectName);
    // console.log('---| isObjectTreeWithError:keys: ', keys);
    // console.log('---| isObjectTreeWithError:constructor.name: ', model['constructor'].name);

    if (model['constructor'].name === errorObjectName && model.__local__ === true) {
      return true;
    } else {
      for (const key of keys) {
        const value = model[key];
        if (value instanceof Array) {
          console.log('---| isObjectTreeWithError:key: ', key, ' is an Array');
          for (const childModel of value) {
            if (Reflect.has(childModel, 'ExternalID__c')) {
              const isObjectTreeWithError = this.isObjectTreeWithError(childModel);
              if (isObjectTreeWithError === true) {
                return true;
              }
            }
          }
        }
      }
    }
    return false;
  }

  private static retrieveErrorObjectName(): string {
    if (this.thereIsObjectWithError()) {
      const objectName = localStorage.getItem(this.ErrorObjectNameKey);
      return objectName;
    } else {
      return '';
    }
  }

  private static thereIsObjectWithError(): boolean {
    let thereIsObjectWithError = false;

    const objectName = localStorage.getItem(this.ErrorObjectNameKey);
    if (objectName != null && objectName.length > 0) {
      thereIsObjectWithError = true;
    }
    return thereIsObjectWithError;
  }

  private static isObjectTreeInvalid(model: BaseModel): boolean {
    const isObjectTreeInvalid = false;
    return isObjectTreeInvalid;
  }

}
