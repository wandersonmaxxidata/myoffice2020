import * as moment from 'moment';
export abstract class DateHelper {

  public static formattedCurrentDate(currentDate: Date) {
    // console.log('---| day week: ', DateHelper.dayOfTheWeekByDate(currentDate));
    // console.log('---| day number: ', currentDate.getDay());
    // console.log('---| month: ', DateHelper.monthNameByDate(currentDate));
    // console.log('---| year: ', currentDate.getFullYear());
    // console.log('---| date: ', currentDate.getDate());
    // console.log('---| hours: ', currentDate.getHours());
    // console.log('---| time: ', currentDate.getTime());

    const weekDay = DateHelper.dayOfTheWeekByDate(currentDate);
    const dayOfMonth = currentDate.getDate();
    const monthName = DateHelper.monthNameByDate(currentDate);
    const year = currentDate.getFullYear();

    // Example: 'Quarta-feira, 24 de janeiro de 2018'
    const formattedDate = weekDay + ', ' + dayOfMonth + ' de ' + monthName + ' de ' + year;

    return formattedDate;
  }

  public static formattedLastSyncDate(lastSyncDate: Date) {
    const formattedDate = moment(lastSyncDate).format('DD/MM/YYYY [às] hh:mm A');
    return formattedDate;
  }

  public static formattedDate(date: string) {
    const formattedDate = moment(date).format('DD/MM/YYYY');
    return formattedDate;
  }

  public static formattedDateToEventCard(date: string) {
    if (date) {
      return moment(date).format('HH[:]mm');
    } else {
      return '00:00';
    }
  }

  public static dayOfTheWeekByDate(date: Date): string {

    let weekDay = 'Segunda';

    const weekDays = new Array(7);
    weekDays[0] = 'Domingo';
    weekDays[1] = 'Segunda-feira';
    weekDays[2] = 'Terça-feira';
    weekDays[3] = 'Quarta-feira';
    weekDays[4] = 'Quinta-feira';
    weekDays[5] = 'Sexta-feira';
    weekDays[6] = 'Sábado';

    weekDay = weekDays[date.getDay()];

    return weekDay;
  }

  public static monthNameByDate(date: Date): string {

    let currentMonthName = 'Janeiro';

    const months = new Array(12);
    months[0] = 'Janeiro';
    months[1] = 'Fevereiro';
    months[2] = 'Março';
    months[3] = 'Abril';
    months[4] = 'Maio';
    months[5] = 'Junho';
    months[6] = 'Julho';
    months[7] = 'Agosto';
    months[8] = 'Setembro';
    months[9] = 'Outubro';
    months[10] = 'Novembro';
    months[11] = 'Dezembro';

    currentMonthName = months[date.getMonth()];

    return currentMonthName.toLowerCase();
  }

  public static dateISO(date): string {
    if (date) {
      return moment(date).format();
    } else {
      return moment().add(1, 'hours').set('minute', 0).format();
    }
  }

  public static dateISOwithOneMoreHour(date): string {
    if (date) {
      return moment(date).add(2, 'hours').format();
    } else {
      return moment().add(2, 'hours').set('minute', 0).format();
    }
  }

  public static dateUTC(date): string {
    return moment(date).toISOString().replace('Z', '');
  }

  public static parseISOString(s) {
    const b = s.split(/\D+/);
    const date = moment();
    date.set('year', b[0]);
    date.set('month', --b[1]);
    date.set('date', b[2]);
    date.set('hour', b[3]);
    date.set('minute', b[4]);
    date.set('second', b[5]);
    date.set('millisecond', b[6]);
    return date.format();
  }

  public static updateEndDate(StartDateTime: string, EndDateTime: string) {
    const start = moment(StartDateTime);
    const end = moment(EndDateTime);
    end.set('year', start.get('year'));
    end.set('month', start.get('month'));
    end.set('date', start.get('date'));
    return end.format();
  }

  public static diffDate(startDate: string, endDate: string) {
    const start = moment(startDate);
    const end = moment(endDate);
    const final = moment.duration(end.diff(start));

    return final.asMinutes();
  }

  public static diffInMinutes(startDate: string, endDate: string): number {

    const startDateTemp = new Date(startDate.split('+')[0]);
    const endDateTemp = new Date(endDate.split('+')[0]);

    console.log('startDateTemp: ', startDateTemp, ' endDateTemp: ', endDateTemp);
    const start = moment(startDateTemp);
    const end = moment(endDateTemp);
    const duration = moment.duration(end.diff(start));
    const milliseconds = duration.asMilliseconds();
    const minutes = Math.floor(milliseconds / 60000);

    console.log('---| DateHelper.diffInMinutes:startDate: ', startDate);
    console.log('---| DateHelper.diffInMinutes:endDate: ', endDate);
    console.log('---| DateHelper.diffInMinutes: ', minutes);

    return minutes;
  }

  public static getCurrentDateString(): string {
    return moment().format();
  }

  public static adjustAllDayEventTimezone(date: string): string {
    const eventDay = moment(date);
    eventDay.add(1, 'day').startOf('day');
    return eventDay.format();
  }

  public static getUTCTimestamp(date: string) {
    return moment(date).utc().unix();
  }

  public static isAllDayEventNotAdjusted(event): boolean {
    return event.IsAllDayEvent && String(event.StartDateTime).indexOf('+0000') >= 0;
  }
}
