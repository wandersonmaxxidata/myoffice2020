import { AlertController } from 'ionic-angular';

export abstract class AlertHelper {

  public static showFirstOpenAppAlert(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Bem vindo ao myOffice!';
    const message = 'Vamos realizar o primeiro sincronismo?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler, 'Realizar agora', 'Deixar para depois');
  }

  public static showConfirmCancelSyncAlert(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Interromper sincronismo';
    const message = 'Tem certeza que deseja interromper o sincronismo?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showConfirmLogoutAlert(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void, countSyncBadge?: number) {
    const title = 'Logout';
    const aux = 'Tem certeza que deseja realizar o Logout?';

    const message = (countSyncBadge && countSyncBadge > 0)
      ? `${aux} <br> (Há ${countSyncBadge} objeto(s) para sincronizar)`
      : aux;

    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showStartSyncProcessAlert(alertCtrl: AlertController, message: string, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Informações da sua rede';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showConfirmDialog(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Atenção';
    const message = 'Deseja sair do evento sem salvar?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showConfirmDialogForVisitCancellation(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Cancelar visita';
    const message = 'Tem certeza que deseja cancelar?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showConfirmDialogForVisitCompletion(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Concluir visita';
    const message = 'Você deseja concluir a visita?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showConfirmDialogForSalesCompletion(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Tem certeza que deseja concluir a sessão de Coaching de Vendas?';
    const message = 'Ao concluir, não será possível editá-la e um e-mail será enviado para o seu RTV após o sincronismo.';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showRequiredFieldsSalesAlert(alertCtrl: AlertController, message: string) {
    const title = 'Campos Requeridos';
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showNoConnectionAlert(alertCtrl: AlertController, message: string) {
    const title = 'Informações da sua rede';
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showPlantedAreaIsNotValidAlert(alertCtrl) {
    const title = 'Atenção';
    const message = 'A Área agricultável da fazenda não pode ser Maior que a área total da Fazenda.';
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showTimeLimitAlert(alertCtrl) {
    const title = 'Horário inválido';
    const message = 'Intervalo de início e fim inferior a 30 minutos!';
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showMessageError(alertCtrl: AlertController, msg: string) {
    const title = 'Atenção';
    const message = msg;
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showErrorLogout(alertCtrl: AlertController) {
    const title = 'Atenção';
    const message = 'Erro ao tentar realizar logout. Por favor, tente novamente mais tarde.';
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showNotInComplianceWithArchitecture(alertCtrl: AlertController) {
    const title = 'Chamada Inválida';
    const message = 'Funcionalidade com parâmetros incorretos, por favor, informar ao time de desenvolvimento';
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showCreditExpirationDateAlert(alertCtrl: AlertController) {
    const title = 'Atenção';
    const message = 'Data de validade de crédito inválida. A data de validade deve estar entre 60 e 360 dias a partir da data de hoje.';
    this.openOKAlert(alertCtrl, title, message);
  }

  public static showDialogForVisitAutoSave(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Atenção';
    const message = 'Existem campos obrigatórios na visita.<br>Deseja sair sem salvar?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showDialogSncyNow(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Sincronismo pendente';
    const message = 'Existem visitas a serem sincronizadas.<br>Deseja sincronizar agora?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showDialogForSave(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Salvar';
    const message = 'Você deseja realmente salvar?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showDialogContactNull(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = '<span class="alertLeft">!</span> Atenção! <span class="alertRigth">!</span>';
    const message = 'Caso não haja um e-mail de contato selecionado, o relatório da jornada não será enviado.';
    this.openWarningAlert(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showDialogErrorSyncy(alertCtrl: AlertController, confirmHandler: () => void, cancelHandler: () => void) {
    const title = 'Instabilidade na conexão. ';
    const message = 'Deseja continuar?<span class="obsErro">Isso não afetara o processo de sincronismo</span>';
    this.openWarningAlertGreen(alertCtrl, title, message, confirmHandler, cancelHandler);
  }

  public static showDialogForCancel(alertCtrl: AlertController, confirmHandler, cancelHandler, options?: any) {
    const title = 'Cancelar';
    const message = options ? 'Qual o motivo do cancelamento?' : 'Você deseja realmente cancelar?';
    this.openConfirmAlert(alertCtrl, title, message, confirmHandler, cancelHandler, undefined, undefined, options);
  }

  private static openConfirmAlert(alertCtrl: AlertController, title: string, message: string,
    confirmHandler: (data) => any, cancelHandler: () => any, textY?: string, textN?: string, options?: any) {
    if (!textY) { textY = 'Sim'; }
    if (!textN) { textN = 'Não'; }

    const alert = alertCtrl.create({
      title,
      message,
      mode: 'ios',
      cssClass: 'alert-confirm',
      buttons: [
        {
          text: textN,
          role: 'cancel',
          cssClass: 'alert-no',
          handler: () => {
            cancelHandler();
          },
        },
        {
          text: textY,
          cssClass: 'alert-yes',
          handler: (data: any) => {
            confirmHandler(data);
          },
        },
      ],
    });

    const op = options ? options : [];
    let check = true;

    op.forEach((item) => {
      alert.addInput({type: 'radio', label: item.label, value: item.value, checked: check});
      check = false;
    });

    alert.present();
  }

  private static openWarningAlertGreen(alertCtrl: AlertController, title: string, message: string,
    confirmHandler: (data) => any, cancelHandler: () => any, textY?: string, textN?: string, options?: any) {
    if (!textY) { textY = 'Continuar'; }
    if (!textN) { textN = 'Voltar'; }

    const alert = alertCtrl.create({
      title,
      message,
      mode: 'ios',
      cssClass: 'alert-confirm',
      buttons: [
        {
          text: textN,
          role: 'cancel',
          cssClass: 'alert-no',
          handler: () => {
            cancelHandler();
          },
        },
        {
          text: textY,
          cssClass: 'alert-yes',
          handler: (data: any) => {
            confirmHandler(data);
          },
        },
      ],
    });

    const op = options ? options : [];
    let check = true;

    op.forEach((item) => {
      alert.addInput({type: 'radio', label: item.label, value: item.value, checked: check});
      check = false;
    });

    alert.present();
  }


  private static openWarningAlert(alertCtrl: AlertController, title: string, message: string,
    confirmHandler: (data) => any, cancelHandler: () => any, textY?: string, textN?: string, options?: any) {
    if (!textY) { textY = 'Continuar'; }
    if (!textN) { textN = 'Voltar'; }

    const alert = alertCtrl.create({
      title,
      message,
      mode: 'ios',
      cssClass: 'alert-warning',
      buttons: [
        {
          text: textN,
          role: 'cancel',
          cssClass: 'alert-yes',
          handler: () => {
            cancelHandler();
          },
        },
        {
          text: textY,
          cssClass: 'alert-no',
          handler: (data: any) => {
            confirmHandler(data);
          },
        },
      ],
    });

    const op = options ? options : [];
    let check = true;

    op.forEach((item) => {
      alert.addInput({type: 'radio', label: item.label, value: item.value, checked: check});
      check = false;
    });

    alert.present();
  }

  private static openOKAlert(alertCtrl: AlertController, title: string, message: string, handler?: () => void) {
    const alert = alertCtrl.create({
      title,
      message,
      mode: 'ios',
      cssClass: 'alert-confirm',
      buttons: [
        {
          cssClass: 'alert-ok',
          handler: () => {
            if (handler) {
              handler();
            }
          },
          role: 'cancel',
          text: 'OK',
        },
      ],
    });
    alert.present();
  }

}
