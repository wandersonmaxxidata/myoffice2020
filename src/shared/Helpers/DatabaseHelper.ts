import { DataHelper } from './DataHelper';

export abstract class DatabaseHelper {

    public static isAlreadyInLocalDatabase(model: BaseModel): boolean {
        const isAlreadyInDatabase = model['_soupEntryId'];
        return isAlreadyInDatabase;
    }

    public static isAlreadyInSalesforce(model: BaseModel): boolean {
        const isAlreadyInSalesforce = model['Id'];
        return isAlreadyInSalesforce;
    }

    public static isOnlyInLocalDatabase(model: BaseModel): boolean {
        const isOnlyInDatabase = this.isAlreadyInLocalDatabase(model) && !this.isAlreadyInSalesforce(model);
        return isOnlyInDatabase;
    }

    public static isDeleted(model: BaseModel): boolean {
        const isOnlyInDatabase = model.__locally_deleted__;
        return isOnlyInDatabase;
    }

    public static filterNotDeleted<T extends BaseModel>(models: BaseModel[]): T[] {
        const notDeletedModels: T[] = models.filter((model) => {
            return !model.__locally_deleted__;
        }) as T[];

        return notDeletedModels;
    }

    public static soupEntryId(model: BaseModel) {
        const soupEntryId = model['_soupEntryId'];
        return soupEntryId;
    }

    public static convertNumberFieldsToString(object) {
        console.log('---| DataHelper.convertNumberFieldsToString:object: ', object);
        const keys = Object.keys(object);
        console.log('---| DataHelper.convertNumberFieldsToString:keys: ', keys);
        for (const key in keys) {
            if (key !== '_soupEntryId' && key !== '_soupLastModifiedDate') {
                console.log('---| DatabaseHelper.convertNumberFieldsToString:object[key]: ', object[key]);
                object[key] = DataHelper.ifNumberConvertToString(object[key]);
            }
        }
    }

}
