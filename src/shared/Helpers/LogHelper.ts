import { MustEditRegisterHelper } from './MustEditRegisterHelper';

declare var NativeLogs: any;

export abstract class LogHelper {

  public static SDK(lineNumber?: number) {
    return new Promise((resolve) => {
      if (typeof NativeLogs !== 'undefined') {
        const linesNumber = (lineNumber ? lineNumber : 100);

        NativeLogs.getLog(linesNumber, false, (logs: string) => {
          if (lineNumber) {
            console.error('SDK error log: ', logs);
            resolve();
          } else {
            this.prepareSDKError(logs).then((logJSON: any) => {
              if (logJSON.length > 0) {
                console.error('SDK error log: ', logJSON);
                resolve(logJSON);
              } else {
                resolve();
              }
            }).catch((err) => {
              console.error('Error: prepareSDKError method: ', err);
              resolve();
            });
          }
        });
      } else {
        resolve();
      }
    });
  }

  public static prepareSDKError(logs: string) {
    return new Promise((resolve, reject) => {

      const androidConst = 'JSONException: ';
      const iosConst = 'UserInfo={error=(';
      const errorArray = [];

      if (logs.includes(iosConst)) { // ios
        const iosArray = logs.split(iosConst);

        for (let i = 1; i < iosArray.length; i++) {

          let iosJSON = iosArray[i].substr(0, iosArray[i].indexOf(')}'));

          iosJSON = iosJSON.replace(/\\u([a-f0-9]{4})/gi, (n, hex) => {
            return String.fromCharCode(parseInt(hex, 16));
          });

          iosJSON = iosJSON.replace(/(?:\r\n|\r|\n|\s\s+)/g, '');

          let code = '';
          let field = [];
          let msg = '';

          const codeObj = iosJSON.split('erroCode');
          const fieldObj = iosJSON.split('fields');
          const msgObj = iosJSON.split('message');

          if (codeObj && codeObj.length > 1) {
            code = codeObj[1].split('=')[1].split('"')[1];
          }

          if (fieldObj && fieldObj.length > 1) {
            field = fieldObj[1].split('=')[1].replace(/"/g, '').replace(/[\(\)]/g, '=').split('=')[1].split(',');
          }

          if (msgObj && msgObj.length > 1) {
            msg = msgObj[1].split('=')[1].split('"')[1];
            if (msg.includes('No such column')) {
              msg = 'No such column' + msg.split('No such column')[1].split('.')[0] + '. Favor entrar em contato com o administrador do Salesforce e informar o erro.';
            }
          }

          let soup = '';
          const temp = iosArray[i - 1].split('soupName');

          if (temp && temp.length > 1) {
            const objectName = temp[1].split(':')[1].split('"')[1];
            console.log('---| prepareSDKError:objectName: ', objectName);
            MustEditRegisterHelper.setObjectName(objectName);
            soup = `Nome do Objeto: ${objectName} <br><br> `;
          }

          errorArray.push({
            errorCode: code,
            fields   : field,
            message  : soup + msg,
          });
        }
      }

      if (logs.includes(androidConst)) { // android
        const androidArray = logs.split(androidConst);

        for (let i = 1; i < androidArray.length; i++) {
          errorArray.push({
            errorCode: undefined,
            fields: undefined,
            message: androidArray[i].split('.')[0],
          });
        }
      }
      resolve(errorArray);
    });
  }

  public static logVariable(variable: any, nameVariable?: string) {
    console.log(`
    |------------------------------------------|
            VARIABLE:${nameVariable}
            VALOR:${variable}
    |------------------------------------------|
    `);
  }

  public static logSave(variables: any, nameVariable?: string) {
    console.log(`
    |--------------------------------------|
      THIS VARIABLES ${nameVariable} SAVED
    |--------------------------------------|
                ${variables}
    |--------------------------------------|
    `);
  }

  public static logArray(array: any[], nameArray?: string) {
    console.log(`|--------------------------------|`);
    array.forEach((a) => {
        console.log(a);
    });
    console.log(`|--------------------------------|`);
  }

  public static logErro(variable: any, nameVariable?: string) {
    console.log(`
    |!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!|
            VARIABLE:${nameVariable}
            VALOR:${variable}
    |!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!|
    `);
  }

}
