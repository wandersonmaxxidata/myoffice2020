
export abstract class ArrayHelper {

    public static checkArrayFull(array: any[], amount: number) {
        if (array.length >= amount) {
            return true;
        } else if (array.length < amount) {
            return false;
        }
    }

    public static filterUniqueValues(values: string[]): string[] {
        const set: Set<string> = new Set<string>();
        for (const value of values) {
            if (value != null && value.length > 0) {
                set.add(value);
            }
        }

        const resultArray: string[] = Array.from(set.values());
        return resultArray;
    }

    public static removeItemFromArray(item: any, array: any[]): any[] {

        const indexToRemove = array.indexOf(item);
        if (indexToRemove > -1) {
            array.splice(indexToRemove, 1);
        }

        return array;
    }

}
