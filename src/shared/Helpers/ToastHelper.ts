import { ToastController } from 'ionic-angular';

export abstract class ToastHelper {

    public static showSuccessMessage(toastCtrl: ToastController) {
        const message = '✓  Dados salvos com sucesso!';
        const duration = 1400;
        const css = 'toast-success';
        ToastHelper.showMessage(message, duration, css, toastCtrl);
    }

    private static showMessage(msg: string, time: number, css: string, toastCtrl: ToastController) {
        const toast = toastCtrl.create({
          closeButtonText: '✖',
          cssClass: css,
          dismissOnPageChange: false,
          duration: time,
          message: msg,
          position: 'top',
          showCloseButton: true,
        });
        toast.present();
    }
    public static showAttachmentVideoMessage(toastCtrl: ToastController) {
        const message = 'Video não permitido';
        const duration = 2000;
        const css = 'toast-success';
        ToastHelper.showMessage(message, duration, css, toastCtrl);
    }

}
