import { ServiceProvider } from '../../providers/service-salesforce';
import { IService } from '../../interfaces/IService';
import { EntitiesConfig } from '../EntitiesConfig';

export abstract class ServiceGroupingHelper {

    public static loadServicesFromEntities(entities: Entity[], serviceProvider: ServiceProvider): ServiceModel[] {

        let serviceModels: ServiceModel[] = [];
        for (const entity of entities) {

            const entityName = entity.Name;
            const entityLabel = entity.Label;
            const propertyName = entityName.toLowerCase() + 'Service';
            console.log('---| PropertyName: ', propertyName);

            const currentService: IService = Reflect.get(serviceProvider, propertyName);

            const synchronismServiceModel = new ServiceModel(entityName, entityLabel, entity.RelationFields, currentService);

            serviceModels.push(synchronismServiceModel);
        }
        console.log('---| Services: ', serviceModels);

        return serviceModels
    }

    public static loadEntitiesFromTS(): Entity[] {

        let entitiesString = JSON.stringify(EntitiesConfig.entities)
        let entities: Entity[] = JSON.parse(entitiesString)
        console.log('---| loadEntitiesFromTS:entities: ', entities)
        return entities

    }

}

export class ServiceModel {

    public entityName: string;
    public entityLabel: string;
    public relationFields: string[];
    public service: IService;

    constructor(entityName: string, entityLabel: string, relationFields: string[], service: IService) {
        this.entityName = entityName;
        this.entityLabel = entityLabel
        this.relationFields = relationFields
        this.service = service;
    }

}

export interface Entity {
    Name: string;
    SoupName: string;
    Fields: string;
    Related: string;
    IsMaleGender: boolean;
    Picklists: string[];
    RelationFields: string[];
    IsOtherObjects: boolean;
    Label: string;
}
