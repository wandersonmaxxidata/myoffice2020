import { EventStatusCss, EventStatusLabels } from '../../components/events/events';

export abstract class DataHelper {

  public static stringArrayToCSV(items: string[], separator: string): string {
    const csv = items.join(separator);
    return csv;
  }

  public static in(list: string[], item: string): boolean {
    for (const i of list) {
      if (item.includes(i)) {
        return true;
      }
    }
    return (list.indexOf(item) > -1);
  }

  public static csvToStringArray(csv: string, separator: string): string[] {
    const stringArray = csv.split(separator);
    return stringArray;
  }

  public static createTextObj(obj) {
    const textObj = [];

    for (const item of obj) {
      const aux = { title: item.Name, body: [] };

      for (const content of item.Info) {
        aux.body.push({ subtitle: DataHelper.firstUpperCase(content[0]), text: content[1] });
      }
      textObj.push(aux);
    }
    return textObj;
  }

  public static firstUpperCase(s: string) {
    if(s != undefined){
      return s.substr(0, 1).toUpperCase() + s.substr(1).toLowerCase();
    }else{
      return s;
    }
  
  }

  public static removeHTMLTags(text: string) {
    return text.replace(/<[^>]*>/g, '');
  }

  public static emailMatch(email: string) {
    const regex = new RegExp('[a-zA-Z0-9._]+[@]+[a-zA-Z0-9]+[.]+[a-zA-Z]{2,6}');
    return regex.test(email);
  }

  public static onlyLetters(str: string) {
    const regex = new RegExp('^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ][A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$');
    return regex.test(str);
  }

  public static telMatch(tel: string) {
    const matches = tel ? tel.match(/[0-9]+/g) : undefined;
    const onlyNumbers = matches ? matches.join('') : '';
    const regex = new RegExp('^[0-9]{2}[1-9]{2}(?:[2-5]|9[1-9])[0-9]{3}[0-9]{4}$');
    return regex.test(onlyNumbers);
  }

  public static cpfMatch(cpf: string) {
    const regex = new RegExp('^\\d{3}\\.?\\d{3}\\.?\\d{3}\\-?\\d{2}$');
    return regex.test(cpf);
  }

  public static cnpjMatch(cnpj: string) {
    const regex = new RegExp('^\\d{3}.?\\d{3}.?\\d{3}/?\\d{3}-?\\d{2}$');
    return regex.test(cnpj);
  }

  public static decimalMatch(decimal: string) {
    const regex = new RegExp('^\\d+\\,?\\d{0,3}$');
    return regex.test(decimal);
  }

  public static noSpecialCharacter(str: string) {
    const regex = new RegExp('^[0-9a-zA-Záéíóúàèìòùâêîôûãõç ]+$');
    return regex.test(str);
  }

  public static onlyNumbers(str: string) {
    const regex = new RegExp('^\\d+$');
    return regex.test(str);
  }

  public static getBoolean(str: any): boolean {
    return (str === '1' || str === 'true' || str === true);
  }

  public static checkInput(input) {
    return (input && input.toString().length > 0);
  }

  public static checkToggleObj(toggle, obj) {
    return (toggle ? (obj && obj.length > 0) : !(obj && obj.length > 0));
  }

  public static checkToggleForNumber(toggle, objTemp) {
    const obj = this.ifNumberConvertToString(objTemp);
    return (toggle ? (obj && obj.length >= 0) : (!obj || obj === '0'));
  }

  public static ifNumberConvertToString(value): string {
    return (typeof value === 'number') ? value.toString() : value;
  }

  public static validationForRange(value: string, min: number, max: number) {
    if (value !== null && value !== undefined) {
      const num = parseInt(value, 10);
      return (num >= min && num <= max) ? true : false;
    } else {
      return false;
    }
  }

  public static getFilterSelected(listArray, itemArray) {
    const newList = [];
    for (const item of itemArray) {
      for (const listItem of listArray) {
        if (item === listItem.label) {
          newList.push(listItem);
          break;
        }
      }
    }
    return newList;
  }

  public static mascaraCpf(value: string) {
    return value ? value.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '\$1.\$2.\$3\-\$4') : '';
  }

  public static mascaraCnpj(value) {
    return value ? value.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '\$1.\$2.\$3\/\$4\-\$5') : '';
  }

  public static formatTextReport(text) {
    text = text.toLowerCase();
    text = text.replace(/ /g, '_').replace(/-/g, '_');
    text = text.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'a');
    text = text.replace(new RegExp('[ÉÈÊ]', 'gi'), 'e');
    text = text.replace(new RegExp('[ÍÌÎ]', 'gi'), 'i');
    text = text.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'o');
    text = text.replace(new RegExp('[ÚÙÛ]', 'gi'), 'u');
    text = text.replace(new RegExp('[Ç]', 'gi'), 'c');

    text = text.replace('_e_', '_'); // germinacao_e_emergencia
    return text;
  }

  public static checkMaxLenghtString(str: string, max: number) {
    if (str.length > max) {
      let letters: string[];

      letters = str.split('');
      letters.pop();
      str = letters.join('');

      this.checkMaxLenghtString(str, max);
    }
  }

  public static rangeLenghtAfter(str: string) {
    let letters: string[];
    letters = str.split('.');

    if (letters[1]) {
      letters[1] = letters[1].substr(0, 14);
    }
    return letters.join('.');
  }

  public static checkBooleanValuesObj(obj: any) {
    const list = obj ? obj : [];
    let values = false;

    for (const i of Object.keys(list)) {
      if (obj[i]) {
        values = obj[i];
        break;
      }
    }
    return values;
  }

  public static copyObj(obj) {
    return (JSON.parse(JSON.stringify(obj)));
  }

  public static compareObj(old_obj, new_obj) {
    return (old_obj && JSON.stringify(old_obj) === JSON.stringify(new_obj));
  }

  public static removeEmpty(obj) {
    Object.keys(obj).forEach((key) =>
      (obj[key] && typeof obj[key] === 'object') && DataHelper.removeEmpty(obj[key]) || (obj[key] === undefined) && delete obj[key]);
    return obj;
  }

  public static removeDuplicatesFromObject(obj, field) {
    return obj.filter((item, pos, arr) => {
      return arr.map((mapObj) => mapObj[field]).indexOf(item[field]) === pos;
    });
  }

  public static removeDuplicatesFromArray(arr) {
    const unique_array = arr.filter((elem, index, self) => {
      return index === self.indexOf(elem);
    });
    return unique_array;
  }

  public static cssForStatus(status: string, isOutlook: boolean) {
    if (isOutlook) {
      return EventStatusCss['outlook'];
    }
    for (const key of Object.keys(EventStatusLabels)) {
      const constStatus = EventStatusLabels[key].substring(0, EventStatusLabels[key].length - 1);
      if (status.toLowerCase().includes(constStatus.toLowerCase())) {
        return EventStatusCss[key];
      }
    }
  }

}
