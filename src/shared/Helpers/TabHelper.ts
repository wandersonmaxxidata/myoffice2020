import { NavController, Tab } from 'ionic-angular';
import { Tabs } from 'ionic-angular/navigation/nav-interfaces';

export abstract class TabHelper {

  public static disableAll(navCtrl: NavController) {
    const tabs = this.getTabs(navCtrl);
    if (tabs) {
      tabs.forEach((tab: Tab) => {
        tab.enabled = false;
      });
    }
  }

  public static enableAll(navCtrl: NavController) {
    const tabs = this.getTabs(navCtrl);
    if (tabs) {
      tabs.forEach((tab: Tab) => {
        tab.enabled = true;
      });
    }
  }

  public static disableNonSelectedTabs(navCtrl: NavController) {
    const tabs = this.getTabs(navCtrl);
    if (tabs) {
      tabs.forEach((tab: Tab) => {
        tab.enabled = tab.isSelected;
      });
    }
  }

  private static getTabs(navCtrl: NavController) {
    let tabs: any[];
    const tabsPage: Tabs = navCtrl.parent;

    if (tabsPage && tabsPage._tabs) {
      tabs = tabsPage._tabs;
    }
    return tabs;
  }

  public static popTabNavigationController(navCtrl: NavController, tabIndex: number) {
    const tabs = this.getTabs(navCtrl);

    if (tabIndex > -1 && tabIndex < 5) {
      console.log('---| popTabNavigationController:tabs[tabIndex]: ', tabs[tabIndex]);
      const navController = tabs[tabIndex] as NavController;
      navController.popAll();
    }
  }

  public static popAllTabNavigationController(navCtrl: NavController) {
    const tabs = this.getTabs(navCtrl);

    for (let i = 0; i < tabs.length; i++) {
      TabHelper.popTabNavigationController(navCtrl, i);
    }
  }

}
