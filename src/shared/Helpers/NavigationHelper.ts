import { NotificationPage } from './../../pages/notification/notification';
import { HomePage } from './../../pages/home/home';
import { AgendaPage } from './../../pages/agenda/agenda';
import { ClientsDetailPage } from './../../pages/clients-detail/clients-detail';
import { NavController } from 'ionic-angular';

export abstract class NavigationHelper {

  public static goBackToOrigin(navCtrl: NavController) {

    const views = navCtrl.getViews();
    const targetView = views[views.length - 3];

    if (targetView.instance instanceof ClientsDetailPage ||
      targetView.instance instanceof AgendaPage ||
      targetView.instance instanceof HomePage ||
      targetView.instance instanceof NotificationPage
    ) {
      navCtrl.popTo(targetView);
    }
  }
}
