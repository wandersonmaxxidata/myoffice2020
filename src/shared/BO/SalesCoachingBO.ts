import { BR_Sales_Coaching__cModel } from '../../app/model/BR_Sales_Coaching__cModel';
import { DataHelper } from '../Helpers/DataHelper';
import { EventModel } from '../../app/model/EventModel';
import { Events } from 'ionic-angular/util/events';
import { NotificationKeys } from '../../shared/Constants/Keys';
import { RecordTypeDAO } from '../DAO/RecordTypeDAO';
import { SalesCoachingDAO } from '../DAO/SalesCoachingDAO';
import { ServiceProvider } from '../../providers/service-salesforce';

export const SalesCoachingStatus = {
  open: 'A iniciar',
  inProgress: 'Em andamento',
  canceled: 'Cancelada',
  completed: 'Concluída',
};

export class SalesCoachingBO {

  private events: Events;
  private salesCoachingDAO: SalesCoachingDAO;
  private recordTypeDAO: RecordTypeDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.events = new Events();
    this.salesCoachingDAO = new SalesCoachingDAO(this.serviceProvider);
    this.recordTypeDAO = new RecordTypeDAO(this.serviceProvider);
  }

  public loadSalesCoaching(event: EventModel): Promise<BR_Sales_Coaching__cModel> {
    return this.salesCoachingDAO.loadSalesCoachingByEventId(event.WhatId);
  }

  public createIfNotExistsWithEvent(event: EventModel, salesCoachingRecordType: string): Promise<BR_Sales_Coaching__cModel> {
    return new Promise((resolve, reject) => {
      const eventId = event.WhatId;
      this.salesCoachingDAO.findById(eventId).then((founded) => {
        let promise: Promise<any> = new Promise((finish) => { finish(); });
        if (!founded) {
          promise = this.create(salesCoachingRecordType);
        }
        return promise;

      }).then((newSalesCoaching) => {
        let promise: Promise<any> = new Promise((finish) => { finish(); });
        if (newSalesCoaching) {
          promise = this.saveSalesCoaching(event, newSalesCoaching);
        }
        return promise;

      }).then((savedSalesCoaching) => {
        resolve(savedSalesCoaching);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  public create(salesCoachingRecordType: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const salesCoaching = new BR_Sales_Coaching__cModel(null);
      this.recordTypeDAO.loadRecordTypeForSalesCoaching(salesCoachingRecordType).then((recordTypeId) => {
        salesCoaching.RecordTypeId = recordTypeId;
        salesCoaching.BR_Status__c = SalesCoachingStatus.open;
        salesCoaching.BR_Stage__c = 'Prepare-se';
        resolve(salesCoaching);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  public saveSalesCoaching(event: EventModel, salesCoaching: BR_Sales_Coaching__cModel): Promise<any> {
    return new Promise((resolve, reject) => {
      this.setDefaultValues(event, salesCoaching).then(() => {
        this.loadSalesCoaching(event).then((loadedSalesCoaching: BR_Sales_Coaching__cModel) => {

          const salesCoaching_local = DataHelper.removeEmpty(DataHelper.copyObj(loadedSalesCoaching));
          let salesCoaching_copy = DataHelper.removeEmpty(DataHelper.copyObj(salesCoaching));

          if (salesCoaching_local && DataHelper.compareObj(salesCoaching_local, salesCoaching_copy)) {
            salesCoaching_copy = undefined;
          }

          this.salesCoachingDAO.saveSalesCoaching(salesCoaching_copy).then(() => {
            this.loadSalesCoaching(event).then((savedSalesCoaching: BR_Sales_Coaching__cModel) => {
              if (savedSalesCoaching.BR_Status__c !== SalesCoachingStatus.inProgress) {
                this.events.publish(NotificationKeys.totalNotifications);
              }
              console.log('------| Success:SalesCoaching:save: ', savedSalesCoaching);
              resolve(savedSalesCoaching);
            });
          }).catch((err) => {
            reject(err);
          });
        });
      });
    });
  }

  public cancel(salesCoaching: BR_Sales_Coaching__cModel): Promise<any> {
    salesCoaching.BR_Status__c = SalesCoachingStatus.canceled;
    return this.salesCoachingDAO.saveSalesCoaching(salesCoaching);
  }

  public complete(salesCoaching: BR_Sales_Coaching__cModel): Promise<any> {
    salesCoaching.BR_Status__c = SalesCoachingStatus.completed;
    return this.salesCoachingDAO.saveSalesCoaching(salesCoaching);
  }

  public setDefaultValues(event: EventModel, salesCoaching: BR_Sales_Coaching__cModel) {
    return new Promise ((resolve) => {
      salesCoaching.BR_MobileAppFlag__c = true;

      salesCoaching.BR_Location__c = event.Location;
      salesCoaching.BR_AllDayEvent__c = event.IsAllDayEvent;
      salesCoaching.BR_StartDate__c = event.StartDateTime;
      salesCoaching.BR_EndDate__c = event.EndDateTime;

      if (!salesCoaching.BR_RTV__c) {
        salesCoaching.Name = event.BR_Tipo_de_Visita__c.split('. ')[1];
        event.BR_Tipo_de_Visita__c = undefined;

        salesCoaching.BR_RTV__c = event.WhatId.slice();
        event.WhatId = salesCoaching.Id.slice();
      }
      resolve();
    });
  }

  public getRecordTypeName(recordTypeId: string) {
    return new Promise((resolve) => {
      this.recordTypeDAO.loadRecordTypeId(recordTypeId).then((developerName) => {
        resolve(developerName);
      });
    });
  }

}
