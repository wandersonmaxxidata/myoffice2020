import { ReportDAO } from '../DAO/ReportDAO';
import { AttachmentDAO } from '../DAO/AttachmentDAO';
import { VisitRecordTypes } from '../Constants/Types';
import { ServiceProvider } from '../../providers/service-salesforce';
import { DateHelper } from '../Helpers/DateHelper';
import { DataHelper } from '../Helpers/DataHelper';

export class ReportBO {

  public reportDAO: ReportDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.reportDAO = new ReportDAO(this.serviceProvider);
  }

  public getHybridsLaunch(typeVisit, visit) {
    return new Promise((resolve) => {
      this.reportDAO.hybridsLaunch(typeVisit, visit.Hibrido_de_Visita__c).then((data) => {
        resolve(data);
      });
    });
  }

  public loadTypes(eventType) {
    return new Promise((resolve) => {
      const eventTypeLower = DataHelper.formatTextReport(eventType);
      const currentType = { type: '', visitType: '' };

      console.log('------| eventType: ', eventType);
      console.log('------| eventTypeLower: ', eventTypeLower);

      // condição exclusiva para "Pré-Plantio". Salesforce para query de consulta: "Pré-plantio"
      if (eventTypeLower.includes(VisitRecordTypes.prePlanting.toLowerCase())) {
        currentType.type = 'pp';
        currentType.visitType = 'Pré-plantio';
      } else if (eventTypeLower.includes(VisitRecordTypes.planting.toLowerCase())) {
        currentType.type = 'p';
        currentType.visitType = 'Plantio';
      } else if (eventTypeLower.includes(VisitRecordTypes.germinationAndEmergency.toLowerCase())) {
        currentType.type = 'ge';
        currentType.visitType = 'Germinação e Emergência';
      } else if (eventTypeLower.includes(VisitRecordTypes.vegetativeDevelopment.toLowerCase())) {
        currentType.type = 'dv';
        currentType.visitType = 'Desenvolvimento Vegetativo';
      } else if (eventTypeLower.includes(VisitRecordTypes.reproductiveDevelopment.toLowerCase())) {
        currentType.type = 'dr';
        currentType.visitType = 'Desenvolvimento Reprodutivo';
      } else if (eventTypeLower.includes(VisitRecordTypes.harvest.toLowerCase())) {
        currentType.type = 'c';
        currentType.visitType = 'Colheita';
      }

      resolve(currentType);
    });
  }

  public loadHeaderObj(event, visit) {
    return new Promise((resolve) => {

      const headerObj = {
        brand: null,
        name: null,
        date: null,
        farmer: null,
        farm: null,
        geolocation: null,
      };

      this.reportDAO.getUserBrand().then((data__b: string) => {
        if (data__b === 'ats dkb' || data__b === 'atcdk' || data__b === 'dk') {
          headerObj.brand = 'dekalb';
        } else if (data__b === 'ats ag' || data__b === 'atcag' || data__b === 'ag') {
          headerObj.brand = 'agroceres';
        } else {
          headerObj.brand = data__b;
        }

        this.reportDAO.getUserName().then((data__u) => {
          headerObj.name = data__u;
        });

        headerObj.date = DateHelper.formattedDate(visit.Data_de_Execucao__c);

        this.reportDAO.getClientFarmName(event.WhatId, event.Fazenda__c).then((data) => {
          headerObj.farmer = data[0] ? data[0] : ' ';
          headerObj.farm = data[1] ? data[1] : ' ';
        }, (err) => {
          console.log('------| ERROR getClientFarmName: ', err);
        });

        let aux = '';
        if (visit.BR_Altitude__c) {
          aux += `${visit.BR_Altitude__c};`;
        }
        if (visit.Latitude__c) {
          aux += `${visit.Latitude__c}; ${visit.Longitude__c}`;
        }
        headerObj.geolocation = (aux.length > 0) ? aux : ' ';

        resolve(headerObj);
      });
    });
  }

  public loadMsgObj(visit, brand, session): any {
    return new Promise((resolve) => {
      this.reportDAO.getMsg(visit, DataHelper.firstUpperCase(brand), session).then((data) => {
        resolve((data.length > 0) ? DataHelper.createTextObj(data) : undefined);
      });
    });
  }

  public loadInformationObj(type, visit) {
    return new Promise((resolve) => {
      const hybrid = [];

      if (type.pp) {
        for (const i of visit.Hibrido_de_Visita__c) {
          hybrid.push({
            hybrid__r: i.BR_HybridName__c ? i.BR_HybridName__c : ' ',
            population__r: i.Populacao_de_Plantio__c ? i.Populacao_de_Plantio__c : ' ',
            date__r: i.BR_PlantationDate__c ? DateHelper.formattedDate(i.BR_PlantationDate__c) : ' ',
          });
        }
      } else if (type.p) {
        for (const i of visit.Hibrido_de_Visita__c) {
          hybrid.push({
            hybrid__p: i.BR_HybridName__c ? i.BR_HybridName__c : ' ',
            treatmentSeed__p: i.Tratamento_de_Sementes__c ? i.Tratamento_de_Sementes__c : ' ',
            product__p: i.Produto__c ? i.Produto__c : ' ',
            population__p: i.Populacao_de_Plantio__c ? i.Populacao_de_Plantio__c : ' ',
            area__p: i.Area_Plantada__c ? i.Area_Plantada__c : ' ',
            date__p: i.BR_PlantationDate__c ? DateHelper.formattedDate(i.BR_PlantationDate__c) : ' ',
          });
        }
      }

      const informationObj = (hybrid.length > 0) ? hybrid : [];
      resolve(informationObj);
    });
  }

  public loadWeedObj(type, visit, handleGeralMsgObj) {
    return new Promise((resolve) => {

      let weedObj = {};
      let infoWeedObj = {};

      if (type.pp) {
        weedObj = {
          handle: false,
          visible: visit.BR_ProductUsed__c ? true : false,
          isFounded__pp: visit.BR_ProductUsed__c ? true : false,
          whichProduct__pp: visit.BR_ProductUsed__c ? visit.BR_ProductUsed__c : ' ',
        };

      } else if (type.p) {
        const w = [];
        for (const i of visit.BR_WeedHandling__c) {
          w.push({
            whichFound__p: i.BR_WeedType__c ? i.BR_WeedType__c : ' ',
            levelInfestation__p: i.BR_LevelOfInfestation__c ? i.BR_LevelOfInfestation__c : ' ',
          });
        }
        weedObj = {
          handle: false,
          visible: (w.length > 0) ? true : false,
          isFounded__p: (w.length > 0) ? true : false,
          weed__p: (w.length > 0) ? w : [],
        };
      } else if (type.ge) {
        if (handleGeralMsgObj == undefined) {
          infoWeedObj = {
            handle: true,
            stand__im: visit.BR_stand_well_established__c,
            failPlanting__im: visit.BR_failed_planting__c,
            whichMotive__im: visit.BR_reason__c ? visit.BR_reason__c : undefined,
            subtitle__im: undefined,
            text__im: undefined,
          };
        }else{
          infoWeedObj = {
            handle: true,
            stand__im: visit.BR_stand_well_established__c,
            failPlanting__im: visit.BR_failed_planting__c,
            whichMotive__im: visit.BR_reason__c ? visit.BR_reason__c : undefined,
            subtitle__im: handleGeralMsgObj[0].body[0].subtitle ? handleGeralMsgObj[0].body[0].subtitle : undefined,
            text__im: handleGeralMsgObj[0].body[0].text ? handleGeralMsgObj[0].body[0].text : undefined,
          };
        }

        const h = [];
        const w = [];
        for (const i of visit.ProductUsed) {
          h.push({
            whichHerbicida__ge: i.BR_ProductsUsed__c ? i.BR_ProductsUsed__c : ' ',
            dosedHerbicida__ge: i.BR_DosageAmount__c ? i.BR_DosageAmount__c : ' ',
            typeDosed__ge: i.BR_DosageType__c ? i.BR_DosageType__c : '',
          });
        }
        for (const i of visit.BR_WeedHandling__c) {
          w.push({
            whichFound__ge: i.BR_WeedType__c ? i.BR_WeedType__c : ' ',
            levelOfInfestation__ge: i.BR_LevelOfInfestation__c ? i.BR_LevelOfInfestation__c : ' ',
          });
        }
        weedObj = {
          handle: false,
          visible: (h.length > 0 || w.length > 0) ? true : false,
          usedHerbicida__ge: (h.length > 0) ? true : false,
          herbicida: (h.length > 0) ? h : [],
          scapeWeed__ge: (w.length > 0) ? true : false,
          weed__ge: (w.length > 0) ? w : [],
        };

      } else if (type.dv) {
        if (handleGeralMsgObj == undefined) {
          infoWeedObj = {
            handle: true,
            diseaseControl__im: visit.BR_has_disease_control_fungicide__c ? true : false,
            whichProduct__im: visit.BR_fungicide_product__c ? visit.BR_fungicide_product__c : ' ',
            subtitle__im: undefined,
            text__im: undefined,
          };
        } else {
          infoWeedObj = {
            handle: true,
            diseaseControl__im: visit.BR_has_disease_control_fungicide__c ? true : false,
            whichProduct__im: visit.BR_fungicide_product__c ? visit.BR_fungicide_product__c : ' ',
            subtitle__im: handleGeralMsgObj[0].body[0].subtitle ? handleGeralMsgObj[0].body[0].subtitle : undefined,
            text__im: handleGeralMsgObj[0].body[0].subtitle ? handleGeralMsgObj[0].body[0].text : undefined,
          };
        }

      } else if (type.dr) {
        if (handleGeralMsgObj == undefined) {
          infoWeedObj = {
            handle: true,
            subtitle__im: undefined,
            text__im: undefined,
          };
        } else {
          infoWeedObj = {
            handle: true,
            subtitle__im: handleGeralMsgObj[0].body[0].subtitle ? handleGeralMsgObj[0].body[0].subtitle : undefined,
            text__im: handleGeralMsgObj[0].body[0].subtitle ? handleGeralMsgObj[0].body[0].text : undefined,
          };
        }

      } else if (type.c) {

        const w = [];
        for (const i of visit.BR_WeedHandling__c) {
          w.push({
            whichWeed__c: i.BR_WeedType__c ? i.BR_WeedType__c : ' ',
            levelInfestation__c: i.BR_LevelOfInfestation__c ? i.BR_LevelOfInfestation__c : ' ',
          });
        }
        weedObj = {
          handle: false,
          visible: (w.length > 0) ? true : false,
          weedInfection__c: visit.BR_infestation_weed__c ? true : false,
          weed__c: (w.length > 0) ? w : [],
        };
      }

      resolve({ weed: weedObj, infoWeed: infoWeedObj });
    });
  }

  public loadPlagueObj(type, visit) {
    return new Promise((resolve) => {
      let plagueObj = {};

      if (type.pp) {
        plagueObj = {
          visible: (visit.BR_PragueWhichAttackedArea__c) ? true : false,
          existPlagues__pp: (visit.BR_PragueWhichAttackedArea__c) ? true : false,
          whichPlague__pp: (visit.BR_PragueWhichAttackedArea__c) ? visit.BR_PragueWhichAttackedArea__c : ' ',
        };

      } else if (type.p) {
        // const p = [];
        // for (const i of visit.Hibrido_de_Visita__c) {
        //   p.push({
        //     treatmentSeed__p: i.Tratamento_de_Sementes__c ? true : false,
        //     whichProduct__p: i.Produto__c ? i.Produto__c.toString() : null,
        //   });
        // }

        plagueObj = {
          visible: visit.BR_plant_area_refuge__c ? true : false,
          // plague__p        : (p.length > 0) ? p : [],
          // refugeArea__p    : visit.Porcentagem_de_Produtividade__c ? true : false,
          percRefugeArea__p: visit.BR_percentage_refuge_areas__c ? (visit.BR_percentage_refuge_areas__c + ' %') : ' ',
        };

      } else if (type.ge || type.dv || type.dr) {
        const p = [];

        for (const i of visit.Hibrido_de_Visita__c) {
          for (const y of i.Plagues) {
            p.push({
              whichFound__ge_dv_dr: y.Qual__c ? y.Qual__c : ' ',
              levelInfestation__ge_dv_dr: y.Tipo_de_Praga__c ? y.Tipo_de_Praga__c : ' ',
              whichProduct__ge_dv_dr: y.Quimicos__c ? y.Quimicos__c : ' ',
            });
          }
        }

        plagueObj = {
          visible: (p.length > 0) ? true : false,
          plague__ge_dv_dr: (p.length > 0) ? p : [],
        };
      }

      resolve(plagueObj);
    });
  }

  public loadImageObj(visit) {
    return new Promise((resolve) => {

      const attachmentDAO = new AttachmentDAO(this.serviceProvider);
      const imageObj = [];

      attachmentDAO.load(visit.getId()).then((attach) => {
        attach.forEach((file) => {
          imageObj.push(this.prepareImage(file));
        });

        console.log('### return getAttachment: visitID: ', visit.getId());
        console.log('### return getAttachment: attachment: ', attach);
        console.log('### return getAttachment: imageObj: ', imageObj);
        resolve(imageObj);

      }, (err) => {
        console.log('------| Error loadImageObj (report): ', err);
        resolve(imageObj);
      });
    });
  }

  private prepareImage(file: any): string {
    const type = file.ContentType.substring(0, file.ContentType.indexOf('/'));
    const path = `assets/imgs/${type}.png`;
    const base64 = `data:${file.ContentType};base64,${file.Body}`;
    return ((type === 'image') ? base64 : path);
  }

}
