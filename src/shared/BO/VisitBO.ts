import { DataHelper } from '../Helpers/DataHelper';
import { DateHelper } from '../Helpers/DateHelper';
import { EventModel } from '../../app/model/EventModel';
import { Events } from 'ionic-angular/util/events';
import { GenericDAO } from '../DAO/GenericDAO';
import { NotificationKeys } from './../../shared/Constants/Keys';
import { RecordTypeDAO } from '../DAO/RecordTypeDAO';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Visita__cModel } from './../../app/model/Visita__cModel';
import { VisitDAO } from '../DAO/VisitDAO';

export const VisitStatus = {
  open: 'A Iniciar',
  inProgress: 'Em Andamento',
  canceled: 'Cancelada',
  completed: 'Concluída',
};

export class VisitBO {

  private events: Events;
  private recordTypeDAO: RecordTypeDAO;
  private visitDAO: VisitDAO;

  private visitHybridDAO: GenericDAO;
  private plagueDAO: GenericDAO;
  private weedHandlingDAO: GenericDAO;
  private productUsedDAO: GenericDAO;
  private diseaseDAO: GenericDAO;
  private cropProductDAO: GenericDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.events = new Events();
    this.recordTypeDAO = new RecordTypeDAO(this.serviceProvider);
    this.visitDAO = new VisitDAO(this.serviceProvider);
    this.visitHybridDAO = new GenericDAO(this.serviceProvider.getHibrido_de_Visita__cService());
    this.plagueDAO = new GenericDAO(this.serviceProvider.getPraga__cService());
    this.weedHandlingDAO = new GenericDAO(this.serviceProvider.getBR_WeedHandling__cService());
    this.productUsedDAO = new GenericDAO(this.serviceProvider.getBR_ProductUsed__cService());
    this.diseaseDAO = new GenericDAO(this.serviceProvider.getDoenca__cService());
    this.cropProductDAO = new GenericDAO(this.serviceProvider.getBR_CropProducts__cService());
  }

  public loadVisit(event: EventModel): Promise<Visita__cModel> {
    return this.visitDAO.loadVisitByEventId(event.getId());
  }

  public create(visitRecordType: string, emails: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const visit = new Visita__cModel(null);
      this.recordTypeDAO.loadRecordTypeForVisit(visitRecordType).then((recordTypeId) => {
        visit.RecordTypeId = recordTypeId;
        visit.Status__c = VisitStatus.open;
        visit.BR_EmailContactList__c = emails;
        resolve(visit);
      }).catch((error) => {
        reject(error);
      });
    });
  }


  public createError(visit): Promise<any> {
    return new Promise((resolve, reject) => {
        resolve(visit);
    });
  }

  public createIfNotExistsWithEvent(event: EventModel, visitRecordType: string, emails: string): Promise<Visita__cModel> {
    return new Promise((resolve, reject) => {
      const eventId = event.getId();
      this.visitDAO.findById(eventId).then((founded) => {
        let promise: Promise<any> = new Promise((finish) => { finish(); });
        if (!founded) {
          promise = this.create(visitRecordType, emails);
        }
        return promise;

      }).then((newVisit) => {
        let promise: Promise<any> = new Promise((finish) => { finish(); });
        if (newVisit) {
          promise = this.saveVisit(event, newVisit);
        }
        return promise;

      }).then((savedVisit) => {
        resolve(savedVisit);
      }).catch((error) => {
        reject(error);
      });
    });
  }

  public existsWithEventId(eventId: string): Promise<boolean> {
    return this.visitDAO.findById(eventId);
  }

  public setDefaultValues(event: EventModel, visit: Visita__cModel) {
    return new Promise((resolve) => {
      if (event.BR_Division__c) {
        visit.BR_Division__c = event.BR_Division__c;
      }
      if (visit.Status__c === VisitStatus.completed) {
        visit.BR_GenerateReport__c = true;
      }
      if (event.Tipo_Compromisso__c === 'Canal') {
        visit.Cliente__c = undefined;
        visit.Matriz__c = event.ClienteId__c ? event.ClienteId__c : event.WhatId;
      } else {
        visit.Cliente__c = event.ClienteId__c ? event.ClienteId__c : event.WhatId;
        visit.Matriz__c = undefined;
      }

      visit.EventId__c = event.getId();
      visit.Name = event.BR_Tipo_de_Visita__c;
      visit.Safra__c = event.Safra_ID__c;
      visit.BR_MobileAppFlag__c = true;
      resolve();
    });
  }

  public saveVisit(event: EventModel, visit: Visita__cModel): Promise<any> {
    return new Promise((resolve, reject) => {
      this.setDefaultValues(event, visit).then(() => {
        this.loadVisit(event).then((loadedVisit: Visita__cModel) => {

          const visit_local = DataHelper.removeEmpty(DataHelper.copyObj(loadedVisit));
          let visit_copy = DataHelper.removeEmpty(DataHelper.copyObj(visit));

          const weedHandling = [];
          const productUsed = [];
          const cropProducts = [];

          const hybrids = [];
          const plagues = [];
          const diseases = [];

          visit_copy.BR_WeedHandling__c.forEach((weed, i) => {
            if (!visit_local.BR_WeedHandling__c[i] || !DataHelper.compareObj(visit_local.BR_WeedHandling__c[i], weed)) {
              weedHandling.push(DataHelper.copyObj(weed));
            }
          });
          delete visit_copy.BR_WeedHandling__c;
          delete visit_local.BR_WeedHandling__c;

          visit_copy.ProductUsed.forEach((product, i) => {
            if (!visit_local.ProductUsed[i] || !DataHelper.compareObj(visit_local.ProductUsed[i], product)) {
              productUsed.push(DataHelper.copyObj(product));
            }
          });
          delete visit_copy.ProductUsed;
          delete visit_local.ProductUsed;

          visit_copy.CropProducts.forEach((cropProduct, i) => {
            if (!visit_local.CropProducts[i] || !DataHelper.compareObj(visit_local.CropProducts[i], cropProduct)) {
              cropProducts.push(DataHelper.copyObj(cropProduct));
            }
          });
          delete visit_copy.CropProducts;
          delete visit_local.CropProducts;

          visit_copy.Hibrido_de_Visita__c.forEach((hybrid, i) => {
            hybrid.Plagues.forEach((plague, y) => {
              if (!visit_local.Hibrido_de_Visita__c[i] || !DataHelper.compareObj(visit_local.Hibrido_de_Visita__c[i].Plagues[y], plague)) {
                plagues.push(DataHelper.copyObj(plague));
              }
            });

            hybrid.Diseases.forEach((disease, y) => {
              if (!visit_local.Hibrido_de_Visita__c[i] || !DataHelper.compareObj(visit_local.Hibrido_de_Visita__c[i].Diseases[y], disease)) {
                diseases.push(DataHelper.copyObj(disease));
              }
            });
            delete hybrid.Plagues;
            delete hybrid.Diseases;
            delete hybrid.AllHybrids;

            if (visit_local.Hibrido_de_Visita__c[i]) {
              delete visit_local.Hibrido_de_Visita__c[i].Plagues;
              delete visit_local.Hibrido_de_Visita__c[i].Diseases;
              delete visit_local.Hibrido_de_Visita__c[i].AllHybrids;
            }

            if (!visit_local.Hibrido_de_Visita__c[i] || !DataHelper.compareObj(visit_local.Hibrido_de_Visita__c[i], hybrid)) {
              hybrids.push(DataHelper.copyObj(hybrid));
            }
          });
          delete visit_copy.Hibrido_de_Visita__c;
          delete visit_local.Hibrido_de_Visita__c;

          if (visit_local && DataHelper.compareObj(visit_local, visit_copy)) {
            visit_copy = undefined;
          }

          this.visitDAO.saveVisit(visit_copy).then(() => {
            return this.visitHybridDAO.saveAll(hybrids);
          }).then(() => {
            return this.plagueDAO.saveAll(plagues);
          }).then(() => {
            return this.diseaseDAO.saveAll(diseases);
          }).then(() => {
            return this.weedHandlingDAO.saveAll(weedHandling);
          }).then(() => {
            return this.productUsedDAO.saveAll(productUsed);
          }).then(() => {
            return this.cropProductDAO.saveAll(cropProducts);
          }).then(() => {
            this.loadVisit(event).then((savedVisit: Visita__cModel) => {
              if (savedVisit.Status__c !== VisitStatus.inProgress) {
                this.events.publish(NotificationKeys.totalNotifications);
              }
              console.log('------| Success:Visit:save: ', savedVisit);
              resolve(savedVisit);
            });
          }).catch((err) => {
            reject(err);
          });
        });
      });
    });
  }

  //MAXXIDATA
  public saveVisitError(event: EventModel, visit: Visita__cModel): Promise<any> {
    return new Promise((resolve, reject) => {
      this.setDefaultValues(event, visit).then(() => {
        let loadedVisit = visit;

        const visit_local = DataHelper.removeEmpty(DataHelper.copyObj(loadedVisit));
        let visit_copy = DataHelper.removeEmpty(DataHelper.copyObj(visit));

        const weedHandling = [];
        const productUsed = [];
        const cropProducts = [];

        const hybrids = [];
        const plagues = [];
        const diseases = [];

        visit_copy.BR_WeedHandling__c.forEach((weed, i) => {
          if (!visit_local.BR_WeedHandling__c[i] || !DataHelper.compareObj(visit_local.BR_WeedHandling__c[i], weed)) {
            weedHandling.push(DataHelper.copyObj(weed));
          }
        });
        delete visit_copy.BR_WeedHandling__c;
        delete visit_local.BR_WeedHandling__c;

        visit_copy.ProductUsed.forEach((product, i) => {
          if (!visit_local.ProductUsed[i] || !DataHelper.compareObj(visit_local.ProductUsed[i], product)) {
            productUsed.push(DataHelper.copyObj(product));
          }
        });
        delete visit_copy.ProductUsed;
        delete visit_local.ProductUsed;

        visit_copy.CropProducts.forEach((cropProduct, i) => {
          if (!visit_local.CropProducts[i] || !DataHelper.compareObj(visit_local.CropProducts[i], cropProduct)) {
            cropProducts.push(DataHelper.copyObj(cropProduct));
          }
        });
        delete visit_copy.CropProducts;
        delete visit_local.CropProducts;

        visit_copy.Hibrido_de_Visita__c.forEach((hybrid, i) => {
          hybrid.Plagues.forEach((plague, y) => {
            if (!visit_local.Hibrido_de_Visita__c[i] || !DataHelper.compareObj(visit_local.Hibrido_de_Visita__c[i].Plagues[y], plague)) {
              plagues.push(DataHelper.copyObj(plague));
            }
          });

          hybrid.Diseases.forEach((disease, y) => {
            if (!visit_local.Hibrido_de_Visita__c[i] || !DataHelper.compareObj(visit_local.Hibrido_de_Visita__c[i].Diseases[y], disease)) {
              diseases.push(DataHelper.copyObj(disease));
            }
          });
          delete hybrid.Plagues;
          delete hybrid.Diseases;
          delete hybrid.AllHybrids;

          if (visit_local.Hibrido_de_Visita__c[i]) {
            delete visit_local.Hibrido_de_Visita__c[i].Plagues;
            delete visit_local.Hibrido_de_Visita__c[i].Diseases;
            delete visit_local.Hibrido_de_Visita__c[i].AllHybrids;
          }

          if (!visit_local.Hibrido_de_Visita__c[i] || !DataHelper.compareObj(visit_local.Hibrido_de_Visita__c[i], hybrid)) {
            hybrids.push(DataHelper.copyObj(hybrid));
          }
        });
        delete visit_copy.Hibrido_de_Visita__c;
        delete visit_local.Hibrido_de_Visita__c;

        if (visit_local && DataHelper.compareObj(visit_local, visit_copy)) {
          visit_copy = undefined;
        }

        this.visitDAO.saveVisit(visit_copy).then(() => {
          return this.visitHybridDAO.saveAll(hybrids);
        }).then(() => {
          return this.plagueDAO.saveAll(plagues);
        }).then(() => {
          return this.diseaseDAO.saveAll(diseases);
        }).then(() => {
          return this.weedHandlingDAO.saveAll(weedHandling);
        }).then(() => {
          return this.productUsedDAO.saveAll(productUsed);
        }).then(() => {
          return this.cropProductDAO.saveAll(cropProducts);
        }).then(() => {
  
            if (loadedVisit.Status__c !== VisitStatus.inProgress) {
              this.events.publish(NotificationKeys.totalNotifications);
            }
            resolve(loadedVisit);
    
        }).catch((err) => {
          reject(err);
        });

      });
    });
  }
  //FIM MAXXIDATA

  public updateEmails(event: EventModel, emails: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.loadVisit(event).then((visit: Visita__cModel) => {
        visit.BR_EmailContactList__c = emails;
        this.visitDAO.saveVisit(visit).then(() => {
          resolve();
        });
      });
    });
  }

  public cancel(visit: Visita__cModel): Promise<any> {
    visit.Status__c = VisitStatus.canceled;
    return this.visitDAO.saveVisit(visit);
  }

  public complete(visit: Visita__cModel): Promise<any> {
    visit.Status__c = VisitStatus.completed;
    visit.BR_GenerateReport__c = true;
    visit.Data_de_Execucao__c = DateHelper.getCurrentDateString();
    return this.visitDAO.saveVisit(visit);
  }

  public isDemandGeneration(visit: Visita__cModel): boolean {
    return visit.GerDemandaId__c && visit.GerDemandaId__c.length > 0;
  }

}
