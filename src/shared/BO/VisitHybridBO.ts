import { GenericDAO } from './../DAO/GenericDAO';
import { VisitHybridDAO } from '../DAO/VisitHybridDAO';
import { PlagueDAO } from '../DAO/PlagueDAO';
import { ServiceProvider } from '../../providers/service-salesforce';
import { Hibrido_de_Visita__cModel } from '../../app/model/Hibrido_de_Visita__cModel';
import { RecordTypeDAO } from '../DAO/RecordTypeDAO';
import { ArrayHelper } from '../Helpers/ArrayHelper';

export class VisitHybridBO {

    private hybridDAO: GenericDAO;
    private plagueDAO__g: GenericDAO;
    private diseaseDAO: GenericDAO;
    private recordTypeDAO: RecordTypeDAO;
    private visitHybridDAO: VisitHybridDAO;
    private plagueDAO: PlagueDAO;

    constructor(private serviceProvider: ServiceProvider) {
        this.hybridDAO = new GenericDAO(this.serviceProvider.getHibrido_de_Visita__cService());
        this.plagueDAO__g = new GenericDAO(this.serviceProvider.getPraga__cService());
        this.diseaseDAO = new GenericDAO(this.serviceProvider.getDoenca__cService());

        this.visitHybridDAO = new VisitHybridDAO(this.serviceProvider);
        this.plagueDAO = new PlagueDAO(this.serviceProvider);
        this.recordTypeDAO = new RecordTypeDAO(this.serviceProvider);
    }

    public create(visitHybridRecordType: string): Promise<Hibrido_de_Visita__cModel> {
        return new Promise((resolve, reject) => {
            const visitHybrid = new Hibrido_de_Visita__cModel(null);
            this.recordTypeDAO.loadRecordTypeForVisitHybrid(visitHybridRecordType).then((recordTypeId) => {
                visitHybrid.BR_MobileAppFlag__c = true;
                visitHybrid.RecordTypeId = recordTypeId;
                resolve(visitHybrid);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    public saveHybrid(hybrid: Hibrido_de_Visita__cModel): Promise<any> {
      return new Promise((resolve, reject) => {
        this.visitHybridDAO.saveHybrid(hybrid).then((savedHybrid) => {
          resolve(savedHybrid);
        }, (err) => {
          reject(err);
        });
      });
    }

    public removeHybrid(hybrid: Hibrido_de_Visita__cModel): Promise<any> {
        const promises: Array<Promise<any>> = [];
        const plagues = hybrid.Plagues;

        for (const plague of plagues) {
            promises.push(this.plagueDAO.removePlague(plague));
        }

        return Promise.all(promises).then(() => {
            return this.visitHybridDAO.removeHybrid(hybrid);
        });
    }

    public delete(visitHybrids: Hibrido_de_Visita__cModel[], hybridIndex: number): Promise<any> {
      return new Promise((resolve, reject) => {
        const visitHybrid = visitHybrids[hybridIndex];

        this.hybridDAO.deleteAll([visitHybrid]).then(() => {
          const newvisitHybrids = ArrayHelper.removeItemFromArray(visitHybrid, visitHybrids);
          resolve(newvisitHybrids);
        }).catch((err) => {
          reject(err);
        });
      });
    }

  public loadHybrid(visitId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.visitHybridDAO.loadHybrids(visitId).then((hybrids: Hibrido_de_Visita__cModel[]) => {
        resolve(hybrids);
      }, (err) => {
        reject(err);
      });
    });
  }

}
