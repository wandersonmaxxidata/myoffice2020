import { ServiceProvider } from '../../providers/service-salesforce';
import { ArrayHelper } from '../Helpers/ArrayHelper';
import { GenericDAO } from './../DAO/GenericDAO';
import { Doenca__cModel } from './../../app/model/Doenca__cModel';

export class DiseaseBO {

  private diseaseDAO: GenericDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.diseaseDAO = new GenericDAO(this.serviceProvider.getDoenca__cService());
  }

  public delete(diseases: Doenca__cModel[], diseaseIndex: number): Promise<any> {
    return new Promise((resolve, reject) => {
      const disease = diseases[diseaseIndex];

      this.diseaseDAO.deleteAll([disease]).then(() => {
        const newDiseasesObj = ArrayHelper.removeItemFromArray(disease, diseases);
        resolve(newDiseasesObj);
      }).catch((err) => {
        reject(err);
      });
    });
  }

}
