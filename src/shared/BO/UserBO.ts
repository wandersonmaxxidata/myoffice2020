import { ServiceProvider } from './../../providers/service-salesforce';
import { UserDAO } from '../DAO/UserDAO';
import { UserModel } from '../../app/model/UserModel';

export class UserBO {

  private userDAO: UserDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.userDAO = new UserDAO(this.serviceProvider);
  }

  public getUserNameById(Id: string) {
    return new Promise((resolve, reject) => {
      this.userDAO.getUserNameById(Id).then((user: UserModel) => {
        resolve(user.Name);
      }, (err) => {
        reject();
      });
    });
  }

}
