import { ServiceProvider } from '../../providers/service-salesforce';
import { ArrayHelper } from '../Helpers/ArrayHelper';
import { GenericDAO } from './../DAO/GenericDAO';
import { BR_ProductUsed__cModel } from './../../app/model/BR_ProductUsed__cModel';

export class ProductUsedBO {

  private productUsedDAO: GenericDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.productUsedDAO = new GenericDAO(this.serviceProvider.getBR_ProductUsed__cService());
  }

  public delete(productUsedArray: BR_ProductUsed__cModel[], productUsedIndex: number): Promise<any> {
    return new Promise((resolve, reject) => {
      const productUsed = productUsedArray[productUsedIndex];

      this.productUsedDAO.deleteAll([productUsed]).then(() => {
        const newProductUsedArray = ArrayHelper.removeItemFromArray(productUsed, productUsedArray);
        resolve(newProductUsedArray);
      }).catch((err) => {
        reject(err);
      });
    });
  }

}
