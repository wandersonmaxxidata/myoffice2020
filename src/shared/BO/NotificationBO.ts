import { EventDAO } from './../DAO/EventDAO';
import { NotificationDAO } from '../DAO/NotificationDAO';
import { ServiceProvider } from './../../providers/service-salesforce';
import { ServiceGroupingHelper } from '../Helpers/ServiceGroupingHelper';
import { ArrayHelper } from '../Helpers/ArrayHelper';

export class NotificationBO {

  private eventDAO: EventDAO;
  private notificationDAO: NotificationDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.eventDAO = new EventDAO(this.serviceProvider);
    this.notificationDAO = new NotificationDAO(this.serviceProvider);
  }

  public load(user_id) {
    return new Promise((resolve, reject) => {
      this.notificationDAO.getEventsToNotifications(user_id).then((data: any) => {
        console.log('------| Success:notification:Data returned: ', data);
        resolve(data);
      }, (err) => {
        console.log('------| Error:notification:Data returned: ', err);
        reject(err);
      });
    });
  }

  public totalNotifications(user_id) {
    return new Promise((resolve, reject) => {
      this.notificationDAO.getTotalEventsToNotifications(user_id).then((total: any) => {
        console.log('------| Success: Notification.totalNotifications:total: ', total);
        resolve(total);
      }, (err) => {
        console.log('------| Error: Notification.totalNotifications:err: ', err);
        reject(err);
      });
    });
  }

  public getCountSyncBadge() {
    return new Promise((resolve, reject) => {

      const promises: Array<Promise<any>> = [];
      const objCount = {};
      let entitiesFromTS = [];
      let total = 0;

      ServiceGroupingHelper.loadEntitiesFromTS().forEach((item) => {
        entitiesFromTS.push(item.Name);
      });

      entitiesFromTS = ArrayHelper.filterUniqueValues(entitiesFromTS);

      entitiesFromTS.forEach((item) => {
        const promise = this.notificationDAO.getCountSyncBadge(item).then((count) => {
          objCount[item] = count;
          total += count;
        });
        promises.push(promise);
      });

      Promise.all(promises).then(() => {
        console.log('------| CountSyncBadge:objCount: ', objCount);
        resolve(total);
      });
    });
  }

  //MAXXIDATA
  public getCountSyncVisitsSnycy() {
    return new Promise((resolve, reject) => {
        const promise = this.notificationDAO.getCountSyncVisitsSnycy('Visita__c').then((count) => {
          resolve(count);
        });
    });
  }
  //FIM MAXXIDATA

}
