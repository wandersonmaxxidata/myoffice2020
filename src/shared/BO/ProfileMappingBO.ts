import { ProfileMappingDAO } from '../DAO/ProfileMappingDAO';
import { ServiceProvider } from './../../providers/service-salesforce';

export class ProfileMappingBO {

  public profileMappingDAO: ProfileMappingDAO;

  constructor(public serviceProvider: ServiceProvider) {
    this.profileMappingDAO = new ProfileMappingDAO(this.serviceProvider);
  }

  public RADL(clientID: string) {
    return new Promise((resolve) => {
      this.profileMappingDAO.getRADL(clientID).then((result) => {
        console.log('------| Success:RADL: ', result);
        resolve(result);
      });
    });
  }

  public recordType(recordTypeId: string) {
    return new Promise((resolve) => {
      this.profileMappingDAO.getRecordType(recordTypeId).then((result) => {
        console.log('------| Success:recordType: ', result[0]);
        resolve(result[0]);
      }).catch((err) => {
        resolve(undefined);
      });
    });
  }

  public brandList(profileMapping) {
    return new Promise((resolve) => {
      const promises: Array<Promise<any>> = [];
      const list = [];

      profileMapping.forEach((item) => {
        const promise = this.profileMappingDAO.getBrandList(item.OwnerId).then((value: string[]) => {
          if (value && value.length > 0) {
            list.push(value[0]);
          }
        });
        promises.push(promise);
      });

      Promise.all(promises).then(() => {
        resolve(list);
      });
    });
  }

}
