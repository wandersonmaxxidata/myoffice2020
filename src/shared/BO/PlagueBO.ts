import { ServiceProvider } from '../../providers/service-salesforce';
import { ArrayHelper } from '../Helpers/ArrayHelper';
import { GenericDAO } from './../DAO/GenericDAO';
import { Praga__cModel } from '../../app/model/Praga__cModel';

export class PlagueBO {

  private plagueDAO: GenericDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.plagueDAO = new GenericDAO(this.serviceProvider.getPraga__cService());
  }

  public create(): Promise<Praga__cModel>{
    return new Promise((resolve, reject) => {
        const plague = new Praga__cModel(null)

        resolve(plague)
    })
  }

  public delete(plagues: Praga__cModel[], plagueIndex: number): Promise<any> {
    return new Promise((resolve, reject) => {
      const plague = plagues[plagueIndex];

      this.plagueDAO.deleteAll([plague]).then(() => {
        const newPlagues = ArrayHelper.removeItemFromArray(plague, plagues);
        resolve(newPlagues);
      }).catch((error) => {
        reject(error);
      });
    });
  }

}
