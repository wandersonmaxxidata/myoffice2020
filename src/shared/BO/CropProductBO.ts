import { ServiceProvider } from './../../providers/service-salesforce';
import { ArrayHelper } from '../Helpers/ArrayHelper';
import { GenericDAO } from './../DAO/GenericDAO';
import { BR_CropProducts__cModel } from './../../app/model/BR_CropProducts__cModel';

export class CropProductBO {

  private cropProductDAO: GenericDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.cropProductDAO = new GenericDAO(this.serviceProvider.getBR_CropProducts__cService());
  }

  public delete(cropProductArray: BR_CropProducts__cModel[], cropProductIndex: number): Promise<any> {
    return new Promise((resolve, reject) => {
      const cropProduct = cropProductArray[cropProductIndex];

      this.cropProductDAO.deleteAll([cropProduct]).then(() => {
        const newCropProductUsedArray = ArrayHelper.removeItemFromArray(cropProduct, cropProductArray);
        resolve(newCropProductUsedArray);
      }).catch((err) => {
        reject(err);
      });
    });
  }
}
