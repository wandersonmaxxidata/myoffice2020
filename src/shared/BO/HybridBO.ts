import { HybridDAO } from './../DAO/HybridDAO';
import { AppPreferences } from '@ionic-native/app-preferences';
import { ServiceProvider } from './../../providers/service-salesforce';

export class HybridBO {

  private appPreferences: AppPreferences;
  private hybridDAO: HybridDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.hybridDAO = new HybridDAO(this.serviceProvider);
    this.appPreferences = new AppPreferences();
  }

  public hybridList(pageSize: number, division: string, filter?: string) {
    return new Promise ((resolve, reject) => {
      const searchFilter = filter ? filter : '';

        this.hybridDAO.getHybridList(division, pageSize, searchFilter).then((list) => {
          console.log('------| Success:getHybridList: ', list);
          resolve(list);
        }, (err) => {
          console.log('------| Error:getHybridList: ', err);
          resolve ([]);
        });
      });
  }

  public recordTypeId(recordType: string) {
    return new Promise((resolve, reject) => {
      this.hybridDAO.getRecordTypeId(recordType).then((recordTypeId: any) => {
        console.log('------| Success:getRecordType: ', recordTypeId);
        resolve(recordTypeId.currentPageOrderedEntries[0][0]);
      }, (err) => {
        console.log('------| Error:getRecordType: ', err);
        resolve(null);
      });
    });
  }

}
