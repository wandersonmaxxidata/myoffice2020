import { ServiceProvider } from '../../providers/service-salesforce';
import { ArrayHelper } from '../Helpers/ArrayHelper';
import { GenericDAO } from './../DAO/GenericDAO';
import { BR_WeedHandling__cModel } from './../../app/model/BR_WeedHandling__cModel';

export class WeedHandlingBO {

  private weedHandlingDAO: GenericDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.weedHandlingDAO = new GenericDAO(this.serviceProvider.getBR_WeedHandling__cService());
  }

  public delete(weedHandlings: BR_WeedHandling__cModel[], weedHandlingIndex: number): Promise<BR_WeedHandling__cModel[]> {
    return new Promise((resolve, reject) => {
      const weedHandling = weedHandlings[weedHandlingIndex];

      this.weedHandlingDAO.deleteAll([weedHandling]).then(() => {
        const newWeedHandlings = ArrayHelper.removeItemFromArray(weedHandling, weedHandlings);
        resolve(newWeedHandlings);
      }).catch((err) => {
        reject(err);
      });
    });
  }

}
