import { AppPreferences } from '@ionic-native/app-preferences';
import { ClientDAO } from '../DAO/ClientDAO';
import { ContactDAO } from './../DAO/ContactDAO';
import { EventDAO } from './../DAO/EventDAO';
import { EventModel } from '../../app/model/EventModel';
import { EventVisitType, RecordTypeDeveloperName } from '../Constants/Types';
import { LocalStorageUser } from './../../shared/Constants/Keys';
import { RecordTypeDAO } from './../DAO/RecordTypeDAO';
import { SalesCoachingDAO } from '../DAO/SalesCoachingDAO';
import { ServiceProvider } from '../../providers/service-salesforce';
import { VisitDAO } from '../DAO/VisitDAO';

export const EventStatus = {
  open: 'A Iniciar',
  inProgress: 'Em Andamento',
  canceled: 'Cancelado',
  executed: 'Executado',
};

export class EventBO {

  private eventDAO: EventDAO;
  private contactDAO: ContactDAO;
  private visitDAO: VisitDAO;
  private salesCoachingDAO: SalesCoachingDAO;
  private clientDAO: ClientDAO;
  private recordTypeDAO: RecordTypeDAO;
  private appPreferences: AppPreferences;

  constructor(private serviceProvider: ServiceProvider) {
    this.eventDAO = new EventDAO(this.serviceProvider);
    this.contactDAO = new ContactDAO(this.serviceProvider);
    this.visitDAO = new VisitDAO(this.serviceProvider);
    this.salesCoachingDAO = new SalesCoachingDAO(this.serviceProvider);
    this.clientDAO = new ClientDAO(this.serviceProvider);
    this.recordTypeDAO = new RecordTypeDAO(this.serviceProvider);
    this.appPreferences = new AppPreferences();
  }

  public create(event: EventModel, eventRecordType: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.saveEventToSalesforceGambi(event).then((newEvent: any) => {
        this.recordTypeDAO.loadRecordTypeForEvent(eventRecordType).then((recordTypeId) => {

          newEvent.RecordTypeId = recordTypeId;
          newEvent.Status__c = EventStatus.open;
          newEvent.IsOutlook__c = false;

          this.eventDAO.create(newEvent).then((data__e) => {
            console.log('------| Success:Event:created: ', data__e);
            resolve(data__e);
          }, (err) => {
            reject(err);
          });
        });
      });
    });
  }

  public update(event: EventModel): Promise<any> {
    return new Promise((resolve, reject) => {
      this.saveEventToSalesforceGambi(event).then((newEvent: any) => {
        this.eventDAO.update(newEvent).then((data__e) => {
          console.log('------| Success:Event:update: ', data__e);
          resolve(data__e);
        }, (err) => {
          console.log('-*-| Error:Event:update: ', err);
        });
      });
    });
  }

  public cancel(event: EventModel, motivation: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (motivation) {
        this.salesCoachingDAO.deleteByEventId(event.WhatId, motivation).then(() => {
          event.Status__c = EventStatus.canceled;
          return this.eventDAO.update(event);
        }).then(() => {
          resolve();
        }).catch((error) => {
          reject(error);
        });

      } else {
        this.visitDAO.deleteByEventId(event.getId()).then(() => {
          event.Status__c = EventStatus.canceled;
          return this.eventDAO.update(event);
        }).then(() => {
          resolve();
        }).catch((error) => {
          reject(error);
        });
      }
    });
  }

  public execute(event: EventModel): Promise<any> {
    return new Promise((resolve, reject) => {
      event.Status__c = EventStatus.executed;
      this.update(event).then(() => {
        resolve();
      }, (err) => {
        console.log('------| Error report event save: ', err);
      });
    });
  }

  public exists(eventId: string): Promise<boolean> {
    return this.eventDAO.findById(eventId);
  }

  public contactList(event: EventModel) {
    return new Promise((resolve) => {
      this.appPreferences.fetch('user_brand').then((brand) => {
        this.contactDAO.getContactList(event.WhatId, brand).then((contactList) => {
          resolve(contactList);
        });
      });
    });
  }

  public farmList(event: EventModel) {
    return new Promise((resolve) => {
      this.clientDAO.getFarmList(event.WhatId).then((farmList) => {
        resolve(farmList);
      });
    });
  }

  public listVisitType(BR_Division__c) {
    return new Promise((resolve, reject) => {
      if (BR_Division__c) {
        const visitTypeList = EventVisitType[BR_Division__c];
        if (visitTypeList) {
          resolve(visitTypeList);
        }
      } else {
        reject();
      }
    });
  }

  public cropVisitChange(model) {
    return new Promise((resolve) => {
      this.culture(model.BR_Division__c).then((BR_Division__c: string) => {
        model.BR_Division__c = BR_Division__c;
        if (model.BR_Tipo_de_Visita__c) {
          model.BR_Tipo_de_Visita__c = model.BR_Tipo_de_Visita__c.replace('Quimico', 'Crop');
        }
        resolve(model);
      });
    });
  }

  public culture(value?: string) {
    return new Promise((resolve) => {
      let userCultureString = value ? value : localStorage.getItem(LocalStorageUser.userCultureKey);
      if (userCultureString && userCultureString.includes('Quimico')) {
        userCultureString = userCultureString.replace('Quimico', 'Crop');
      }
      resolve(userCultureString);
    });
  }

  public loadEventToSalesforceGambi(event: EventModel) {
    return new Promise((resolve) => {
      const newEvent = JSON.parse(JSON.stringify(event));
      if (newEvent.ClienteId__c) {
        newEvent.Fazenda__c = newEvent.WhatId;
        newEvent.WhatId = newEvent.ClienteId__c;
        newEvent.ClienteId__c = undefined;
      }

      const status = newEvent.Status__c
        ? newEvent.Status__c.substring(0, newEvent.Status__c.length - 1)
        : EventStatus.open;

      if (EventStatus.executed.includes(status)) {
        newEvent.Status__c = EventStatus.executed;
      } else if (EventStatus.canceled.includes(status)) {
        newEvent.Status__c = EventStatus.canceled;
      }

      resolve(newEvent);
    });
  }

  public saveEventToSalesforceGambi(event: EventModel) {
    return new Promise((resolve) => {
      const newEvent = JSON.parse(JSON.stringify(event));
      if (newEvent.Fazenda__c) {
        newEvent.ClienteId__c = newEvent.WhatId;
        newEvent.WhatId = newEvent.Fazenda__c;
        newEvent.Fazenda__c = undefined;
      }
      resolve(newEvent);
    });
  }

  public getRecordTypeToClients() {
    return new Promise((resolve) => {
      const Ids = [];
      this.recordTypeDAO.loadRecordTypeForClient('Agricultores').then((rtId_farmer) => {
        Ids.push(`"${rtId_farmer}"`);
        this.recordTypeDAO.loadRecordTypeForClient('Parceiros').then((rtId_channel) => {
          Ids.push(`"${rtId_channel}"`);
          resolve(Ids);
        });
      });
    });
  }

  public getRecordTypeName(recordTypeId: string) {
    return new Promise((resolve) => {
      if (recordTypeId) {
        this.recordTypeDAO.loadRecordTypeId(recordTypeId).then((developerName) => {
          resolve(developerName);
        });
      } else {
        resolve();
      }
    });
  }

  public getTipo_Compromisso__c(recordTypeId: string) {
    return new Promise((resolve) => {
      let result = '';
      if (recordTypeId) {
        this.recordTypeDAO.loadRecordTypeId(recordTypeId).then((developerName) => {
          result = (developerName === RecordTypeDeveloperName.channel) ? 'Canal' : 'Cliente';
          resolve(result);
        });
      } else {
        resolve(result);
      }
    });
  }

  public willOpenReport(event, user_profile) {
    if (user_profile && (user_profile.isRC || user_profile.isRTV)) {
      return (
        //  event.Geracao_de_Demanda__c
        //  && event.Tipo_de_Geracao_de_Demanda__c
        event.BR_Tipo_de_Visita__c === 'Desenvolvimento Vegetativo'
        || event.BR_Tipo_de_Visita__c === 'Plantio'
        || event.BR_Tipo_de_Visita__c === 'Germinação e Emergência'
        || event.BR_Tipo_de_Visita__c === 'Desenvolvimento Vegetativo'
        || event.BR_Tipo_de_Visita__c === 'Desenvolvimento Reprodutivo'
        || event.BR_Tipo_de_Visita__c === 'Colheita'
        || event.BR_Tipo_de_Visita__c === 'Pré-Plantio'
        && event.BR_Division__c === 'Milho'
        && event.Tipo_Compromisso__c !== 'Canal'
      );
    } else {
      return true;
    }
  }

  public getSafra() {
    return new Promise((resolve) => {
      this.serviceProvider.getSafraService().queryAllFromSoup('Name', 'ascending', 100, (response) => {
        resolve(response.currentPageOrderedEntries);
      }, (err) => {
        console.log(err);
        resolve([]);
      });
    });
  }

  public getFarmById(farmId: string) {
    return new Promise((resolve, reject) => {
      this.clientDAO.getFarmById(farmId).then((farm) => {
        resolve(farm);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  public getFarmerWithParentId(parentId: string) {
    return new Promise((resolve, reject) => {
      this.clientDAO.getFarmerByParentId(parentId).then((farmer) => {
        resolve(farmer[0] ? farmer[0] : []);
      }).catch((err) => {
        reject();
      });
    });
  }

  public searchEvent(filter: string, filterByOwnerId?: string) {
    return new Promise((resolve, reject) => {
      this.eventDAO.searchEvent(filter, filterByOwnerId).then((result) => {
        console.log('------| Success:EventBO:searchEvent: ', result);
        resolve(result);
      }).catch((err) => {
        console.log('------| Error:EventBO:searchEvent: ', err);
        reject(err);
      });
    });
  }

  public eventDetail(event) {
    return new Promise((resolve, reject) => {
      this.eventDAO.getEventDetail(event).then((data: any) => {
        console.log('------| Success:notification:EventDetail returned: ', data);
        resolve(data);
      }, (err) => {
        console.log('------| Erro:notification:EventDetail returned: ', err);
        reject(err);
      });
    });
  }

}
