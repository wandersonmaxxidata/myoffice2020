import { ClientDAO } from '../DAO/ClientDAO';
import { ClientsModel } from '../../app/model/ClientsModel';
import { GenericDAO } from '../DAO/GenericDAO';
import { SaveHelper } from '../Helpers/SaveHelper';
import { ServiceProvider } from './../../providers/service-salesforce';

export class ClientBO {

  private clientDAO: ClientDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.clientDAO = new ClientDAO(this.serviceProvider);
  }

  public farmerByParentId(parentID: string) {
    return new Promise((resolve, reject) => {
      this.clientDAO.getFarmerByParentId(parentID).then((result: any) => {
        resolve(result[0]);
      }, (err) => {
        resolve(undefined);
      });
    });
  }

  public getFamilyGroupNameById(id: string) {
    return new Promise((resolve, reject) => {
      this.clientDAO.getFamilyGroupNameById(id).then((result: any) => {
        resolve(result[0]);
      }, (err) => {
        resolve(undefined);
      });
    });
  }

  public saveClientGeolocation(client: ClientsModel) {
    return new Promise((resolve, reject) => {

      SaveHelper.update_Local_(client);
      const genericDAO = new GenericDAO(this.serviceProvider.getClientsService());

      genericDAO.saveAll([client]).then((result: any) => {
        resolve(result[0]);
      }, (err) => {
        resolve(undefined);
      });
    });
  }

  public getClientById(model: ClientsModel) {
    return new Promise((resolve, reject) => {
      this.clientDAO.getClientById(model).then((result: any) => {
        const currentPageOrderedEntries = result.currentPageOrderedEntries;
        if (currentPageOrderedEntries) {
          const client = new ClientsModel(currentPageOrderedEntries[0]);
          client.setRecordType(this.serviceProvider.getRecordTypeService()).then(() => {
            return client.setBR_ProfileMapping__c(this.serviceProvider.getBR_ProfileMapping__cService());
          }).then(() => {
            return client.setMunicipio(this.serviceProvider.getMunicipioService());
          }).then(() => {
            return client.setContact(this.serviceProvider.getContactService());
          }).then(() => {
            resolve(client);
          });
        } else {
          resolve(model);
        }
      }, (err) => {
        reject(err);
      });
    });
  }

  public getClientsList(filter: any, developerName: string) {
    return new Promise((resolve, reject) => {
      this.clientDAO.getClientsList(developerName, filter.term, filter.whereCondition).then((clients) => {
        console.log('------| Success:getClientsList: ', clients);
        resolve(clients);
      }, (err) => {
        console.log('------| Error:getClientsList: ', err);
        reject(err);
      });
    });
  }

  public returnOnebyOneOptions() {
    return new Promise<any>((resolve, reject) => {
      this.clientDAO.returnOnebyOneOptions().then((clients) => {
        console.log('------| Success:getOnebyOneList: ', clients);
        resolve(clients);
      }, (err) => {
        console.log('------| Error:getOnebyOneList: ', err);
        reject(err);
      });
    });
  }

  public getClientsListRadla(filter: any, developerName: string, radl) {
    return new Promise((resolve, reject) => {
      this.clientDAO.getClientsListRadla(radl,developerName, filter.term, filter.whereCondition).then((clients) => {
        console.log('------| Success:getClientsList: ', clients);
        resolve(clients);
      }, (err) => {
        console.log('------| Error:getClientsList: ', err);
        reject(err);
      });
    });
  }

}
