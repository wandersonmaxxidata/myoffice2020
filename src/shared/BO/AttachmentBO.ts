import { AttachmentDAO } from '../DAO/AttachmentDAO';
import { AttachmentModel } from './../../app/model/AttachmentModel';
import { GenericDAO } from '../DAO/GenericDAO';
import { ServiceProvider } from './../../providers/service-salesforce';

export class AttachmentBO {

  private attachmentDAO: AttachmentDAO;
  private genericDAO: GenericDAO;

  constructor(private serviceProvider: ServiceProvider) {
    this.attachmentDAO = new AttachmentDAO(this.serviceProvider);
    this.genericDAO = new GenericDAO(this.serviceProvider.getAttachmentService());
  }

  public save(attachment: AttachmentModel): Promise<AttachmentModel> {
    return this.attachmentDAO.save(attachment);
  }

  public saveAll(attachments: AttachmentModel[]): Promise<AttachmentModel[]> {
    return this.attachmentDAO.saveAll(attachments);
  }

  public load(visitId: string): Promise<AttachmentModel[]> {
    return this.attachmentDAO.load(visitId);
  }

  public loadNotUploaded(visitId: string): Promise<AttachmentModel[]> {
    return this.attachmentDAO.loadNotUploaded(visitId);
  }

  public loadAll(): Promise<AttachmentModel[]> {
    return this.attachmentDAO.loadAll();
  }

  public loadAllNotUploaded(): Promise<AttachmentModel[]> {
    return this.attachmentDAO.loadAllNotUploaded();
  }

  public loadAllNotUpdated(): Promise<AttachmentModel[]> {
    return this.attachmentDAO.loadAllNotUpdated();
  }

  public remove(attachment: AttachmentModel): Promise<any> {
    return new Promise((resolve, reject) => {
      this.genericDAO.deleteAll([attachment]).then(() => {
        resolve();
      }).catch((err) => {
        reject(err);
      });
    });
  }

}
