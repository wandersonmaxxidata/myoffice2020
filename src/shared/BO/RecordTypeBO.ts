import { RecordTypeDAO } from '../DAO/RecordTypeDAO';
import { ServiceProvider } from '../../providers/service-salesforce';

export class RecordTypeBO {

    private recordTypeDAO: RecordTypeDAO;

    constructor(private serviceProvider: ServiceProvider) {
        this.recordTypeDAO = new RecordTypeDAO(this.serviceProvider);
    }

    public loadRecordTypeForVisit(developerName: string): Promise<string> {
        console.log('---| loadRecordTypeForVisit:developerName on Start: ', developerName);
        const sobjectType = 'Visita__c';
        return this.recordTypeDAO.loadRecordType(developerName, sobjectType);
    }

    public loadRecordTypeForVisitHybrid(developerName: string): Promise<string> {
        console.log('---| loadRecordTypeForVisit:developerName on Start: ', developerName);
        const sobjectType = 'Hibrido_de_Visita__c';
        return this.recordTypeDAO.loadRecordType(developerName, sobjectType);
    }

    public loadRecordTypeForProfileMapping(developerName: string): Promise<string> {
        console.log('---| loadRecordTypeForVisit:developerName on Start: ', developerName);
        const sobjectType = 'BR_ProfileMapping__c';
        return this.recordTypeDAO.loadRecordType(developerName, sobjectType);
    }

}
