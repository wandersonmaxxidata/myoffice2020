import { Loading, LoadingController } from 'ionic-angular';

export class LoadingManager {

  private loading: Loading;
  private loadingController: LoadingController;
  private static instance: LoadingManager = null;

  private constructor(private loadingCtrl: LoadingController) {
    this.loadingController = loadingCtrl;
  }

  public static createInstance(loadingController: LoadingController) {
    if (this.instance === null) {
      this.instance = new LoadingManager(loadingController);
    }
    return this.instance;
  }

  public static getInstance() {
    return this.instance;
  }

  private createLoadingInstance() {
    this.loading = this.loadingController.create({ content: '', duration: 6000 });
  }

  public show(duration?: number): Promise<any> {
    return new Promise((resolve) => {
      if (!this.loading || this.loading.instance === null) {
        this.createLoadingInstance();
        // console.log('---| LoadingManager.showLoading:this.loading: ', this.loading);
        if (duration) { this.loading.setDuration(duration); }
        resolve(this.loading.present());
      }
      resolve();
    });
  }

  public hide(): Promise<any> {
    return new Promise((resolve) => {
      if (this.loading) {
        // console.log('---| LoadingManager.hideLoading:this.loading: ', this.loading);
        resolve(this.loading.dismiss());
      }
      resolve();
    });
  }
}

export interface ILoadingManagerParam {
  loading: Loading;
  loadingController: LoadingController;
}
