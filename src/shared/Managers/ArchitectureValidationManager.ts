import { Platform } from 'ionic-angular';
import { AlertManager } from './AlertManager';

export class ArchitectureValidationManager {

    private platform: Platform;
    private static instance: ArchitectureValidationManager = null;

    private constructor(private platformParam: Platform) {
        this.platform = platformParam;
    }

    public static createInstance(platformParam: Platform) {
        if (this.instance === null) {
            this.instance = new ArchitectureValidationManager(platformParam);
        }
        return this.instance;
    }

    public static getInstance() {
        return this.instance;
    }

    public precondition(unsatisfiedCondition: boolean) {
        if (unsatisfiedCondition) {
            console.log('|----------------------------------------------------------------------------------------------|');
            console.log('---| ArchitectureValidationManager:precondition:unsatisfiedCondition: ', unsatisfiedCondition);
            console.log('|----------------------------------------------------------------------------------------------|');

            AlertManager.getInstance().showNotInComplianceWithArchitecture();
            // this.platform.exitApp();
        }
    }

}
