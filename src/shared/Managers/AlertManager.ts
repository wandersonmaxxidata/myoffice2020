import { AlertHelper } from '../Helpers/AlertHelper';
import { AlertController } from 'ionic-angular';

export class AlertManager {

    private alertController: AlertController;
    private static instance: AlertManager = null;

    private constructor(private alertControllerParam: AlertController) {
        this.alertController = alertControllerParam;
    }

    public static createInstance(alertControllerParam: AlertController) {
        if (this.instance === null) {
            this.instance = new AlertManager(alertControllerParam);
        }
        return this.instance;
    }

    public static getInstance() {
        return this.instance;
    }

    public showNotInComplianceWithArchitecture() {
        AlertHelper.showNotInComplianceWithArchitecture(this.alertController);
    }

}
