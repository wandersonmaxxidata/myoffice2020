import { Visita__cService } from '../providers/visita__c/visita__c-service-salesforce';

export abstract class SmartSQLTests {

  public static testVisitsEventID(service: Visita__cService) {

    const sql = 'SELECT {Visita__c:ExternalID__c} FROM {Visita__c} WHERE {Visita__c:EventId__c} = \'00UW0000002Te98MAC\'';

    service.querySmartFromSoup(sql, 20, (data) => {
      console.log('---| testVisitsEventID:data: ', data);
    }, (error) => {
      console.log('-*-| testVisitsEventID:error: ', error);
    });

  }

}
