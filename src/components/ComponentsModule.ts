import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import {IonicPageModule} from 'ionic-angular';

@NgModule({
  imports: [
    IonicModule,
    IonicPageModule
  ],
  declarations: [
  ],
  entryComponents: [
  ],
  exports: [
  ],
})
export class ComponentsModule { }
