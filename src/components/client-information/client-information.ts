import { AppPreferences } from '@ionic-native/app-preferences';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { ClientsDetailPage } from './../../pages/clients-detail/clients-detail';
import { Component, Input, ViewChild } from '@angular/core';
import { ClientsModel } from '../../app/model/ClientsModel';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ServiceProvider } from '../../providers/service-salesforce';
import { Events } from 'ionic-angular';
import { ClientResponseDAO } from '../../shared/DAO/RepositoryDAO';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { ClientDAO } from '../../shared/DAO/ClientDAO';
import { RecordTypeDeveloperName } from '../../shared/Constants/Types';
import { IGeolocation } from '../geolocation/IGeolocation';
import { GeolocationComponent } from '../geolocation/geolocation';
import * as moment from 'moment';

@Component({
  selector: 'client-information',
  templateUrl: 'client-information.html',
  providers: [CallNumber, EmailComposer],
})
export class ClientInformationComponent {

  /**
   * Objeto com uma determinada conta, trazida via parametro
   */
  public client: ClientsModel;
  public showPersonalData = true;
  public showContactInformation = true;
  public showFarms: boolean;
  public showGrower: boolean;
  public showAddress = true;
  public isGrower = false;
  public isFarm = false;
  public isChannel = false;
  public clientType;
  public typeName;
  public titleHeader: string;
  public clientsParent: ClientsModel[] = [];

  public geolocation: IGeolocation;

  @Input()
  private isFromAssociated = false;
  @Input()
  private associatedClientIdToReturn = '';

  @ViewChild(GeolocationComponent)
  private geolocationComponent: GeolocationComponent;

  private isReadOnly = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private callNumber: CallNumber,
    private emailComposer: EmailComposer,
    private service: ServiceProvider,
    private events: Events,
    public appPreferences: AppPreferences,
  ) {
    const data = this.navParams.data;
    console.log('paramData ---ClientInformationComponent', data);

    if (data.RecordType !== null && data.RecordType !== undefined && data.RecordType !== 'undefined') {
      this.clientType = data.RecordType.DeveloperName;
    }
    if (data.Clients !== null && data.Clients !== undefined && data.Clients !== 'undefine') {
      this.clientsParent = data.Clients;
    }

    if (data.clientTypeName !== null && data.clientTypeName !== undefined && data.clientTypeName !== 'undefined') {
      this.typeName = data.clientTypeName;
      this.clientType = this.typeName;
    }

    if (this.clientType === 'Agricultores') {
      this.showFarms = true;
      this.showGrower = false;
      this.titleHeader = 'Dados pessoais';
    } else if (this.clientType === 'GC_ACC_Farm') {
      this.showGrower = true;
      this.showFarms = false;
      this.titleHeader = 'Dados da fazenda';
    } else if (this.clientType === 'Parceiros') {
      this.titleHeader = 'Dados do Canal';
    }

    const celPhone = data.Phone;
    if (celPhone !== null && celPhone !== undefined) {
      const replacement = celPhone.replace(/\(?([0-9]{2})\)?([0-9]{4})([0-9]{4})/, '+55($1)$2-$3');
      data.Phone = replacement;
    }

    this.client = (data.clientTypeName !== null && data.clientTypeName !== undefined && data.clientTypeName !== 'undefine')
      ? new ClientsModel(data.item)
      : new ClientsModel(data);

    this.setLocation();
    this.checkEditPermission();
  }

  public ngOnInit(): void {
    this.extractData(this.client);

    if (this.client.RecordType.DeveloperName) {
      console.log('---| this.recordType: ', this.client.RecordType.DeveloperName);

      const recordType = this.client.RecordType.DeveloperName;

      if (recordType === RecordTypeDeveloperName.farmer) {
        this.isGrower = true;
        this.isFarm = false;
        this.isChannel = false;
      } else if (recordType === RecordTypeDeveloperName.farm) {
        this.isGrower = false;
        this.isFarm = true;
        this.isChannel = false;
      } else if (recordType === RecordTypeDeveloperName.channel) {
        this.isGrower = false;
        this.isFarm = false;
        this.isChannel = true;
      }
    } else if (this.typeName !== null && this.typeName !== undefined && this.typeName !== 'undefine') {

      const recordType = this.typeName;

      if (recordType === RecordTypeDeveloperName.farmer) {
        this.isGrower = true;
        this.isFarm = false;
        this.isChannel = false;
      } else if (recordType === RecordTypeDeveloperName.farm) {
        this.isGrower = false;
        this.isFarm = true;
        this.isChannel = false;
      } else if (recordType === RecordTypeDeveloperName.channel) {
        this.isGrower = false;
        this.isFarm = false;
        this.isChannel = true;
      }
    }

    LoadingManager.getInstance().show().then(() => {
      this.getAssociatedClients(this.client).then(() => {
        this.maskInArray(this.clientsParent);
        LoadingManager.getInstance().hide();
      }).catch((error) => {
        console.log('-*-| getAssociatedClients:error: ', error);
        LoadingManager.getInstance().hide();
      });
    });

  }

  public phoneNumber(phone) {
    this.callNumber.callNumber(phone, true)
      .then(() => console.log('Launched dialer'))
      .catch((err) => console.log('Error launching dialer', err));
  }

  public mailTo(userEmail) {
    this.emailComposer.open({ app: 'mailto', to: userEmail })
      .then(() => console.log('Launched email'))
      .catch((err) => console.log('Error launching email', err));
  }

  public openClientDetail(client) {

    console.log('---| associatedClientIdToReturn: ', this.associatedClientIdToReturn, ' client.Id: ', client.Id);
    if (this.associatedClientIdToReturn === client.Id) {
      this.navCtrl.pop();
    } else {
      const clientDAO = new ClientDAO(this.service);
      clientDAO.syncMountClients(client, (error, clientModel) => {

        if (error) {
          console.log('---| clientDAO.syncMountClients:', error);
          return;
        }

        clientModel.isFromAssociated = true;
        clientModel.associatedClientIdToReturn = this.client.Id;
        console.log('---| ClientInformationComponent:Client: ', clientModel);
        this.navCtrl.push(ClientsDetailPage, clientModel);
      });
    }

  }

  private getAssociatedClients(client: ClientsModel) {
    return new Promise((resolve, reject) => {

      let key = 'ParentId';
      let value = client.Id;

      if (this.isFarm) {
        key = 'Id';
        value = client.ParentId;
      }

      console.log('---| getAssociatedClients:key: ', key, ' value: ', value);

      this.service.getClientsService().queryExactFromSoup(key, value, 100, 'ascending', 'Name', (data: ClientResponseDAO) => {

        console.log('---| getAssociatedClients: ', data);
        if (this.isGrower || this.isFarm) {
          this.clientsParent = data.currentPageOrderedEntries;
        }
        resolve();
      }, (error) => {
        reject(error);
      });
    });
  }

  private mascaraCpf(valor) {
    return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '\$1.\$2.\$3\-\$4');
  }

  private mascaraCnpj(valor) {
    return valor.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '\$1.\$2.\$3\/\$4\-\$5');
  }

  private extractData(client: ClientsModel) {
    if (client.GC_VATIN__c !== null && client.GC_VATIN__c !== undefined && client.GC_VATIN__c.length <= 11) {
      client.GC_VATIN__c = this.mascaraCpf(client.GC_VATIN__c);
    } else if (client.GC_VATIN__c !== null && client.GC_VATIN__c !== undefined) {
      client.GC_VATIN__c = this.mascaraCnpj(client.GC_VATIN__c);
    }
  }

  private maskInArray(clientsParent: ClientsModel[]) {
    if (clientsParent.length > 0) {

      clientsParent.forEach((client) => {
        this.extractData(client);
      });

    }
  }

  private setLocation() {
    this.geolocation = {
      altitude: 0,
      latitude: this.client.GC_Latitude__c,
      longitude: this.client.GC_Longitude__c,
    };

    if (this.geolocationComponent) {
      this.geolocationComponent.setLocation(this.geolocation);
    }
  }

  private updateGeolocation(location) {
    if (location) {
      this.client.GC_Latitude__c = location.latitude;
      this.client.GC_Longitude__c = location.longitude;
    }
  }

  private checkEditPermission() {
    let userId: string;
    this.appPreferences.fetch('user_id').then((id) => {
      userId = id;
    });
    this.isReadOnly = !(
      this.client.OwnerId === userId && (
        this.client.BR_CreditExpirationDate__c &&
        !this.client.BR_RenewForm__c &&
        this.client.GC_SAP_ID__c && (
          moment(this.client.BR_CreditExpirationDate__c) > moment().startOf('day').add(60, 'days') &&
          moment(this.client.BR_CreditExpirationDate__c) < moment().startOf('day').add(360, 'days')
        )
      )
    );
  }
}
