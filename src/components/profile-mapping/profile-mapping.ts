import { ProfileMappingBO } from './../../shared/BO/ProfileMappingBO';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { LocalStorageUser } from './../../shared/Constants/Keys';
import { IPicklistItem } from './../../interfaces/IPicklistItem';
import { MultipicklistModal } from '../modals/modal-multipicklist/modal-multipicklist';
import { AppPreferences } from '@ionic-native/app-preferences';
import { ToastController } from 'ionic-angular';
import { Component, ElementRef, Input } from '@angular/core';
import { AlertController, Events, ModalController, ModalOptions, NavController, NavParams } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { ServiceProvider } from '../../providers/service-salesforce';
import { ClientsModel } from '../../app/model/ClientsModel';
import { BR_ProfileMapping__cModel } from '../../app/model/BR_ProfileMapping__cModel';
import { BR_ProfileMapping__cService } from '../../providers/br_profilemapping__c/br_profilemapping__c-service-salesforce';
import { ToastHelper } from '../../shared/Helpers/ToastHelper';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { WindowStorageHelper } from '../../shared/Helpers/WindowStorageHelper';
import { LogHelper } from '../../shared/Helpers/LogHelper';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { RecordTypeDAO } from '../../shared/DAO/RecordTypeDAO';
import { SaveHelper } from '../../shared/Helpers/SaveHelper';
import { TabHelper } from '../../shared/Helpers/TabHelper';

@Component({
  providers: [BR_ProfileMapping__cService, DatePipe],
  selector: 'profile-mapping',
  templateUrl: 'profile-mapping.html',

})
export class ProfileMappingComponent {
  [x: string]: any;

  @Input()
  public client: ClientsModel;

  public profileMappings: BR_ProfileMapping__cModel = new BR_ProfileMapping__cModel(null);
  public profileMappingCursor: any = { currentPageOrderedEntries: [] };
  public context: ProfileMappingComponent = this;
  public sortBy = { field: 'Id', label: 'Id' };
  public space = ' ';
  public showProfileView = false;
  public isEditMode = false;
  public showEditButton = false;
  public showProfileFarmer: boolean;
  public showProfileFarm: boolean;
  public showProfileChannel: boolean;
  public showProfileMappingFarmer: boolean;
  public showProfileMappingFarm: boolean;
  public showProfileMappingChannel: boolean;
  public showCreateProfile: boolean;
  public user: any[] = [];
  public userId;
  public userBrand;
  public profileType: string;
  public clientType;
  public titleHeader: string;
  public filtersMarketing = [];
  public filtersSalesSplit = [];
  public hiddenNotRefuge: boolean;
  public cultureValue: string;

  public iBR_RefugeHave__c: IPicklistItem[] = [];
  public iBR_WhatMotive__c: IPicklistItem[] = [];
  public iBR_Income__c: IPicklistItem[] = [];
  public iBR_Culture__c: IPicklistItem[] = [];
  public iBR_LarggestSupplyCrop__c: IPicklistItem[] = [];
  public iBR_EmployeeAmount__c: IPicklistItem[] = [];

  // crop
  public profileMappingBO: ProfileMappingBO;
  public showCropProfile: boolean;
  public iBR_ProgramCategoryGTM__c: IPicklistItem[] = [];

  public masks: any = {
    // numberMask: [/^[0-9]+$/,/^[0-9]+$/,/^[0-9]+$/,/^[0-9]+$/,/^[0-9]+$/,/^[0-9]+$/],
    numberMaskProfile: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
  };
  /**
   * Constructor com a chamada dos métodos de registro da tabela da base de dados, sincronização
   * com salesforce e chamada dos registros locais.
   * @param navCtrl
   * Recurso do próprio Angular que permite a navegação entre páginas
   * @param service
   * Serviço com os recursos do salesforce.
   */

  constructor(
    public navCtrl: NavController,
    public datepipe: DatePipe,
    public navParams: NavParams,
    public service: ServiceProvider,
    public elementRef: ElementRef,
    public modal: ModalController,
    public events: Events,
    public toastCtrl: ToastController,
    public appPreferences: AppPreferences,
    public alertCtrl: AlertController,
  ) {
    this.profileMappingBO = new ProfileMappingBO(this.service);
    this.loadVariablesValues().then(() => {
      this.constructorProfileMapping();
    });
  }

  public loadVariablesValues() {
    return new Promise((resolve) => {
      this.appPreferences.fetch('user_id').then((user_id) => {
        this.userId = user_id;

        this.appPreferences.fetch('user_brand').then((user_brand) => {
          this.userBrand = user_brand;
          this.cultureValue = localStorage.getItem(LocalStorageUser.userCultureKey).toLowerCase();
          this.iBR_RefugeHave__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_RefugeHave__c');
          this.iBR_WhatMotive__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_WhatMotive__c');
          this.iBR_Income__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_Income__c');
          this.iBR_Culture__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_Culture__c');
          this.iBR_LarggestSupplyCrop__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_LarggestSupplyCrop__c');
          this.iBR_EmployeeAmount__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_EmployeeAmount__c');
          this.iBR_ProgramCategoryGTM__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_ProgramCategoryGTM__c');
          resolve();
        });
      });
    });
  }

  public constructorProfileMapping() {
    const data = this.navParams.data;

    this.clientType = (data.clientTypeName !== null && data.clientTypeName !== undefined && data.clientTypeName !== 'undefined')
      ? data.clientTypeName
      : data.RecordType.DeveloperName;

    this.mountProfile();
    this.profileMappingCursor = '';

    LoadingManager.getInstance().show().then(async () => {
      this.profileMappingBO.brandList(this.client.BR_ProfileMapping__c).then((brand_list: string[]) => {
        const profileMappingHasBrand = brand_list.indexOf(this.userBrand);

        if (profileMappingHasBrand > -1) {
          this.showCreateProfile = false;

          this.mountProfileMapping(this.client.BR_ProfileMapping__c[profileMappingHasBrand]).then(() => {
            console.log('------| profileMapping loaded: ', this.profileMappings);
            if (this.profileMappings.OwnerId === this.userId) {
              this.showEditButton = true;
            }
          });
        } else {
          this.loadProfileMapping();
        }
        LoadingManager.getInstance().hide();
      });
    });
  }

  public haveRefuge() {
    if (this.profileMappings.BR_RefugeHave__c !== 'Não') {
      this.hiddenNotRefuge = true;
      this.profileMappings.BR_WhatMotive__c = '';
    }
  }

  public notHaveRefuge(): boolean {
    return !(this.profileMappings.BR_RefugeHave__c === 'Não');
  }

  public mountProfile() {
    if (this.cultureValue === 'quimico') {
      this.cultureValue = 'crop';
    }

    if (this.cultureValue === 'crop') {
      if (this.clientType === 'Agricultores') {

        this.showCropProfile = true;
        this.titleHeader = 'Perfil de agricultor';
        this.profileType = 'BR_GrowerCropProtection';

      } else if (this.clientType === 'GC_ACC_Farm') {

        this.showCropProfile = true;
        this.titleHeader = 'Perfil da fazenda';
        this.profileType = 'BR_FarmCrop';

      } else if (this.clientType === 'Parceiros') {

        this.showCropProfile = true;
        this.titleHeader = 'Perfil de Canal';
        this.profileType = 'BR_ChannelCropProtection';

      }
    } else if (this.cultureValue === 'milho') {
      if (this.clientType === 'Agricultores') {

        this.showProfileFarmer = true;
        this.titleHeader = 'Perfil de agricultor';

        if (this.userBrand === 'DEKALB') {
          this.profileType = 'BR_GrowerCornDekalb';
        } else if (this.userBrand === 'AGROESTE') {
          this.profileType = 'BR_GrowerCornAgroeste';
        } else if (this.userBrand === 'AGROCERES') {
          this.profileType = 'BR_GrowerCornAgroceres';
        }
      } else if (this.clientType === 'GC_ACC_Farm') {

        this.showProfileFarm = true;
        this.titleHeader = 'Perfil da fazenda';

        if (this.userBrand === 'DEKALB') {
          this.profileType = 'BR_FarmCornDekalb';
        } else if (this.userBrand === 'AGROESTE') {
          this.profileType = 'BR_FarmCornAgroeste';
        } else if (this.userBrand === 'AGROCERES') {
          this.profileType = 'BR_FarmCornAgroceres';
        }
      } else if (this.clientType === 'Parceiros') {

        this.showProfileChannel = true;
        this.titleHeader = 'Perfil de Canal';

        if (this.userBrand === 'DEKALB') {
          this.profileType = 'BR_ChannelCorn';
        } else if (this.userBrand === 'AGROESTE') {
          this.profileType = 'BR_DealerCornAgroeste';
        } else if (this.userBrand === 'AGROCERES') {
          this.profileType = 'BR_ChannelCorn';
        }
      }
    } else {
      this.showCreateProfile = false;
    }
  }

  public editProfile() {
    this.isEditMode = true;
    this.showProfileView = true;
    this.showCreateProfile = false;
    TabHelper.disableNonSelectedTabs(this.navCtrl);
  }

  public mountProfileMapping(profileMappingObject): Promise<any> {
    return new Promise((resolve, reject) => {
      const push = new BR_ProfileMapping__cModel(profileMappingObject);
      if (push.getId() != null) {
        push.setRecordType(this.service.getRecordTypeService());
      }
      if (push.BR_Account__c != null) {
        push.setAccount(this.service.getClientsService());
      }

      this.profileMappings = push;
      this.loadProfileMapping();
      resolve(push);
    });
  }

  public error(err: string) {
    console.log(err);
    LoadingManager.getInstance().hide();
  }

  public createProfile() {
    this.isEditMode = true;
    this.showProfileView = true;
    this.showCreateProfile = false;
    TabHelper.disableNonSelectedTabs(this.navCtrl);
  }

  public openSalesSplitModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const SalesSplitModal = this.modal.create(MultipicklistModal, {
      title: 'Split de Vendas',
      items: this.profileMappings.BR_SalesSplit__c,
      list: 'BR_SalesSplit__c',
      // limit: 1,
    }, options);
    SalesSplitModal.present();

    SalesSplitModal.onDidDismiss((data) => {
      if (data) {
        this.profileMappings.BR_SalesSplit__c = data.join(';');
      }
    });
  }

  public openMarketingModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const SalesSplitModal = this.modal.create(MultipicklistModal, {
      title: 'Iniciativas',
      items: this.profileMappings.BR_MarketingInitiative__c,
      list: 'BR_MarketingInitiative__c',
      // limit: 1,
    }, options);
    SalesSplitModal.present();

    SalesSplitModal.onDidDismiss((data) => {
      if (data) {
        this.profileMappings.BR_MarketingInitiative__c = data.join(';');
      }
    });
  }

  public openModalOthersLoyalProgram() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const SalesSplitModal = this.modal.create(MultipicklistModal, {
      title: 'Programas de Relacionamento/Fidelidade',
      items: this.profileMappings.BR_OthersLoyalProgram__c,
      list: 'BR_OthersLoyalProgram__c',
      // limit: 1,
    }, options);
    SalesSplitModal.present();

    SalesSplitModal.onDidDismiss((data) => {
      if (data) {
        this.profileMappings.BR_OthersLoyalProgram__c = data.join(';');
      }
    });
  }

  public openCultureModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const CultureModal = this.modal.create(MultipicklistModal, {
      title: 'Cultura',
      items: this.profileMappings.BR_Culture__c,
      list: 'BR_Culture__c',
      // limit: 1,
    }, options);
    CultureModal.present();

    CultureModal.onDidDismiss((data) => {
      if (data) {
        this.profileMappings.BR_Culture__c = data.join(';');
      }
    });
  }

  public completedProfile() {
    if (this.cultureValue === 'milho' && this.clientType === 'GC_ACC_Farm' && !this.isBR_PlantedArea__cValid()) {
      AlertHelper.showPlantedAreaIsNotValidAlert(this.alertCtrl);
    } else {
      AlertHelper.showDialogForSave(this.alertCtrl, () => {
        this.saveProfileMapping();
      }, () => {
        console.log('cancel');
      });
    }
  }

  public canceledProfile() {
    AlertHelper.showDialogForCancel(this.alertCtrl, () => {
      this.cancelProfileMapping();
    }, () => {
      console.log('cancel');
    });
  }

  public saveProfileMapping() {
    LoadingManager.getInstance().show().then(() => {
      const entries = [];
      console.log(this.profileMappings.BR_Account__c, '--------------------------| BR_Account__c');

      this.profileMappings.BR_Account__c = this.client.getId();
      const recordTypeDAO: RecordTypeDAO = new RecordTypeDAO(this.service);
      recordTypeDAO.loadRecordTypeForProfileMapping(this.profileType).then((recordTypeId) => {

        this.profileMappings.RecordTypeId = recordTypeId;
        this.profileMappings.CreatedDate = new Date().toString();
        this.profileMappings.OwnerId = this.userId;

        SaveHelper.update_Local_(this.profileMappings);

        console.log('---| loadRecordTypeForProfileMapping:this.profileMappings: ', this.profileMappings);
        this.service.getBR_ProfileMapping__cService().upsertSoupEntries([this.profileMappings],
          (response) => {
            this.profileMappings = new BR_ProfileMapping__cModel(response[0]);
            console.log(response, '-----------save');
            this.showProfileView = true;
            this.isEditMode = false;
            this.showCreateProfile = false;

            this.client.BR_ProfileMapping__c = response;
            LoadingManager.getInstance().hide();
            console.log('---| ToastHelper.showSuccessMessage 1');
            ToastHelper.showSuccessMessage(this.toastCtrl);
            TabHelper.enableAll(this.navCtrl);
          },
          (err) => {
            console.log(err);
            TabHelper.enableAll(this.navCtrl);
          });
      }).catch((err) => {
        console.log(err);
        TabHelper.enableAll(this.navCtrl);
      });
    });

    LogHelper.logSave(this.profileMappings);
  }

  public cancelProfileMapping() {
    this.mountProfileMapping(this.client.BR_ProfileMapping__c[0]);
    this.loadProfileMapping();
    TabHelper.enableAll(this.navCtrl);
  }

  private checkPlantedArea(): boolean {
    if (this.profileMappings.BR_PlantedArea__c !== undefined || this.profileMappings.BR_PlantedArea__c !== '') {
      return DataHelper.onlyNumbers(this.profileMappings.BR_PlantedArea__c);
    }
    return true;
  }

  private checkTotalArea(): boolean {
    if (this.profileMappings.BR_TotalArea__c !== undefined || this.profileMappings.BR_TotalArea__c !== '') {
      return DataHelper.onlyNumbers(this.profileMappings.BR_TotalArea__c);
    }
    return true;
  }

  private validInputNumber() {
    if (this.cultureValue === 'milho' && this.clientType === 'GC_ACC_Farm') {
      return !(this.checkPlantedArea() && this.checkTotalArea());
    } else {
      return false;
    }
  }

  public isSaveDisabled() {
    return !(
      // this.profileMappings.RADL__c ||
      this.profileMappings.BR_Rating__c ||
      this.profileMappings.BR_MarketingInitiative__c ||
      this.profileMappings.BR_IsInfluencer__c ||
      this.profileMappings.BR_SalesSplit__c ||
      // this.profileMappings.BR_Description__c ||
      this.profileMappings.BR_LarggestSupplyCrop__c ||
      (this.profileMappings.BR_PlantedArea__c && this.profileMappings.BR_TotalArea__c) ||
      this.profileMappings.BR_ProgramCategoryGTM__c ||
      this.profileMappings.BR_Culture__c

    );
  }

  private isBR_PlantedArea__cValid() {
    return Number(this.profileMappings.BR_PlantedArea__c) >= Number(this.profileMappings.BR_TotalArea__c);
  }

  private async loadProfileMapping() {
    if (this.profileMappings.BR_Account__c != null ||
      this.profileMappings.BR_IsInfluencer__c != null ||
      this.profileMappings.BR_MarketingInitiative__c != null ||
      this.profileMappings.BR_SalesSplit__c != null ||
      this.profileMappings.BR_Description__c != null ||
      this.profileMappings.BR_OthersLoyalProgram__c
    ) {
      this.showProfileView = true;
      this.isEditMode = false;
      this.showCreateProfile = false;

    } else {
      this.showProfileView = false;
      this.isEditMode = false;
      this.showCreateProfile = true;
    }
  }

  private showProfileMappingFarmerButton() {
    this.showProfileMappingFarmer = !this.showProfileMappingFarmer;
  }

  private showProfileMappingFarmButton() {
    this.showProfileMappingFarm = !this.showProfileMappingFarm;
  }

  private showProfileMappingChannelButton() {
    this.showProfileMappingChannel = !this.showProfileMappingChannel;
  }

  public blockTab(evt) {
    if (evt.keyCode === 9) {
      evt.keyCode = 0;
      evt.returnValue = false;
    }
  }

}
