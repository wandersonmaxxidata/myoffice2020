import { ClientBO } from '../../shared/BO/ClientBO';
import { ClientsDetailPage } from '../../pages/clients-detail/clients-detail';
import { ClientsModel } from '../../app/model/ClientsModel';
import { Component, Input } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { RecordTypeDeveloperName } from '../../shared/Constants/Types';
import { ServiceProvider } from '../../providers/service-salesforce';

@Component({
  selector: 'client-items',
  templateUrl: 'client-items.html',
})
export class ClientItemsComponent {

  @Input('type')
  public recordType;

  @Input('searchInputType')
  public searchInputType: any;

  public clientBO: ClientBO;
  public clients = [];
  public clientsTotal = [];
  public cursor = 40;
  public radla = [];
  public filter:any;

  constructor(
    public navCtrl: NavController,
    public service: ServiceProvider,
    public events: Events,
  ) {
    this.clientBO = new ClientBO(this.service);
  }

  public ngOnInit(): void {
    console.log(`------| Events: client-items: !!! unsubscribe !!!`);

    this.events.unsubscribe(this.searchInputType);
    this.events.unsubscribe('filterRadl');
  }

  public ngAfterContentInit() {
    console.log(`------| Events: client-items: !!! subscribe !!!`);
    this.events.subscribe('filterRadl', (filter: any) => {
      this.radla = filter;
      this.loadClients(this.filter, this.recordType);
    //   console.log(filter)

    // if (filter.length > 0) {

    //   this.clients =[];
    //     this.clients = this.clientsTotal.filter(async (data) => {
    //       await filter.forEach(function (value) {
    //         console.log(value)
    //         if (data.RADL__c == value) {
    //           this.clients.push(data);
    //         }
    //       });
    //      });

    // } else {
    //   this.clients = this.clientsTotal.splice(0, this.cursor);
    // }

    });

    this.events.subscribe(this.searchInputType, (filter: any) => {
      console.log(`------| FILTRO CLIENTES`);
      console.log(filter);
      this.loadClients(filter, this.recordType);
    });
  }

  public openClientsDetail(i: number) {
    this.clientBO.getClientById(new ClientsModel(this.clients[i])).then((response) => {
      this.navCtrl.push(ClientsDetailPage, response);
    });
  }

  public loadClients(filter: any, recordType?: string) {
    return new Promise((resolve) => {
      LoadingManager.getInstance().show().then(() => {
        const objSearch = filter ? filter : { term: '', whereCondition: [] };
        const developerName = recordType ? recordType : this.recordType;

        // this.clientBO.getClientsList(objSearch, RecordTypeDeveloperName[developerName]).then((listClients: any) => {
        this.clientBO.getClientsListRadla(objSearch, RecordTypeDeveloperName[developerName], this.radla).then((listClients: any) => {  
          LoadingManager.getInstance().hide();
          this.clientsTotal = listClients;
          console.log(listClients)

          this.clients = this.clientsTotal.splice(0, this.cursor);
          resolve();
        });
      });
    });
  }

  public doInfinite(): Promise<any> {
    return this.moveCursorToNextPage();
  }

  public moveCursorToNextPage(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.clients.length <= this.clientsTotal.length) {
        setTimeout(() => {
          console.log('------| Clients loaded: ', this.clients.length);
          const temp = this.cursor * 1.5;

          this.cursor = (temp < this.clientsTotal.length) ? temp : (this.clientsTotal.length - 1);
          this.clients = this.clientsTotal.splice(0, this.cursor);
          resolve();
        }, 750);
      } else {
        resolve();
      }
    });
  }

}
