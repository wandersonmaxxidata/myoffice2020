import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { NotificationKeys } from '../../shared/Constants/Keys';

@Component({
  selector: 'progress-bar',
  templateUrl: 'progress-bar.html',
})
export class ProgressBarComponent {

  @Input('progress') public progress;
  public syncStatusClassSuffix = '';

  constructor(public events: Events) {}

  public isCompleted() {
    return (this.progress >= 100) ? true : false;
  }

  public ngOnInit(): void {
    console.log('------| Events: progress-bar : !!! unsubscribe !!!');

    this.events.unsubscribe(NotificationKeys.startSyncFromProgressBar);
    this.events.unsubscribe(NotificationKeys.errorSyncFromProgressBar);
  }

  public ngAfterContentInit(): void {
    console.log('------| Events: progress-bar : ### subscribe ###');

    this.events.subscribe(NotificationKeys.startSyncFromProgressBar, () => {
      this.syncStatusClassSuffix = '';
    });

    this.events.subscribe(NotificationKeys.errorSyncFromProgressBar, () => {
      this.syncStatusClassSuffix = '-error';
    });
  }

}
