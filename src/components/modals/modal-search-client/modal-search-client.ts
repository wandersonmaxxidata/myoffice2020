import { ClientsModel } from '../../../app/model/ClientsModel';
import { NotificationKeys } from '../../../shared/Constants/Keys';
import { ServiceProvider } from '../../../providers/service-salesforce';
import { EventModel } from '../../../app/model/EventModel';
import { Component, NgZone } from '@angular/core';
import { Events, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'modal-search-client',
  templateUrl: 'modal-search-client.html',
})
export class SearchClientModal {

  public RecordTypeId = '';
  public clientModel: ClientsModel;
  public context: SearchClientModal = this;
  public event: EventModel;
  public title: string;
  public objectName: string;
  public clientsName: string;
  public parentId;
  public showOk: boolean;

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public service: ServiceProvider,
    public zone: NgZone,
    public events: Events,
  ) {
    this.showOk = false;
    this.RecordTypeId = this.navParams.data.RecordTypeId;
    this.parentId = this.navParams.data.parentId;

    this.title = this.navParams.data.isGR ? 'RTV' : 'Cliente';
    this.objectName = this.navParams.data.isGR ? 'User' : 'Clients';
  }

  public ngOnInit(): void {
    console.log('------| Events: modal-search-client : !!! unsubscribe !!!');

    this.events.unsubscribe(NotificationKeys.cleanLoockup);
  }

  public ngAfterContentInit() {
    console.log('------| Events: modal-search-client : ### subscribe ###');

    this.events.subscribe(NotificationKeys.cleanLoockup, () => {
      this.showOk = false;
    });
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  public applyModal() {
    this.viewCtrl.dismiss(this.clientModel);
  }

  public accountSelected(account: any) {
    console.log(account, '-------accountSelected');
    if (account.Name) {
      account.Name = account.Name.toUpperCase();
      this.clientModel = account;
      setTimeout(() => {
        this.showOk = true;
      }, 500);
    }
  }

}
