import { AppPreferences } from '@ionic-native/app-preferences';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { EventBO } from '../../../shared/BO/EventBO';
import { EventsPage } from '../../../pages/events/events';
import { FormControl } from '@angular/forms';
import { HeaderComponent } from '../../header/header';
import { LoadingManager } from '../../../shared/Managers/LoadingManager';
import { MenuController, NavController, NavParams, Searchbar, ViewController } from 'ionic-angular';
import { ServiceProvider } from '../../../providers/service-salesforce';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'modal-search-event',
  templateUrl: 'modal-search-event.html',
})
export class SearchEventModal {

  @ViewChild('searchbar')
  public searchbar: Searchbar;

  public eventBO: EventBO;
  public title = 'Pesquise pelo evento';
  public searchControl: FormControl;
  public searching = false;
  public list = [];
  public searchTerm = '';
  public notFound = false;
  public userId;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public viewCtrl: ViewController,
    public service: ServiceProvider,
    public appPreferences: AppPreferences,
    public storage: Storage,
  ) {
    this.eventBO = new EventBO(this.service);
    this.searchControl = new FormControl();

    this.storage.get('user_profile').then((user_profile: any) => {
      if (user_profile && (user_profile.isGR || user_profile.isRC || user_profile.isRTV)) {
        this.appPreferences.fetch('user_id').then((user_id) => {
          this.userId = user_id;
        });
      }
    });
  }

  public ionViewDidEnter() {
    if (this.header) {
      this.header.updatePageTitle(this.title);
    }

    if (this.searchTerm && this.searchTerm.length > 0) {
      this.onSearchInput();
    }

    setTimeout(() => this.searchbar.setFocus(), 350);
    this.subscribeSearch();
  }

  public subscribeSearch() {
    this.searchControl.valueChanges.pipe(debounceTime(750)).subscribe((search) => {
      this.list = [];

      if (this.searchTerm) {
        this.searching = true;
        this.onSearchInput().then(() => this.searching = false );
      }
    });
  }

  public onSearchInput() {
    return new Promise((resolve) => {
      this.eventBO.searchEvent(this.searchTerm, this.userId).then((result: any) => {
        this.notFound = (result.length === 0);
        this.list = result;
        resolve();
      });
    });
  }

  public openEvent(index: number) {
    LoadingManager.getInstance().show().then(() => {
      setTimeout(() => {
        this.eventBO.eventDetail(this.list[index]).then((event) => {
          this.navCtrl.push(EventsPage, event).then(() => {
            LoadingManager.getInstance().hide();
          });
        });
      }, 1500);
    });
  }

}
