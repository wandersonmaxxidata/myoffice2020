import { ApplicationRef, Component } from '@angular/core';
import { DataHelper } from '../../../shared/Helpers/DataHelper';
import { HybridBO } from '../../../shared/BO/HybridBO';
import { ClientBO } from '../../../shared/BO/ClientBO';
import { LoadingManager } from '../../../shared/Managers/LoadingManager';
import { NavParams, ViewController } from 'ionic-angular';
import { ServiceProvider } from '../../../providers/service-salesforce';
import { WindowStorageHelper } from '../../../shared/Helpers/WindowStorageHelper';
import { Events } from 'ionic-angular/util/events';


@Component({
  selector: 'modal-multipicklist',
  templateUrl: 'modal-multipicklist.html',
})
export class MultipicklistModal {

  public clientBO: ClientBO;
  public hybridBO: HybridBO;
  public currentSearch = '';
  public title: string;
  public selected = [];
  public selectedOne = [];
  public list: any[];
  public listOne: any[];
  public pageSize = 40;
  public limit: number;
  public max: number;
  public division: string;
  public obj;
  public order;
  public namePageGet;

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private appRef: ApplicationRef,
    public events: Events,
    private serviceProvider: ServiceProvider,
  ) {
    this.clientBO = new ClientBO(this.serviceProvider);
    this.hybridBO = new HybridBO(this.serviceProvider);
    this.obj = this.navParams.data;

    this.title = this.obj.title;
    this.order = this.obj.order ? this.obj.order : 'label';
    this.limit = this.max = (this.obj.limit) ? this.obj.limit : 1000;
    this.division = this.navParams.data['division'];
    this.namePageGet = this.obj.namePage;
    console.log(this.obj.namePage)

    if (this.obj.items && this.obj.items.length > 0) {
      this.selected = (typeof this.obj.items === 'string') ? this.obj.items.split(';') : this.obj.items;
    }


    //MAXXIDATA 
    this.loadList(this.pageSize, this.currentSearch);
    this.clientBO.returnOnebyOneOptions().then(listOneSoql => {
      this.listOne = listOneSoql.map(a => {
        return { name: a[0], checked: false }
      });

    });
    //FIM MAXXIDATA 
  }


  ionViewDidEnter() {
    if (sessionStorage.getItem('fone')) {
      this.selectedOne = [];
      let currentSelect = JSON.parse(sessionStorage.getItem('fone'));
      sessionStorage.removeItem('fone');
      currentSelect.forEach(b => {
        for (const i of this.listOne) {
          if (b === i.name) {
            console.log('ENCONTRADO')
            i.checked = true;
          }
        }
      });

    }
  }

  public searchSQL() {
    if (typeof this.obj.list !== 'object') {
      setTimeout(() => {
        this.loadList(this.pageSize, this.currentSearch);
      }, 350);
    }
  }

  public loadList(pageSize: number, filter: string) {
    return new Promise((resolve) => {

      if (typeof this.obj.list === 'object') {
        this.list = DataHelper.removeDuplicatesFromObject(this.obj.list, 'label');
        for (const list of this.list) {
          list.checked = false;
        }
        this.loadChecked().then(() => {
          resolve();
        });
      } else if (this.obj.list === 'HybridList') {
        this.hybridBO.hybridList(pageSize, this.division, filter).then((list: any) => {
          console.log('TOTAL LISTA')
          console.log(list.length)
          console.log(list)
          this.list = DataHelper.removeDuplicatesFromObject(list, 'label');
          console.log(this.list.length)
          console.log(this.list)
          this.loadChecked().then(() => {
            resolve();
          });
        });
      } else {
        WindowStorageHelper.retrievePicklistsWithFilter(this.obj.list, pageSize, filter).then((list: any) => {
          this.list = DataHelper.removeDuplicatesFromObject(list, 'label');
          this.loadChecked().then(() => {
            resolve();
          });
        });
      }
    });
  }

  public loadChecked() {
    return new Promise((resolve) => {
      this.selected.forEach((item) => {
        for (const i of this.list) {
          if (item && (item === i.label || item.label === i.label)) {
            i.checked = true;
            break;
          }
        }
      });
      resolve();
    });
  }

  public tick() {
    this.appRef.tick();
  }

  public limitCtrl(item) {
    if (item.checked) {
      if (this.obj.sql) {
        this.selected.push(item);
      } else {
        this.selected.push(item.label);
      }
    } else {
      for (let i = 0; i < this.selected.length; i++) {
        if (this.selected[i] && (this.selected[i] === item.label || this.selected[i].label === item.label)) {
          this.selected.splice(i, 1);
          break;
        }
      }
    }
    this.tick();
  }


  //MAXXIDATA
  removeA(arr, item) {
    console.log('REMOVENDO')
    console.log(item)
    console.log(arr)
    this.selectedOne = arr.filter(e => e !== item);
    sessionStorage.setItem('fone', JSON.stringify(this.selectedOne));
    console.log(this.listOne)
    return (this.selectedOne);
  }


  public limitCtrlOneByOne(item,check) {
    if (this.selectedOne.indexOf(item) !== -1) {
      let currentArry = this.removeA(this.selectedOne, item);
      sessionStorage.setItem('fone', JSON.stringify(currentArry));
    } else {
      this.addOneFilter(item);
    }
    if(check== true){
      check =false;
    }else{
      check=true;
    }

  }

  public addOneFilter(item) {
    if (sessionStorage.getItem('fone')) {
      this.selectedOne = JSON.parse(sessionStorage.getItem('fone'));
    }
    this.selectedOne.push(item);
    sessionStorage.setItem('fone', JSON.stringify(this.selectedOne))

  }
  //FIM MAXXIDATA


  public clear() {
    sessionStorage.removeItem('fone');

    this.listOne.map((t) => {
      t.checked = false;
      this.tick();
    });

    this.list.map((t) => {
      t.checked = false;
      this.tick();
    });
    setTimeout(() => { this.tick(); }, 400);
  }

  public registerItems() {
    if (this.obj.sql && this.selected.length === 0) {
      this.selected = null;
    }
    this.closeModal(this.selected);
  }

  public closeModal(response) {
    console.log(response)
    this.events.publish('filterRadl', response)
    this.viewCtrl.dismiss(response);
  }

}
