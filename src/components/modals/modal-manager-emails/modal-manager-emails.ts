import { ContactModel } from '../../../app/model/ContactModel';
import { ContactsDetailPage } from '../../../pages/contacts-detail/contacts-detail';
import { ClientsModel } from '../../../app/model/ClientsModel';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  templateUrl: 'modal-manager-emails.html',
})
export class ManagerEmailsModal {

  public client: ClientsModel;
  public emailsSelected = [];
  public contactsList = [];
  public currentSearch = '';
  public currentAddEmail;


  constructor(
    private navParams: NavParams,
    private navCtrl: NavController,
    private viewCtrl: ViewController,
  ) {
    this.emailsSelected = this.navParams.data.emailsSelected ? this.navParams.data.emailsSelected : [];

    this.loadList(this.navParams.data.contactsList).then(() => {
      this.loadChecked();
    });
  }

  public loadList(contactsList) {
    return new Promise((resolve) => {
      this.currentAddEmail = sessionStorage.getItem('currentAddEmail');
      if (contactsList && contactsList.length > 0) {
        contactsList.forEach((item) => {
          if(item === sessionStorage.getItem('currentAddEmail')){
            this.emailsSelected.push(item);
            this.contactsList.push({ label: item, checked: true });
          }else{
            this.contactsList.push({ label: item, checked: false });
          }
        });
      }
      resolve();
    });
  }

  public loadChecked() {
    return new Promise((resolve) => {
      this.emailsSelected.forEach((emailSelected) => {
        for (const item of this.contactsList) {
          if (emailSelected && (emailSelected === item.label)) {
            item.checked = true;
            break;
          }
        }
      });
      resolve();
    });
  }

  public managerItem(item) {
    if (item.checked) {
      this.emailsSelected.push(item.label);
    } else {
      for (let i = 0; i < this.emailsSelected.length; i++) {
        if (this.emailsSelected[i] && (this.emailsSelected[i] === item.label)) {
          this.emailsSelected.splice(i, 1);
          break;
        }
      }
    }
  }

  public apply() {
    this.viewCtrl.dismiss({ emailsSelected: this.emailsSelected });
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  public addContact() {
    const clientParam = this.navParams.data.client;
    this.navCtrl.push(ContactsDetailPage, { client: clientParam,
      callback: (contact: ContactModel) => {
        return new Promise((resolve, reject) => {
          if (contact && contact.Email) {
            this.emailsSelected.push(contact.Email);
            this.contactsList.push({ label: contact.Email, checked: true });
          }
          resolve();
        });
      },
    });
  }

}
