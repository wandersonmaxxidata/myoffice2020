import { Component, Input } from '@angular/core';

@Component({
  selector: 'report-plague',
  templateUrl: 'report-plague.html',
})
export class ReportPlagueComponent {

  @Input('info')
  public info: any;

  public hiddenCard = false;

  constructor() {}

  public hideCard() {
    this.hiddenCard = !this.hiddenCard;
  }

}
