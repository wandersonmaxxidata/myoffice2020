import { Component, Input } from '@angular/core';

@Component({
  selector: 'report-image',
  templateUrl: 'report-image.html',
})
export class ReportImageComponent {

  @Input('info')
  public info: any;

  public hiddenCard = false;

  constructor() {}

  public hideCard() {
    this.hiddenCard = !this.hiddenCard;
  }

}
