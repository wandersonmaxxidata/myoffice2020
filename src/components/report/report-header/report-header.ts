import { Component, Input } from '@angular/core';

@Component({
  selector: 'report-header',
  templateUrl: 'report-header.html',
})
export class ReportHeaderComponent {

  @Input('info')
  public info: any = {};

  public brandInfo = {
    default  : { icon: './assets/imgs/company/agroceres.png', color: 'green' },
    agroceres: { icon: './assets/imgs/company/agroceres.png', color: 'green' },
    agroeste : { icon: './assets/imgs/company/agroeste.png', color: 'blue' },
    dekalb   : { icon: './assets/imgs/company/dekalb.png', color: 'red' },
  };

  constructor() {}

}
