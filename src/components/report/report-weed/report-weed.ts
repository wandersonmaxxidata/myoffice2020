import { Component, Input } from '@angular/core';

@Component({
  selector: 'report-weed',
  templateUrl: 'report-weed.html',
})
export class ReportWeedComponent {

  @Input('info')
  public info: any;

  public hiddenCard = false;

  constructor() {}

  public hideCard() {
    this.hiddenCard = !this.hiddenCard;
  }

}
