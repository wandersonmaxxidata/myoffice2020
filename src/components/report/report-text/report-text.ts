import { Component, Input } from '@angular/core';

@Component({
  selector: 'report-text',
  templateUrl: 'report-text.html',
})
export class ReportTextComponent {

  @Input('info')
  public info: any;

  public hiddenCard = false;

  constructor() {}

  public hideCard() {
    this.hiddenCard = !this.hiddenCard;
  }

}
