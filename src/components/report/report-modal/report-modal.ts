import { NavParams, ViewController } from 'ionic-angular';
import { Component } from '@angular/core';
import { ServiceProvider } from './../../../providers/service-salesforce';
import { DataHelper } from '../../../shared/Helpers/DataHelper';

@Component({
  selector: 'report-modal',
  templateUrl: 'report-modal.html',
})
export class ReportModal {

  public hybridObj: any;
  public textObj: any;

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public service: ServiceProvider,
  ) {
    this.hybridObj = this.navParams.data;
    this.loadText();
  }

  private closeModal() {
    this.viewCtrl.dismiss();
  }

  private loadText() {
    this.textObj = DataHelper.createTextObj(this.hybridObj);
  }

}
