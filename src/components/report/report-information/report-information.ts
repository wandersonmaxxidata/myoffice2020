import { Component, Input } from '@angular/core';

@Component({
  selector: 'report-information',
  templateUrl: 'report-information.html',
})
export class ReportInformationComponent {

  @Input('info')
  public info: any;

  public hiddenCard = false;

  constructor() {}

  public hideCard() {
    this.hiddenCard = !this.hiddenCard;
  }

}
