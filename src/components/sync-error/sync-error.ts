import { Component, Input } from '@angular/core';
import { AlertController, ModalOptions, NavController, NavParams, Platform } from 'ionic-angular';
import { Events, ModalController, ToastController } from 'ionic-angular';
import { AppPreferences } from '@ionic-native/app-preferences';
import { LocalStorageKeys } from '../../shared/Constants/Keys';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { DateHelper } from '../../shared/Helpers/DateHelper';
import { DataErrorDaoProvider } from './../../providers/data-error-dao/data-error-dao';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Storage } from '@ionic/storage';
import { ToastHelper } from '../../shared/Helpers/ToastHelper';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';

import { NotificationKeys } from './../../shared/Constants/Keys';
import { CornDayDPage } from '../../pages/visits/corn/corn-day-d/corn-day-d';
import { CornGerminationEmergencyPage } from '../../pages/visits/corn/corn-germination-emergency/corn-germination-emergency';
import { CornHarvestPage } from '../../pages/visits/corn/corn-harvest/corn-harvest';
import { CornPlantingCreatePage } from '../../pages/visits/corn/corn-planting-create/corn-planting-create';
import { CornPrePlantingCreatePage } from '../../pages/visits/corn/corn-pre-planting-create/corn-pre-planting-create';
import { CornReproductiveDevelopmentPage } from '../../pages/visits/corn/corn-reproductive-development/corn-reproductive-development';
import { CornVegetativeDevelopmentPage } from '../../pages/visits/corn/corn-vegetative-development/corn-vegetative-development';
import { CornVisitEventPage } from '../../pages/visits/corn/corn-visit-event/corn-visit-event';
import { CropClimatePage } from '../../pages/visits/crop/crop-climate/crop-climate';
import { CropComercialPage } from '../../pages/visits/crop/crop-comercial/crop-comercial';
import { CropEventPage } from '../../pages/visits/crop/crop-event/crop-event';
import { SoyGreenVisitPage } from '../../pages/visits/soy/soy-green-visit/soy-green-visit';
import { SoyHarvestPage } from '../../pages/visits/soy/soy-harvest/soy-harvest';
import { SoyPlantingVisitPage } from '../../pages/visits/soy/soy-planting-visit/soy-planting-visit';

import { EventModel } from '../../app/model/EventModel';
import { Visita__cModel } from './../../app/model/Visita__cModel';
import { Hibrido_de_Visita__cModel } from './../../app/model/Hibrido_de_Visita__cModel';
import { Praga__cModel } from './../../app/model/Praga__cModel';
import { BR_CropProducts__cModel } from './../../app/model/BR_CropProducts__cModel';
import { BR_Sales_Coaching__cModel } from '../../app/model/BR_Sales_Coaching__cModel';
import { BR_WeedHandling__cModel } from '../../app/model/BR_WeedHandling__cModel';

import { EventBO } from '../../shared/BO/EventBO';
import { UserBO } from '../../shared/BO/UserBO';
import { VisitBO, VisitStatus } from '../../shared/BO/VisitBO';
import { SalesCoachingBO, SalesCoachingStatus } from '../../shared/BO/SalesCoachingBO';

import { EventDeveloperName, SalesCoachingRecordTypes } from '../../shared/Constants/Types';

import { VisitRecordTypes } from '../../shared/Constants/Types';
import { VisitSalesCoachingPage } from '../../pages/visits-sales-coaching/visits-sales-coaching';
import { WindowStorageHelper } from '../../shared/Helpers/WindowStorageHelper';
import * as moment from 'moment';


@Component({
  selector: 'sync-error',
  templateUrl: 'sync-error.html'
})
export class SyncErrorComponent {

  @Input('filter')
  public filter: any;

  public lastSynchronismDateMessage: string;
  public timeToLastSync: any;
  public lastSyncErros: any;
  public eventsObj = [];
  public allErrorsLength: number;
  public isGR: boolean;
  public isOnline = true;
  public event: EventModel = new EventModel(null);
  public userBO: UserBO;
  public eventBO: EventBO;
  public visitBO: VisitBO;
  public salesCoaching: BR_Sales_Coaching__cModel;
  public salesCoachingBO: SalesCoachingBO;
  public userProfile: any;

  constructor(
    public appPreferences: AppPreferences,
    public service: ServiceProvider,
    public dataErrorDAO: DataErrorDaoProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public events: Events,
    public platform: Platform,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public storage: Storage,
  ) {

    this.userBO = new UserBO(this.service);
    this.eventBO = new EventBO(this.service);
    this.visitBO = new VisitBO(this.service);
    this.salesCoachingBO = new SalesCoachingBO(this.service);

    this.storage.get("user_profile").then((user_profile: any) => {
      this.userProfile = user_profile;
    });

    // this.events.unsubscribe(NotificationKeys.closeSync);
    // this.events.unsubscribe('openSyncy');

    this.events.subscribe(NotificationKeys.closeSync, () => {
      this.ngOnInit();
    });

    this.events.subscribe('openSyncErr', () => {
      this.ngOnInit();
    });

  }

  public ngOnInit() {

    // this.service.hibrido_de_visita__cService.removeEntries('29', () => {

    // }, (error) => {
    //   console.log(error);
    // });

    // this.service.hibrido_de_visita__cService.removeEntries('28', () => {

    // }, (error) => {
    //   console.log(error);
    // });

    // this.service.hibrido_de_visita__cService.removeEntries('27', () => {

    // }, (error) => {
    //   console.log(error);
    // });

    // this.service.hibrido_de_visita__cService.removeEntries('23', () => {

    // }, (error) => {
    //   console.log(error);
    // });

    // this.service.hibrido_de_visita__cService.removeEntries('22', () => {

    // }, (error) => {
    //   console.log(error);
    // });



    // this.service.hibrido_de_visita__cService.removeEntries('38', () => {
    // }, (error) => {
    //   console.log(error);
    // });



    this.dataErrorDAO.getAllVisits().then(data => {
      this.eventsObj = [];
      console.log('TODOS OS ERROS ++++++++++++++');
      console.log(data);
      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          let item = data.item(i);
          this.checkVisitExistSncy(item.dat_event_sop_id).then(data => {
            if (data) {
              console.log('VISITA APARECE')
              console.log(item)
              console.log(item.sda_type)
              this.allErrorsLength = data.length;
              this.getEvent(item.dat_event_sop_id, item.der_date, item.err_type, item.dat_data_type, item.der_error_id, item.idData, item.idDataError, item.sda_data, item.shildId, item.idDataError, item.idData);
            } else {
              this.service.visita__cService.deletLogErrorEntities(item.idDataError, item.idData, item.shildId);
            }
          });
        };
      }
    });

    // this.dataErrorDAO.getAllNotShild().then(data => {
    //   console.log('TODOS OS ERROS ++++++++++++++');
    //   console.log(data);
    //   if (data.length > 0) {
    //     for (var i = 0; i < data.length; i++) {
    //       let item = data.item(i);
    //       console.log(item.sda_data)
    //       console.log(item.sda_type)
    //       this.allErrorsLength = data.length;
    //       this.getEvent(item.dat_event_sop_id, item.der_date, item.err_type, item.dat_data_type, item.der_error_id, item.idData, item.idDataError)
    //     };
    //   }
    // })

    this.lastSyncErros = localStorage.getItem('lastSyncErros');
    this.handleLastSynchronismPresentation();
  }

  public checkFilter(id){
    if(this.filter != false){
      let check = id == this.filter?true:false;
      return check;
    }else{
      return true;
    }
  }


  public ngAfterContentInit() {

    // const visit = new Visita__cModel(null);
    // // visit.BR_DesiccationMadeInPlanting__c = false;
    // visit.BR_Division__c = "Milho";
    // visit.BR_EventVisitType__c = "Tour";
    // visit.BR_GenerateReport__c = false;
    // // visit.BR_MadeEarlyDesiccation__c = false;
    // // visit.BR_MobileAppFlag__c = true;
    // // visit.BR_PresentVisit__c = false;
    // // visit.BR_Silage__c = false;
    // // visit.BR_UsedHerbicideInTheWinter__c = false;
    // // visit.BR_failed_planting__c = false;
    // // visit.BR_handling_held_harvest_postharvest__c = false;
    // // visit.BR_has_disease_control_fungicide__c = false;
    // // visit.BR_infestation_weed__c = false;
    // // visit.BR_plagues_remaning_previous_culture__c = false;
    // // visit.BR_plant_area_refuge__c = false;
    // // visit.BR_plants_weeds_resistant__c = false;
    // // visit.BR_stand_well_established__c = false;
    // visit.Cliente__c = "001g000002APzERAA1";
    // visit.Data_de_Execucao__c = "2019-09-04T16:43:52-03:00";
    // //visit.Matriz__c = "0011Y00002jt5f4QAA";
    // // visit.Data_de_Execucao__c= "2019-09-03T12:49:13-03:00";
    // //visit.Cliente__c = "001g000002APzERAA1";
    // visit.EventId__c = "00Ug0000006tPHbEAM";
    // visit.Name = "Evento";
    // visit.RecordTypeId = "0121Y000001QSiAQAW";
    // visit.Safra__c = "a0Tg0000004aJ8CEAU";
    // visit.Status__c = "Concluída";
    // visit.__local__ = true;
    // visit.__locally_created__ = true;
    // visit.__locally_deleted__ = false;
    // visit.__locally_updated__ = true;
    // // this.service.getVisita__cService().upsertSoupEntries([visit],
    // //   (savedVisits: Visita__cModel[]) => {
    // //     const model = new Visita__cModel(savedVisits[0]);
    // //     console.log(model);
    // //     this.service.getVisita__cService().syncUpForce(true).then(data => {
    // //       console.log(data);
    // //     }, (error) => {
    // //       console.log(error);
    // //     });
    // //   },
    // //   (error) => {
    // //     console.log(error);
    // //   });

  }


  public openVisit(eventID, dataShild, shildId, idDataError, idData) {
    let dataShildJson = JSON.parse(dataShild);
    const errorVisit = { editVisitError: false, dataShild: null, event: null, idDataError: null, idData: null, shildId: null };

    console.log(dataShildJson.Id)
    console.log(dataShildJson.attributes)
    this.getEventAllFilds(eventID, dataShildJson, idData).then((event) => {
      errorVisit.event = event;
      this.getRecordInfoPage().then((options) => {
        if (dataShild) {
          errorVisit.editVisitError = true;
          errorVisit.dataShild = dataShildJson;
          errorVisit.idDataError = idDataError;
          errorVisit.idData = idData;
          errorVisit.shildId = shildId;
        }
        this.navCtrl.push(options.page, errorVisit);
      });
    }, (error) => {
      console.log(error);
    });

    //   this.dataErrorDAO.delete(idDataError).then((data) => {
    //     this.dataErrorDAO.deleteData(idData).then((data) => {
    //         this.dataErrorDAO.deleteShild(shildId).then((data) => {
    //         });
    //     });
    // });

  }


  private getEvent(id, dateError, typeErrorText, tableError, errorId, dataId, dataErrorId, dataShild, shildId, idDataError, idData) {
    const smartSql = `
    SELECT
      {Event:Id},
      {Event:StartDateTime},
      {Event:EndDateTime},
      {Event:IsOutlook__c},
      {Event:Status__c},
      {Event:BR_Tipo_de_Visita__c},
      {Event:Location},
      {Event:ExternalID__c},
      {Event:IsAllDayEvent},
      {Event:Subject}
    FROM
      {Event}
    Where
      {Event:Id} = "${id}"`;

    this.querySmartFromSoup(smartSql, 1).then((response) => {
      console.log('SELECT DO EVENTO ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

      response.currentPageOrderedEntries[0].dateError = dateError;
      response.currentPageOrderedEntries[0].typeErrorText = typeErrorText;
      response.currentPageOrderedEntries[0].tableError = tableError;
      response.currentPageOrderedEntries[0].errorId = errorId;
      response.currentPageOrderedEntries[0].dataId = dataId;
      response.currentPageOrderedEntries[0].dataErrorId = dataErrorId;
      response.currentPageOrderedEntries[0].dataShild = dataShild;
      response.currentPageOrderedEntries[0].shildId = shildId;
      response.currentPageOrderedEntries[0].idDataError = idDataError;
      response.currentPageOrderedEntries[0].idData = idData;
      // console.log(response.currentPageOrderedEntries[0])
      this.eventsObj.push(response.currentPageOrderedEntries[0]);
    }, (error) => {
      console.log(error);
    });
  }


  private checkVisitExist(id) {
    return new Promise<any>((resolve, reject) => {
      const smartSql = `
      SELECT
      {Visita__c:EventId__c}
      FROM
        {Visita__c}
      Where
        {Visita__c:EventId__c} = "${id}"`;
      this.querySmartFromSoup(smartSql, 1).then((response) => {
        resolve(response);
      },
        (error) => {
          reject(error);
        });
    });
  }


  private checkVisitExistSncy(id) {
    return new Promise<any>((resolve, reject) => {
      const smartSql = `
      SELECT
      {Visita__c:Id}
      FROM
        {Visita__c}
      Where
        {Visita__c:EventId__c} = "${id}"`;
      this.querySmartFromSoup(smartSql, 1).then((response) => {
        if (response.currentPageOrderedEntries.length > 0) {
          if (response.currentPageOrderedEntries[0][0].match("local")) {
            console.log('VISITA VERDADE')
            resolve(true);
          } else {
            resolve(false);
          }
        } else {
          resolve(true);
        }
      },
        (error) => {
          reject(error);
        });
    });
  }


  private creatShild(item, type) {

    return new Promise((resolve) => {
      delete item._soupEntryId;
      delete item._soupLastModifiedDate;
      item.__local__ = true;
      item.__locally_created__ = true;
      item.__locally_deleted__ = false;
      item.__locally_updated__ = true;
      console.log(item)

      switch (type) {
        case 'BR_ProductUsed__c':
          let product = new BR_CropProducts__cModel(item);
          this.service.getBR_ProductUsed__cService().upsertSoupEntries([product],
            (savedproduct: BR_CropProducts__cModel[]) => {
              const model = new BR_CropProducts__cModel(savedproduct[0]);
              resolve()
            },
            (error) => {
              resolve()
            });
          break;
        case 'Hibrido_de_Visita__c':
          let hibrido = new Hibrido_de_Visita__cModel(item);
          this.service.getHibrido_de_Visita__cService().upsertSoupEntries([hibrido],
            (savedHibrido: Hibrido_de_Visita__cModel[]) => {
              const model = new Hibrido_de_Visita__cModel(savedHibrido[0]);
              resolve()
            },
            (error) => {
              resolve()
            });
          break;

        case 'Praga__c':
          let praga = new Praga__cModel(item);
          this.service.getPraga__cService().upsertSoupEntries([praga],
            (savedPraga: Praga__cModel[]) => {
              const model = new Praga__cModel(savedPraga[0]);
              resolve()
            },
            (error) => {
              resolve()
            });
          break;

        case 'BR_WeedHandling__c':
          console.log('BR_WeedHandling__c CREAT =====================')
          let weedHandling = new BR_WeedHandling__cModel(item);
          this.service.getBR_WeedHandling__cService().upsertSoupEntries([weedHandling],
            (savedPraga: BR_WeedHandling__cModel[]) => {
              const model = new BR_WeedHandling__cModel(savedPraga[0]);
              console.log('CRIADO')
              resolve()
            },
            (error) => {
              console.log(error)
              resolve()
            });
          break;
        default:
          break;
      }

    });
  }


  private checkShildCreate(idData, type) {
    return new Promise((resolve) => {
      //CRIA HIBRIDO CASO EXISTA BKP
      this.dataErrorDAO.checkShild(idData, type).then(async data => {
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            let item = data.item(i);
            let itemEnvia = JSON.parse(item.sda_data);
            await this.creatShild(itemEnvia, type).then(() => {
              if (i === (data.length - 1)) {
                resolve();
              }
            });
          }
        } else {
          resolve();
        }
      },
        (error) => {
          console.log('ERRO NA BUSCA')
          resolve();
        });
      //FIM CRIA HIBRIDO CASO EXISTA BKP
    });
  }


  private getEventAllFilds(id, dataShild, idData) {
    return new Promise((resolve, reject) => {
      const smartSql = `
      SELECT *
      FROM
        {Event}
      Where
        {Event:Id} = "${id}"`;

      this.querySmartFromSoup(smartSql, 1).then((response) => {
        this.isGR = response.Status__c;
        this.event = new EventModel(response.currentPageOrderedEntries[0][1]);
        delete dataShild._soupEntryId;
        delete dataShild._soupLastModifiedDate;
        dataShild.__local__ = false;
        dataShild.__locally_created__ = false;
        dataShild.__locally_deleted__ = false;
        dataShild.__locally_updated__ = false;
        // dataShild.Id = dataShild.ExternalID__c = 'local_' + new Date().getTime();
        let visit = new Visita__cModel(dataShild);
        console.log('NOVA VISITA ===========================')
        console.log(visit)
        let visitNew = new Visita__cModel(null);
        console.log('NOVA VISITA ZERADA ===========================')
        console.log(visitNew)
        this.checkVisitExist(dataShild.EventId__c).then(data => {

          console.log('RETORNO CONSULTA')
          console.log(data)
          console.log(data.currentPageIndex)

          if (data.currentPageOrderedEntries.length == 0) {

            //CRIA VISTA BKP
            this.service.getVisita__cService().upsertSoupEntries([visit],
              async (savedVisits: Visita__cModel[]) => {
                const model = new Visita__cModel(savedVisits[0]);
                console.log(model);
                console.log('CONSOLE EVENTO');
                console.log(id);
                console.log(response);

                //CRIA HIBRIDO CASO EXISTA BKP
                await this.checkShildCreate(idData, 'Hibrido_de_Visita__c').then(async (dataRet) => {
                  console.log('EXEC HIBRIDO =====================')
                  await this.checkShildCreate(idData, 'Praga__c').then(async (dataRet) => {
                    console.log('EXEC PRAGA =====================')
                    await this.checkShildCreate(idData, 'BR_ProductUsed__c').then(async (dataRet) => {
                      console.log('EXEC PRODUTOS =====================')
                      await this.checkShildCreate(idData, 'BR_WeedHandling__c').then((dataRet) => {
                        console.log('EXEC WWE =====================')
                        resolve(this.event);
                      });
                    });
                  });
                });
                //FIM CRIA HIBRIDO CASO EXISTA BKP

              },
              (error) => {
                console.log(error);
              });


          } else {
            resolve(this.event);
          }

        },
          (error) => {
            console.log(error);
          });
      }, (error) => {
        reject(error);
      });
    });
  }


  private querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getEventService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }


  private handleLastSynchronismPresentation() {
    const lastSyncDateString = localStorage.getItem(LocalStorageKeys.lastSyncDateKey);
    console.log('---| LocalStorege lastSyncDateString: ', lastSyncDateString);

    this.appPreferences.fetch('totalTimeSync').then((value) => {
      this.timeToLastSync = value;
    });

    if (lastSyncDateString != null) {
      const lastSyncDate: Date = new Date(lastSyncDateString);
      console.log('---| LocalStorege lastSyncDate: ', lastSyncDate);

      const formattedDate = DateHelper.formattedLastSyncDate(lastSyncDate);
      this.lastSynchronismDateMessage = formattedDate;
    }
  }


  showErrorsTable(focusError) {
    let errorLocal;
    switch (focusError) {
      case 'Event':
        errorLocal = 'Evento';
        break;
      case 'Visita__c':
        errorLocal = 'Visita';
        break;
      case 'BR_ProductUsed__c':
        errorLocal = 'Produtos';
        break;
      case 'Hibrido_de_Visita__c':
        errorLocal = 'Hibrido';
        break;
      case 'Attachment':
        errorLocal = 'Anexos';
        break;
      case 'Praga__c':
        errorLocal = 'Pragas';
        break;
      case 'BR_WeedHandling__c':
        errorLocal = 'Manipulação de Ervas Daninhas';
        break;
      case 'BR_CropProducts__c':
        errorLocal = 'Produtos';
        break;
      case 'Doenca__c':
        errorLocal = 'Doenças';
        break;
      case 'User':
        errorLocal = 'Dados de usuário';
        break;
      default:
        break;
    }
    return errorLocal;
  }


  private getRecordInfoPage(): any {
    return new Promise((resolve) => {

      const options = { eventRecordTypeName: '', recordTypeName: '', page: {} };
      let online;
      if (this.isGR) {
        options.eventRecordTypeName = EventDeveloperName.coaching;
        options.page = VisitSalesCoachingPage;

        if (this.event.Subject.includes('Identificação')) {
          options.recordTypeName = SalesCoachingRecordTypes.needs;

        } else if (this.event.Subject.includes('Plano e Articular')) {
          options.recordTypeName = SalesCoachingRecordTypes.planArticulate;

        } else if (this.event.Subject.includes('Negociar e Fechar')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.negotiateCloseOnline : SalesCoachingRecordTypes.negotiateCloseRideAlong;
          });

        } else if (this.event.Subject.includes('Assistência com a Compra')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.assistPurchaseOnline : SalesCoachingRecordTypes.assistPurchaseRideAlong;
          });

        } else if (this.event.Subject.includes('Entrega & Order-To-Cash')) {
          options.recordTypeName = SalesCoachingRecordTypes.trackDeliveryOTC;

        } else if (this.event.Subject.includes('Geração de Demanda')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.demandGenerationOnline : SalesCoachingRecordTypes.demandGenerationRideAlong;
          });

        } else if (this.event.Subject.includes('Final da Safra')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.endSeasonOnline : SalesCoachingRecordTypes.endSeasonRideAlong;
          });
        }

      } else {
        options.eventRecordTypeName = EventDeveloperName.default;

        if (this.event.BR_Division__c.includes('Crop')) {
          if (this.event.BR_Tipo_de_Visita__c.includes('Comercial')) {
            options.recordTypeName = VisitRecordTypes.comercial;
            options.page = CropComercialPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Climate')) {
            options.recordTypeName = VisitRecordTypes.climate;
            options.page = CropClimatePage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Evento')) {
            options.recordTypeName = VisitRecordTypes.eventCrop;
            options.page = CropEventPage;
          }
        } else if (this.event.BR_Division__c.includes('Soja')) {
          if (this.event.BR_Tipo_de_Visita__c.includes('Plantio')) {
            options.recordTypeName = VisitRecordTypes.soyPlanting;
            options.page = SoyPlantingVisitPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Colheita')) {
            options.recordTypeName = VisitRecordTypes.cropSoy;
            options.page = SoyHarvestPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Visita Verde')) {
            options.recordTypeName = VisitRecordTypes.soyGreenVisit;
            options.page = SoyGreenVisitPage;
          }
        } else if (this.event.BR_Division__c.includes('Milho')) {
          if (this.event.BR_Tipo_de_Visita__c.includes('Pré-Plantio')) {
            options.recordTypeName = VisitRecordTypes.prePlanting;
            options.page = CornPrePlantingCreatePage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Plantio')) {
            options.recordTypeName = VisitRecordTypes.planting;
            options.page = CornPlantingCreatePage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Germinação e Emergência')) {
            options.recordTypeName = VisitRecordTypes.germinationAndEmergency;
            options.page = CornGerminationEmergencyPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Desenvolvimento Vegetativo')) {
            options.recordTypeName = VisitRecordTypes.vegetativeDevelopment;
            options.page = CornVegetativeDevelopmentPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Desenvolvimento Reprodutivo')) {
            options.recordTypeName = VisitRecordTypes.reproductiveDevelopment;
            options.page = CornReproductiveDevelopmentPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Colheita')) {
            options.recordTypeName = VisitRecordTypes.harvest;
            options.page = CornHarvestPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Dia D')) {
            options.recordTypeName = VisitRecordTypes.dDay;
            options.page = CornDayDPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Evento')) {
            options.recordTypeName = VisitRecordTypes.cornEvent;
            options.page = CornVisitEventPage;
          }
        }
      }
      resolve(options);
    });
  }








}
