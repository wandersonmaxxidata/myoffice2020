import { ServiceProvider } from '../../providers/service-salesforce';
import { IService } from '../../interfaces/IService';
import { ResponseDAO } from '../../shared/DAO/RepositoryDAO';
import { SaveHelper } from '../../shared/Helpers/SaveHelper';

export class RelationShipHandler {

    constructor(private serviceProvider: ServiceProvider) { }

    public handle(entityName: string, relationFields: string[]): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log('---| prshm:RelationShipHandler.handle:entityName: ', entityName);

            const relationModels: RelationShipHandlerModel[] = [];
            for (const relationField of relationFields) {
                const relationModel = this.createRelationModel(entityName, relationField, this.serviceProvider);
                relationModels.push(relationModel);
            }

            if (relationModels.length <= 0) {
                console.log('---| prshm:RelationShipHandler.handle:relationModel is null for entity: ', entityName);
                resolve();
                return;
            }

            this.executePromisesInSerieWith(relationModels).then(() => {
                console.log('---| prshm:RelationShipHandler.handle Completed for entityName: ', entityName);
                resolve();
            }).catch((error) => {
                console.log('-*-| RelationShipHandler.handle:error: ', error, ' for entityName: ', entityName);
                reject(error);
            });

        });
    }

    private executePromisesInSerieWith(relationModels: RelationShipHandlerModel[]): Promise<any> {
        return new Promise((resolve, reject) => {
            this.executePromise(relationModels, 0, (error) => {

                if (error != null) {
                    reject(error);
                    return;
                }

                resolve();
            });
        });
    }

    private executePromise(relationModels: RelationShipHandlerModel[], index: number, finished: (error) => void) {

        if (index >= relationModels.length) {
            finished(null);
        }

        const relationModel = relationModels[index];
        const promise = this.processRelationShipHandlerModel(relationModel);

        promise.then(() => {
            index++;
            this.executePromise(relationModels, index, finished);
        }).catch((error) => {
            finished(error);
        });

    }


    private createRelationModel(entityName: string, relationField: string, serviceProvider: ServiceProvider): RelationShipHandlerModel {

        const composedEntityName = entityName + ':' + relationField;
        let relationModel: RelationShipHandlerModel = null;

        switch (composedEntityName) {
            case 'Event:WhatId':
                relationModel = {
                    entityNameMaster: 'BR_Sales_Coaching__c',
                    entityNameDetail: 'Event',
                    relationFieldName: 'WhatId',
                    serviceMaster: serviceProvider.getBR_Sales_Coaching__cService(),
                    serviceDetail: serviceProvider.getEventService(),
                };
                
                break;
            case 'Visita__c:EventId__c':
                relationModel = {
                    entityNameMaster: 'Event',
                    entityNameDetail: 'Visita__c',
                    relationFieldName: 'EventId__c',
                    serviceMaster: serviceProvider.getEventService(),
                    serviceDetail: serviceProvider.getVisita__cService(),
                };
                break;
            case 'BR_ProductUsed__c:BR_VisitUsedProduct__c':
                relationModel = {
                    entityNameMaster: 'Visita__c',
                    entityNameDetail: 'BR_ProductUsed__c',
                    relationFieldName: 'BR_VisitUsedProduct__c',
                    serviceMaster: serviceProvider.getVisita__cService(),
                    serviceDetail: serviceProvider.getBR_ProductUsed__cService(),
                };
                break;
            case 'Hibrido_de_Visita__c:Visita__c':
                relationModel = {
                    entityNameMaster: 'Visita__c',
                    entityNameDetail: 'Hibrido_de_Visita__c',
                    relationFieldName: 'Visita__c',
                    serviceMaster: serviceProvider.getVisita__cService(),
                    serviceDetail: serviceProvider.getHibrido_de_Visita__cService(),
                };
                break;
            case 'Attachment:ParentId':
                relationModel = {
                    entityNameMaster: 'Visita__c',
                    entityNameDetail: 'Attachment',
                    relationFieldName: 'ParentId',
                    serviceMaster: serviceProvider.getVisita__cService(),
                    serviceDetail: serviceProvider.getAttachmentService(),
                };
                break;
            case 'Hibrido_de_Visita__c:BR_VisitMobileId__c':
                relationModel = {
                    entityNameMaster: 'Visita__c',
                    entityNameDetail: 'Hibrido_de_Visita__c',
                    relationFieldName: 'BR_VisitMobileId__c',
                    serviceMaster: serviceProvider.getVisita__cService(),
                    serviceDetail: serviceProvider.getHibrido_de_Visita__cService(),
                };
                break;
            case 'Hibrido_de_Visita__c:Hibrido_Visita__c':
                relationModel = {
                    entityNameMaster: 'Hibrido__c',
                    entityNameDetail: 'Hibrido_de_Visita__c',
                    relationFieldName: 'Hibrido_Visita__c',
                    serviceMaster: serviceProvider.getHibrido__cService(),
                    serviceDetail: serviceProvider.getHibrido_de_Visita__cService(),
                };
                break;
            case 'Praga__c:Hibrido_de_Visita__c':
                relationModel = {
                    entityNameMaster: 'Hibrido_de_Visita__c',
                    entityNameDetail: 'Praga__c',
                    relationFieldName: 'Hibrido_de_Visita__c',
                    serviceMaster: serviceProvider.getHibrido_de_Visita__cService(),
                    serviceDetail: serviceProvider.getPraga__cService(),
                };
                break;
            case 'BR_WeedHandling__c:BR_Visit__c':
                relationModel = {
                    entityNameMaster: 'Visita__c',
                    entityNameDetail: 'BR_WeedHandling__c',
                    relationFieldName: 'BR_Visit__c',
                    serviceMaster: serviceProvider.getVisita__cService(),
                    serviceDetail: serviceProvider.getBR_WeedHandling__cService(),
                };
                break;
            case 'BR_CropProducts__c:BR_VisitCrop__c':
                relationModel = {
                    entityNameMaster: 'Visita__c',
                    entityNameDetail: 'BR_CropProducts__c',
                    relationFieldName: 'BR_VisitCrop__c',
                    serviceMaster: serviceProvider.getVisita__cService(),
                    serviceDetail: serviceProvider.getBR_CropProducts__cService(),
                };
                break;
            case 'Doenca__c:Hibrido_de_Visita__c':
                relationModel = {
                    entityNameMaster: 'Hibrido_de_Visita__c',
                    entityNameDetail: 'Doenca__c',
                    relationFieldName: 'Hibrido_de_Visita__c',
                    serviceMaster: serviceProvider.getHibrido_de_Visita__cService(),
                    serviceDetail: serviceProvider.getDoenca__cService(),
                };
                break;
            default:
                break;
        }

        return relationModel;
    }

    private processRelationShipHandlerModel(relationModel: RelationShipHandlerModel): Promise<any> {
        return new Promise((resolve, reject) => {
            const relationFieldName = relationModel.relationFieldName;

            this.getLocallyModifiedDetails(relationModel).then((models: any[]) => {
                const masterExternalIds: string[] = this.cleanDuplicatedRelationFieldId(models, relationFieldName);

                console.log('---| prshm:this.getLocallyModifiedDetails:models: ', models);
                console.log('---| prshm:this.getLocallyModifiedDetails:masterExternalIds: ', masterExternalIds);

                let promise: Promise<any> = Promise.resolve();

                if (masterExternalIds != null && masterExternalIds.length > 0) {
                    promise = this.getMastersWithExternalIDEqualsTo(masterExternalIds, relationModel);
                }

                return promise;
            }).then((masters) => {

                let promises: Array<Promise<any>> = [];

                if (masters != null && masters.length > 0) {
                    promises = this.createPromisesForModels(masters, relationModel);
                }

                console.log('---| prshm:promisesToExecute:count: ', promises.length);
                console.log('---| prshm:this.getMastersWithExternalIDEqualsTo:masters: ', masters);
                return Promise.all(promises);
            }).then(() => {
                console.log('---| prshm:processRelationShipHandlerModel Completed');
                resolve();
            }).catch((error) => {
                console.log('-*-| prshm:processRelationShipHandlerModel:catch:error: ', error);
                reject(error);
            });
        });

    }

    private createPromisesForModels(masters: MasterSmartSQLModel[], relationModel: RelationShipHandlerModel): Array<Promise<any>> {

        const promises: Array<Promise<any>> = [];

        for (const master of masters) {
            const promise = new Promise((resolve, reject) => {
                this.getDetailsWithRelationFieldEqualsToMasterId(master, relationModel).then((details) => {
                    const masterId = master.id;
                    const relationFieldName = relationModel.relationFieldName;
                    const modifiedModels = this.updateRelationFieldWithMasterId(details, relationFieldName, masterId);
                    this.saveDetail(modifiedModels, relationModel).then((savedModels) => {
                        resolve(savedModels);
                    }).catch((error) => {
                        reject(error);
                    });
                }).catch((error) => {
                    reject(error);
                });
            });

            promises.push(promise);
        }

        return promises;
    }

    private smartSQLForLocallyModifiedDetails(entityNameDetail: string, relationField: string): string {

        const smartSQL = `SELECT {${entityNameDetail}:${relationField}}
        FROM {${entityNameDetail}}
        WHERE {${entityNameDetail}:${relationField}} LIKE 'local%'`;

        return smartSQL;
    }

    private getLocallyModifiedDetails(relationModel: RelationShipHandlerModel): Promise<any[]> {
        return new Promise((resolve, reject) => {
            const entityName = relationModel.entityNameDetail;
            const relationFieldName = relationModel.relationFieldName;
            const serviceDetail = relationModel.serviceDetail;
            const smartSQL = this.smartSQLForLocallyModifiedDetails(entityName, relationFieldName);

            console.log('---| prshm:getLocallyModifiedDetails:smartSQL: ', smartSQL, ' for entity: ', entityName);

            serviceDetail.querySmartFromSoup(smartSQL, 100, (results) => {

                const models: any[] = [];
                for (const entry of results.currentPageOrderedEntries) {
                    const model = {};
                    model[relationFieldName] = entry[0];
                    models.push(model);
                }

                console.log('---| prshm:getLocallyModifiedDetails:relationFieldName: ', relationFieldName, ' for entity: ', entityName);
                console.log('---| prshm:getLocallyModifiedDetails:models: ', models, ' for entity: ', entityName);
                resolve(models);
            }, (error) => {
                console.log('-*-| getLocallyModifiedModels:error: ', error);
                reject(error);
            });
        });
    }

    private cleanDuplicatedRelationFieldId(models: any[], relationField: string): string[] {

        console.log('---| prshm:cleanDuplicatedRelationFieldId:models: ', models);

        const set: Set<string> = new Set<string>();
        for (const model of models) {
            set.add(model[relationField]);
        }

        const array = Array.from(set.values());
        console.log('---| prshm:cleanDuplicatedRelationFieldId:set: ', set);
        console.log('---| prshm:cleanDuplicatedRelationFieldId:relationField: ', relationField);
        console.log('---| prshm:cleanDuplicatedRelationFieldId:array: ', array);

        return array;
    }

    private smartSQLForMastersWithExternalID(entityNameMaster: string, masterIds: string[]): string {

        const smartSQL = `SELECT {${entityNameMaster}:Id},{${entityNameMaster}:ExternalID__c}
        FROM {${entityNameMaster}}
        WHERE {${entityNameMaster}:ExternalID__c} IN (\"${masterIds.join('\",\"')}\")`;

        return smartSQL;
    }

    private getMastersWithExternalIDEqualsTo(masterExternalIds: string[], relationModel: RelationShipHandlerModel): Promise<MasterSmartSQLModel[]> {
        return new Promise((resolve, reject) => {
            const entityName = relationModel.entityNameMaster;
            const relationFieldName = relationModel.relationFieldName;
            const serviceMaster = relationModel.serviceMaster;
            const smartSQL = this.smartSQLForMastersWithExternalID(entityName, masterExternalIds);

            console.log('---| prshm:getMastersWithExternalIDEqualsTo:smartSQL: ', smartSQL);

            serviceMaster.querySmartFromSoup(smartSQL, 100, (results) => {

                console.log('---| prshm:getMastersWithExternalIDEqualsTo:querySmartFromSoup:results: ', results);

                const models: MasterSmartSQLModel[] = [];
                for (const entry of results.currentPageOrderedEntries) {
                    const model: MasterSmartSQLModel = {
                        id: entry[0],
                        externalId: entry[1],
                    };
                    models.push(model);
                }

                console.log('---| prshm:getMastersWithExternalIDEqualsTo:entityName: ', entityName);
                console.log('---| prshm:getMastersWithExternalIDEqualsTo:relationFieldName: ', relationFieldName);
                console.log('---| prshm:getMastersWithExternalIDEqualsTo:models: ', models);
                resolve(models);
            }, (error) => {
                console.log('-*-| getMastersWithExternalIDEqualsTo:error: ', error);
                reject(error);
            });
        });
    }

    private getDetailsWithRelationFieldEqualsToMasterId(master: MasterSmartSQLModel, relationModel: RelationShipHandlerModel): Promise<BaseModel[]> {
        return new Promise((resolve, reject) => {
            const serviceDetail = relationModel.serviceDetail;
            const masterExternalId = master.externalId;
            const relationFieldName = relationModel.relationFieldName;

            serviceDetail.queryExactFromSoup(relationFieldName, masterExternalId, 100, 'ascending', relationFieldName, (data: ResponseDAO) => {

                const models: BaseModel[] = [];
                for (const modelData of data.currentPageOrderedEntries) {
                    console.log('---| prshm:getDetailsWithRelationFieldEqualsToMasterId:relationFieldName: ', relationFieldName);
                    console.log('---| prshm:getDetailsWithRelationFieldEqualsToMasterId:modelData: ', modelData);
                    const model = serviceDetail.transformEntrie(modelData);
                    console.log('---| prshm:getDetailsWithRelationFieldEqualsToMasterId:model: ', model);
                    models.push(model);
                }
                console.log('---| prshm:getDetailsWithRelationFieldEqualToMasterId:models: ', models);
                resolve(models);
            }, (error) => {
                console.log('-*-| getDetailsWithRelationFieldEqualToMasterId:error: ', error);
                reject(error);
            });
        });
    }

    private updateRelationFieldWithMasterId(models: BaseModel[], relationField: string, masterId: string): BaseModel[] {
        const modifiedModels: BaseModel[] = Object.assign([], models);

        for (const modifiedModel of modifiedModels) {
            modifiedModel[relationField] = masterId;
            SaveHelper.update_Local_(modifiedModel);
        }

        console.log('---| prshm:updateRelationFieldWithMasterId:models: ', models);
        console.log('---| prshm:updateRelationFieldWithMasterId:relationField: ', relationField);
        console.log('---| prshm:updateRelationFieldWithMasterId:masterId: ', masterId);
        console.log('---| prshm:updateRelationFieldWithMasterId:modifiedModels: ', modifiedModels);

        return modifiedModels;
    }

    private saveDetail(models: BaseModel[], relationModel: RelationShipHandlerModel): Promise<any> {
        return new Promise((resolve, reject) => {
            const serviceDetail = relationModel.serviceDetail;

            console.log('---| prshm:saveDetail:models: ', models);
            console.log('---| prshm:saveDetail:relationModel: ', relationModel);

            serviceDetail.upsertSoupEntries(models, (savedModels) => {
                console.log('---| prshm:saveDetail:savedModels: ', savedModels);
                resolve(savedModels);
            }, (error) => {
                console.log('---| prshm:saveDetail:error: ', error);
                reject(error);
            });
        });
    }

}

interface RelationShipHandlerModel {
    entityNameMaster: string;
    entityNameDetail: string;
    relationFieldName: string;
    serviceMaster: IService;
    serviceDetail: IService;
}

interface MasterSmartSQLModel {
    id: string;
    externalId: string;
}
