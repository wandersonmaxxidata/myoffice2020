import { AlertController, App, NavController, NavParams, Platform } from 'ionic-angular';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { AppPreferences } from '@ionic-native/app-preferences';
import { BR_SincronizedVisit__cModel } from '../../app/model/BR_SincronizedVisit__cModel';
import { DataErrorModel } from './../../app/model/DataErrorModel';
import { DataModel } from './../../app/model/DataModel';
import { ShildDataModel } from './../../app/model/ShildDataModel';
import { Component, NgZone } from '@angular/core';
import { LoadingManager } from '../../shared/Managers/LoadingManager';
import { Entity, ServiceGroupingHelper, ServiceModel } from '../../shared/Helpers/ServiceGroupingHelper';
import { Events } from 'ionic-angular/util/events';
import { HomePage } from '../../pages/home/home';
import { Insomnia } from '@ionic-native/insomnia';
import { IPicklistItem } from '../../interfaces/IPicklistItem';
import { IService } from './../../interfaces/IService';
import { LogHelper } from '../../shared/Helpers/LogHelper';
import { MustEditRegisterHelper } from '../../shared/Helpers/MustEditRegisterHelper';
import { Network } from '@ionic-native/network';
import { LocalStorageKeys, NotificationKeys } from '../../shared/Constants/Keys';
import { RelationShipHandler } from './relationship-handler';
import { ServiceProvider } from './../../providers/service-salesforce';
import { DataDaoProvider } from './../../providers/data-dao/data-dao';
import { DataErrorDaoProvider } from './../../providers/data-error-dao/data-error-dao';
import { ErrorLogProvider } from './../../providers/error-log/error-log';
import { SmartSQLTests } from '../../shared/SmartSQLTests';
import { Storage } from '@ionic/storage';
import { TabHelper } from '../../shared/Helpers/TabHelper';
import { WindowStorageHelper } from '../../shared/Helpers/WindowStorageHelper';
import * as moment from 'moment';
import { Observable } from 'rxjs/Rx';
import { resolveDep } from '@angular/core/src/view/provider';

export const SynchronismStatus = {
  completed: 'completed',
  failed: 'failed',
  idle: 'idle',
  inProgress: 'inProgress',
};

export const SynchronismStatusClasses = {
  completed: 'completed',
  failed: 'failed',
  idle: 'idle',
  inProgress: 'in-progress',
};

export const SynchronismStatusIcons = {
  completed: './assets/icon/synchronism/check-copy@3x.png',
  failed: './assets/icon/synchronism/check-copy-2@3x.png',
  idle: './assets/icon/salesforce/account.png',
  inProgress: './assets/icon/synchronism/check@2x.png',
};

const SynchronismTimeout = {
  picklistsTimeout: 60000,
  syncDownTimeout: 60000,
  syncUpTimeout: 90000,
};

const SynchronismTimeoutWithoutWifi = {
  picklistsTimeout: 110000,
  syncDownTimeout: 120000,
  syncUpTimeout: 180000,
};

export const TimeoutPromise = (ms, promise) => {

  // Create a promise that rejects in <ms> milliseconds
  const timeout = new Promise((resolve, reject) => {
    const id = setTimeout(() => {
      clearTimeout(id);
      reject('Timed out in ' + ms + 'ms.');
    }, ms);
  });

  // Returns a race between our timeout and the passed in promise
  return Promise.race([
    promise,
    timeout,
  ]);
};

@Component({
  selector: 'synchronism',
  templateUrl: 'synchronism.html',
})
export class SynchronismComponent {

  public syncSharpener: any;
  public observableSyncyUp: any;
  public waitSyncOnebyOne: boolean = false;

  public syncError: boolean;
  public closedSync: boolean;
  public cancelSync: boolean;
  public networkType: string;
  public totalTimeSync: any;
  public totalTime: number;
  public isNotFisrtSync: boolean;
  public errorSyncAttachment: boolean;
  public errorTimeOutSyncAttachment: boolean;
  public uiModelsMapping = {};
  public uiModelsMappingKeys = Object.keys(this.uiModelsMapping);
  public progressFeedbackMessage = '';
  public progressFeedbackError = '';
  public progress = 0;
  public isOnline = true;
  public syncCanceled: boolean;

  public progressFeedbackNoConnectionMessage = 'Sem conexão';
  public progressFeedbackInitialMessage = 'Preparando sincronização...';
  public progressFeedbackSuccessMessage = 'Sincronização concluída';
  public progressFeedbackTryAgainMessage = 'Tente sincronizar novamente';
  public progressFeedbackConnectionTimeout = 'Tempo de conexão esgotado';
  public progressFeedbackConnectionError = 'Erro de conexão com o Salesforce. Por favor, tente novamente';
  public progressFeedbackSalesforceError = 'Favor entrar em contato com o administrador do Salesforce.';
  public permissionErrorMessage = '';

  public entities: Entity[] = [];
  public attachmentSyncRunning = false;
  public syncStatusClassSuffix = '';
  public syncRunning = false;
  public synchronizedWithError = false;
  public entity = '';
  public progressStyle: any;
  public showSpinner: any;
  public syncMore = 'VER MAIS';
  public showMore = true;
  public serviceModels: ServiceModel[] = [];
  public contTimeSync;
  public instanceServer: string;
  public instanceUrl: string;
  public acessToken: string;
  public attachmentSync: boolean;
  public attachmentProgress = 0;
  public countObjSync = 0;

  constructor(
    public app: App,
    public network: Network,
    public navCtrl: NavController,
    public appPreferences: AppPreferences,
    public navParams: NavParams,
    public serviceProvider: ServiceProvider,
    public zone: NgZone,
    public events: Events,
    public platform: Platform,
    public storage: Storage,
    public insomnia: Insomnia,
    public alertCtrl: AlertController,
    public dataDAO: DataDaoProvider,
    public dataErrorDAO: DataErrorDaoProvider,
    public errorLogProvider:ErrorLogProvider
  ) {
    this.showSpinner = true;
    this.errorSyncAttachment = false;
    this.attachmentSync = false;
    this.errorTimeOutSyncAttachment = false;
  }

  public ngOnInit(): void {
    console.log('------| Events: synchronism : ### unsubscribe ###');

    this.events.unsubscribe(NotificationKeys.startSync);
    this.events.unsubscribe(NotificationKeys.errorSync);
    this.events.unsubscribe(NotificationKeys.endSync);
  }

  public ngAfterContentInit() {
    console.log('------| Events: synchronism : !!! subscribe !!!');

    this.events.subscribe(NotificationKeys.startSync, () => {
      this.events.publish(NotificationKeys.startSyncFromProgressBar);
      this.events.publish(NotificationKeys.startSyncFromHome);

      this.countObjSync = 0;
      this.cancelSync = false;
      this.closedSync = false;
      this.insomnia.keepAwake();

      SmartSQLTests.testVisitsEventID(this.serviceProvider.getVisita__cService());
      this.eraseSyncStatus();

      if (!this.isInternetAvailable()) {
        if (!this.synchronizedWithError) {
          this.events.publish(NotificationKeys.errorSync);
        }
      } else {
        if (!this.syncRunning) {
          clearInterval(this.totalTime);
          this.contTimeSync = '';
          this.startSyncProcess();
          this.syncRunning = true;
          this.syncCanceled = false;
          this.synchronizedWithError = false;
          this.syncStatusClassSuffix = '';
        }
      }
    });

    this.events.subscribe(NotificationKeys.errorSync, () => {
      this.syncStatusClassSuffix = '-error';
      this.synchronizedWithError = true;
      this.syncRunning = false;
      this.cancelSync = true;
      clearInterval(this.totalTime);
      setTimeout(() => this.cancelSync = true, 1000);
      this.events.publish(NotificationKeys.errorSyncFromProgressBar);
    });

    this.events.subscribe(NotificationKeys.endSync, () => {
      MustEditRegisterHelper.cleanObjectName();
      this.closeSync();
    });

    this.network.onchange().subscribe((status) => {
      if (status.type === 'online') {
        this.isOnline = true;
      } else if (status.type === 'offline') {
        this.isOnline = false;
      } else {
        this.isOnline = true;
      }
    });
  }

  public showAlertCancelSync() {
    AlertHelper.showConfirmCancelSyncAlert(this.alertCtrl, () => {
      this.syncCanceled = true;
      this.closeSync();
    }, () => {
      console.log('cancel');
    });
  }

  public handleProgress(progress: number) {
    if (!this.synchronizedWithError) {
      this.zone.run(() => {
        if (progress !== 0) {
          this.showSpinner = false;
        }
        this.progress = progress;
        this.updateInProgressFeedbackMessage(progress);
      });
    }
  }

  public startSyncProcess() {
    this.networkType = this.network.type.toUpperCase();

    const temp = this;
    let countTime = 0;
    this.totalTime = setInterval(() => {
      temp.contTimeSync = moment.utc(countTime++ * 1000).format('HH:mm:ss');
    }, 1000);

    TabHelper.disableAll(this.navCtrl);
    this.progressFeedbackMessage = this.progressFeedbackInitialMessage;
    this.progressFeedbackError = '';

    this.entities = ServiceGroupingHelper.loadEntitiesFromTS();
    this.loadUiModelsMappingsFromEntities(this.entities);
    this.serviceModels = ServiceGroupingHelper.loadServicesFromEntities(this.entities, this.serviceProvider);

    this.eraseSyncStatus();
    this.continueSyncProcess();
  }

  private saveSincronizedVisit(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.zone.run(() => {
        this.appPreferences.fetch('user_id').then((id) => {
          const generateReport = new BR_SincronizedVisit__cModel(null);
          const userId = id;
          console.log('---| synchronism:saveSincronizedVisit:userId: ', userId);
          generateReport.BR_RtvID__c = userId;
          if (generateReport.BR_RtvID__c && generateReport.BR_RtvID__c.length > 0) {
            const synchronizedService = this.serviceProvider.getBR_SincronizedVisit__cService();
            synchronizedService.upsertSoupEntries([generateReport], () => {
              const Promise = TimeoutPromise(SynchronismTimeout.picklistsTimeout, synchronizedService.syncUp(true, (value) => { console.log('---| synchronism:synchronizedService.syncUp:value: ', value); }, this));
              Promise.then(() => {
                this.zone.run(() => {
                  this.attachmentSync = false;
                });
                resolve();
              }).catch((error) => {
                console.log('-*-| synchronism:saveSincronizedVisit.syncUp:error: ', error);
                this.zone.run(() => {
                  this.errorTimeOutSyncAttachment = true;
                });
                reject(error);
              });
            }, (error) => {
              console.log('-*-| synchronism:saveSincronizedVisit.upsertSoupEntries:error: ', error);
              this.zone.run(() => {
                this.errorSyncAttachment = true;
              });
              reject(error);
            });

          } else {
            console.log('-*-| synchronism:saveSincronizedVisit:generateReport.BR_RtvID__c: ', generateReport.BR_RtvID__c);
            this.zone.run(() => {
              this.errorSyncAttachment = true;
            });
            const error = Error('generateReport.BR_RtvID__c Id not found');
            reject(error);
          }
        });
      });
    });
  }

  private isInternetAvailable(): boolean {
    if (!this.isOnline) {
      this.progressFeedbackMessage = this.progressFeedbackNoConnectionMessage;
      return false;
    }
    return true;
  }

  private continueSyncProcess() {
    this.serviceProvider.autenticateService.getCurrentUser().then((success) => {
      if (success) {
        const hasWhere = this.serviceProvider.userService.targetSync.query.search('where');
        if (!hasWhere) {
          this.serviceProvider.userService.targetSync.query += ' where Id = \'' + success.userId + '\'';
        }
      } else {
        const hasWhere = this.serviceProvider.userService.targetSync.query.search('where');
        if (!hasWhere) {
          this.appPreferences.fetch('user_id').then((value) => {
            this.zone.run(() => {
              this.serviceProvider.userService.targetSync.query += ' where Id = \'' + value + '\'';
            });
          }).catch((err) => {
            console.log('-*-| USER ID APPPREFERENCES ERROR: ', err);
          });
        }
      }

      const promises: Array<Promise<any>> = this.loadEntitiesForPicklists(success);
      const picklistTimeoutPromise = TimeoutPromise(SynchronismTimeout.picklistsTimeout, Promise.all(promises));

      picklistTimeoutPromise.then(() => {
        if (this.permissionErrorMessage && this.permissionErrorMessage.length > 0) {
          throw (this.permissionErrorMessage);
        } else {
          const getUserTimeoutPromise = TimeoutPromise(SynchronismTimeout.picklistsTimeout, this.serviceProvider.getUserService().syncDown(this.handleProgress, this));
          return getUserTimeoutPromise;
        }
      }).then(() => {
        this.startSync();

      }).catch((error) => {
        this.checkThisError(error);
        console.log(error, '---------------------------------picklistTimeoutPromise####');
      });
    });
  }

  private loadUiModelsMappingsFromEntities(entities: Entity[]) {
    for (const entity of entities) {
      const uiModel: SynchronismUiModel = new SynchronismUiModel(entity.Label, entity.IsMaleGender);
      this.uiModelsMapping[entity.Label] = uiModel;
    }

    console.log('------| synchronism:All uiModelsMapping:', this.uiModelsMapping);
    this.uiModelsMappingKeys = Object.keys(this.uiModelsMapping);
  }

  private getUiModelBySynchronismServiceModel(synchronismServiceModel: ServiceModel): SynchronismUiModel {
    const uiModel = this.uiModelsMapping[synchronismServiceModel.entityLabel];
    return uiModel;
  }

  private updateItemStatusIdle(model: SynchronismUiModel) {
    if (model != null) {
      model.itemStatusIcon = SynchronismStatusIcons.idle;
      model.itemStatusText = 'Sincronizando ' + model.entityName.toLowerCase() + '...';
      model.itemStatusClass = SynchronismStatusClasses.idle;
    }
  }

  private updateItemStatusInProgress(model: SynchronismUiModel) {
    if (model != null) {
      model.itemStatusIcon = SynchronismStatusIcons.inProgress;
      model.itemStatusText = 'Sincronizando ' + model.entityName.toLowerCase() + '...';
      model.itemStatusClass = SynchronismStatusClasses.inProgress;
    }
  }

  private updateItemStatusFailed(model: SynchronismUiModel) {
    if (model != null) {
      model.itemStatusIcon = SynchronismStatusIcons.failed;
      model.itemStatusText = 'Falha na sincronização de ' + model.entityName.toLowerCase();
      model.itemStatusClass = SynchronismStatusClasses.failed;
      clearInterval(this.totalTime);

      this.logSDKError();
    }
  }

  private logSDKError() {
    LogHelper.SDK().then((log: any) => {
      if (log && log[0].message) {
        AlertHelper.showMessageError(this.alertCtrl, log[0].message);
      }
    });
  }

  private updateItemStatusCompleted(model: SynchronismUiModel) {
    if (model != null) {
      const genderCharactere = model.isMaleGender ? 'o' : 'a';
      model.itemStatusIcon = SynchronismStatusIcons.completed;
      model.itemStatusText = model.entityName + ' sincronizad' + genderCharactere + 's';
      model.itemStatusClass = SynchronismStatusClasses.completed;
    }
  }

  private updateInProgressFeedbackMessage(progress: number) {
    this.progressFeedbackMessage = progress + '% sincronizados';
  }

  public checkRotate(status: string) {
    return (status === SynchronismStatusClasses.inProgress);
  }

  private eraseSyncStatus() {
    for (const uiModelKey of Object.keys(this.uiModelsMapping)) {
      const uiModel: SynchronismUiModel = this.uiModelsMapping[uiModelKey];
      this.updateItemStatusIdle(uiModel);
    }
  }


  private closeSync() {
    return new Promise((resolve) => {
      this.stopObservableSyncyUp();
      this.storage.set(LocalStorageKeys.milliNextSyncKey, 450).then(() => {
        TabHelper.enableAll(this.navCtrl);
        clearInterval(this.totalTime);

        // this.dataErrorDAO.existsErrors().then(dataRet => {
        //   this.syncError = dataRet > 0 ? true : false;
          this.errorLogProvider.getVisitNotInEvent().then(()=>{
            this.zone.run(() => {
              //MAXXIDATA
              this.events.publish(NotificationKeys.closeSync);
              this.navCtrl.setRoot(HomePage).then(() => {
                this.closedSync = true;
                this.cancelSync = false;
                this.synchronizedWithError = false;
                this.syncRunning = false;
                this.insomnia.allowSleepAgain();
                // this.events.publish('openSyncErr');
                resolve();
              // });

              //FIM MAXXIDATA

              // this.events.publish(NotificationKeys.closeSync);

              // this.navCtrl.setRoot(HomePage).then(() => {
              //   this.closedSync = true;
              //   this.cancelSync = false;
              //   this.synchronizedWithError = false;
              //   this.syncRunning = false;
              //   this.insomnia.allowSleepAgain();
              //   resolve();
              // });
            });
          });
        });
      });
    });
  }


  private checkThisError(error) {
    console.log('-*-| synchronism:checkThisError:error: ', error);
    if (typeof (error) === 'string') {
      if (error.toLowerCase().includes('timeout')) {
        this.events.publish(NotificationKeys.errorSync);
        this.progressFeedbackMessage = this.progressFeedbackConnectionTimeout;

      } else if (error.toLowerCase().includes('timed out')) {
        this.events.publish(NotificationKeys.errorSync);
        this.progressFeedbackMessage = this.progressFeedbackConnectionTimeout;

      } else if (error.toLowerCase().includes('certification path not found')) {
        this.events.publish(NotificationKeys.errorSync);
        this.progressFeedbackMessage = this.progressFeedbackTryAgainMessage;
        AlertHelper.showMessageError(this.alertCtrl, this.progressFeedbackConnectionError);

      } else if (error.toLowerCase().includes('erro de permissão')) {
        this.events.publish(NotificationKeys.errorSync);
        this.progressFeedbackMessage = this.progressFeedbackSalesforceError;
        AlertHelper.showMessageError(this.alertCtrl, error);
      }
    } else {
      this.events.publish(NotificationKeys.errorSync);
      this.progressFeedbackMessage = this.progressFeedbackSalesforceError;
      this.logSDKError();
    }
  }

  private startSync() {
    localStorage.setItem('lastSyncErros', '0');
    const startIndex = 0;

    this.executeSynchronismServiceModels(this.serviceModels, startIndex, (error) => {
      if (error) {
        this.checkThisError(error);
      } else {
        this.saveSincronizedVisit().then(() => {
          this.postSynchronismSuccess();
        }).catch((err) => {
          this.checkThisError(error);
        });
      }
    });
  }

  private postSynchronismSuccess() {
    if (!this.closedSync) {
      TabHelper.enableAll(this.navCtrl);
      this.zone.run(() => {
        this.progressFeedbackMessage = this.progressFeedbackSuccessMessage;
      });

      const currentDate = new Date();
      localStorage.setItem(LocalStorageKeys.lastSyncDateKey, currentDate.toString());

      this.zone.run(() => {
        clearInterval(this.totalTime);
        this.totalTimeSync = this.contTimeSync;
        TabHelper.popTabNavigationController(this.navCtrl, 2);
        this.appPreferences.store('totalTimeSync', this.totalTimeSync);
        this.appPreferences.store('isFisrtSync', 'true');
        this.events.publish(NotificationKeys.totalNotifications);
        this.events.publish(NotificationKeys.endSync);
        this.insomnia.allowSleepAgain();
      });
      this.setSuccessPreferencesTags().then(() => {
        this.showMore = true;
        this.syncMore = 'VER MAIS';
      }).catch((error) => {
        console.log('-*-| synchronism:setSuccessPreferencesTags:error: ', error);
      });
    }
  }

  //MAXXIDATA
  //Gerando storage sincyUp
  createStorageSyncUp(table, serviceModel) {
    // serviceModel.service.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('BASE ATUAL =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });

    console.log(serviceModel)
    console.log('CRIANDO STORAGE UM A UM ===============================')
    // console.log(table)
    sessionStorage.setItem('statusSync' + table, 'DONE')
    return new Promise<any>((resolve, reject) => {
      let sql:any;
      if (this.platform.is('ios')) {
        sql = ' select * from {' + table + '} where {' + table + ':__locally_created__} = "1" or {' + table + ':__locally_updated__} = "1" or {' + table + ':__locally_deleted__} = "1"  ';
      }else{
        sql = ' select * from {' + table + '} where {' + table + ':__locally_created__} = "true" or {' + table + ':__locally_updated__} = "true" or {' + table + ':__locally_deleted__} = "true"  ';
      }
      serviceModel.service.querySmartFromSoup(sql, 1000, (data) => {
       // console.log('STORAGE ATUAL ===============================')
        // console.log(data.currentPageOrderedEntries)
        if (serviceModel.entityLabel == "Eventos de Geração de Demanda") {
        } else {
          this.storage.set(table, data.currentPageOrderedEntries);
        }

        let newStatus = data.currentPageOrderedEntries.map((data) => {
          return [{ id: data[1].Id, __local__: data[1].__local__, __locally_created__: data[1].__locally_created__, __locally_deleted__: data[1].__locally_deleted__, __locally_updated__: data[1].__locally_updated__ }];

        })
        // console.log(newStatus)
        if (serviceModel.entityLabel == "Eventos de Geração de Demanda") {
        } else {
          sessionStorage.setItem('temp' + table, JSON.stringify(newStatus));
        }

        // console.log(JSON.parse(sessionStorage.getItem('temp'+table)))


        sessionStorage.setItem('contSync' + table, String(data.currentPageOrderedEntries.length));
        this.syncSharpener = data.currentPageOrderedEntries.length;
        resolve(data.currentPageOrderedEntries)
      }, (error) => {
        // console.log('ERRO SET TABLE UP=======================================================================')
        reject(error);
      });
    });
  }

  statusNoneAll(currentStorage, serviceModel) {
    return new Promise<any>((resolve, reject) => {
      serviceModel.service.statusNoneAll(currentStorage).then(dataReturn => {
        resolve(dataReturn)
      })
    });
  }



  //Iniciando syncUp
  syncUpStart(dataStatus, includeSecondaryIdFields, serviceModel, handleProgress, serviceModels, nextIndex, finished, uiModel, context) {

    console.log('INICIANDO UP VISITAS ===============================')
    return new Promise<any>((resolve, reject) => {
      ///handleProgress(10);
      var newdataSy: any;

      this.observableSyncyUp = Observable.interval(600).subscribe(x => {
        let syncCurrentSharpener = parseInt(sessionStorage.getItem('contSync' + serviceModel.entityName));
        console.log('DATA INDICADOR')
        console.log(syncCurrentSharpener)
        console.log(this.syncSharpener)
        console.log(sessionStorage.getItem('statusSync' + serviceModel.entityName))


        if (syncCurrentSharpener === this.syncSharpener) {
          if (syncCurrentSharpener === 0) {
            this.waitSyncOnebyOne = false;
            this.stopObservableSyncyUp();
            // if(serviceModel.entityName != 'Visita__c'){
            this.startSncyNextOneOne(serviceModel, nextIndex, serviceModels, finished, uiModel);
            // }
            resolve();
          } else {
            console.log('MANDANDO SALESFORCE LINHA ')
           // console.log(sessionStorage.getItem('statusSync' + serviceModel.entityName))
            if (sessionStorage.getItem('statusSync' + serviceModel.entityName) === 'DONE') {
              //  setTimeout(() => {
              if (parseInt(sessionStorage.getItem('contSync' + serviceModel.entityName)) > 0) {
                serviceModel.service.syncUpOnebyOne(includeSecondaryIdFields, serviceModel.entityName, handleProgress, context).then(dataSy => {
                  newdataSy = dataSy;
                  sessionStorage.setItem('contSync' + serviceModel.entityName, String(dataSy));
                  sessionStorage.setItem('statusSync' + serviceModel.entityName, 'DONE');
                  syncCurrentSharpener = dataSy;
                  this.syncSharpener = dataSy;
                });
              } else {
                this.waitSyncOnebyOne = false;
                this.stopObservableSyncyUp();
                // if(serviceModel.entityName != 'Visita__c'){
                this.startSncyNextOneOne(serviceModel, nextIndex, serviceModels, finished, uiModel);
                // }
                resolve();
              }
              //  }, 200);
            }
          }
        }
      });

    });
  }

  stopObservableSyncyUp() {
    if(this.observableSyncyUp){
      this.observableSyncyUp.unsubscribe();
    }
  }
  //FIM MAXXIDATA 

  syncNone() {
    return new Promise((resolve, reject) => {
      resolve();
    });
  }


  private executeSynchronismServiceModels(serviceModels: ServiceModel[], nextIndex: number, finished: (error) => void) {

    //console.log('MODEL ATUAL SYNC============================================================================================================' + this.serviceModels.length + nextIndex);
    if (nextIndex > this.serviceModels.length - 1) {
      finished(null);
      return;
    }

    if (this.syncCanceled) {
      finished(null);
      return;
    }

    if (!this.isInternetAvailable()) {
      console.log('-*-| synchronism:No connection');
      const error = new Error('No connection');
      finished(error);
      return;
    }

    const serviceModel = serviceModels[nextIndex];
    const uiModel = this.getUiModelBySynchronismServiceModel(serviceModel);

    this.updateItemStatusInProgress(uiModel);
    setTimeout(() => this.cancelSync = true, 1000);

    console.log('---| synchronism: Start 1 sync down for: ', serviceModel.entityName);
    const syncDownTimeoutPromise = TimeoutPromise(SynchronismTimeout.syncDownTimeout, serviceModel.service.syncDown(this.handleProgress, this));
    //  const syncDownTimeoutPromise = TimeoutPromise(SynchronismTimeout.syncDownTimeout, this.syncNone());
    syncDownTimeoutPromise.then(() => {
      const relationShipHandler = new RelationShipHandler(this.serviceProvider);
      return relationShipHandler.handle(serviceModel.entityName, serviceModel.relationFields);

    }).then(() => {
      console.log('---| synchronism: Start sync Up entity: ', serviceModel.entityName);
      if (this.network.type.toUpperCase() === 'WIFI') {
        // const syncUpTimeoutPromise = TimeoutPromise(SynchronismTimeoutWithoutWifi.syncUpTimeout, serviceModel.service.syncUp(true, this.handleProgress, this));
        // return syncUpTimeoutPromise;
        // MAXXIDATA
        this.createStorageSyncUp(serviceModel.entityName, serviceModel).then(data => {
          this.waitSyncOnebyOne = true;
          //console.log('UM A UM VISITAS============================================================================================================');
          this.syncUpStart(data, true, serviceModel, this.handleProgress, serviceModels, nextIndex, finished, uiModel, this).then(returnSyncUp => {
          })
        });
        //FIM MAXXIDATA 

      } else {
        // const syncUpTimeoutPromise = TimeoutPromise(SynchronismTimeoutWithoutWifi.syncUpTimeout, serviceModel.service.syncUp(true, this.handleProgress, this));
        // return syncUpTimeoutPromise;
        // MAXXIDATA
        this.createStorageSyncUp(serviceModel.entityName, serviceModel).then(data => {
          this.waitSyncOnebyOne = true;
          //console.log('UM A UM VISITAS============================================================================================================');
          this.syncUpStart(data, true, serviceModel, this.handleProgress, serviceModels, nextIndex, finished, uiModel, this).then(returnSyncUp => {
          })
        });
        //FIM MAXXIDATA 

      }


    })
      // .then(() => {
      //   //CONTROLE LOOP EM LOTE PROXIMO
      //   //    if(!this.waitSyncOnebyOne){
      //   // console.log('---| synchronism: Start 2 sync Down entity: ', serviceModel.entityName);
      //   // const syncDownTimeoutPromiseTemp = TimeoutPromise(SynchronismTimeout.syncDownTimeout, serviceModel.service.syncDown(this.handleProgress, this));
      //   // return syncDownTimeoutPromiseTemp;
      //   //    }

      // }).then(() => {
      //      //CONTROLE LOOP EM LOTE PROXIMO
      //   //    if(!this.waitSyncOnebyOne){
      //   // this.countObjSync += 1;
      //   // return this.cleanGhostRecords(serviceModel.service);
      //   //    }

      // }).then(() => {
      //   //CONTROLE LOOP EM LOTE PROXIMO
      //   // if(!this.waitSyncOnebyOne){
      //   // nextIndex += 1;
      //   // this.updateItemStatusCompleted(uiModel);

      //   // return this.executeSynchronismServiceModels(serviceModels, nextIndex, finished);
      //   // }

      // })
      .catch((error) => {
        console.log('-*-| synchronism:Sync error: ', JSON.stringify(error), ' for model: ', JSON.stringify(uiModel));

        if (this.isInternetAvailable) {
          this.progressFeedbackMessage = this.progressFeedbackTryAgainMessage;
        }

        if (typeof (error) === 'string' && error.toLowerCase().includes('timed out')) {
          //CONTROLE LOOP EM LOTE PROXIMO ERRO
          this.executeSynchronismServiceModels(serviceModels, nextIndex, finished);
        } else {
          this.updateItemStatusFailed(uiModel);
          finished(error);
        }
      });
  }

  //MAXXIDATA
  private startSncyNextOneOne(serviceModel, nextIndex, serviceModels, finished, uiModel) {
    console.log('---| synchronism: Start 2 sync Down entity: ', serviceModel.entityName);
    const syncDownTimeoutPromiseTemp = TimeoutPromise(SynchronismTimeout.syncDownTimeout, serviceModel.service.syncDown(this.handleProgress, this));
    syncDownTimeoutPromiseTemp.then(() => {
      this.countObjSync += 1;
      return this.cleanGhostRecords(serviceModel.service);
    }).then(() => {
      nextIndex += 1;
      this.updateItemStatusCompleted(uiModel);
      return this.executeSynchronismServiceModels(serviceModels, nextIndex, finished);
    }).catch((error) => {
      console.log('-*-| synchronism:Sync error: ', JSON.stringify(error), ' for model: ', JSON.stringify(uiModel));

      if (this.isInternetAvailable) {
        this.progressFeedbackMessage = this.progressFeedbackTryAgainMessage;
      }

      if (typeof (error) === 'string' && error.toLowerCase().includes('timed out')) {
        //CONTROLE LOOP EM LOTE PROXIMO ERRO
        this.executeSynchronismServiceModels(serviceModels, nextIndex, finished);
      } else {
        // this.executeSynchronismServiceModels(serviceModels, nextIndex, finished);
        this.updateItemStatusFailed(uiModel);
        finished(error);
      }
    });

  }

  //FIM MAXXIDATA

  private getUser(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.serviceProvider.getUserService().queryAllFromSoup('Id', 'descending', 10, (response) => {
        resolve(response);
      }, (err) => {
        console.log(err);
      });
    });
  }

  private setErrorPreferencesTags(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.appPreferences.store('syncOk', 'error').then((value) => {
        resolve(value);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  private setSuccessPreferencesTags(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.appPreferences.store('syncOk', 'success').then((value) => {
        resolve();
      }).catch((err) => {
        reject(err);
      });
    });
  }

  private handleZone(syncCurrentIconName: string, entity: string) {
    this.zone.run(() => {
      this.entity = entity;
    });
  }

  private loadEntitiesForPicklists(autenticateServiceResponse): Array<Promise<any>> {
    const entities: Entity[] = ServiceGroupingHelper.loadEntitiesFromTS();
    const picklistPromises: Array<Promise<any>> = [];

    for (const entity of entities) {
      const soupName = entity.SoupName;
      for (const picklistField of entity.Picklists) {
        const picklistPromise = this.createPicklistRequest(autenticateServiceResponse, soupName, picklistField);
        picklistPromises.push(picklistPromise);
      }
    }
    return picklistPromises;
  }

  private createPicklistRequest(autenticateServiceResponse, soupName: string, property: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const instanceServer = this.normalizeServerDomainForPicklist(autenticateServiceResponse.instanceServer);
      const tableName = soupName;
      const context = this;
      console.log('-*-| window.hasOwnPropertyGetTableMetaData 1', tableName, 'property: ', property);

      if (window.hasOwnProperty('getTableMetaData')) {
        window['getTableMetaData'].call(null, instanceServer, tableName, context, (getTableMetaDataResponse) => {
          console.log('-*-| window.hasOwnPropertyGetTableMetaData 2', tableName, 'property: ', property);

          if (!getTableMetaDataResponse.fields) {
            if (!this.permissionErrorMessage) {
              this.permissionErrorMessage = 'Erro de permissão para os objetos/campos:<br><br>';
            }
            this.permissionErrorMessage += `${tableName}: ${property}<br>`;

            console.log('-*-| Error:Synchronism:tableName: ', tableName);
            console.log('-*-| Error:Synchronism:property: ', property);
            resolve();

          } else {
            const field = getTableMetaDataResponse.fields.filter((obj) => {
              return obj.name === property;
            });
            if (field !== null && field !== undefined && field.length > 0) {
              const picklistValues: IPicklistItem[] = field[0].picklistValues;
              WindowStorageHelper.savePicklistsToLocalStorage(property, picklistValues);
            }
            resolve();
          }

        }, (getTableMetaDataResponseError) => {
          reject(getTableMetaDataResponseError);
        });
      } else {
        reject('Propriedade getTableMetaData não encontrada');
      }
    });
  }

  private normalizeServerDomainForPicklist(instanceServer: string): string {
    return this.platform.is('android') ? instanceServer : '';
  }

  private cleanGhostRecords(service: IService): Promise<any> {
    return new Promise((resolve, reject) => {
      service.cleanResyncGhosts(
        () => resolve(),
        (error) => reject(error),
      );
    });
  }

  public waitingSyncStatus(serviceModel: IService): Promise<any> {
    return new Promise((resolve) => {
      const timer = setInterval(() => {
        this.getSyncStatus(serviceModel).then((sync) => {
          if (!sync || (sync && sync.status.includes('DONE'))) {
            console.log(`------| Sync status DONE: ${moment().format('HH:mm:ss')} : `, sync);
            clearInterval(timer);
            resolve();
          } else if (sync && sync.status.includes('RUNNING')) {
            console.log(`------| Sync status RUNNING: ${moment().format('HH:mm:ss')} : `, sync);
          }
        });
      }, 2000);
    });
  }

  private getSyncStatus(service: IService): Promise<any> {
    return new Promise((resolve, reject) => {
      service.getSyncStatus(
        (sync) => resolve(sync),
        (error) => reject(error),
      );
    });
  }

}

// tslint:disable-next-line:max-classes-per-file
class SynchronismUiModel {
  public entityName = '';
  public entityLabel = '';
  public isMaleGender: boolean;
  public itemStatusIcon: string = SynchronismStatusIcons.idle;
  public itemStatusText = 'no text';
  public itemStatusClass: string = SynchronismStatusClasses.idle;

  constructor(name: string, isMaleGender: boolean) {
    this.entityName = name;
    this.isMaleGender = isMaleGender;
  }
}
