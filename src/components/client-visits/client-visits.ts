import { EventBO } from './../../shared/BO/EventBO';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { RecordTypeDeveloperName } from '../../shared/Constants/Types';
import { ClientResponseDAO } from '../../shared/DAO/RepositoryDAO';
import {
    ClientsModel,
} from '../../app/model/ClientsModel';
import {
    Component,
    Input,
    NgZone,
} from '@angular/core';
import {
    Events,
    NavController,
} from 'ionic-angular';
import {
    NotificationKeys,
} from '../../shared/Constants/Keys';
import {
    ServiceProvider,
} from '../../providers/service-salesforce';
import {
    EventResponseDAO,
} from '../../shared/DAO/RepositoryDAO';
import {
    EventModel,
} from '../../app/model/EventModel';
import { EventsPage } from '../../pages/events/events';
import { DateHelper } from '../../shared/Helpers/DateHelper';
import * as moment from 'moment';
import { AppPreferences } from '@ionic-native/app-preferences';
import { OutlookPage } from '../../pages/outlook/outlook';

/**
 * Generated class for the ClientVisitsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: 'client-visits',
    templateUrl: 'client-visits.html',
})

export class ClientVisitsComponent {
    // add aki o evento de pesquisa
    @Input()
    public client: ClientsModel;

    public eventBO: EventBO;
    public showCreateVisit = true;
    public showVisitsData = true;
    public showSearch = true;

    public eventDates: EventDateUiModel[] = [];
    public eventsCursor: EventResponseDAO;
    private criteria: string = null;

    constructor(public events: Events,
        public serviceProvider: ServiceProvider,
        public navCtrl: NavController,
        private appPreferences: AppPreferences,
        private zone: NgZone,
    ) {
        this.eventBO = new EventBO(this.serviceProvider);
        this.loadClientVisits();
    }

    public ngOnInit() {
      console.log('------| Events: client-visits : !!! unsubscribe !!!');

      this.events.unsubscribe(NotificationKeys.loadClientVisits);
    }

    public ngAfterContentInit() {
      console.log('------| Events: client-visits : ### subscribe ###');

      this.events.subscribe(NotificationKeys.loadClientVisits, () => {
        this.loadClientVisits();
      });
    }

    public ionViewWillEnter() {



    }


    public monthNameByDate(date: Date): string {
        const monthName = DateHelper.monthNameByDate(date);
        return monthName.toUpperCase();
    }

    // public createEvent() {

    //     this.client.setRecordType(this.serviceProvider.getRecordTypeService()).then(() => {
    //         const newEvent = new EventModel(null);
    //         newEvent.Account = this.client;
    //         let isFarmer:boolean;
            
    //         if (this.client.RecordType.DeveloperName === 'Agricultores' ||
    //             this.client.RecordType.DeveloperName === 'Parceiros') {
    //             newEvent.WhatId = this.client.Id;
    //             isFarmer = false;
    //         } else if (this.client.RecordType.DeveloperName === 'GC_ACC_Farm') {
    //             newEvent.Fazenda__c = this.client.Id;
    //             isFarmer = true;
    //         }
    //         newEvent.StartDateTime = moment().startOf('hour').format();
    //       this.eventBO.getTipo_Compromisso__c(this.client.RecordTypeId).then((Tipo_Compromisso__c: string) => {
    //           newEvent.Tipo_Compromisso__c = Tipo_Compromisso__c;

    //         const data = {
    //             event: newEvent,
    //             isCreateMode: true,
    //             isFarmer,
    //             isChannel: (this.client.RecordType.DeveloperName === 'Parceiros') ? true : false,
    //         };
    //         this.navCtrl.push(EventsPage, data);
    //     });

    //     }).catch((err) => {
    //         console.log(err);
    //     });

    // }

    public createEvent() {

  
        this.client.setRecordType(this.serviceProvider.getRecordTypeService()).then(() => {
            const newEvent = new EventModel(null);
            newEvent.Account = this.client;
            let isFarmer:boolean;
            let farmToVisit = false;
            let associateFarm:any;
            
            if (this.client.RecordType.DeveloperName === 'Agricultores' ||
                this.client.RecordType.DeveloperName === 'Parceiros') {
                newEvent.WhatId = this.client.Id;
                isFarmer = false;
                farmToVisit = false;
            } else if (this.client.RecordType.DeveloperName === 'GC_ACC_Farm') {
                // newEvent.WhatId = this.client.ParentId;
                newEvent.ClienteId__c = this.client.Id;
                //newEvent.Fazenda__c = this.client.Id;
                isFarmer = true;
                farmToVisit = true;

                // this.getAssociatedClients(this.client).then((data) => {
                //     associateFarm = data.currentPageOrderedEntries[0];
                //   }).catch((error) => {
                //   });
            }
            newEvent.StartDateTime = moment().startOf('hour').format();
          this.eventBO.getTipo_Compromisso__c(this.client.RecordTypeId).then((Tipo_Compromisso__c: string) => {
              newEvent.Tipo_Compromisso__c = Tipo_Compromisso__c;

            const data = {
                event: newEvent,
                isCreateMode: true,
                isFarmer,
                isChannel: (this.client.RecordType.DeveloperName === 'Parceiros') ? true : false,
                associateFarm: this.client,
                clientId:this.client.ParentId,
                farmToVisit:farmToVisit,
            };
            this.navCtrl.push(EventsPage, data);
        });

        }).catch((err) => {
            console.log(err);
        });

    }

    //MAXXIDATA

    private getAssociatedClients(client: ClientsModel) {
        return new Promise<any>((resolve, reject) => {
    
            let key = 'Id';
            let value = client.ParentId;
    
          console.log('---| getAssociatedClients:key: ', key, ' value: ', value);
    
          this.serviceProvider.getClientsService().queryExactFromSoup(key, value, 100, 'ascending', 'Name', (data: ClientResponseDAO) => {
    
            console.log('---| getAssociatedClients: ', data);

            resolve(data);
          }, (error) => {
            reject(error);
          });
        });
      }

    //FIM MAXXIDATA
    

    private loadClientVisits() {
        LoadingManager.getInstance().show().then(() => {
            this.loadDataFromDatabase(() => {
                LoadingManager.getInstance().hide();
            });
        });
    }

    public openEventDetail(event: EventModel) {
        const page = event.IsOutlook__c ? OutlookPage : EventsPage;
        if (event && (event.Id || event.ExternalID__c)) {
            LoadingManager.getInstance().show().then(() => {
                this.queryExactFromSoup(event).then(async (response) => {
                    LoadingManager.getInstance().hide();
                    if (response.currentPageOrderedEntries.length > 0) {
                        const data = {
                            event: await this.mountEvent(response.currentPageOrderedEntries[0]),
                            isCreateMode: false,
                        };
                        this.navCtrl.push(page, data);
                    }
                }, (error) => {
                    this.error(error);
                    LoadingManager.getInstance().hide();
                });
            });
        }
    }

    private queryExactFromSoup(event: EventModel): Promise<any> {
        const matchKey = event.getId();
        const indexPath = event.getIdFieldName();

        return new Promise((resolve, reject) => {
            this.serviceProvider.getEventService().queryExactFromSoup(indexPath, matchKey, 1, 'ascending', indexPath, (response) => {
                resolve(response);
            }, (error) => {
                this.error(error);
                reject(error);
            });
        });
    }

    private mountEvent(eventObject): Promise<any> {
        return new Promise((resolve, reject) => {
            const push = new EventModel(eventObject);
            if (push.WhatId != null) {
                push.setAccount(this.serviceProvider.getClientsService()).then((resClients) => {
                    resolve(push);
                  }).catch((err) => {
                    reject(err);
                  });
                } else {
                resolve(push);
            }
        });
    }

    private error(err: string) {
        console.log(err);
        LoadingManager.getInstance().hide();
    }

    private changeShowEventsDataButton(index: number) {
        const state = this.eventDates[index].showContent;
        this.eventDates[index].showContent = !state;
    }

    private onCancel(ev: any) {
        this.showSearch = true;
        this.criteria = null;
        LoadingManager.getInstance().show().then(() => {
            this.loadDataFromDatabase(() => {
                LoadingManager.getInstance().hide();
            });
        });
    }

    private onClear(ev: any) {
        this.criteria = null;
        LoadingManager.getInstance().show().then(() => {
            this.loadDataFromDatabase(() => {
                LoadingManager.getInstance().hide();
            });
        });
    }

    private getItems(ev: any) {
        let val = ev.target.value;

        if (!val || (val && val.length >= 3)) {
            if (!val) { val = ''; }
            this.criteria = val;
            LoadingManager.getInstance().show().then(() => {
                this.loadDataFromDatabase(() => {
                    LoadingManager.getInstance().hide();
                });
            });
        }
    }

    private loadDataFromDatabase(finished: () => void) {
        const eventDAO = this.serviceProvider.getEventService();
        console.log('---| this.client: ', this.client);

        let pageSize = 1;
        let sql = 'select count(*) from {Event} where {Event:WhatId} = "' + this.client.Id + '"';

        eventDAO.querySmartFromSoup(sql, 1, (response) => {
            response.currentPageOrderedEntries.forEach((res) => {
                pageSize = res[0];
            });

            this.appPreferences.fetch('user_id').then((id) => {

                sql = `SELECT
                      {Event:Id},
                      {Event:BR_Tipo_de_Visita__c},
                      {Event:Status__c},
                      {Event:Safra__c},
                      {Event:Lembrete__c},
                      {Event:BR_Division__c},
                      {Event:Location},
                      {Event:StartDateTime},
                      {Event:ExternalID__c}
                    FROM
                      {Event}
                    WHERE
                      {Event:OwnerId} = "${id}" AND `;

                if (this.client.RecordType.DeveloperName === RecordTypeDeveloperName.farm) {
                    sql += `{Event:Fazenda__c} = "${this.client.Id}"
                        OR {Event:WhatId} = "${this.client.Id}" `;
                } else {
                    sql += `{Event:WhatId} = "${this.client.Id}"
                        OR {Event:ClienteId__c} = "${this.client.Id}" `;
                }

                sql += `ORDER BY
                        {Event:StartDateTime},
                        {Event:EndDateTime},
                        {Event:CreatedDate}`;

                eventDAO.querySmartFromSoup(sql, pageSize, (success) => {
                    this.eventsCursor = success;
                    this.reloadData();
                    finished();
                }, (error) => {
                    console.log('-*-| eventDAO.querySmartFromSoup: ', error);
                });
            }).catch((err) => {
                console.log(err, 'USER ID APPPREFERENCES ERROR');
            });
        }, (error) => {
            console.log('-*-| eventDAO.querySmartFromSoup: ', error);
        });
    }

    private reloadData() {
        const promises: Array<Promise<any>> = [];
        const list = [];

        this.eventsCursor.currentPageOrderedEntries.forEach((item) => {
            const promise = this.mountEventList(item).then((model) => {
                list.push(model);
            });
            promises.push(promise);
        });

        Promise.all(promises).then(() => {
            this.eventDates = this.turnEventModelsIntoEventDates(list);
            console.log('---| this.eventDates: ', this.eventDates);
        });
    }

    private mountEventList(item) {
        return new Promise((resolve) => {
            const model = new EventModel(null);

            model.Id = item[0];
            model.BR_Tipo_de_Visita__c = item[1];
            model.Status__c = item[2];
            model.Safra__c = item[3];
            model.Lembrete__c = item[4];
            model.BR_Division__c = item[5];
            model.Location = item[6];
            model.StartDateTime = item[7];
            model.ExternalID__c = item[8];

            this.eventBO.cropVisitChange(model).then((data) => {
                resolve(data);
            });
        });
    }

    private async setAccountToEventModel(eventModel: EventModel) {
        await this.mountEvents(eventModel);
    }

    private mountEvents(eventModel: EventModel): Promise<any> {
        return new Promise((resolve, reject) => {

            eventModel.setAccount(this.serviceProvider.getClientsService()).then(() => {
                resolve();
            }).catch((error) => {
                reject(error);
            });

        });
    }

    private turnEventModelsIntoEventDates(eventsModels: EventModel[]): EventDateUiModel[] {

        const eventDates: EventDateUiModel[] = [];
        const eventDatesTempObject = {};
        const criteria = this.criteria;
        const eventmodelFiltred: EventModel[] = [];

        for (const eventModel of eventsModels) {
            // this.setAccountToEventModel(eventModel);
            // console.log('---| eventModel:Account: ', eventsModels);
            let toInclude = false;
            if (this.criteria !== null && this.criteria !== undefined) {
                let buffer = ''
                    + eventModel.BR_Tipo_de_Visita__c
                    + eventModel.Status__c
                    + eventModel.Safra__c
                    + eventModel.Lembrete__c
                    + eventModel.BR_Division__c;
                buffer = buffer.toLowerCase();
                if (buffer !== null && buffer !== undefined && buffer.includes(criteria.toLowerCase())) {
                    toInclude = true;
                }
            } else {
                toInclude = true;
            }
            if (toInclude) {
                const tempKey = eventModel.StartDateTime.split('T')[0];

                const dateKey = tempKey.substring(0, 7);
                console.log('---| eventModel.StartDateTime: ', dateKey);

                if (eventDatesTempObject[dateKey] === null ||
                    eventDatesTempObject[dateKey] === undefined) {
                    eventDatesTempObject[dateKey] = [];
                }

                eventDatesTempObject[dateKey].push(eventModel);
            }
        }

        for (const key of Object.keys(eventDatesTempObject)) {
            // console.log('---| eventDatesTempObject: ', eventDatesTempObject);
            // console.log('---| key.Date: ', key);
            const date = moment(key).utc().toDate();
            const eventModelsTemp: EventModel[] = eventDatesTempObject[key];

            const eventDate: EventDateUiModel = {
                date,
                events: eventModelsTemp,
                showContent: true,
            };

            eventDates.push(eventDate);
        }
        console.log('---| turnEventModelsIntoEventDates: ', eventDates);
        return eventDates;
    }
}

// tslint:disable-next-line:max-classes-per-file
export class EventDateUiModel {
    public showContent: boolean;
    public date: Date;
    public events: EventModel[] = [];
}
