import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { AppPreferences } from '@ionic-native/app-preferences';
import { ContactModel } from './../../app/model/ContactModel';
import { ContactService } from './../../providers/contact/contact-service-salesforce';
import { ContactsDetailPage } from './../../pages/contacts-detail/contacts-detail';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Component, Input, NgZone } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { ClientsModel } from '../../app/model/ClientsModel';
import { NotificationKeys } from '../../shared/Constants/Keys';

@Component({
  selector: 'client-contacts',
  templateUrl: 'client-contacts.html',
  providers: [ContactService],
})
export class ClientContactsComponent {

  public contacts = [];
  public contactsCursor: any = { currentPageOrderedEntries: [] };
  public space = ' ';

  @Input()
  public client: ClientsModel;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public events: Events,
    public zone: NgZone,
    private appPreferences: AppPreferences,
  ) {
    this.loadContacts();
  }

  public ngOnInit() {
    console.log('------| Events: client-contacts : !!! unsubscribe !!!');

    this.events.unsubscribe(NotificationKeys.contacsUpdate);
  }

  public ngAfterContentInit() {
    console.log('------| Events: client-contacts : ### subscribe ###');

    this.events.subscribe(NotificationKeys.contacsUpdate, () => {
      this.loadContacts();
    });
  }

  public loadContacts() {
    this.contactsCursor = '';
    LoadingManager.getInstance().show().then(() => {
      this.zone.run(() => {
        this.execStandardQuery();
        LoadingManager.getInstance().hide();
      });
    });
  }

  public execStandardQuery() {
    let recordTypeGeneral = '';
    let pageSize = 1;
    let smartSql = 'select {RecordType:Id} from {RecordType} where ' +
      '{RecordType:DeveloperName} = "BR_Contact"';
    this.querySmartFromSoup(smartSql, pageSize).then((successRecordType) => {
      successRecordType.currentPageOrderedEntries.forEach((resRecordType) => {
        recordTypeGeneral = resRecordType[0];
        smartSql = 'select count(*) from {Contact}';
        pageSize = 1;

        this.querySmartFromSoup(smartSql, pageSize).then((successPageSize) => {
          successPageSize.currentPageOrderedEntries.forEach((resPageSize) => {
            pageSize = resPageSize[0];

            this.contacts = [];

            this.appPreferences.fetch('user_brand').then((value) => {
              const clientId = this.client.Id;
              smartSql = `select {Contact:Id},{Contact:FirstName},{Contact:LastName},{Contact:Phone},{Contact:MobilePhone},{Contact:Email},{Contact:BR_Brand__c},{Contact:Profissao__c},{Contact:RecordTypeId},{Contact:AccountId},{Contact:_soupEntryId},{Contact:_soupLastModifiedDate}
              from {Contact}
              where {Contact:RecordTypeId} = '${recordTypeGeneral}'
              and {Contact:BR_Marca__c} = '${value}'
              and {Contact:AccountId} = '${clientId}'
              order by {Contact:Name}`;

              console.log(smartSql, '------------| smartSql');

              this.querySmartFromSoup(smartSql, pageSize).then((response) => {
                response.currentPageOrderedEntries.forEach((item) => {
                  this.mountContacts(item);
                });
                this.contactsCursor = response;
                LoadingManager.getInstance().hide();
              }, (error) => {
                LoadingManager.getInstance().hide();
                this.error(error);
              });
            });
          });
        });
      });
    });
  }

  public querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getContactService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  public mountContacts(item): Promise<any> {
    return new Promise((resolve, reject) => {

      const contact = {
        Id: item[0],
        FirstName: item[1],
        LastName: item[2],
        Phone: item[3],
        MobilePhone: item[4],
        Email: item[5],
        BR_Brand__c: item[6],
        Profissao__c: item[7],
        RecordTypeId: item[8],
        AccountId: item[9],
        _soupEntryId: item[10],
        _soupLastModifiedDate: item[11],
      };

      const push = new ContactModel(contact);
      if (push.RecordTypeId != null) {
        push.setRecordType(this.service.getRecordTypeService()).then((resRecordType) => {
          if (push.AccountId != null) {
            push.setAccount(this.service.getClientsService()).then((resClients) => {
              this.contacts.push(push);
              resolve(push);
            }).catch((err) => {
              reject(err);
            });
          } else {
            this.contacts.push(push);
          }
        }).catch((err) => {
          reject(err);
        });
      } else {
        resolve(push);
      }
    });
  }

  public error(err: string) {
    console.log(err);
    LoadingManager.getInstance().hide();
  }

  public ionViewWillLeave() {
    LoadingManager.getInstance().hide();
  }

  public openContactsDetail(contact) {
    LoadingManager.getInstance().hide();
    this.navCtrl.push(ContactsDetailPage, {
      client: this.client,
      contact,
    });
  }

  public addContact() {
    this.navCtrl.push(ContactsDetailPage, {
      client: this.client,
    });
  }
}
