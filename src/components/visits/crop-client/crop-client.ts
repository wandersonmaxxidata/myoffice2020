import { ServiceProvider } from './../../../providers/service-salesforce';
import { ClientsModel } from './../../../app/model/ClientsModel';
import { EventModel } from './../../../app/model/EventModel';
import { Component, Input } from '@angular/core';
import { ProfileMappingBO } from '../../../shared/BO/ProfileMappingBO';

@Component({
  selector: 'crop-client',
  templateUrl: 'crop-client.html',
})
export class CropClientComponent {

  @Input('event')
  public event: EventModel = new EventModel(null);

  public client: ClientsModel;
  public profileMappingBO: ProfileMappingBO;

  public name: string;
  public radl: string;

  constructor(public serviceProvider: ServiceProvider) {}

  public ngOnInit() {
    this.loadInfo(this.event);
  }

  public loadInfo(event: EventModel) {
    this.client = new ClientsModel(event.Account);
    this.profileMappingBO = new ProfileMappingBO(this.serviceProvider);

    this.name = this.client.Name;
    this.profileMappingBO.RADL(this.event.Account.Id).then((radl: string) => {
      this.radl = radl;
    });
  }

}
