import { AlertController, ModalController, ModalOptions, NavController, Platform, Searchbar } from 'ionic-angular';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { AutenticateService } from './../../providers/autenticate/autenticate-service-salesforce';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { EventModel } from '../../app/model/EventModel';
import { Events } from 'ionic-angular/util/events';
import { EventsPage } from '../../pages/events/events';
import { LocalStorageKeys, NotificationKeys } from '../../shared/Constants/Keys';
import { Moment } from 'moment';
import { MultipicklistModal } from '../modals/modal-multipicklist/modal-multipicklist';
import { Network } from '@ionic-native/network';
import { PopoverController } from 'ionic-angular';
import { SearchEventModal } from '../modals/modal-search-event/modal-search-event';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { HomePage } from '../../pages/home/home';
import { AgendaPage } from '../../pages/agenda/agenda';

@Component({
  providers: [ServiceProvider],
  selector: 'header',
  templateUrl: 'header.html',
})
export class HeaderComponent {

  @ViewChild('searchBar')
  public searchBar: Searchbar;

  @Input('namePage')
  public namePage: any;

  @Input('hiddenSearch')
  public hiddenSearch: any;

  @Input('showPageTitle')
  public showPageTitle: any;

  @Input('showBack')
  public showBack: any;

  @Input('showLogo')
  public showLogo: any;

  @Input('showAddEvent')
  public showAddEvent: any;

  @Input('showSearchEvent')
  public showSearchEvent: any;

  @Input('hiddenSync')
  public hiddenSync: any;

  @Input('countSyncBadge')
  public countSyncBadge: any;

  @Input('hiddenFilter')
  public hiddenFilter: any;

  @Input('hiddenAgenda')
  public hiddenAgenda: any;

  @Input('objectSearch')
  public objectSearch: any;

  @Input('objectSync')
  public objectSync: any;

  @Input('context')
  public context: any;

  @Input('showBackLogError')
  public showBackLogError: boolean;

  @Input('pageReturn')
  public pageReturn: any;

  @Output()
  public onClearSearch: EventEmitter<any> = new EventEmitter<any>();

  public filterList;
  public filterSelected;

  public pageTitle: string;
  public searchQuery = '';
  public items: string[];
  public showSearch: boolean;
  public showIconsStyle: any;
  public showSearchStyle: any;
  public cancel: any;
  public displayName: string;
  public localLabels: any = {};
  public unitOfTime = 'week';
  public showAgendaFilter = false;
  public showTodayButton = false;
  public showSyncButton;
  public rotate;
  public selectDate: Moment;
  public pageObject:any;

  constructor(
    public storage: Storage,
    public events: Events,
    public navCtrl: NavController,
    public auth: AutenticateService,
    public service: ServiceProvider,
    public platform: Platform,
    public network: Network,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
  ) {
    this.service.autenticateService.getCurrentUser().then((sucess) => {
      console.log(sucess, ' ------------- autenticateService');
      this.displayName = sucess.display_name;
    }).catch((error) => {
      console.log(error);
    });
  }

  public ngOnInit(): void {
    console.log(`------| Events: header (${this.namePage}): !!! unsubscribe !!!`);

    this.events.unsubscribe(`${this.namePage}:${NotificationKeys.selectedDate}`);
    this.events.unsubscribe(`${this.namePage}:${NotificationKeys.changeTab}`);
    this.events.unsubscribe(`${this.namePage}:${NotificationKeys.firstSync}`);

  }

  public ngAfterContentInit() {
    console.log(`------| BOTAO ERROR (${this.showBackLogError})`);
    console.log(`------| Events: header (${this.namePage}): !!! subscribe !!!`);

    this.events.subscribe(`${this.namePage}:${NotificationKeys.selectedDate}`, (date: Moment) => {
      this.showTodayButton = this.checkTodayButtonVisibility(date);
      this.selectDate = date;
    });

    this.events.subscribe(`${this.namePage}:${NotificationKeys.changeTab}`, (filterList) => {
      this.hiddenFilter = true;
      this.showSearch = false;

      this.filterList = filterList;
      this.filterSelected = [];
      this.searchQuery = '';
    });

    this.events.subscribe(`${this.namePage}:${NotificationKeys.firstSync}`, () => {
      this.showSyncButton = false;
    });

    if (this.hiddenSearch === false) {
      this.registerBackButton();
    }
  }

  public updatePageTitle(title: string) {
    if (title) {
      this.pageTitle = title;
      if (title === 'Clientes') {
        this.hiddenFilter = true;
        this.showSearch = false;
        this.onClearSearch.emit();
      }
    }
  }

  private checkTodayButtonVisibility(date) {
    const diff = moment().startOf('day').diff(moment(date).startOf('day'), 'days');
    return (diff !== 0);
  }

  public openToday() {
    this.showTodayButton = false;
    this.events.publish(NotificationKeys.openToday);
  }

  public openSearchEvent() {
    this.navCtrl.push(SearchEventModal);
  }

  public registerBackButton() {
    this.storage.get('priorityBack').then((value) => {
      if (value) {
        value += 1;
      } else {
        value = 10;
      }
      this.storage.set('priorityBack', value).then(() => {
        this.platform.registerBackButtonAction(() => {
          if (this.showSearch === true) {
            this.closeSearchBar();
          } else {
            if (this.navCtrl.getViews().length > 1) {
              this.navCtrl.pop();
            } else {
              this.storage.set('priorityBack', 10);
              this.platform.exitApp();
            }
          }
        }, value);
      });
    });
  }

  public addEvent() {
    const event = new EventModel(null);

    if (this.selectDate) {
      const current = moment();
      this.selectDate.set('hour', current.get('hour'));

      event.StartDateTime = this.selectDate.format();
      event.Data_do_Plantio__c = this.selectDate.format();
    }

    const data = {
      event,
      isCreateMode: true,
    };
    this.navCtrl.push(EventsPage, data);
  }

  public syncProcess() {
    let message = 'Você está sem conexão com internet no momento. Conecte-se a internet e tente novamente.';

    if (this.network.type === 'none') {
      AlertHelper.showNoConnectionAlert(this.alertCtrl, message);
    } else {
      if (this.network.type !== 'wifi') {
        message = 'Você está utilizando <strong>' + this.network.type.toUpperCase() + '</strong>, deseja continuar o sincronismo?';

        AlertHelper.showStartSyncProcessAlert(this.alertCtrl, message, () => {
          if (this.showSyncButton) {
            this.showSyncButton = false;
            this.events.publish(NotificationKeys.startSync);
          }
        }, () => {
          console.log('Sync cancelled');
        });
      } else {
        if (this.showSyncButton) {
          this.showSyncButton = false;
          this.events.publish(NotificationKeys.startSync);
        }
      }
    }
  }

  public syncButtonManager() {
    this.storage.get(LocalStorageKeys.milliNextSyncKey).then((milli) => {
      const time = milli ? milli : 0;

      if (!this.showSyncButton) {
        this.rotate = true;
      }

      setTimeout(() => {
        this.storage.set(LocalStorageKeys.milliNextSyncKey, 0).then(() => {
          this.rotate = false;
          this.showSyncButton = true;
        });
      }, time);
    });
  }

  public openAgendaFilter() {
    this.showAgendaFilter = true;
  }

  public loadSchedule() {
    this.events.publish(NotificationKeys.filterAgenda, this.unitOfTime);
    this.showAgendaFilter = false;
  }

  public backView() {
    if (this.navCtrl.getPrevious() && this.navCtrl.getPrevious().data) {
      this.navCtrl.getPrevious().data.shouldCloseSearchBar = true;
    }
    this.navCtrl.pop();
  }

  public openSearchBar() {
    this.hiddenFilter = !(this.filterList && this.filterList.length > 0);
    this.showSearch = true;

    setTimeout(() => {
      this.searchBar.setFocus();
    }, 150);
  }

  public openFilter() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Filtro',
      items: this.filterSelected,
      list: this.filterList,
      order: 'value',
      namePage:this.namePage,
    }, options);
    modal.present();

    modal.onDidDismiss((listSelected: any) => {
      if (listSelected) {
        this.filterSelected = listSelected;
        this.getItems(350);
      }
    });
  }

  public closeSearchBar() {
    this.hiddenFilter = true;
    this.showSearch = false;
  }

  public onCancel(ev: any) {
    const val = ev.target ? ev.target.value : undefined;
    this.hiddenFilter = true;
    this.showSearch = false;

    if (!val) {
      this.onClearSearch.emit();
    }
  }

  public getItems(timer?: number) {
    const time = timer ? timer : 1500;

    const objSearch = {
      term: this.searchQuery ? this.searchQuery : '',
      whereCondition: DataHelper.getFilterSelected(this.filterList, this.filterSelected),
    };

    setTimeout(() => {
      if (this.searchQuery && this.searchQuery.length >= 3) {
        this.events.publish(this.objectSearch, objSearch);
      }
    }, time);
  }

  //MAXXIDATA
  backViewErrorLog(page){
    sessionStorage.removeItem('filter')
    console.log(page)
    switch(page) {
      case "agenda": {
        this.goToPage(AgendaPage);
         break; 
      } 
      case "home": {
        this.goToPage(HomePage);
       break; 
    } 
      default: {
         this.goToPage(HomePage);
         break;              
      } 
    }
  }


  goToPage(page){
    this.navCtrl.setRoot(page,{syncError: false});
  }
  //FIM MAXXIDATA

}
