import { AppPreferences } from '@ionic-native/app-preferences';
import { Component } from '@angular/core';
import { DateHelper } from '../../shared/Helpers/DateHelper';
import { Events } from 'ionic-angular/util/events';
import { LocalStorageKeys, NotificationKeys } from '../../shared/Constants/Keys';
import { LocalStorageUser } from './../../shared/Constants/Keys';
import { Platform } from 'ionic-angular';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Storage } from '@ionic/storage';
import { UserModel } from './../../app/model/UserModel';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { Profile } from '../../shared/Constants/Types';

@Component({
  selector: 'welcome-card',
  templateUrl: 'welcome-home.html',
  providers: [ServiceProvider],
})
export class WelcomeHomeComponent {
  public timeToLastSync: any;
  public welcome: string;
  public currentDateMessage: string;
  public lastSynchronismDateMessage: string;
  public user: any[] = [];
  public context: WelcomeHomeComponent = this;
  public sortBy = { field: 'Name', label: 'Name' };
  public space = ' ';
  public currentUser;
  public showWelcomeUser = false;

  constructor(
    public service: ServiceProvider,
    public events: Events,
    public appPreferences: AppPreferences,
    public platform: Platform,
    public storage: Storage,
  ) {
    this.appPreferences.fetch('user_name').then((name) => {
      this.currentUser = name;
    });
  }

  public ngOnInit() {
    this.execStandardQueryUser();
    this.handleLastSynchronismPresentation();
  }

  public setName() {
    this.appPreferences.fetch('user_name').then((name) => {
      this.currentUser = name;
    });
  }

  public execStandardQueryUser() {
    this.appPreferences.fetch('user_id').then((value) => {
      this.queryExactFromSoupUser(value).then((response) => {
        if (response.currentPageOrderedEntries.length === 0) {
          this.events.publish(NotificationKeys.afterEndSync);
        } else {
          response.currentPageOrderedEntries.forEach((item) => {
            const user = new UserModel(item);
            console.log('execStandardQueryUser USER: ', user);

            this.currentUser = user.Name;
            this.appPreferences.store('user_id', user.Id ? user.Id : '');
            this.appPreferences.store('user_culture', user.BR_Culture__c ? user.BR_Culture__c : '');
            this.appPreferences.store('user_district', user.BR_DistrictName__c ? user.BR_DistrictName__c : '');
            this.appPreferences.store('user_regional', user.BR_Regional__c ? user.BR_Regional__c : '');
            this.appPreferences.store('user_type', user.BR_User_Type__c ? user.BR_User_Type__c : '');
            this.appPreferences.store('user_language', user.LanguageLocaleKey ? user.LanguageLocaleKey : '');
            this.appPreferences.store('user_name', user.Name ? user.Name : '');
            this.appPreferences.store('user_brand', user.Marca__c ? user.Marca__c : '');
            this.appPreferences.store('user_profile', user.BR_ProfileName__c ? user.BR_ProfileName__c : '');
            this.setName();

            const BR_User_Type__c = user.BR_User_Type__c ? user.BR_User_Type__c : 'RTV';

            const user_profile = {
              isAdmin: (!user.BR_KAMUser__c && DataHelper.in(Profile.admin, BR_User_Type__c)),
              isGR   : (!user.BR_KAMUser__c && DataHelper.in(Profile.gr, BR_User_Type__c)),
              isKAM  : (user.BR_KAMUser__c),
              isRC   : (!user.BR_KAMUser__c && DataHelper.in(Profile.rc, BR_User_Type__c)),
              isRTV  : (!user.BR_KAMUser__c && DataHelper.in(Profile.rtv, BR_User_Type__c)),
            };

            localStorage.setItem(LocalStorageUser.userCultureKey, user.BR_Culture__c ? user.BR_Culture__c : '');
            localStorage.setItem(LocalStorageUser.userProfile, user.BR_ProfileName__c ? user.BR_ProfileName__c : '');

            this.storage.set('user_profile', user_profile).then(() => {
              this.events.publish(NotificationKeys.afterEndSync);
            });
          });
        }
      }).catch((err) => {
        console.log('execStandardQueryUser USER ERROR', err);
      });
    }).catch((err) => {
      console.log('USER ID APPPREFERENCES ERROR', err);
    });
  }

  public queryExactFromSoupUser(userId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.platform.is('android')) {
        console.log('queryExactFromSoupUser userId', userId);
        this.service.getUserService().queryExactFromSoup('Id', userId, 1, 'ascending', 'Id', (response) => {
          resolve(response);
        }, (err) => {
          console.log('Error:queryExactFromSoupUser-------|', err);
        });
      } else if (this.platform.is('ios')) {
        console.log('queryExactFromSoupUser userId', userId);
        this.service.getUserService().queryExactFromSoup('BR_UserId__c', userId, 1, 'ascending', 'BR_UserId__c', (response) => {
          resolve(response);
        }, (err) => {
          console.log('Error:queryExactFromSoupUser-------|', err);
        });
      }
    });
  }

  public handleCurrentDatePresentation() {
    const currentDate = new Date();
    const formattedDate = DateHelper.formattedCurrentDate(currentDate);
    this.currentDateMessage = formattedDate;
  }

  private handleLastSynchronismPresentation() {
    const lastSyncDateString = localStorage.getItem(LocalStorageKeys.lastSyncDateKey);
    console.log('---| LocalStorege lastSyncDateString: ', lastSyncDateString);

    this.appPreferences.fetch('totalTimeSync').then((value) => {
      this.timeToLastSync = value;
    });

    if (lastSyncDateString != null) {
      const lastSyncDate: Date = new Date(lastSyncDateString);
      console.log('---| LocalStorege lastSyncDate: ', lastSyncDate);

      const formattedDate = DateHelper.formattedLastSyncDate(lastSyncDate);
      this.lastSynchronismDateMessage = formattedDate;
    }
  }

}
