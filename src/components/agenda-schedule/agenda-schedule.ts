import { EventStatus } from './../../shared/BO/EventBO';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { Component,NgZone } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { Moment } from 'moment';
import { EventModel } from './../../app/model/EventModel';
import { EventService } from './../../providers/event/event-service-salesforce';
import { ServiceProvider } from './../../providers/service-salesforce';
import { EventsPage } from '../../pages/events/events';
import * as moment from 'moment';
import { AppPreferences } from '@ionic-native/app-preferences';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { DateHelper } from '../../shared/Helpers/DateHelper';
import { OutlookPage } from '../../pages/outlook/outlook';
import { DataErrorDaoProvider } from './../../providers/data-error-dao/data-error-dao';
import { HomePage } from '../../pages/home/home';

@Component({
  providers: [EventService],
  selector: 'agenda-schedule',
  templateUrl: 'agenda-schedule.html',
})
export class AgendaScheduleComponent {

  public keys = [];
  public keysLabel = false;
  public events = {};
  public eventsCursor: any = { currentPageOrderedEntries: [] };
  public context: AgendaScheduleComponent = this;
  public sortBy = { field: 'StartDateTime', label: 'StartDateTime' };
  public space = ' ';
  public current: Moment;

  private beginKey: number;
  private endKey: number;
  private isMonthView: boolean;
  public hideVisitsCanceled: any;

  constructor(public navCtrl: NavController,
    public dataErrorDAO: DataErrorDaoProvider,
    public service: ServiceProvider,
    private appPreferences: AppPreferences,
    public zone: NgZone,
    public eventsT: Events,
  ) {
    this.current = moment();
    this.eventsCursor = '';
  }

  public loadSchedule(current: Moment) {
    if (current) {
      this.current = current;

      this.keys = [];
      this.events = {};

      this.beginKey = DateHelper.getUTCTimestamp(moment(current).startOf('day').format());
      this.endKey = DateHelper.getUTCTimestamp(moment(current).endOf('day').format());

      this.execStandardQuery();
    }
  }

  public checkError(id) {
    return new Promise(resolve => {
      this.dataErrorDAO.getError(id).then(data => {
        if (data > 0) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  public openEventDetail(event: EventModel) {
    this.dataErrorDAO.getError(event.Id).then(data => {
      console.log(data);
      if (data > 0) {
        sessionStorage.setItem('pageReturn','home');
        sessionStorage.setItem('filter',String(event.Id));
        this.navCtrl.parent.select(0);
       
      } else {
        const page = event.IsOutlook__c ? OutlookPage : EventsPage;
        if (event && (event.Id || event.ExternalID__c)) {
          LoadingManager.getInstance().show().then(() => {
            this.queryExactFromSoup(event).then(async (response) => {
              LoadingManager.getInstance().hide();
              if (response.currentPageOrderedEntries.length > 0) {
                const data = {
                  event: await this.mountEvent(response.currentPageOrderedEntries[0]),
                  isCreateMode: false,
                };
                this.navCtrl.push(page, data);
              }
            }, (error) => {
              this.error(error);
              LoadingManager.getInstance().hide();
            });
          });
        }
      }
    });
  }

  public getKeyName(key: string) {
    const diff = Number(key);
    const day = moment(this.current).add(diff, 'days');
    return day.format('dddd[,] DD[/]MM');
  }

  public getFormattedDate(date: string) {
    return DateHelper.formattedDateToEventCard(date);
  }

  public getEventBoxClass(item: EventModel) {
    const status = item.Status__c.substring(0, item.Status__c.length - 1);

    if (item.IsOutlook__c) {
      return 'event-box-outlook';
    } else if (EventStatus.open.includes(status)) {
      return 'event-box-next';
    } else if (EventStatus.inProgress.includes(status)) {
      return 'event-box-current';
    } else if (EventStatus.executed.includes(status)) {
      return 'event-box-finished';
    } else if (EventStatus.canceled.includes(status)) {
      return 'event-box';
    }
    return 'event-box-next';
  }

  public getVisitStatusClass(item: EventModel) {
    const status = item.Status__c.substring(0, item.Status__c.length - 1);

    if (item.IsOutlook__c) {
      return 'visit-status-outlook';
    } else if (EventStatus.open.includes(status)) {
      return 'visit-status-next';
    } else if (EventStatus.inProgress.includes(status)) {
      return 'visit-status-current';
    } else if (EventStatus.executed.includes(status)) {
      return 'visit-status-finished';
    } else if (EventStatus.canceled.includes(status)) {
      return 'visit-status';
    }
    return 'visit-status';
  }

  public hideCanceled(type) {
    if (type === 'Cancelado') {
      if (sessionStorage.getItem('hideVisitsCanceled') == 'true') {
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }

  }

  public getVisitTypeClass(item: EventModel) {
    if (item.IsOutlook__c) {
      return 'visit-type-outlook';
    }
    return 'visit-type';
  }

  public getFarmNameClass(item: EventModel) {
    if (item.IsOutlook__c) {
      return 'location-outlook';
    }
    return 'farm-name';
  }

  private execStandardQuery() {
    this.appPreferences.fetch('user_id').then((id) => {

      const smartSql = `
        SELECT
          {Event:Id},
          {Event:StartDateTime},
          {Event:EndDateTime},
          {Event:IsOutlook__c},
          {Event:Status__c},
          {Event:BR_Tipo_de_Visita__c},
          {Event:Location},
          {Event:ExternalID__c},
          {Event:IsAllDayEvent},
          {Event:Subject}
        FROM
          {Event}
        Where
          {Event:OwnerId} = "${id}"
          AND {Event:BR_StartTime__c} >= "${this.beginKey}"
          AND {Event:BR_StartTime__c} <= "${this.endKey}"
        ORDER BY
          {Event:BR_StartTime__c}, {Event:BR_EndTime__c}, {Event:CreatedDate}
      `;

      this.querySmartFromSoup(smartSql, 50).then((response) => {
        this.keysLabel = response.totalPages === 0;
        response.currentPageOrderedEntries.forEach((item) => {
          this.mountItem(item);
        });
        this.eventsCursor = response;
      }, (error) => {
        this.error(error);
      });
    }, (error) => {
      this.error(error);
    });
  }

  private mountItem(item) {
    let nameClient = item[9].split('- ');
    this.checkError(item[0]).then(data => {
      const event = {
        Id: item[0],
        StartDateTime: item[1],
        EndDateTime: item[2],
        IsOutlook__c: DataHelper.getBoolean(item[3]),
        Status__c: item[4],
        BR_Tipo_de_Visita__c: item[5],
        Location: item[6],
        ExternalID__c: item[7],
        IsAllDayEvent: DataHelper.getBoolean(item[8]),
        Subject: nameClient[1],
        errorSincy: data,
      };


      if (DateHelper.isAllDayEventNotAdjusted(event)) {
        event.StartDateTime = DateHelper.adjustAllDayEventTimezone(event.StartDateTime);
        event.EndDateTime = DateHelper.adjustAllDayEventTimezone(event.EndDateTime);
      }

      const key = String(moment(event.StartDateTime).diff(this.current, 'days'));

      if (!this.events[key]) {
        this.events[key] = [];
      }

      this.events[key].push(event);
      this.keys = Object.keys(this.events);
    });
  }

  private mountEvent(eventObject): Promise<any> {
    return new Promise((resolve, reject) => {
      const push = new EventModel(eventObject);
      if (push.WhatId != null) {
        push.setAccount(this.service.getClientsService()).then((resClients) => {
          resolve(push);
        }).catch((err) => {
          reject(err);
        });
      } else {
        resolve(push);
      }
    });
  }

  private queryExactFromSoup(event): Promise<any> {
    const matchKey = event.Id || event.ExternalID__c;
    const indexPath = event.Id ? 'Id' : 'ExternalID__c';

    return new Promise((resolve, reject) => {
      this.service.getEventService().queryExactFromSoup(indexPath, matchKey, 1, 'ascending', indexPath, (response) => {
        resolve(response);
      }, (error) => {
        this.error(error);
        reject(error);
      });
    });
  }

  private error(err: string) {
    console.log(err);
    LoadingManager.getInstance().hide();
  }

  private querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getEventService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }
}
