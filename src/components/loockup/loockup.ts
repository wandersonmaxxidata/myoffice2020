import { AppPreferences } from '@ionic-native/app-preferences';
import { ClientBO } from '../../shared/BO/ClientBO';
import { ClientsModel } from './../../app/model/ClientsModel';
import { Component, ElementRef, Input, NgZone, ViewChild } from '@angular/core';
import { Events } from 'ionic-angular/util/events';
import { NavController, Searchbar } from 'ionic-angular';
import { NotificationKeys } from './../../shared/Constants/Keys';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'loockup',
  templateUrl: 'loockup.html',
  providers: [ServiceProvider],
})

export class LoockupComponent {

  @ViewChild('searchBar')
  public searchBar: Searchbar;

  @Input('objectName')
  public objectName: string;

  @Input('titleObject')
  public titleObject: any;

  @Input('objectValue')
  public objectValue: string;

  @Input('objectLabel')
  public objectLabel: string;

  @Input('callBack')
  public callBack: () => {};

  @Input('context')
  public context: any;

  @Input('lookupId')
  public lookupId: string;

  @Input('lookupItem')
  public lookupItem: string;

  @Input('orderBy')
  public orderBy: string;

  @Input('searchTerm')
  public searchTerm: string;

  @Input('idComponent')
  public idComponent: string;

  @Input('typeId')
  public typeId: string;

  @Input('ParentId')
  public ParentId: string;

  public user_profile: any;

  public itens: any[] = [];
  public groups = [];
  public emptySearch = false;
  private typeClient = '';

  constructor(
    public navCtrl: NavController,
    public service: ServiceProvider,
    public zone: NgZone,
    public elementRef: ElementRef,
    public events: Events,
    public appPreferences: AppPreferences,
    public storage: Storage,
  ) {
    this.storage.get('user_profile').then((user_profile: any) => {
      this.user_profile = user_profile;
    });
  }

  public select(item) {
    this.zone.run(() => {
      this.searchTerm = item[this.objectLabel];
    });
    this.callBack.call(this.context, item);
    this.setLookUpVisibility('none');
  }

  public setLookUpVisibility(visibility: string) {
    this.zone.run(() => {
      const element = this.elementRef.nativeElement.querySelector('#' + this.lookupId);
      if (element && element.style) {
        element.style.display = visibility;
      }
    });
  }

  public getLabel(item): string {
    return Reflect.get(item, this.objectLabel);
  }

  public getValue(item): string {
    return Reflect.get(item, this.objectValue);
  }

  public getItems(ev: any) {
    this.emptySearch = false;

    setTimeout(() => {
      this.setLookUpVisibility('block');
      this.zone.run(() => {
        this.groups = [];
        this.itens = [];
      });

      // set val to the value of the searchbar
      const val = ev.target.value;

      // if the value is an empty string don't filter the items
      if (val.length >= 3) {
        const currentService = Reflect.get(this.service, this.objectName.toLowerCase() + 'Service');
        let typeClausule = '';
        let parentClausule = '';

        let sql = `select {${this.objectName}:_soup} from {${this.objectName}} where ({${this.objectName}:[objectLabel]} like \'%[term]%\') typeClausule parentClausule`;
        const ids = this.typeId.split(',');

        if (this.objectName === 'Clients') {
          // if (this.user_profile.isKAM) {
          //   sql = sql.replace(`'%[term]%')`, `'%[term]%' or {${this.objectName}:SAP} like '%[term]%')`);
          // }

          typeClausule = (ids.length !== 1)
            ? `and {${this.objectName}:RecordTypeId} in (${this.typeId})`
            : `and {${this.objectName}:RecordTypeId} = ${this.typeId}`;

        } else if (this.objectName === 'User') {
          typeClausule = `and {${this.objectName}:BR_User_Type__c} = 'RTV'`;
        }

        if (this.ParentId !== null && this.ParentId !== undefined) {
          parentClausule = `and {${this.objectName}:ParentId} = \'[ParentId]\'`;
          parentClausule = parentClausule.replace('[ParentId]', this.ParentId);
        }

        if (this.orderBy != null) {
          sql += ` order by {${this.objectName}:${this.orderBy}}`;
        }

        sql = sql.replace('[objectLabel]', this.objectLabel).replace(/\[term\]/g, val).replace('typeClausule', typeClausule).replace('parentClausule', parentClausule);
        console.log(sql);

        currentService.querySmartFromSoup(sql, 10, (response) => {
          console.log(response);

          const type = this.typeId;

          const itensResponse = response.currentPageOrderedEntries;
          this.zone.run(() => {
            for (const item of itensResponse) {

              const model = (this.objectName === 'Clients') ? new ClientsModel(item[0]) : item[0];

              if (type.includes(model.RecordTypeId)) {
                model.setRecordType(this.service.getRecordTypeService()).then(() => {
                  if (this.user_profile.isKAM && model.BR_FamilyGroup__c) {
                    const clientBO = new ClientBO(this.service);
                    clientBO.getFamilyGroupNameById(model.BR_FamilyGroup__c).then((name: string) => {
                      this.groups.push(name);
                      this.itens.push(model);
                      this.getTypeClient(this.itens);
                    });
                  } else {
                    this.groups.push(undefined);
                    this.itens.push(model);
                    this.getTypeClient(this.itens);
                  }
                });
              } else {
                this.groups.push(undefined);
                this.itens.push(model);
              }
            }

            this.emptySearch = (itensResponse.length === 0) ? true : false;
          });
        }, (err) => {
          console.log(err);
        });
      }
    }, 550);
  }


  private getTypeClient(client: ClientsModel[]) {
    client.forEach((c) => {
      switch (c.RecordType.DeveloperName) {
        case 'Parceiros':
          this.typeClient = 'Canal';
          c['typeOfClient'] = 'Canal';
          break;
        case 'Agricultores':
          this.typeClient = 'Agricultor';
          c['typeOfClient'] = 'Agricultor';
          break;
      }
    });

  }

  public ngOnInit() {
    this.setFocus();
  }

  private setFocus() {
    if (!this.searchBar.isFocus()) {
      setTimeout(() => {
        this.searchBar.setFocus();
        setTimeout(() => {
          this.setFocus();
        }, 400);
      }, 750);
    }
  }

  public updateSearch() {
    this.events.publish(NotificationKeys.cleanLoockup);
  }

  public onCancel(ev: any) {
    this.events.publish(NotificationKeys.cleanLoockup);
    this.zone.run(() => {
      this.groups = [];
      this.itens = [];
      this.searchTerm = '';
    });

    this.callBack.call(this.context, { Id: '' });
    this.setLookUpVisibility('none');
  }

  private queryLikeFromSoup(index: string, key: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getClientsService().
        queryLikeFromSoup(index, key, 'ascending', pageSize, 'Name', (response) => {
          resolve(response);
        }, (err) => {
          console.log(err);
        });
    });
  }

}
