import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { EventModel } from '../../app/model/EventModel';
import { Component, ViewChild } from '@angular/core';
// tslint:disable-next-line:no-import-side-effect
import 'moment/locale/pt-br';
import { Moment } from 'moment';
import { AgendaScheduleComponent } from '../agenda-schedule/agenda-schedule';
import * as moment from 'moment';
import { Events, NavController } from 'ionic-angular';
import { NotificationKeys } from '../../shared/Constants/Keys';
import { EventsPage } from '../../pages/events/events';
import { ServiceProvider } from '../../providers/service-salesforce';
import { AppPreferences } from '@ionic-native/app-preferences';
import { DataHelper } from '../../shared/Helpers/DataHelper';

@Component({
  selector: 'agenda',
  templateUrl: 'agenda.html',
})
export class AgendaComponent {
  public eventCursor: any = { currentPageOrderedEntries: [] };

  public current: Moment;
  public currentIndex = 0; // Initial selection
  // public allDayLabel = 'DIA';
  public calendarDay = 'day';

  public calendar = {
    currentDate: new Date(),
    mode: 'month',
  };

  public dateList;
  public eventSource;

  public lastMonth: string;
  public currentMonth: string;
  public nextMonth: string;

  public isMonthView: boolean;
  public hideSchedule: boolean;
  public hideVisitsCanceled:boolean=false;

  @ViewChild(AgendaScheduleComponent)
  public agendaScheduleComponent: AgendaScheduleComponent;


  private tempEventSource = [];

  // Quantity of days to add or subtract
  private readonly day = 1;
  private readonly longDay = 2;
  private readonly week = 5;

  constructor(
    private navCtrl: NavController,
    public events: Events,
    private service: ServiceProvider,
    private appPreferences: AppPreferences,
  ) {
    moment.locale('pt-BR');
    this.current = moment();
    this.hideSchedule = false;
    this.isMonthView = false;

    this.calendar.currentDate = this.current.toDate();
    this.loadWeekView();
    this.refresh();
  }

  public ngOnInit(): void {
    console.log('------| Events: agenda-component : !!! unsubscribe !!!');

    this.events.unsubscribe(NotificationKeys.filterAgenda);
    this.events.unsubscribe(NotificationKeys.openToday);
  }

  public ionViewDidEnter() {
    sessionStorage.setItem('hideVisitsCanceled',String(this.hideVisitsCanceled));
  }

  public ngAfterContentInit() {
    console.log('------| Events: agenda-component : ### subscribe ###');

    this.events.subscribe(NotificationKeys.filterAgenda, (data) => {
      this.hideSchedule = (data === 'day');
      this.eventSource = undefined;
      this.loadSelectedDate(this.current, this.currentIndex, true);
    });

    this.events.subscribe(NotificationKeys.openToday, () => {
      const today = moment();
      this.calendar.currentDate = today.toDate();

      if (!this.isMonthView) {
        this.loadSelectedDate(today, this.currentIndex, false);
      }
    });
  }

  public refresh() {
    this.formatMonthsToView();

    if (!this.isMonthView) {
      for (let i = 0; i < this.dateList.length; i++) {
        const day = this.dateList[i];
        day.selected = (i === this.currentIndex);
      }
    }
  }

  public reloadSelectedDate() {
    this.loadSelectedDate(this.current, this.currentIndex, false);
  }

  public toggleAgendaView() {
    this.isMonthView = !this.isMonthView;
    this.eventSource = undefined;

    if (!this.isMonthView) {
      this.loadWeekView();
      this.reloadSelectedDate();
    } else {
      this.calendar.currentDate = this.current.toDate();
    }
  }

  public getCurrentView() {
    return this.isMonthView ? 'view' : '';
  }

  public loadSelectedDate(date: Moment, index: number, updateCalendar?: boolean) {

    const diff = moment(date).startOf('day').diff(moment(this.current).startOf('day'), 'days');
    const monthDiff = this.isMonthView ? moment(date).startOf('month').diff(moment(this.current).startOf('month'), 'months') : 0;
    this.current = date;
    this.events.publish(`AgendaPage:${NotificationKeys.selectedDate}`, this.current);

    if (this.isMonthView) {
      this.refresh();

      if (updateCalendar) {
        this.calendar.currentDate = this.current.toDate();
      } else {
        if (monthDiff !== 0 || this.eventSource === undefined) {
          this.loadEventSource();
        } else {
          if (this.agendaScheduleComponent) {
            this.agendaScheduleComponent.loadSchedule(this.current);
          }
        }
      }
    } else {
      if (!updateCalendar) { index = index + diff; }

      if (index < 0) {
        index = this.currentIndex = 0;
        updateCalendar = true;
        this.loadWeekView();
      } else if (index > 6) {
        index = this.currentIndex = 6;
        updateCalendar = true;
        this.loadWeekView();
      }

      this.currentIndex = index;
      this.refresh();

      if (updateCalendar) {
        this.calendar.currentDate = this.current.toDate();
      }
      this.loadEventSource();
    }
  }

  public swipeEvent(e) {
    const deltaX = e.deltaX;
    const velocity = e.velocity;

    const date = moment(this.current);
    let calendarDate;

    // Swipe event to left or right
    calendarDate = (deltaX > 0) ?
      (velocity > 2) ? date.subtract(this.week, 'days') : date.subtract(this.day, 'days') :
      (velocity < -2) ? date.add(this.week, 'days') : date.add(this.day, 'days');

    this.eventSource = undefined;
    this.loadSelectedDate(moment(calendarDate), this.currentIndex, false);
  }

  public isSelected(item) {
    return item.selected ? 'selected' : '';
  }

  // Calendar events
  public onCurrentDateChanged(e) {
    const m = moment(e);
    this.loadSelectedDate(m, this.currentIndex, this.hideSchedule);
  }

  public onEventSelected(event) {
    const data = {
      event: event.model,
      isCreateMode: false,
    };

    this.navCtrl.push(EventsPage, data);
  }

  public onTimeSelected(time) {
    if (time.events.length === 0) {
      const event = new EventModel(null);
      event.StartDateTime = moment(time.selectedTime).format();

      const data = {
        event,
        isCreateMode: true,
      };
      this.navCtrl.push(EventsPage, data);
    }
  }

  public goToLastMonth() {
    this.eventSource = undefined;
    this.loadSelectedDate(moment(this.current).subtract(1, 'months'), this.currentIndex, true);
  }

  public goToNextMonth() {
    this.eventSource = undefined;
    this.loadSelectedDate(moment(this.current).add(1, 'months'), this.currentIndex, true);
  }

  private formatMonthsToView() {
    const diffInYears = moment().startOf('year').diff(moment(this.current).startOf('year'), 'years');

    this.currentMonth = (diffInYears !== 0) ? this.current.format('MMM[ de ]YYYY') : this.current.format('MMMM');
    this.lastMonth = moment(this.current).subtract(1, 'month').format('MMMM');
    this.nextMonth = moment(this.current).add(1, 'month').format('MMMM');
  }

  private loadWeekView() {
    this.dateList = [];

    for (let i = 0; i < 7; i++) {
      const diff = i - this.currentIndex;
      this.dateList.push({
        day: moment(this.current).add(diff, 'days'),
        select: false,
      });
    }
  }

  private loadEventSource() {

    let beginKey;
    let endKey;

    if (!this.isMonthView) {
      beginKey = moment(this.current).startOf('day').format();
      endKey = moment(this.current).endOf('day').format();
    } else {
      beginKey = moment(this.current).startOf('month').format();
      endKey = moment(this.current).endOf('month').format();
    }

    let pageSize = 1;

    LoadingManager.getInstance().show().then(() => {
      this.appPreferences.fetch('user_id').then((id) => {
        let smartSql = 'select count(*) from {Event} where {Event:OwnerId} = "' + id + '"';

        this.querySmartFromSoup(smartSql, pageSize).then((success) => {
          success.currentPageOrderedEntries.forEach((res) => {
            pageSize = res[0];
          });

          smartSql = 'select {Event:Id}, {Event:StartDateTime}, {Event:EndDateTime}, ' +
            '{Event:IsAllDayEvent}, {Event:IsOutlook__c}, {Event:Status__c} ' +
            'from {Event} where {Event:StartDateTime} >= "' + beginKey +
            '" and {Event:EndDateTime} <= "' + endKey + '" and ' +
            '{Event:OwnerId} = "' + id +
            '" order by {Event:StartDateTime}, {Event:EndDateTime}, {Event:CreatedDate}';

          this.querySmartFromSoup(smartSql, pageSize).then((response) => {
            this.tempEventSource = [];

            response.currentPageOrderedEntries.forEach((res) => {
              const isAllDayEvent = DataHelper.getBoolean(res[3]);
              const eventId = res[0];
              const start = isAllDayEvent ? moment(res[1]).utc().startOf('day').toDate() : moment(res[1]).toDate();
              const end = isAllDayEvent ? moment(res[1]).utc().endOf('day').toDate() : moment(res[2]).toDate();
              const isOutlook = DataHelper.getBoolean(res[4]);
              const status = res[5];

              this.tempEventSource.push({
                allDay: isAllDayEvent,
                endTime: end,
                id: eventId,
                startTime: start,
                title: isOutlook ? 'Outlook' : status,
              });
            });

            this.updateEventSource();

            LoadingManager.getInstance().hide();

            if (this.agendaScheduleComponent) {
              this.agendaScheduleComponent.loadSchedule(this.current);
            }

          }).catch((err) => {
            this.error(err);
          });

        }).catch((err) => {
          this.error(err);
        });

      }).catch((err) => {
        this.error(err);
      });
    });
  }

  private updateEventSource() {
    this.eventSource = this.tempEventSource;
  }

  private querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getEventService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  private queryRangeFromSoup(beginKey, endKey, pageSize): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getEventService().queryRangeFromSoup('StartDateTime', beginKey, endKey,
        'ascending', pageSize, 'StartDateTime', (response) => {
          resolve(response);
        }, (error) => {
          reject(error);
        });
    });
  }

  private cssForStatus(status: string, isOutlook: boolean) {
    return DataHelper.cssForStatus(status, isOutlook);
  }

  private error(err: string) {
    console.log(err);
    LoadingManager.getInstance().hide();
  }

  //MAXXIDATA
  public hideCanceled(e){
    console.log(e.checked);
    this.hideVisitsCanceled = e.checked;
    sessionStorage.setItem('hideVisitsCanceled',String(e.checked));
  }
  //FIM MAXXIDATA

}
