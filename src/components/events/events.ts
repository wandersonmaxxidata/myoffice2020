import { AppPreferences } from '@ionic-native/app-preferences';
import { Component } from '@angular/core';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { DateHelper } from '../../shared/Helpers/DateHelper';
import { EventModel } from './../../app/model/EventModel';
import { Events } from 'ionic-angular/util/events';
import { EventService } from './../../providers/event/event-service-salesforce';
import { EventsPage } from '../../pages/events/events';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { NavController } from 'ionic-angular';
import { NotificationKeys } from '../../shared/Constants/Keys';
import { OutlookPage } from './../../pages/outlook/outlook';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { DataErrorDaoProvider } from './../../providers/data-error-dao/data-error-dao';
import { HomePage } from '../../pages/home/home';

export const EventStatusLabels = {
  canceled: 'Cancelado',
  executed: 'Executado',
  inProgress: 'Em Andamento',
  nextEvent: 'Item Created',
  outlook: 'Outlook',
  toStart: 'A Iniciar',
};

export const EventStatusCss = {
  canceled: 'canceled',
  executed: 'executed',
  inProgress: 'in-progress',
  nextEvent: 'next-event',
  outlook: 'outlook',
  toStart: 'to-start',
};

@Component({
  providers: [EventService],
  selector: 'events-card',
  templateUrl: 'events.html',
})
export class EventsComponent {

  /**
   * Array com as contas trazidas do salesforce.
   */
  public keys = [];
  public keysLabel = false;
  public hasPerfil = false;
  public firstSync = false;
  public eventsObj = {};
  public eventsCursor: any = { currentPageOrderedEntries: [] };
  public context: EventsComponent = this;
  public sortBy = { field: 'StartDateTime', label: 'StartDateTime' };
  public space = ' ';

  /**
   * Constructor com a chamada dos métodos de registro da tabela da base de dados,
   * sincronização com salesforce e chamada dos registros locais.
   * @param navCtrl
   * Recurso do próprio Angular que permite a navegação entre páginas
   * @param service
   * Serviço com os recursos do salesforce.
   */

  constructor(
    public navCtrl: NavController,
    public dataErrorDAO: DataErrorDaoProvider,
    public service: ServiceProvider,
    public events: Events,
    public appPreferences: AppPreferences,
    public storage: Storage,
  ) {
    this.eventsObj = {};
    this.eventsCursor = '';
  }

  public ngOnInit() {
    console.log('------| Events: events-component : !!! unsubscribe !!!');
    this.events.unsubscribe(NotificationKeys.openHome);
  }

  public ngAfterContentInit() {
    console.log('------| Events: events-component : ### subscribe ###');
    this.events.subscribe(NotificationKeys.openHome, () => {
      this.loadPage();
    });
  }

  //MAXXIDATA
  public checkError(id) {
    return new Promise(resolve => {
      this.dataErrorDAO.getError(id).then(data => {
        if (data > 0) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  public openEventDetail(event: EventModel) {
    this.dataErrorDAO.getError(event.Id).then(data => {
      //console.log(data);
      if (data > 0) {
        sessionStorage.setItem('pageReturn','home');
        sessionStorage.setItem('filter',String(event.Id));
        this.navCtrl.setRoot(HomePage).then(() => {});
      } else {
        const page = event.IsOutlook__c ? OutlookPage : EventsPage;
        if (event && (event.Id || event.ExternalID__c)) {
          LoadingManager.getInstance().show().then(() => {
            this.queryExactFromSoup(event).then(async (response) => {
              LoadingManager.getInstance().hide();
              if (response.currentPageOrderedEntries.length > 0) {
                const data = {
                  event: await this.mountEvent(response.currentPageOrderedEntries[0]),
                  isCreateMode: false,
                };
                this.navCtrl.push(page, data);
              }
            }, (error) => {
              this.error(error);
            });
          });
        }
      }
    });
  }
  //FIM MAXXIDATA

  public getKeyName(key: string) {
    return (Number(key) === 0) ? 'HOJE' : 'AMANHÃ';
  }

  public getFormattedDate(date: string) {
    return DateHelper.formattedDateToEventCard(date);
  }

  private loadPage() {
    LoadingManager.getInstance().show().then(() => {
      setTimeout(() => {
        this.checkPerfil();
        this.execStandardQuery();
      }, 350);
    });
  }

  private checkPerfil() {
    this.storage.get('user_profile').then((user_profile: any) => {
      if (user_profile) {
        this.firstSync = true;
        this.hasPerfil = DataHelper.checkBooleanValuesObj(user_profile);
      } else {
        this.firstSync = false;
      }
    });
  }

  private execStandardQuery() {
    this.appPreferences.fetch('user_id').then((id) => {

      const startDate = moment().startOf('day');
      const beginKey = DateHelper.getUTCTimestamp(startDate.format());
      const endKey = DateHelper.getUTCTimestamp(startDate.add(1, 'days').endOf('day').format());

      const smartSql = `
        SELECT
          {Event:Id},
          {Event:StartDateTime},
          {Event:EndDateTime},
          {Event:IsOutlook__c},
          {Event:Status__c},
          {Event:BR_Tipo_de_Visita__c},
          {Event:Location},
          {Event:ExternalID__c},
          {Event:IsAllDayEvent},
          {Event:Subject}
        FROM
          {Event}
        Where
          {Event:OwnerId} = "${id}"
          AND {Event:BR_StartTime__c} >= "${beginKey}"
          AND {Event:BR_StartTime__c} <= "${endKey}"
        ORDER BY
          {Event:BR_StartTime__c}, {Event:BR_EndTime__c}, {Event:CreatedDate}
      `;

      this.querySmartFromSoup(smartSql, 50).then((response) => {
        this.eventsObj = [];
        this.keysLabel = (response.totalPages === 0);

        response.currentPageOrderedEntries.forEach((item) => {
          this.mountItem(item);
        });
        this.eventsCursor = response;
        LoadingManager.getInstance().hide();
      }, (error) => {
        this.error(error);
      });
    });
  }

  private mountItem(item) {
    const IsOutlook__c = DataHelper.getBoolean(item[3]);
    const Status__c = item[4];
    let nameClient = item[9].split('- ');
    this.checkError(item[0]).then(data => {
      const event = {
        Id: item[0],
        StartDateTime: item[1],
        EndDateTime: item[2],
        IsOutlook__c,
        Status__c,
        BR_Tipo_de_Visita__c: item[5],
        Location: item[6],
        ExternalID__c: item[7],
        IsAllDayEvent: DataHelper.getBoolean(item[8]),
        Subject: nameClient[1],
        cssSuffix: DataHelper.cssForStatus(Status__c, IsOutlook__c),
        errorSincy: data,
      };

      if (DateHelper.isAllDayEventNotAdjusted(event)) {
        event.StartDateTime = DateHelper.adjustAllDayEventTimezone(event.StartDateTime);
        event.EndDateTime = DateHelper.adjustAllDayEventTimezone(event.EndDateTime);
      }

      const key = String(moment(event.StartDateTime).startOf('day').diff(moment().startOf('day'), 'days'));

      if (!this.eventsObj[key]) {
        this.eventsObj[key] = [];
      }

      this.eventsObj[key].push(event);
      this.keys = Object.keys(this.eventsObj);
    });
  }

  private querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getEventService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }

  private queryExactFromSoup(event): Promise<any> {
    const matchKey = event.Id || event.ExternalID__c;
    const indexPath = event.Id ? 'Id' : 'ExternalID__c';

    return new Promise((resolve, reject) => {
      this.service.getEventService().queryExactFromSoup(indexPath, matchKey, 1, 'ascending', indexPath, (response) => {
        resolve(response);
      }, (error) => {
        this.error(error);
        reject(error);
      });
    });
  }

  private error(err: string) {
    console.log(err);
    LoadingManager.getInstance().hide();
  }

  private mountEvent(eventObject): Promise<any> {
    return new Promise((resolve, reject) => {
      const push = new EventModel(eventObject);
      if (push.WhatId != null) {
        push.setAccount(this.service.getClientsService()).then((resClients) => {
          resolve(push);
        }).catch((err) => {
          reject(err);
        });
      } else {
        resolve(push);
      }
    });
  }

}
