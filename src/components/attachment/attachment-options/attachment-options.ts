import { ApplicationRef, Component } from '@angular/core';
import { AttachmentModel } from '../../../app/model/AttachmentModel';
import { AttachmentSuffix } from '../../../shared/Constants/Types';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController, NavParams, PopoverController, ToastController, ViewController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

@Component({
  selector: 'page-attachment-options',
  templateUrl: 'attachment-options.html',
})
export class AttachmentOptions {

  public parentId: string;
  public attachment: AttachmentModel;
  public attachments: AttachmentModel[];
  public currentPhotoCommentary = '';
  public haveImage = false;

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    private camera: Camera,
    public params: NavParams,
    public app: ApplicationRef,
    public popoverCtrl: PopoverController,
    private toastCtrl: ToastController,
    public statusBar: StatusBar,
  ) {
    this.parentId = params.get('visitParentId');
  }

  public initialize(): void {
    this.attachments = new Array<AttachmentModel>();
  }
  public ionViewDidLoad(): void {
    this.initialize();
  }

  public performAction(action?: any): void {
    if (action === 'camera') {
      this.takePhoto('camera');
    } else if (action === 'galeria') {
      this.takePhoto('galeria');
    }
  }

  public takePhoto(sourceTypeSelected): void {
    let source;
    if (sourceTypeSelected === 'camera') {
      source = this.camera.PictureSourceType.CAMERA;
    } else if (sourceTypeSelected === 'galeria') {
      source = this.camera.PictureSourceType.PHOTOLIBRARY;
    }
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: source,
      quality: 40,
      targetWidth: 720,
      targetHeight: 540,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
    };

    this.camera.getPicture(options)
      .then((data_url) => {
        const attachment: AttachmentModel = new AttachmentModel(null);
        attachment.Id = `${Math.random().toString(36).substring(7).toUpperCase()}`;
        attachment.Name = `${Math.random().toString(36).substring(7).toUpperCase()}.jpeg`;
        attachment.Body = data_url;
        attachment.ParentId = this.parentId;
        attachment.ContentType = 'image/jpeg';
        setTimeout(() => {
          this.attachment = attachment;
          this.haveImage = true;
          this.app.tick();
        }, 500);

      });
  }

  public fileToBase64(file: any): void {
    const reader = new FileReader();
    reader.onload = (e) => {
      const base64 = reader.result;
      const attachment: AttachmentModel = new AttachmentModel(null);
      attachment.Id = `${Math.random().toString(36).substring(7).toUpperCase()}`;
      attachment.Name = file.name;
      attachment.Body = base64.substring(base64.indexOf(',') + 1, base64.length);
      attachment.ParentId = this.parentId;
      attachment.ContentType = file.type;
      setTimeout(() => {
        this.attachment = attachment;
        this.haveImage = true;
        this.app.tick();
      }, 500);
    };
    reader.readAsDataURL(file);
  }

  public prepareImage(file: any): string {
    if (file) {
      const type = file.ContentType.substring(0, file.ContentType.indexOf('/'));
      const path = `assets/imgs/${type}.png`;
      const base64 = `data:${file.ContentType};base64,${file.Body}`;
      return (type === 'image') ? base64 : path;
    }
  }

  public openFileChooser(event: any): void {
    const file = event.target.files[0];
    this.performAction(file);
  }

  public closeModal() {
    this.navCtrl.pop();
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }
  public savePhoto() {
    this.attachment.Name = this.attachment.Name.replace('.', `${this.portraitLandScape()}.`);
    this.attachment.Description = this.currentPhotoCommentary;
    this.viewCtrl.dismiss(this.attachment);
  }

  public presentToast(message: string): void {
    const toast = this.toastCtrl.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  public portraitLandScape() {
    const img = document.createElement('img');
    img.src =  this.prepareImage(this.attachment);

    const w = (img.naturalWidth || img.width);
    const h = (img.naturalHeight || img.height);
    return ((h > w) ? AttachmentSuffix.portrait : AttachmentSuffix.landscape);
  }

}
