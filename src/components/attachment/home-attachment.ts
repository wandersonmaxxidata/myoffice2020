import { ApplicationRef, Component, Input } from '@angular/core';
import { AttachmentBO } from '../../shared/BO/AttachmentBO';
import { AttachmentModel } from '../../app/model/AttachmentModel';
import { AttachmentOptions } from './attachment-options/attachment-options';
import { Camera } from '@ionic-native/camera';
import { ModalController, PopoverController, ToastController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service-salesforce';
import { Visita__cModel } from '../../app/model/Visita__cModel';

@Component({
  selector: 'page-home-attachment',
  templateUrl: 'home-attachment.html',
})
export class AttachmentComponent {

  @Input('isReadOnly')
  public isReadOnly: boolean;

  @Input('visit')
  public visit: Visita__cModel;

  public attachments: AttachmentModel[] = [];
  public attachment: AttachmentModel;
  public attachmentBO: AttachmentBO;
  public parentId: any = {};

  constructor(
    public camera: Camera,
    public popoverCtrl: PopoverController,
    public toastCtrl: ToastController,
    public app: ApplicationRef,
    public modalCtrl: ModalController,
    public serviceProvider: ServiceProvider,
  ) {
    this.attachments = new Array<AttachmentModel>();
    this.attachmentBO = new AttachmentBO(this.serviceProvider);
  }

  public setParentId(parentId) {
    this.parentId = parentId;
    this.loadStackUp(this.parentId);
    console.log('### setParentId: parentId, this.parentId', parentId, this.parentId);
  }

  private loadStackUp(parentId) {
    return this.attachmentBO.load(parentId).then((attachmentFromModal) => {
      console.log('### loadStackUp return getAttachment: attachment and parentID:', attachmentFromModal, parentId);
      if (attachmentFromModal) {
        this.attachments = attachmentFromModal;
        console.log('------| ATTACHMENTS: ', this.attachments);
        this.app.tick();
      }
    });
  }

  private cameraModal() {
    const modal = this.modalCtrl.create(AttachmentOptions, { visitParentId: this.parentId }, {showBackdrop: true, enableBackdropDismiss: true, cssClass: 'cameraModalClass'});
    console.log('### AttachmentModal create: parentId: ', this.parentId);
    modal.onDidDismiss((data) => {
      if (data) {
        console.log('### AttachmentModal returned data: ', data);
        this.saveStackUp(data);
      }
    });
    modal.present();
  }

  private saveStackUp(attachmentFromModal: AttachmentModel) {
    console.log('### saveStackUp receive: attachmentFromModal and parentId:', attachmentFromModal, this.parentId);

    attachmentFromModal.IsUploaded = false;
    return this.attachmentBO.save(attachmentFromModal).then((newAttachment) => {
      this.attachments.push(newAttachment);
      console.log('### saveStackUp return setAttachment: this.attachment', this.attachment);
      this.loadStackUp(this.parentId);
      this.app.tick();
    });
  }

  private removeAttachment(attachment: AttachmentModel, index: number) {
    return this.attachmentBO.remove(attachment).then(() => {
      this.attachments.splice(index, 1);
      this.app.tick();
    });
  }

  private prepareImageHome(file: any): string {
    if (file) {
      const type = file.ContentType.substring(0, file.ContentType.indexOf('/'));
      const path = `assets/imgs/${type}.png`;
      const base64 = `data:${file.ContentType};base64,${file.Body}`;
      const value = (type === 'image') ? base64 : path;
      return value;
    }
  }

  private presentToast(message: string): void {
    const toast = this.toastCtrl.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

}
