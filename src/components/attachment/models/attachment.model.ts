export class AttachmentToUploadModel {
    public ParentId: string = null;
    public Name: string = null;
    public Body: string = null;
    public ContentType: string = null;
    public Description: string = null;


    constructor(values: Object = {}) {
        Object.keys(this).forEach((key) => {
            if (values.hasOwnProperty(key)) {
                this[key] = values[key];
            }
        });
    }
}
