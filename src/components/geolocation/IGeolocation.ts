export interface IGeolocation {
    altitude;
    longitude;
    latitude;
}
