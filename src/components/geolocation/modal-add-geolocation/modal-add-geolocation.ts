import { Component } from '@angular/core';
import { Events, ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { ServiceProvider } from '../../../providers/service-salesforce';

@Component({
  selector: 'modal-add-geolocation',
  templateUrl: 'modal-add-geolocation.html',
  providers: [Geolocation],
})
export class ModalAddGeolocationPage {

  public altitude;
  public longitude;
  public latitude;
  public clientsLocation;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    public geolocation: Geolocation,
    public events: Events,
    public view: ViewController,
    public service: ServiceProvider,
  ) { }

  public ionViewWillEnter() {
    this.clientsLocation = this.navParams.data.isClientsLocal;
  }

  public ionViewDidEnter() {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('---| selectedOption:geo: ', resp);
      this.altitude = resp.coords.altitude ? resp.coords.altitude : undefined;
      this.latitude = resp.coords.latitude ? resp.coords.latitude : undefined;
      this.longitude = resp.coords.longitude ? resp.coords.longitude : undefined;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  public dismiss() {
    this.view.dismiss();
  }

  public saveGeo() {
    const geolocation = {
      modalAlt: this.altitude,
      modalLat: this.latitude,
      modalLong: this.longitude,
    };
    this.view.dismiss(geolocation);
  }

}
