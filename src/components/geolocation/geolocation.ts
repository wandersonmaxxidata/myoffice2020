import { AlertController, Events, ModalController, ToastController } from 'ionic-angular';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { AppPreferences } from '@ionic-native/app-preferences';
import { ClientBO } from '../../shared/BO/ClientBO';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { IGeolocation } from './IGeolocation';
import { ModalAddGeolocationPage } from './modal-add-geolocation/modal-add-geolocation';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ServiceProvider } from '../../providers/service-salesforce';
import { ToastHelper } from '../../shared/Helpers/ToastHelper';
import { LoadingManager } from '../../shared/Managers/LoadingManager';

@Component({
  selector: 'geolocation',
  templateUrl: 'geolocation.html',
  providers: [Geolocation],
})
export class GeolocationComponent {

  public geolocationCard = true;
  public altitude = undefined;
  public longitude = undefined;
  public latitude = undefined;
  public clientBO: ClientBO;

  @Output()
  public onLocationChange = new EventEmitter();

  @Input()
  private isReadOnly = false;

  @Input()
  private isClientsLocal = false;

  @Input()
  private client;

  @Input()
  private location: IGeolocation;

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public modalCtrl: ModalController,
    public events: Events,
    public appPreferences: AppPreferences,
    private geolocation: Geolocation,
    private alertCtrl: AlertController,
  ) { }

  public ngOnInit() {
    if (this.isClientsLocal) {
      this.clientBO = new ClientBO(this.service);
      this.latitude = this.client.GC_Latitude__c;
      this.longitude = this.client.GC_Longitude__c;
    }
  }

  public setLocation(location: IGeolocation) {
    this.altitude = location.altitude;
    this.latitude = location.latitude;
    this.longitude = location.longitude;
  }

  // public geoModal() {
  //   if (this.isReadOnly && this.isClientsLocal) {
  //     AlertHelper.showCreditExpirationDateAlert(this.alertCtrl);
  //   } else {
  //     const modal = this.modalCtrl.create(ModalAddGeolocationPage, { isClientsLocal: this.isClientsLocal });

  //     modal.onDidDismiss((geolocationData) => {
  //       if (geolocationData) {
  //         console.log('---| Modal returned data: Geolocation: ', geolocationData);
  //         const geoAlt = geolocationData.modalAlt ? String(geolocationData.modalAlt).split('.') : geolocationData.modalAlt;
  //         const geoLat = geolocationData.modalLat ? String(geolocationData.modalLat).split('.') : geolocationData.modalLat;
  //         const geoLong = geolocationData.modalLong ? String(geolocationData.modalLong).split('.') : geolocationData.modalLong;

  //         this.altitude = (geoAlt && geoAlt[1]) ? `${geoAlt[0]}.${geoAlt[1].substr(0, 14)}` : geolocationData.modalAlt;
  //         this.latitude = (geoLat && geoLat[1]) ? `${geoLat[0]}.${geoLat[1].substr(0, 14)}` : geolocationData.modalLat;
  //         this.longitude = (geoLong && geoLong[1]) ? `${geoLong[0]}.${geoLong[1].substr(0, 14)}` : geolocationData.modalLong;

  //         const geo = {
  //           altitude: this.altitude,
  //           latitude: this.latitude,
  //           longitude: this.longitude,
  //         };

  //         this.onLocationChange.emit(geo);
  //       }
  //     });

  //     modal.present();
  //   }
  // }


  public geoModal() {
    LoadingManager.getInstance().show().then(() => { });
    if (this.isReadOnly && this.isClientsLocal) {
      LoadingManager.getInstance().hide();
      AlertHelper.showCreditExpirationDateAlert(this.alertCtrl);
    } else {
      this.geolocation.getCurrentPosition().then((resp) => {

        const geoAlt = resp.coords.altitude ? String(resp.coords.altitude).split('.') : resp.coords.altitude;
        const geoLat = resp.coords.latitude ? String(resp.coords.latitude).split('.') : resp.coords.latitude;
        const geoLong = resp.coords.longitude ? String(resp.coords.longitude).split('.') : resp.coords.longitude;

        this.altitude = (geoAlt && geoAlt[1]) ? `${geoAlt[0]}.${geoAlt[1].substr(0, 14)}` : resp.coords.altitude;
        this.latitude = (geoLat && geoLat[1]) ? `${geoLat[0]}.${geoLat[1].substr(0, 14)}` : resp.coords.latitude;
        this.longitude = (geoLong && geoLong[1]) ? `${geoLong[0]}.${geoLong[1].substr(0, 14)}` : resp.coords.longitude;

        const geo = {
          altitude: this.altitude,
          latitude: this.latitude,
          longitude: this.longitude,
        };

        this.onLocationChange.emit(geo);

        LoadingManager.getInstance().hide();
      }).catch((error) => {
        LoadingManager.getInstance().hide();
        AlertHelper.showMessageError(this.alertCtrl, 'Erro ao acessar sua geolocalização.');
        console.log('Error getting location', error);
      });
    }
  }

  public sliceGeo(geoType) {

    if (geoType === 'lat') {
      let geoLat: string[];
      geoLat = this.latitude.split('.');
      if (geoLat[1]) {
        geoLat[1] = geoLat[1].substr(0, 14);
        this.latitude = geoLat[0] + '.' + geoLat[1];
      } else {
        this.latitude = this.latitude;
      }
    }

    if (geoType === 'alt') {
      let geoAlt: string[];
      geoAlt = this.altitude.split('.');
      if (geoAlt[1]) {
        geoAlt[1] = geoAlt[1].substr(0, 14);
        this.altitude = geoAlt[0] + '.' + geoAlt[1];
      } else {
        this.altitude = this.altitude;
      }
    }

    if (geoType === 'long') {
      let geoLong: string[];
      geoLong = this.longitude.split('.');
      if (geoLong[1]) {
        geoLong[1] = geoLong[1].substr(0, 14);
        this.longitude = geoLong[0] + '.' + geoLong[1];
      } else {
        this.longitude = this.longitude.modalLong;
      }
    }
  }

  private emitChange() {
    this.onLocationChange.emit({
      altitude: this.altitude,
      latitude: this.latitude,
      longitude: this.longitude,
    });
  }

  public save() {
    this.client.GC_Latitude__c = this.latitude;
    this.client.GC_Longitude__c = this.longitude;

    this.clientBO.saveClientGeolocation(this.client).then((result: any) => {
      ToastHelper.showSuccessMessage(this.toastCtrl);
    });
  }

  private showgeolocationCardButton() {
    this.geolocationCard = !this.geolocationCard;
  }

  private isGeolocationButtonDisabled() {
    return this.isReadOnly && !this.isClientsLocal;
  }

}
