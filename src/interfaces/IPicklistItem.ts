export interface IPicklistItem {
    active: boolean;
    defaultValue: boolean;
    label: string;
    validFor: string;
    value: string;
}
