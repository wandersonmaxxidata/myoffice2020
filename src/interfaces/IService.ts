export interface IService {

    syncDown(handleProgress: Function, context: any): Promise<any>;
    syncUp(includeSecondaryIdFields: boolean, handleProgress: Function, context: any);
    registerSoup(sucess: Function, error: Function);
    soupExists(sucess: Function, error: Function);

    queryAllFromSoup(orderPath:string, direction:string, pageSize:number, sucess: Function, error: Function)
    moveCursorToNextPage(cursor:any, sucess: Function, error: Function)
    querySmartFromSoup(smartSql:string, pageSize:number, sucess: Function, error: Function)
    queryExactFromSoup(indexPath:string, matchKey:string, pageSize:number, direction:string, orderPath:string, sucess: Function, error: Function)
    transformEntrie(data:any): BaseModel
    upsertSoupEntries(entries: Array<any>, sucess: Function, error: Function)

    // Ghost
    cleanResyncGhosts?(success: Function, error: Function)

    getSyncStatus?(success: Function, error: Function)
    
    removeAllEntries(soupEntryIds: string[], sucess: Function, error: Function)
}
