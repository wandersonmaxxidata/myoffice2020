export interface WeedInput {
    weedType: string
    levelOfInfestation: string
}

export interface WeedOutput {
    weedType: string
    levelOfInfestation: string
}

export interface WeedHandlingModalInput {
    weedType: string;
    dosage: string;
  }
  export interface WeedHandlingModalOutput {
    weedType: string;
    dosage: string;
  }