import { Visita__cModel } from '../app/model/Visita__cModel';

export interface IVisit {
    loadData();
    handleInitialState(visit: Visita__cModel);
    saveVisit();
    completeVisit();
    cancelVisit();
}
