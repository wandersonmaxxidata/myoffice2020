import { Hibrido_de_Visita__cModel } from '../app/model/Hibrido_de_Visita__cModel';
import { Praga__cModel } from '../app/model/Praga__cModel';

export interface IHybridUiModel {
    hybrid: Hibrido_de_Visita__cModel;
    plagues: Praga__cModel[];
}
