import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from './../../providers/database/database';
import { DataModel } from '../../app/model/DataModel';
import { Injectable } from '@angular/core';
import { ShildDataModel } from '../../app/model/ShildDataModel';


@Injectable()
export class DataDaoProvider {

  constructor(
    public db: DatabaseProvider
    ) {
  }

  public insert(DataModel: DataModel){
    return new Promise<any>(resolve => {

      this.db.getDb().then((db: SQLiteObject) =>{
          let sqlDataTrue = 'select id from data WHERE dat_sop_id = ? ';
          let valuesDataTrue = [DataModel.getSopId()];
          db.executeSql(sqlDataTrue, valuesDataTrue).then((retrunCheck:any)=>{
              if(retrunCheck.rows.length == 0){
                  let sql = 'insert into data ( id,dat_data_type, dat_sop_id, dat_event_sop_id, dat_data) values (?,?,?,?,?)';
                  let values = [DataModel.getId(), DataModel.getDataType(),DataModel.getSopId(),DataModel.getEventId(),DataModel.getData()];
                  db.executeSql(sql,values).then((dataInsert)=>{
                    resolve(dataInsert);
                  }).catch(e => {
                    //console.log(e)
                    resolve(0);
                  });
              }else{
                resolve(0);
              }
          }).catch(e => {
            //console.log(e)
            resolve(0);
          });
      }).catch(e => {
        //console.log(e)
        resolve(0);
      });

     });
   }


   public insertShildData(ShildDataModel: ShildDataModel){
    return new Promise(resolve => {

      this.db.getDb().then((db: SQLiteObject) =>{
          let sqlDataTrue = 'select id from shild_data WHERE sda_sop_id = ? ';
          let valuesDataTrue = [ShildDataModel.getSopId()];
          db.executeSql(sqlDataTrue, valuesDataTrue).then((retrunCheck:any)=>{
              if(retrunCheck.rows.length == 0){
                  let sql = 'insert into shild_data ( id,sda_type, sda_data, sda_sop_id, sda_id_data) values (?,?,?,?,?)';
                  let values = [ShildDataModel.getId(), ShildDataModel.getType(),ShildDataModel.getData(),ShildDataModel.getSopId(),ShildDataModel.getDataId()];
                  db.executeSql(sql,values).then(()=>{
                    resolve(1);
                  }).catch(e => {
                    //console.log(e)
                    resolve(0);
                  });
              }else{
                resolve(0);
              }
          }).catch(e => {
           // console.log(e)
            resolve(0);
          });
      }).catch(e => {
       // console.log(e)
        resolve(0);
      });

     });
   }

   public delete(id: number){
    return new Promise(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'delete FROM data where id = ?';
        let values = [id];
        resolve(1);
        db.executeSql(sql,values).then(()=>{
        }).catch(e => {
          //console.log(e)
          resolve(0);
        });
      }).catch(e => {
         // console.log(e)
          resolve(0);
        });
    });
  }


  public getAll(){
    return new Promise<any>(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'select * from data';
        db.executeSql(sql,[]).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }
        }).catch(e => {
         // console.log(e)
          resolve(0);
        });
      }).catch(e => {
        //  console.log(e)
          resolve(0);
        });
    });
  }


}