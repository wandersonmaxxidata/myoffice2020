import { BR_SincronizedVisit__cModel } from './../../app/model/BR_SincronizedVisit__cModel';
import { Platform,AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AppPreferences } from '@ionic-native/app-preferences';
import { IService } from '../../interfaces/IService';
import { Storage } from '@ionic/storage';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { Events } from 'ionic-angular/util/events';
/**
 * Serviço com os recursos de armazenamento local e sync do salesforce.
 */
@Injectable()
export class BR_SincronizedVisit__cService implements IService {
    /**
     * Nome de nossa base de dados
     */
    storeName = "myoffice";
    /**
     * classe model de nossa tabela
     */
    entity:BR_SincronizedVisit__cModel;
    /**
     * Configuração da base de dados
     */
    storeConfig = { storeName: this.storeName, isGlobalStore: true };
    /**
     * Query para sincronização entre nossa aplicação e o salesforce
     */
    targetSync = {
        type: "soql", query: "SELECT Id, BR_RtvID__c FROM BR_SincronizedVisit__c "
    };

    allSyncUpFields: Array<string> = ["Id","BR_RtvID__c"];

    secondaryIdFields: Array<string> = [];

    getSyncUpField(includeSecondaryIdFields:boolean): Array<string>{
        let _this = this;
        if(includeSecondaryIdFields){
            return _this.allSyncUpFields;
        }else{
            return _this.allSyncUpFields.filter(function(value){
                return _this.secondaryIdFields.indexOf(value).toString() === '-1';
            })
        }
    }
    constructor(
        platform: Platform, 
        private appPreferences: AppPreferences, 
        public storage:Storage,
        public alertCtrl: AlertController, 
        public events: Events) {
    }

    /**
     * Função com o objetivo de criverificar a exeitencia da "Tabela"
     *
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    soupExists(sucess: Function, error: Function) {
        if (window.hasOwnProperty('soupExists')) {
            window['soupExists'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, function (soupExistsSucess) {
                console.log('soupExistsSucess', soupExistsSucess);
                sucess(soupExistsSucess);
            }, function (err) {
                error(err);
            });
        } else {
            error('propertie \'soupExists\' not found!');
        }
    }

    /**
     * Função com o objetivo de criar a "Tabela"
     *
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    registerSoup(sucess: Function, error: Function) {
        if (window.hasOwnProperty('registerSoup')) {
            window['registerSoup'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, BR_SincronizedVisit__cModel.indexes, function (registerSoupSucess) {
                console.log('registerSoupSucess', registerSoupSucess);
                sucess();
            }, function (err) {
                error(err);
            });
        } else {
            error('propertie \'registerSoup\' not found!');
        }
    }

    /**
     * Função com o objetivo de retornar os recursos do salesforce para a nossa aplicação.
     *
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    syncDown(handleProgress: Function, context: any): Promise<any> {
        var self = this;
        return new Promise((resolve, reject) => {

            var lastSyncDownId;
            if (this.targetSync.query === 'SELECT  FROM Attachment ') {
                resolve();
                return
            }
            BR_SincronizedVisit__cModel.getLastSyncDownId(self.appPreferences).then((response) => {
                lastSyncDownId = response;
                if (window.hasOwnProperty('syncDown')) {
                    window['syncDown'].call(null, this.storeConfig, this.targetSync, BR_SincronizedVisit__cModel.soupName, lastSyncDownId, handleProgress, context, function (syncDownSucess) {
                            console.log('syncDown finalizado com sucesso: BR_SincronizedVisit__c', syncDownSucess);
                            BR_SincronizedVisit__cModel.setLastSyncDownId(self.appPreferences, syncDownSucess.lastSyncDownId).then((response) => {
                                resolve();
                        }).catch((error) => {
                            console.log('syncDown finalizado com erro: BR_SincronizedVisit__c', error);
                            reject(error);
                        });
                    }, function (err) {
                        console.log(err);
                        reject(err);
                    });
                } else {
                    reject('propertie \'syncDown\' not found!');
                }

            }).catch((error) => {
                reject(error);
            });
        })
    }

    /**
     * Função com o objetivo de enviar os registros locais para o salesforce.
     *
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    syncUp(includeSecondaryIdFields: boolean, handleProgress: Function, context: any) {
        var fieldlist = this.getSyncUpField(includeSecondaryIdFields);
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('syncUp')) {
                window['syncUp'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, fieldlist, handleProgress, context, function (syncUpSucess) {
                    console.log('syncUp finalizado com sucesso: BR_SincronizedVisit__c', syncUpSucess);
                    resolve();
                }, function (err) {
                    console.log(err);
                    reject(err);
                });
            } else {
                reject('propertie \'syncUp\' not found!');
            }
        })
    }

    //SYNC UM A UM 
    /**
     * Função com o objetivo de enviar os registros locais para o salesforce um a um.
     *
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    syncUpOnebyOne(includeSecondaryIdFields: boolean,entityName,handleProgress: Function, context: any){

        return new Promise((resolve, reject) => {
            this.storage.get(entityName).then(resultStorage=>{
                console.log(resultStorage)
                let newModel = resultStorage;
                this.statusNoneAll(newModel,parseInt(sessionStorage.getItem('contSync'+entityName)),resultStorage.length).then(data=>{
                    let cont=0;
                    let currentSharpener: any;
                    if (parseInt(sessionStorage.getItem('contSync' + entityName)) == 0) {
                        currentSharpener = 0;
                    } else {
                        currentSharpener = parseInt(sessionStorage.getItem('contSync' + entityName)) - 1;
                    }
                    console.log(resultStorage)
                    console.log('LINHA ATIVA')
                    console.log(currentSharpener)
                    console.log(resultStorage[currentSharpener][1])
                    this.updateStatus(resultStorage[currentSharpener][1],entityName).then( data  =>{
    
                        console.log(resultStorage[currentSharpener][1])

                        this.syncUpNew(includeSecondaryIdFields,entityName,handleProgress,context).then(datasy =>{
                            cont++;
                            if(datasy){
                                let sharpener = parseInt(sessionStorage.getItem('contSync'+entityName));
                                    this.updateStatusNone(resultStorage[currentSharpener][1]).then( data  =>{
                                        if (sharpener === 0) {
                                            resolve(0);
                                        } else {
                                            resolve(sharpener - 1);
                                        }
                                    });
                                let dateCurrent = new Date();
                                console.log('END 10 ==================================================================')
                                console.log(dateCurrent)
                            }
                        });

                    });
                });
            });
        });

    }


    /**
     * Função com o objetivo de enviar os registros locais para o salesforce.
     *
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    syncUpNew(includeSecondaryIdFields: boolean,entityName,handleProgress: Function, context: any) {
        var fieldlist = this.getSyncUpField(includeSecondaryIdFields);
        return new Promise((resolve, reject) => {
            let alertCtrl= this.alertCtrl;
            let event = this.events;
                if (window.hasOwnProperty('syncUp02')) {
                    window['syncUp02'].call(null, entityName,this.storeConfig, BR_SincronizedVisit__cModel.soupName, fieldlist,handleProgress, context, function (syncUpSucess) {
                        console.log('syncUp finalizado com sucesso: Visita__c', syncUpSucess);
                        resolve(syncUpSucess);
                    }, function (err) {
                        console.log(err);
                        
                        AlertHelper.showDialogErrorSyncy(alertCtrl, () => {
                            resolve(true);
                          }, () => {
                            event.publish('endSync');
                            sessionStorage.setItem('statusSync' + entityName, 'DONE');
                            resolve(true);
                          });

                        // reject(err);
                    });
                } else {
                    resolve(true);
                    // reject('propertie \'syncUp\' not found!');
                }
        })
    }
//FIM SYNC UM A UM 
//PREPARANDO ITEM

    public statusNoneAll(inativeData,onecheck01,onecheck02){
        return new Promise((resolve) => {
            let cont=0;
            if(onecheck01 === onecheck02){
                for (const master of  inativeData) {
                    console.log('ZERANDO TABELAS'+cont+'=='+inativeData.length+'=====================================================================')
                    this.updateStatusNone(master[1]).then( data  =>{
                        cont++;
                        if(cont == inativeData.length){
                            console.log('RETORNOU PARA O START=============================================================')
                            resolve(inativeData);
                        }
                    });
                }
            }else{
                resolve();
            }
        });
    }

    public updateStatus(visit: BR_SincronizedVisit__cModel,entityName): Promise<any> {
        return new Promise((resolve, reject) => {
            let filterFilds = JSON.parse(sessionStorage.getItem('temp'+entityName)).filter(data=>{ return data[0].id === visit.Id; });
            console.log('ALTERANDO STATUS')
            console.log(visit)
            visit.__local__= filterFilds[0][0].__local__;
            visit.__locally_created__= filterFilds[0][0].__locally_created__;
            visit.__locally_deleted__= filterFilds[0][0].__locally_deleted__;
            visit.__locally_updated__= filterFilds[0][0].__locally_updated__;
            this.saveVisit(visit).then(() => {
              resolve();
          });
        });
    }

    public updateStatusNone(visit: BR_SincronizedVisit__cModel): Promise<any> {
        return new Promise((resolve, reject) => {
            visit.__local__= false;
            visit.__locally_created__= false;
            visit.__locally_deleted__= false;
            visit.__locally_updated__= false;
            this.saveVisit(visit).then(() => {
              resolve();
          });
        });
    }

    public saveVisit(visit: BR_SincronizedVisit__cModel) {
        return new Promise((resolve, reject) => {
          if (!visit) {
            resolve();
            return;
          }
          this.upsertSoupEntries([visit],
            (savedVisits: BR_SincronizedVisit__cModel[]) => {
              const model = new BR_SincronizedVisit__cModel(savedVisits[0]);
              resolve(model);
            },
            (error) => {
              reject(error);
            });
        });
    }


//FIM PREPARANDO ITEM

    /**
     * Função com o objetivo de retornar as tabelas de nossa base local.
     *
     * @param indexPath
     * index a ser aplicado o filtro de busca
     * @param beginKey
     * valor inicial do filtro de busca
     * @param endKey
     * valor final do filtro de busca
     * @param direction
     * sentido da ordenação -- ascending, descending
     * @param pageSize
     * valor do numer de elementos da pagina
     * @param orderPath
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    queryRangeFromSoup(indexPath:string, beginKey:string, endKey:string, direction:string, pageSize:number, orderPath:string, sucess: Function, error: Function) {
        if (window.hasOwnProperty('queryRangeFromSoup')) {
            window['queryRangeFromSoup'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, indexPath, beginKey, endKey, direction, pageSize, orderPath, sucess, error);
        } else {
            error('propertie \'queryRangeFromSoup\' not found!');
        }
    }

    /**
     * Função com o objetivo de retornar as tabelas de nossa base local.
     *
     * @param indexPath
     * index a ser aplicado o filtro de busca
     * @param likeKey
     * valor do termo no filtro de busca
     * @param direction
     * sentido da ordenação -- ascending, descending
     * @param pageSize
     * valor do numer de elementos da pagina
     * @param orderPath
     * coluna de ordenação
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    queryLikeFromSoup(indexPath:string, likeKey:string, direction:string, pageSize:number, orderPath:string, sucess: Function, error: Function) {
        if (window.hasOwnProperty('queryLikeFromSoup')) {
            window['queryLikeFromSoup'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, indexPath, likeKey, direction, pageSize, orderPath, sucess, error);
        } else {
            error('propertie \'queryLikeFromSoup\' not found!');
        }
    }

    /**
     * Função com o objetivo de retornar as tabelas de nossa base local.
     *
     * @param orderPath
     * index a ser aplicado como ordenação
     * @param direction
     * sentido da ordenação -- ascending, descending
     * @param pageSize
     * valor do numer de elementos da pagina
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    queryAllFromSoup(orderPath:string, direction:string, pageSize:number, sucess: Function, error: Function) {
        if (window.hasOwnProperty('queryAllFromSoup')) {
            window['queryAllFromSoup'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, orderPath, direction, pageSize, sucess, error);
        } else {
            error('propertie \'queryAllFromSoup\' not found!');
        }
    }

    /**
     * Função com o objetivo de retornar as tabelas de nossa base local.
     *
     * @param indexPath
     * index a ser aplicado o filtro de busca
     * @param matchKey
     * valor do filtro de busca
     * @param pageSize
     * valor do numer de elementos da pagina
     * @param direction
     * sentido da ordenação -- ascending, descending
     * @param orderPath
     * coluna de ordenação
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    queryExactFromSoup(indexPath:string, matchKey:string, pageSize:number, direction:string, orderPath:string, sucess: Function, error: Function) {
        if (window.hasOwnProperty('queryExactFromSoup')) {
            window['queryExactFromSoup'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, indexPath, matchKey, pageSize, direction, orderPath, sucess, error);
        } else {
            error('propertie \'queryExactFromSoup\' not found!');
        }
    }

    /**
     * Função com o objetivo de retornar as tabelas de nossa base local.
     *
     * @param smartSql
     * query sql a ser executada
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    querySmartFromSoup(smartSql:string, pageSize:number, sucess: Function, error: Function) {
        if (window.hasOwnProperty('querySmartFromSoup')) {
            window['querySmartFromSoup'].call(null, this.storeConfig, smartSql, pageSize, sucess, error);
        } else {
            error('propertie \'querySmartFromSoup\' not found!');
        }
    }

    moveCursorToNextPage(cursor:any, sucess: Function, error: Function) {
        if (window.hasOwnProperty('moveCursorToPageIndex')) {
            window['moveCursorToNextPage'].call(null, this.storeConfig, cursor, sucess, error);
        } else {
            error('propertie \'moveCursorToNextPage\' not found!');
        }
    }

    moveCursorToPreviousPage(cursor:any, sucess: Function, error: Function) {
        if (window.hasOwnProperty('moveCursorToPreviousPage')) {
            window['moveCursorToPreviousPage'].call(null, this.storeConfig, cursor, sucess, error);
        } else {
            error('propertie \'moveCursorToPreviousPage\' not found!');
        }
    }

    /**
     * Função que possui o objetivo de Alterar/Inserir registros em nossa base local.
     *
     * @param entries
     * Valores no qual iremos inserir/alterar na base local
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    upsertSoupEntries(entries: Array<any>, sucess: Function, error: Function) {
        if (window.hasOwnProperty('upsertSoupEntries')) {
            window['upsertSoupEntries'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, entries, sucess, error);
        } else {
            error('propertie \'upsertSoupEntries\' not found!');
        }
    }

    /**
     * Função com o objetivo de retornar os registros de nossa base local.
     *
     * @param inputStr
     * Conjunto de ids que iremos utilizar para buscar os registros
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    retrieveEntries(inputStr: string, sucess: Function, error: Function) {
        let ids = [inputStr];
        if (window.hasOwnProperty('retrieveEntries')) {
            window['retrieveEntries'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, ids, sucess, error);
        } else {
            error('propertie \'retrieveEntries\' not found!');
        }
    }

    /**
     * Função com o objetivo de remover os registros de nossa base local
     *
     * @param inputStr
     * Conjunto de ids que iremos utilizar para buscar os registros
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    removeEntries(inputStr: string, sucess: Function, error: Function) {
        let ids = [inputStr];
        if (window.hasOwnProperty('removeEntries')) {
            window['removeEntries'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, ids, sucess, error);
        } else {
            error('propertie \'removeEntries\' not found!');
        }
    }

    /**
     * Função com o objetivo de remover os registros de nossa base local
     *
     * @param soupEntryIds
     * Conjunto de ids que iremos utilizar para remover os registros
     * @param sucess
     * Callback de sucesso
     * @param error
     * Callback de erro
     */
    removeAllEntries(soupEntryIds: string[], sucess: Function, error: Function) {
        let ids = soupEntryIds;
        if (window.hasOwnProperty('removeEntries')) {
            window['removeEntries'].call(null, this.storeConfig, BR_SincronizedVisit__cModel.soupName, ids, sucess, error);
        } else {
            error('propertie \'removeEntries\' not found!');
        }
    }

    /**
     * Função com o objetivo de retornar a base para a criação de um novo registro.
     *
     */
    createEntrie(): any {
        return new BR_SincronizedVisit__cModel(null);
    }

    /**
     * Função com o objetivo de retornar um registro preenchido com os dados passados por parametro.
     *
     */
    transformEntrie(data:any): BaseModel {
        return new BR_SincronizedVisit__cModel(data);
    }

    clearSoup() {
      return new Promise((resolve, reject) => {
        const self = this;
        if (window.hasOwnProperty('clearSoup')) {
            window['clearSoup'].call(null, self.storeConfig, BR_SincronizedVisit__cModel.soupName, (clearSoupSucess) => {
                console.log('clearSoupSucess', clearSoupSucess);
                BR_SincronizedVisit__cModel.setLastSyncDownId(self.appPreferences, '').then(() => {
                    resolve(clearSoupSucess);
                }).catch((err) => {
                    reject(err);
                });
            }, (err) => {
                reject(err);
            });
        } else {
            reject('propertie \'clearSoup\' not found!');
        }
      });
    }

    // Ghost
    cleanResyncGhosts(success: Function, error: Function){
        var self = this;
        BR_SincronizedVisit__cModel.getLastSyncDownId(self.appPreferences).then((response) => {
            let syncId: number = response;
            console.log('---| cleanResyncGhosts:syncId: ', syncId)
            if (window.hasOwnProperty('clearGhostRecords') && typeof(syncId) === "number") {
                console.log('---| cleanResyncGhosts:hasClearGhostRecords')
                window['clearGhostRecords'].call(null, self.storeConfig, syncId, success, error)
            }else{
                success()
            }
        })

    }

    getSyncStatus(success: Function, error: Function){
        var self = this;
        BR_SincronizedVisit__cModel.getLastSyncDownId(self.appPreferences).then((response) => {
            let syncId: number = response;
            console.log('---| getSyncStatus:syncId: ', syncId)
            if (window.hasOwnProperty('syncStatus') && typeof(syncId) === "number") {
                console.log('---| getSyncStatus:hasSyncStatus')
                window['syncStatus'].call(null, self.storeConfig, syncId, success, error)
            }else{
                success()
            }
        })

    }

}
