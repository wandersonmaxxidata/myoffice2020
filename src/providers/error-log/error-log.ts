import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BR_SincronizedVisit__cModel } from '../../app/model/BR_SincronizedVisit__cModel';
import { DataErrorModel } from './../../app/model/DataErrorModel';
import { DataModel } from './../../app/model/DataModel';
import { ShildDataModel } from './../../app/model/ShildDataModel';
import { ServiceProvider } from './../../providers/service-salesforce';
import { DataDaoProvider } from './../../providers/data-dao/data-dao';
import { DataErrorDaoProvider } from './../../providers/data-error-dao/data-error-dao';
import { LoadingManager } from '../../shared/Managers/LoadingManager';

@Injectable()
export class ErrorLogProvider {

  constructor(
    public serviceProvider: ServiceProvider,
    public storage: Storage,
    public dataDAO: DataDaoProvider,
    public dataErrorDAO: DataErrorDaoProvider
  ) {
  }



  public  getVisitNotInEvent() {

    return new Promise<any>((resolve) => {
      const smartSql = `
      SELECT
        {Event:Id},
        {Event:StartDateTime},
        {Event:EndDateTime},
        {Event:IsOutlook__c},
        {Event:Status__c},
        {Event:BR_Tipo_de_Visita__c},
        {Event:Location},
        {Event:ExternalID__c},
        {Event:IsAllDayEvent},
        {Event:Subject},
        {Event:CreatedDate}
      FROM
        {Event}
      WHERE NOT EXISTS(SELECT NULL FROM {Visita__c} WHERE {Visita__c:EventId__c}={Event:Id})
      AND {Event:Status__c} = "Executado" AND {Event:BR_Tipo_de_Visita__c} NOT NULL AND {Event:CreatedDate} > '2019-11-13T21:00:00-03:00' ORDER BY {Event:CreatedDate} DESC LIMIT 10 `;

      this.querySmartFromSoup(smartSql, 100).then((response) => {
        console.log(response.currentPageOrderedEntries)
        if (response.currentPageOrderedEntries.length > 0) {
          localStorage.setItem('lastSyncErros',String(response.currentPageOrderedEntries.length));
          LoadingManager.getInstance().show().then(() => {

            for (let i = 0; i < response.currentPageOrderedEntries.length; i++) {
              let item = response.currentPageOrderedEntries[i];
              this.creatErrorEventNotVisit(item, item[0], item[0], 'Visita__c').then(() => {
                if (i === (response.currentPageOrderedEntries.length - 1)) {
                  LoadingManager.getInstance().hide();
                  console.log('Finalizou criação de log linha')
                  resolve();
                }
              }, (error) => {
                console.log(error);
                resolve();
              });
            }

          });
        } else {
          resolve();
        }
      }, (error) => {
        console.log(error);
        resolve();
      });
    });
  }


  creatErrorEventNotVisit(currentData, idSoap, eventId, tableName) {
    return new Promise((resolve) => {
      console.log('EVENTO ATUAL=================')
      console.log(currentData)

      this.storage.get(tableName).then(shildsDatas => {
        console.log('VISTA SALVA ERRO=================')
        console.log(shildsDatas)
        console.log('ID COMPARA =================')
        console.log(idSoap)

        let shildDataError = shildsDatas.filter((data) => { return data[1].EventId__c == idSoap; });

        let dataError = JSON.stringify(currentData);
        let dataInsert = new DataModel(null, tableName, idSoap, eventId, dataError);
        console.log('SHILD ERRO=================')

        this.dataDAO.insert(dataInsert).then(data => {
          console.log('Inserido log data erro');
          if (shildDataError.length > 0) {
            let dataInsertShild = JSON.stringify(shildDataError[0][1]);
            let shildInsert = new ShildDataModel(null, 'Visita__c', shildDataError[0][1].Id, data.insertId, dataInsertShild);
            this.dataDAO.insertShildData(shildInsert).then(dataShild => {
              this.checkBRProductUsed(shildDataError[0][1].Id, data.insertId).then(() => {
                this.checkBRBRWeedHandlingUsed(shildDataError[0][1].Id, data.insertId).then(() => {
                  this.checkHibrids(shildDataError[0][1].Id, data.insertId).then(() => {

                    let errorInsert = new DataErrorModel(null, idSoap, 1);
                    this.dataErrorDAO.insert(errorInsert).then(data => {
                      console.log('Inserido log erros');
                      resolve();
                    }).catch((error) => {
                      console.log(error);
                      resolve();
                    });

                  });
                });
              });

            }).catch((error) => {
              console.log(error);
              resolve();
            });
          } else {
            resolve();
          }

        }).catch((error) => {
          console.log(error);
          resolve();
        });

      });
    });
  }


  private querySmartFromSoup(smartSql: string, pageSize: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.serviceProvider.getEventService().querySmartFromSoup(smartSql, pageSize, (response) => {
        resolve(response);
      }, (error) => {
        reject(error);
      });
    });
  }


  //CHAMANDO ADICIONAIS
  private checkBRBRWeedHandlingUsed(visitId, insertId) {
    return new Promise((resolve, reject) => {
      let idHibrid;
      this.storage.get('BR_WeedHandling__c').then(async shildsDatas => {
        console.log('BR_WeedHandling__c STORAGE')
        console.log(shildsDatas)
        let shildDataError = shildsDatas.filter((data) => { return data[1].BR_Visit__c == visitId; });
        console.log('BR_WeedHandling__c DA VISTA TODOS+======')
        console.log(shildDataError)
        if (shildDataError.length > 0) {
          for (var i = 0; i < shildDataError.length; i++) {
            idHibrid = shildDataError[i][1].Id;
            let dataInsertShild = JSON.stringify(shildDataError[i][1]);
            let shildInsert = new ShildDataModel(null, 'BR_WeedHandling__c', shildDataError[i][1].Id, insertId, dataInsertShild);

            await this.dataDAO.insertShildData(shildInsert).then(async data => {
              console.log('ID DO BR_WeedHandling__c ATUAL ==================================' + idHibrid)

              if ((shildDataError.length - 1) === i) {
                resolve();
              }

            }).catch((error) => {
              console.log(error);
              resolve();
            });
          }
        } else {
          resolve();
        }
      });
    });
  }


  private checkBRProductUsed(visitId, insertId) {
    return new Promise((resolve, reject) => {
      let idHibrid;
      this.storage.get('BR_ProductUsed__c').then(async shildsDatas => {
        console.log('BR_ProductUsed__c STORAGE')
        console.log(shildsDatas)
        let shildDataError = shildsDatas.filter((data) => { return data[1].BR_VisitUsedProduct__c == visitId; });
        console.log('BR_ProductUsed__c DA VISTA TODOS+======')
        console.log(shildDataError)
        if (shildDataError.length > 0) {
          for (var i = 0; i < shildDataError.length; i++) {
            idHibrid = shildDataError[i][1].Id;
            let dataInsertShild = JSON.stringify(shildDataError[i][1]);
            let shildInsert = new ShildDataModel(null, 'BR_ProductUsed__c', shildDataError[i][1].Id, insertId, dataInsertShild);

            await this.dataDAO.insertShildData(shildInsert).then(async data => {
              console.log('ID DO BR_ProductUsed__c ATUAL ==================================' + idHibrid)

              if ((shildDataError.length - 1) === i) {
                resolve();
              }

            }).catch((error) => {
              console.log(error);
              resolve();
            });
          }
        } else {
          resolve();
        }
      });
    });
  }


  private checkHibrids(visitId, insertId) {
    return new Promise((resolve) => {
      let idHibrid;
      this.storage.get('Hibrido_de_Visita__c').then(async shildsDatas => {
        console.log('Hibrido_de_Visita__c STORAGE')
        console.log(shildsDatas)
        let shildDataError = shildsDatas.filter((data) => { return data[1].BR_VisitMobileId__c == visitId; });
        console.log('HIBRIDOS DA VISTA TODOS+======')
        console.log(shildDataError)
        if (shildDataError.length > 0) {
          for (var i = 0; i < shildDataError.length; i++) {
            idHibrid = shildDataError[i][1].Id;
            let dataInsertShild = JSON.stringify(shildDataError[i][1]);
            let shildInsert = new ShildDataModel(null, 'Hibrido_de_Visita__c', shildDataError[i][1].Id, insertId, dataInsertShild);

            await this.dataDAO.insertShildData(shildInsert).then(async data => {
              console.log('ID DO HIBRIDO ATUAL ==================================' + idHibrid)

              await this.checkPraga(idHibrid, insertId).then(() => {
                if ((shildDataError.length - 1) === i) {
                  console.log('Recebeu')
                  resolve();
                }
              }).catch((error) => {
                console.log(error);
                resolve();
              });

            }).catch((error) => {
              console.log(error);
              resolve();
            });

          }

        } else {
          resolve();
        }

      });
    });
  }


  private async checkPraga(hibridId, insertId) {
    return new Promise((resolve, reject) => {
      this.storage.get('Praga__c').then(async shildsDatas => {
        console.log('Praga__c STORAGE')
        console.log(shildsDatas)
        let shildDataError = shildsDatas.filter((data) => { return data[1].Hibrido_de_Visita__c == hibridId; });

        if (shildDataError.length > 0) {
          for (var i = 0; i < shildDataError.length; i++) {

            let dataInsertShild = JSON.stringify(shildDataError[i][1]);
            let shildInsert = new ShildDataModel(null, 'Praga__c', shildDataError[i][1].Id, insertId, dataInsertShild);
            await this.dataDAO.insertShildData(shildInsert).then(data => {
              console.log('Inserido log shild Praga__c================================');
              console.log(dataInsertShild);
              console.log('HIBRIDO ID================================');
              console.log(hibridId);
              console.log(shildDataError.length - 1)
              console.log(i)

              if ((shildDataError.length - 1) === i) {
                console.log('voltou')
                resolve();
              } else {
                console.log(shildDataError.length - 1)
                console.log(i)
              }

            }).catch((error) => {
              console.log(error);
              reject(error);
            });

          }
        } else {
          resolve();
        }

      });
    });
  }
  //FIM CHACAMDO ADICIONAIS




}
