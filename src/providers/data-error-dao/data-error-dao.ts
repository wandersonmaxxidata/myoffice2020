import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from './../../providers/database/database';
import { DataErrorModel } from '../../app/model/DataErrorModel';
import { Injectable } from '@angular/core';

@Injectable()
export class DataErrorDaoProvider {

  constructor(
    public db: DatabaseProvider
    ) {
  }

  public insert(dataErrorModel: DataErrorModel){
    return new Promise(resolve => {

      this.db.getDb().then((db: SQLiteObject) =>{
            let sqlDataTrue = 'select id from data_error WHERE der_data_id = ? ';
            let valuesDataTrue = [dataErrorModel.getDataId()];
            db.executeSql(sqlDataTrue, valuesDataTrue).then((retrunCheck:any)=>{

              if(retrunCheck.rows.length == 0){
                  let sql = 'insert into data_error ( id,der_data_id, der_error_id) values (?,?,?)';
                  let values = [dataErrorModel.getId(), dataErrorModel.getDataId(),dataErrorModel.getErrorId()];
                  db.executeSql(sql,values).then(()=>{
                    resolve(1);
                  }).catch(e => {
                    console.log(e)
                    resolve(0);
                  });
              }else{
                resolve(0);
              }

          }).catch(e => {
            console.log(e)
            resolve(0);
          });
      }).catch(e => {
        console.log(e)
        resolve(0);
      });

     });
   }

   public delete(id: number){
    return new Promise(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'delete FROM data_error where id = ?';
        let values = [id];
        db.executeSql(sql,values).then(()=>{
          resolve(1);
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }

  public deleteShild(id: number){
    return new Promise(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'delete FROM shild_data where sda_id_data = ?';
        let values = [id];
        db.executeSql(sql,values).then(()=>{
          resolve(1);
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }


  public deleteData(id: number){
    return new Promise(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'delete FROM data where id = ?';
        let values = [id];
        db.executeSql(sql,values).then(()=>{
          resolve(1);
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }


  public getAll(){
    return new Promise<any>(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT *, data.id as idData, data_error.id as idDataError, shild_data.id as shildId FROM data_error INNER JOIN data on data.dat_sop_id = data_error.der_data_id INNER JOIN error on error.id = data_error.der_error_id INNER JOIN shild_data on shild_data.sda_id_data = idData';
        db.executeSql(sql,[]).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }

  public getAllVisits(){
    return new Promise<any>(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT *, data.id as idData, data_error.id as idDataError, shild_data.id as shildId FROM data_error INNER JOIN data on data.dat_sop_id = data_error.der_data_id INNER JOIN error on error.id = data_error.der_error_id INNER JOIN shild_data on shild_data.sda_id_data = idData and shild_data.sda_type = "Visita__c" ';
        db.executeSql(sql,[]).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }


  public getVisit(id){
    return new Promise<any>(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT *, data.id as idData, data_error.id as idDataError, shild_data.id as shildId FROM data_error INNER JOIN data on data.dat_sop_id = data_error.der_data_id INNER JOIN error on error.id = data_error.der_error_id INNER JOIN shild_data on shild_data.sda_id_data = idData and shild_data.sda_type = "Visita__c" AND data_error.der_data_id = ?';
        let values = [id];
        db.executeSql(sql,values).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }


  public checkShild(id,type){
    return new Promise<any>(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT sda_data, data.id as idData FROM data INNER JOIN shild_data on shild_data.sda_id_data = idData and shild_data.sda_type = ?  where idData = ? ';
        let values = [type,id];
        db.executeSql(sql,values).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }else{
            resolve(0);
          }
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }

  public getAllNotShild(){
    return new Promise<any>(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT *, data.id as idData, data_error.id as idDataError FROM data_error INNER JOIN data on data.dat_sop_id = data_error.der_data_id INNER JOIN error on error.id = data_error.der_error_id ';
        db.executeSql(sql,[]).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }

  public existsErrors(){
    return new Promise(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT id FROM data_error';
        db.executeSql(sql,[]).then((resultData)=>{
            resolve(resultData.rows.length);
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }

  public getError(id){
    return new Promise(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT id FROM data_error WHERE der_data_id = ?';
        let values = [id];
        db.executeSql(sql,values).then((resultData)=>{
            resolve(resultData.rows.length);
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }


  public getAllErrrors(){
    return new Promise(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT * from error';
        db.executeSql(sql,[]).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }


  public getEspErrrors(text){
    return new Promise<any>(resolve => {
      this.db.getDb().then((db: SQLiteObject) =>{
        let sql = 'SELECT * from error where err_type like "%?%"';
        let values = [text];
        db.executeSql(sql,values).then((resultData)=>{
          if(resultData.rows.length > 0){
            resolve(resultData.rows);
          }
        }).catch(e => {
          console.log(e)
          resolve(0);
        });
      }).catch(e => {
          console.log(e)
          resolve(0);
        });
    });
  }


  public returnEventId(itemId,type) {
    return new Promise<any>((resolve, reject) => {
//WHERE {Hibrido_de_Visita__c:ExternalID__c} = "${hibridId}
let smartSql:any;
switch(type) { 
  case "hib": {
    smartSql = `
    SELECT
      {Visita__c:EventId__c}
    FROM
      {Hibrido_de_Visita__c}
    INNER JOIN {Visita__c}
    ON {Hibrido_de_Visita__c:BR_VisitMobileId__c} = {Visita__c:Id}
    WHERE {Hibrido_de_Visita__c:Id} = "${itemId}"`;
     break; 
  } 
  case "praga": {
    smartSql = `
        SELECT
          {Visita__c:EventId__c}
        FROM
          {Praga__c}
        INNER JOIN {Hibrido_de_Visita__c}
        ON {Praga__c:Hibrido_de_Visita__c} = {Hibrido_de_Visita__c:Id}
        INNER JOIN {Visita__c}
        ON {Hibrido_de_Visita__c:BR_VisitMobileId__c} = {Visita__c:Id} 
        WHERE {Praga__c:Id} = "${itemId}"`;
     break; 
  }
  case "prod": {
    smartSql = `
        SELECT
          {Visita__c:EventId__c}
        FROM
          {BR_ProductUsed__c}
        INNER JOIN {Visita__c}
        ON {BR_ProductUsed__c:BR_VisitUsedProduct__c} = {Visita__c:Id}
        WHERE {BR_ProductUsed__c:Id} = "${itemId}"`;
     break; 
  }
  case "wee": {
    smartSql = `
        SELECT
          {Visita__c:EventId__c}
        FROM
          {BR_WeedHandling__c}
        INNER JOIN {Visita__c}
        ON {BR_WeedHandling__c:BR_Visit__c} = {Visita__c:Id}
        WHERE {BR_WeedHandling__c:Id} = "${itemId}"`;
     break; 
  }

  default: { 
    smartSql = `""`;
     break;              
  } 
}


        
        this.querySmartFromSoup(smartSql, 1, (response) => {

         if(response.currentPageOrderedEntries.length > 0){
            resolve(response.currentPageOrderedEntries[0][0]);
          }else{
            resolve();
          }
             
          }, (error) => {
             reject(error);
          });
    });
}


querySmartFromSoup(smartSql:string, pageSize:number, sucess: Function, error: Function) {
  let storeConfig = { storeName: "myoffice", isGlobalStore: true };
  if (window.hasOwnProperty('querySmartFromSoup')) {
      window['querySmartFromSoup'].call(null, storeConfig, smartSql, pageSize, sucess, error);
  } else {
      error('propertie \'querySmartFromSoup\' not found!');
  }
}



}