import { Injectable } from '@angular/core';

    @Injectable()
    export class AutenticateService {

      constructor() {
      }

      logoutSf() {
        if (window.hasOwnProperty('logout')) {
              window['logout'].call();
        }else{
            console.log("propertie 'logout' not found!");
        }
      }

      getCurrentUser():Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('getCurrentUser')) {
                window['getCurrentUser'].call(null, resolve, reject);
            }else{
                reject("propertie 'getCurrentUser' not found!");
            }
        });
      }

      authenticate(sucess: Function, error: Function):Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('authenticate')) {
                window['authenticate'].call(null, resolve, reject);
            }else{
                reject("propertie 'authenticate' not found!");
            }
        });
      }

      removeAllStores(sucess: Function, error: Function):Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('removeAllStores')) {
                window['removeAllStores'].call(null, resolve, reject);
            }else{
                reject("propertie 'removeAllStores' not found!");
            }
        });
      }

      removeAllGlobalStores(sucess: Function, error: Function):Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('removeAllGlobalStores')) {
                window['removeAllGlobalStores'].call(null, resolve, reject);
            }else{
                reject("propertie 'removeAllGlobalStores' not found!");
            }
        });
      }

    }
    