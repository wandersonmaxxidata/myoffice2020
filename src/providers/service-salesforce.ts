import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { ClientsService } from "./clients/clients-service-salesforce";
import { RecordTypeService } from "./recordtype/recordtype-service-salesforce";
import { MunicipioService } from "./municipio/municipio-service-salesforce";
import { BrandService } from "./brand/brand-service-salesforce";
import { Hibrido__cService } from "./hibrido__c/hibrido__c-service-salesforce";
import { ContactService } from "./contact/contact-service-salesforce";
import { UserService } from "./user/user-service-salesforce";
import { BR_ProfileMapping__cService } from "./br_profilemapping__c/br_profilemapping__c-service-salesforce";
import { EventService } from "./event/event-service-salesforce";
import { Visita__cService } from "./visita__c/visita__c-service-salesforce";
import { Hibrido_de_Visita__cService } from "./hibrido_de_visita__c/hibrido_de_visita__c-service-salesforce";
import { Praga__cService } from "./praga__c/praga__c-service-salesforce";
import { Doenca__cService } from "./doenca__c/doenca__c-service-salesforce";
import { BR_CropProducts__cService } from "./br_cropproducts__c/br_cropproducts__c-service-salesforce";
import { HybridRelationshipService } from "./hybridrelationship/hybridrelationship-service-salesforce";
import { BR_WeedHandling__cService } from "./br_weedhandling__c/br_weedhandling__c-service-salesforce";
import { BR_ProductUsed__cService } from "./br_productused__c/br_productused__c-service-salesforce";
import { SafraService } from "./safra/safra-service-salesforce";
import { BR_SincronizedVisit__cService } from "./br_sincronizedvisit__c/br_sincronizedvisit__c-service-salesforce";
import { MobileTextsService } from "./mobiletexts/mobiletexts-service-salesforce";
import { AttachmentService } from "./attachment/attachment-service-salesforce";
import { BR_Sales_Coaching__cService } from "./br_sales_coaching__c/br_sales_coaching__c-service-salesforce";
import { AutenticateService } from "./autenticate/autenticate-service-salesforce";


/**
 * Serviço com os recursos de armazenamento local e sync do salesforce.
 */ 
@Injectable()
export class ServiceProvider  {
    
    constructor(
        platform: Platform, 
        public clientsService: ClientsService, 
        public recordtypeService: RecordTypeService, 
        public municipioService: MunicipioService, 
        public brandService: BrandService, 
        public hibrido__cService: Hibrido__cService, 
        public contactService: ContactService, 
        public userService: UserService, 
        public br_profilemapping__cService: BR_ProfileMapping__cService, 
        public eventService: EventService, 
        public visita__cService: Visita__cService, 
        public hibrido_de_visita__cService: Hibrido_de_Visita__cService, 
        public praga__cService: Praga__cService, 
        public doenca__cService: Doenca__cService, 
        public br_cropproducts__cService: BR_CropProducts__cService, 
        public hybridrelationshipService: HybridRelationshipService, 
        public br_weedhandling__cService: BR_WeedHandling__cService, 
        public br_productused__cService: BR_ProductUsed__cService, 
        public safraService: SafraService, 
        public br_sincronizedvisit__cService: BR_SincronizedVisit__cService, 
        public mobiletextsService: MobileTextsService, 
        public attachmentService: AttachmentService, 
        public br_sales_coaching__cService: BR_Sales_Coaching__cService, 
        public autenticateService: AutenticateService) {

    }

    /**
     * provider dos serviços da entidade Clients
     */
    getClientsService(){
        return this.clientsService;
    }

    /**
     * provider dos serviços da entidade RecordType
     */
    getRecordTypeService(){
        return this.recordtypeService;
    }

    /**
     * provider dos serviços da entidade Municipio
     */
    getMunicipioService(){
        return this.municipioService;
    }

    /**
     * provider dos serviços da entidade Brand
     */
    getBrandService(){
        return this.brandService;
    }

    /**
     * provider dos serviços da entidade Hibrido__c
     */
    getHibrido__cService(){
        return this.hibrido__cService;
    }

    /**
     * provider dos serviços da entidade Contact
     */
    getContactService(){
        return this.contactService;
    }

    /**
     * provider dos serviços da entidade User
     */
    getUserService(){
        return this.userService;
    }

    /**
     * provider dos serviços da entidade BR_ProfileMapping__c
     */
    getBR_ProfileMapping__cService(){
        return this.br_profilemapping__cService;
    }

    /**
     * provider dos serviços da entidade Event
     */
    getEventService(){
        return this.eventService;
    }

    /**
     * provider dos serviços da entidade Visita__c
     */
    getVisita__cService(){
        return this.visita__cService;
    }

    /**
     * provider dos serviços da entidade Hibrido_de_Visita__c
     */
    getHibrido_de_Visita__cService(){
        return this.hibrido_de_visita__cService;
    }

    /**
     * provider dos serviços da entidade Praga__c
     */
    getPraga__cService(){
        return this.praga__cService;
    }

    /**
     * provider dos serviços da entidade Doenca__c
     */
    getDoenca__cService(){
        return this.doenca__cService;
    }

    /**
     * provider dos serviços da entidade BR_CropProducts__c
     */
    getBR_CropProducts__cService(){
        return this.br_cropproducts__cService;
    }

    /**
     * provider dos serviços da entidade HybridRelationship
     */
    getHybridRelationshipService(){
        return this.hybridrelationshipService;
    }

    /**
     * provider dos serviços da entidade BR_WeedHandling__c
     */
    getBR_WeedHandling__cService(){
        return this.br_weedhandling__cService;
    }

    /**
     * provider dos serviços da entidade BR_ProductUsed__c
     */
    getBR_ProductUsed__cService(){
        return this.br_productused__cService;
    }

    /**
     * provider dos serviços da entidade Safra
     */
    getSafraService(){
        return this.safraService;
    }

    /**
     * provider dos serviços da entidade BR_SincronizedVisit__c
     */
    getBR_SincronizedVisit__cService(){
        return this.br_sincronizedvisit__cService;
    }

    /**
     * provider dos serviços da entidade MobileTexts
     */
    getMobileTextsService(){
        return this.mobiletextsService;
    }

    /**
     * provider dos serviços da entidade Attachment
     */
    getAttachmentService(){
        return this.attachmentService;
    }

    /**
     * provider dos serviços da entidade BR_Sales_Coaching__c
     */
    getBR_Sales_Coaching__cService(){
        return this.br_sales_coaching__cService;
    }

    /**
     * provider dos serviços da entidade Autenticate
     */
    getAutenticateService(){
        return this.autenticateService;
    }
    
}