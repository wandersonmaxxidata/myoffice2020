import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  constructor(private sqlite: SQLite ) {
    
  }

  public getDb(){
    return this.sqlite.create({
     name: 'erroslogdb.db',
     location: 'default'
   });
  }

  public createBase(){
    return this.getDb().then(db=>{
      this.createTables(db);
    }).catch(e => console.log(e));
  }

  private createTables(db:SQLiteObject ){
     db.sqlBatch([
       ['CREATE TABLE IF NOT EXISTS data (id integer primary key AUTOINCREMENT NOT NULL, dat_data_type VARCHAR(64), dat_sop_id VARCHAR(128) NULL, dat_event_sop_id VARCHAR(128) NULL, dat_data TEXT)'],
       ['CREATE TABLE IF NOT EXISTS error (id integer primary key AUTOINCREMENT NOT NULL, err_type TEXT)'],
       ['CREATE TABLE IF NOT EXISTS shild_data (id integer primary key AUTOINCREMENT NOT NULL, sda_type TEXT,sda_data TEXT, sda_sop_id VARCHAR(128) NULL, sda_id_data VARCHAR(128) NULL)'],
       ['CREATE TABLE IF NOT EXISTS data_error (id integer primary key AUTOINCREMENT NOT NULL, der_data_id VARCHAR(128) NULL, der_error_id VARCHAR(128) NULL, der_date datetime default current_timestamp)'],
     ]).then(() =>{ this.setError(db); } ).catch(e =>{ console.log(e); });
  }

  public setError(db:SQLiteObject ){
    let sql = 'select * from error';
    db.executeSql(sql, []).then((errorsAll:any)=>{
      if(errorsAll.rows.length == 0){
        console.log(errorsAll)
        db.sqlBatch([
          ['INSERT INTO error (err_type) VALUES ("Evento criado sem visita")'],
          ['INSERT INTO error (err_type) VALUES ("Sincronismo não finalizado")'],
          ['INSERT INTO error (err_type) VALUES ("Sincronismo finalizado pelo usuário")'],
        ]).then(() => console.log('Executed SQL')).catch(e => {console.log(e); });

      }else{
        // db.sqlBatch([
        //   ['DELETE FROM data'],
        //   ['DELETE FROM data_error'],
        //   ['DELETE FROM shild_data'],
        // ]).then(() => console.log('Executed SQL')).catch(e => {console.log(e); });

        // db.sqlBatch([
        //   ['DROP TABLE data'],
        // ]).then(() => console.log('Executed SQL')).catch(e => {console.log(e); });

        // db.sqlBatch([
        //   ['DROP TABLE data'],
        // ]).then(() => console.log('Executed SQL')).catch(e => {console.log(e); });

        // console.log('ZERANDO ERROS+++++++++++++++++++++++++++++++++++++++=')
        // console.log(errorsAll)
      }
      

      }).catch(e => {
        console.log(e);
      });

      let sqlDataErrors = 'select * from data';
      let sqlErrors = 'select * from data_error';
      let sqlDataShild = 'select * from shild_data';
      db.executeSql(sqlErrors, []).then((errorsAll:any)=>{
        console.log("ERROS INSERIDOS ===============================================")
        console.log(errorsAll)
      }).catch(e => {
        console.log(e);
      });
      db.executeSql(sqlDataErrors, []).then((errorsAll:any)=>{
        console.log("DATA ERROS INSERIDOS ===============================================")
        console.log(errorsAll)
      }).catch(e => {
        console.log(e);
      });

      db.executeSql(sqlDataShild, []).then((errorsAll:any)=>{
        console.log("DATA SHILD INSERIDOS ===============================================")
        console.log(errorsAll)
      }).catch(e => {
        console.log(e);
      });
}

}
