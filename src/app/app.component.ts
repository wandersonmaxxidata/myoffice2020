import { AppPreferences } from '@ionic-native/app-preferences';
import { Component, ViewChild } from '@angular/core';
import { DataService, OAuth } from 'forcejs';
import { ErrorPage } from '../pages/error/error';
import { Events, Nav, Platform } from 'ionic-angular';
import { LocalStorageKeys } from '../shared/Constants/Keys';
import { ServiceProvider } from './../providers/service-salesforce';
import { DatabaseProvider } from './../providers/database/database';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';
import { TabsPage } from './../pages/tabs/tabs';

/**
 * Componente gerado pelo ionic, é o primeiro componente a ser inicializado,
 * está sendo utilizado para abrir a página de autenticação do salesforce (login)
 * quando o mesmo for inicializado, tal ação está sendo feita pelo plugin forceJs.
 *
 * Para acessar os recursos de auth do salesforce, foi necessário utilizar um proxy,
 * o mesmo está sendo utilizado por causa do erro do CORS, para sabe o que é:
 * {@link http://blog.ionic.io/handling-cors-issues-in-ionic}
 */
@Component({
  providers: [ServiceProvider],
  templateUrl: 'app.html',
})
export class MyApp {
  @ViewChild(Nav) public nav: Nav;
  /**
   * Primeira página da aplicação.
   */
  public rootPage: any;
  public success: any;
  public error: any;
  public soupsToValidate: any[] = [];
  private soupsToCreate = [];

  constructor(
    public databaseProvider:DatabaseProvider,
    public appPreferences: AppPreferences,
    public service: ServiceProvider,
    public events: Events,
    public platform: Platform,
    public statusBar: StatusBar,
    public storage: Storage,
  ) {

    this.initializeApp();
  }

  public initializeApp() {
    
    this.platform.ready().then(() => {
      this.databaseProvider.createBase();
      const oauth = OAuth.createInstance('3MVG982oBBDdwyHhLS3GIFJKIuByT17rdeD6xU2d.CQKSfjM1JiM.q5qSmjHPY2HrT7YZ6K0yUTVG.c6gPBVG',
        'https://monsanto-myoffice--test.cs77.my.salesforce.com', 'http://localhost:8100/oauthCallback.html');

      oauth.login().then((oauthData) => {
        console.log('-----| sucesso login', oauthData);
        this.storage.remove(LocalStorageKeys.milliNextSyncKey);
        this.appPreferences.store('user_id', oauthData.userId ? oauthData.userId : '');
        this.processStructure();
        DataService.createInstance(oauthData, {
          proxyURL: 'https://dev-cors-proxy.herokuapp.com/',
        });
      }).catch((err) => {
        console.log('-----| erro login: ', err);
        this.rootPage = ErrorPage;
      });
    });
  }

  public createSoup(soupService, reTry): Promise<any> {
    return new Promise((resolve, reject) => {
      soupService.registerSoup(() => {
        resolve(soupService);
      }, (err) => {
        if (reTry) {
          setTimeout(() => {
            this.createSoup(soupService, false).then(() => {
              resolve(soupService);
            }).catch((err2) => {
              reject(err2);
            });
          }, 100);
        } else {
          reject(err);
        }
      });
    });
  }

  public processStructure() {
    let properties = Object.getOwnPropertyNames(this.service);

    properties = properties.filter((propertie) => {
      const currentService = Reflect.get(this.service, propertie);
      return Reflect.has(currentService, 'registerSoup');
    });

    for (const entityService of properties) {
      const currentServiceTemp = Reflect.get(this.service, entityService);
      this.soupsToCreate.push(new Promise((resolve, reject) => {
        this.createSoup(currentServiceTemp, true).then((service) => {
          console.log('createSoup SUSS', service);
          resolve();
        }).catch((err2) => {
          console.log('createSoup erro', err2);
          reject();
        });
      }));
    }

    Promise.all(this.soupsToCreate).then(() => {
      this.rootPage = TabsPage;
    }).catch((err) => {
      console.log(err);
    });
  }
}
