import { AgendaPage } from '../pages/agenda/agenda';
import { AgendaComponent } from '../components/agenda/agenda';
import { AgendaScheduleComponent } from '../components/agenda-schedule/agenda-schedule';
import { AppPreferences } from '@ionic-native/app-preferences';
import { AppVersion } from '@ionic-native/app-version';
import { SQLite} from '@ionic-native/sqlite';
import { AttachmentComponent } from '../components/attachment/home-attachment';
import { AttachmentOptions } from '../components/attachment/attachment-options/attachment-options';
import { AttachmentService } from '../providers/attachment/attachment-service-salesforce';
import { AutenticateService } from '../providers/autenticate/autenticate-service-salesforce';
import { AutoResizeTextareaDirective } from '../directives/auto-resize-textarea/auto-resize-textarea';
import { BR_CropProducts__cService } from '../providers/br_cropproducts__c/br_cropproducts__c-service-salesforce';
import { BR_ProductUsed__cService } from '../providers/br_productused__c/br_productused__c-service-salesforce';
import { BR_ProfileMapping__cService } from './../providers/br_profilemapping__c/br_profilemapping__c-service-salesforce';
import { BR_Sales_Coaching__cService } from '../providers/br_sales_coaching__c/br_sales_coaching__c-service-salesforce';
import { BR_SincronizedVisit__cService } from '../providers/br_sincronizedvisit__c/br_sincronizedvisit__c-service-salesforce';
import { BR_WeedHandling__cService } from '../providers/br_weedhandling__c/br_weedhandling__c-service-salesforce';
import { BrandService } from './../providers/brand/brand-service-salesforce';
import { BrowserModule } from '@angular/platform-browser';
import { CalendarPage } from '../pages/calendar/calendar';
import { CallNumber } from '@ionic-native/call-number';
import { Camera } from '@ionic-native/camera';
import { ClientContactsComponent } from '../components/client-contacts/client-contacts';
import { ClientInformationComponent } from '../components/client-information/client-information';
import { ClientItemsComponent } from '../components/client-items/client-items';
import { ClientsDetailPage } from './../pages/clients-detail/clients-detail';
import { ClientsListPage } from './../pages/clients-list/clients-list';
import { ClientsService } from '../providers/clients/clients-service-salesforce';
import { ClientVisitsComponent } from '../components/client-visits/client-visits';
import { ContactsDetailPage } from './../pages/contacts-detail/contacts-detail';
import { ContactService } from '../providers/contact/contact-service-salesforce';
import { CornDayDPage } from '../pages/visits/corn/corn-day-d/corn-day-d';
import { CornGerminationEmergencyPage } from '../pages/visits/corn/corn-germination-emergency/corn-germination-emergency';
import { CornHarvestingModal } from '../pages/visits/modals/corn-harvesting-modal/corn-harvesting-modal';
import { CornHarvestPage } from '../pages/visits/corn/corn-harvest/corn-harvest';
import { CornPlantingCreatePage } from '../pages/visits/corn/corn-planting-create/corn-planting-create';
import { CornPrePlantingCreatePage } from './../pages/visits/corn/corn-pre-planting-create/corn-pre-planting-create';
import { CornReproductiveDevelopmentPage } from '../pages/visits/corn/corn-reproductive-development/corn-reproductive-development';
import { CornVegetativeDevelopmentPage } from './../pages/visits/corn/corn-vegetative-development/corn-vegetative-development';
import { CornVisitEventPage } from '../pages/visits/corn/corn-visit-event/corn-visit-event';
import { CropClientComponent } from './../components/visits/crop-client/crop-client';
import { CropClimatePage } from './../pages/visits/crop/crop-climate/crop-climate';
import { CropComercialPage } from '../pages/visits/crop/crop-comercial/crop-comercial';
import { CropEventPage } from './../pages/visits/crop/crop-event/crop-event';
import { CropProductModal } from '../pages/visits/modals/add-crop-product/add-crop-product';
import { DatePickerModule } from 'ionic3-datepicker';
import { DiseaseModal } from './../pages/visits/modals/add-disease/add-disease';
import { Doenca__cService } from '../providers/doenca__c/doenca__c-service-salesforce';
import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { ErrorPage } from '../pages/error/error';
import { EventModalPage } from './../pages/event-modal/event-modal';
import { EventsComponent } from './../components/events/events';
import { EventService } from '../providers/event/event-service-salesforce';
import { EventsPage } from '../pages/events/events';
import { GeolocationComponent } from '../components/geolocation/geolocation';
import { HarvestInfoHybridModal } from '../pages/visits/modals/harvest-info-hybrid-modal/harvest-info-hybrid-modal';
import { HeaderComponent } from '../components/header/header';
import { Hibrido__cService } from '../providers/hibrido__c/hibrido__c-service-salesforce';
import { Hibrido_de_Visita__cService } from './../providers/hibrido_de_visita__c/hibrido_de_visita__c-service-salesforce';
import { HomePage } from './../pages/home/home';
import { HybridRelationshipService } from '../providers/hybridrelationship/hybridrelationship-service-salesforce';
import { Insomnia } from '@ionic-native/insomnia';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard';
import { LoockupComponent } from '../components/loockup/loockup';
import { ManagerEmailsModal } from '../components/modals/modal-manager-emails/modal-manager-emails';
import { MaskDirective } from '../directives/mask/mask';
import { MobileTextsService } from '../providers/mobiletexts/mobiletexts-service-salesforce';
import { ModalAddGeolocationPage } from '../components/geolocation/modal-add-geolocation/modal-add-geolocation';
import { MultipicklistModal } from '../components/modals/modal-multipicklist/modal-multipicklist';
import { MunicipioService } from './../providers/municipio/municipio-service-salesforce';
import { MyApp } from './app.component';
import { Network } from '@ionic-native/network';
import { NgCalendarModule } from 'ionic2-calendar';
import { NoSpecialCharacterDirective } from '../directives/no-special-character/no-special-character';
import { NotificationPage } from './../pages/notification/notification';
import { OnlyLettersDirective } from '../directives/only-letters/only-letters';
import { OnlyNumbersDirective } from '../directives/only-numbers/only-numbers';
import { OrderModule } from 'ngx-order-pipe';
import { OutlookPage } from '../pages/outlook/outlook';
import { PlagueModal } from '../pages/visits/modals/add-plague/add-plague';
import { PlantingAddHybridPage } from './../pages/visits/modals/planting-add-hybrid-modal/planting-add-hybrid-modal';
import { Praga__cService } from '../providers/praga__c/praga__c-service-salesforce';
import { PrePlantingAddHybridPage } from './../pages/visits/modals/pre-planting-add-hybrid-modal/pre-planting-add-hybrid-modal';
import { ProductsUsedModal } from './../pages/visits/modals/add-products-used/add-products-used';
import { ProfileMappingComponent } from '../components/profile-mapping/profile-mapping';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { RecordTypeService } from '../providers/recordtype/recordtype-service-salesforce';
import { ReportHeaderComponent } from './../components/report/report-header/report-header';
import { ReportImageComponent } from '../components/report/report-image/report-image';
import { ReportInformationComponent } from '../components/report/report-information/report-information';
import { ReportModal } from '../components/report/report-modal/report-modal';
import { ReportPage } from '../pages/visits/report/report';
import { ReportPlagueComponent } from '../components/report/report-plague/report-plague';
import { ReportTextComponent } from '../components/report/report-text/report-text';
import { ReportWeedComponent } from '../components/report/report-weed/report-weed';
import { SafraService } from '../providers/safra/safra-service-salesforce';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SearchChemicalPipe } from './../pipes/search/search.chemical';
import { SearchClientModal } from '../components/modals/modal-search-client/modal-search-client';
import { SearchEventModal } from '../components/modals/modal-search-event/modal-search-event';
import { SearchMarketingPipe } from './../pipes/search/search.marketing';
import { SearchPlaguePipe } from '../pipes/search/search.plague';
import { SearchProductUsedPipe } from '../pipes/search/search.product.used';
import { SearchSalesSplitPipe } from './../pipes/search/search.sales-split';
import { SearchWeedPipe } from './../pipes/search/search.weed';
import { ServiceProvider } from '../providers/service-salesforce';
import { SettingsPage } from './../pages/settings/settings';
import { SoyGreenVisitPage } from './../pages/visits/soy/soy-green-visit/soy-green-visit';
import { SoyHarvestInfoHybridModal } from '../pages/visits/modals/soy-harvest-info-hybrid-modal/soy-harvest-info-hybrid-modal';
import { SoyHarvestPage } from '../pages/visits/soy/soy-harvest/soy-harvest';
import { SoyPlantingVisitPage } from '../pages/visits/soy/soy-planting-visit/soy-planting-visit';
import { SoyVisitPlantingModal } from '../pages/visits/modals/soy-visit-planting-modal/soy-visit-planting-modal';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SynchronismComponent } from '../components/synchronism/synchronism';
import { SyncPage } from '../pages/sync/sync';
import { TabsPage } from './../pages/tabs/tabs';
import { TextMaskModule } from 'angular2-text-mask';
import { UserService } from '../providers/user/user-service-salesforce';
import { Visita__cService } from '../providers/visita__c/visita__c-service-salesforce';
import { VisitSalesCoachingPage } from './../pages/visits-sales-coaching/visits-sales-coaching';
import { WeedHandlingModal } from './../pages/visits/modals/add-weed-handling/add-weed-handling';
import { WelcomeHomeComponent } from '../components/welcome-home/welcome-home';
import { SyncErrorComponent } from '../components/sync-error/sync-error';
import { DatabaseProvider } from '../providers/database/database';
import { DataDaoProvider } from '../providers/data-dao/data-dao';
import { DataErrorDaoProvider } from '../providers/data-error-dao/data-error-dao';
import { ErrorLogProvider } from '../providers/error-log/error-log';


/**
 * Módulo principal da aplicação, no mesmo está sendo inicializado e declarado todos os recursos que
 * estão sendo utilizado pela a mesma.
 */
@NgModule({
  declarations: [
    AgendaPage,
    AgendaComponent,
    AgendaScheduleComponent,
    AttachmentComponent,
    AttachmentOptions,
    AutoResizeTextareaDirective,
    CalendarPage,
    ClientContactsComponent,
    ClientInformationComponent,
    ClientItemsComponent,
    ClientsDetailPage,
    ClientsListPage,
    ClientVisitsComponent,
    ContactsDetailPage,
    CornDayDPage,
    CornGerminationEmergencyPage,
    CornHarvestingModal,
    CornHarvestPage,
    CornPlantingCreatePage,
    CornPrePlantingCreatePage,
    CornReproductiveDevelopmentPage,
    CornVegetativeDevelopmentPage,
    CornVisitEventPage,
    CropClientComponent,
    CropClimatePage,
    CropComercialPage,
    CropEventPage,
    CropProductModal,
    DiseaseModal,
    ErrorPage,
    EventModalPage,
    EventsComponent,
    EventsPage,
    GeolocationComponent,
    HarvestInfoHybridModal,
    HeaderComponent,
    HomePage,
    LoockupComponent,
    ManagerEmailsModal,
    MaskDirective,
    ModalAddGeolocationPage,
    MultipicklistModal,
    MyApp,
    NoSpecialCharacterDirective,
    NotificationPage,
    OnlyLettersDirective,
    OnlyNumbersDirective,
    OutlookPage,
    PlagueModal,
    PlantingAddHybridPage,
    PrePlantingAddHybridPage,
    ProductsUsedModal,
    ProfileMappingComponent,
    ProgressBarComponent,
    ReportHeaderComponent,
    ReportImageComponent,
    ReportInformationComponent,
    ReportModal,
    ReportPage,
    ReportPlagueComponent,
    ReportTextComponent,
    ReportWeedComponent,
    SearchChemicalPipe,
    SearchClientModal,
    SearchEventModal,
    SearchMarketingPipe,
    SearchPlaguePipe,
    SearchProductUsedPipe,
    SearchSalesSplitPipe,
    SearchWeedPipe,
    SettingsPage,
    SoyGreenVisitPage,
    SoyHarvestInfoHybridModal,
    SoyHarvestPage,
    SoyPlantingVisitPage,
    SoyVisitPlantingModal,
    SynchronismComponent,
    SyncPage,
    TabsPage,
    VisitSalesCoachingPage,
    WeedHandlingModal,
    WelcomeHomeComponent,
    SyncErrorComponent,
  ],
  imports: [
    BrowserModule,
    DatePickerModule,
    TextMaskModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'ios',
      pageTransition: 'ios',
      tabsPlacement: 'bottom',
      backButtonText: '',
      swipeBackEnabled: false,
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
    }),
    IonicStorageModule.forRoot(),
    NgCalendarModule,
    OrderModule,
    TextMaskModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AgendaPage,
    AttachmentComponent,
    AttachmentOptions,
    CalendarPage,
    ClientContactsComponent,
    ClientsDetailPage,
    ClientsListPage,
    ContactsDetailPage,
    CornDayDPage,
    CornGerminationEmergencyPage,
    CornHarvestingModal,
    CornHarvestPage,
    CornPlantingCreatePage,
    CornPrePlantingCreatePage,
    CornReproductiveDevelopmentPage,
    CornVegetativeDevelopmentPage,
    CornVisitEventPage,
    CropClimatePage,
    CropComercialPage,
    CropEventPage,
    CropProductModal,
    DiseaseModal,
    ErrorPage,
    EventModalPage,
    EventsComponent,
    EventsPage,
    HarvestInfoHybridModal,
    HomePage,
    ManagerEmailsModal,
    ModalAddGeolocationPage,
    MultipicklistModal,
    MyApp,
    NotificationPage,
    OutlookPage,
    PlagueModal,
    PlantingAddHybridPage,
    PrePlantingAddHybridPage,
    ProductsUsedModal,
    ProgressBarComponent,
    ReportModal,
    ReportPage,
    SearchClientModal,
    SearchEventModal,
    SettingsPage,
    SoyGreenVisitPage,
    SoyHarvestInfoHybridModal,
    SoyHarvestPage,
    SoyPlantingVisitPage,
    SoyVisitPlantingModal,
    SyncPage,
    TabsPage,
    VisitSalesCoachingPage,
    WeedHandlingModal,
  ],
  providers: [
    AppPreferences,
    AppVersion,
    SQLite,
    AttachmentOptions,
    AttachmentService,
    AutenticateService,
    BR_CropProducts__cService,
    BR_ProductUsed__cService,
    BR_ProfileMapping__cService,
    BR_Sales_Coaching__cService,
    BR_SincronizedVisit__cService,
    BR_WeedHandling__cService,
    BrandService,
    CallNumber,
    Camera,
    ClientsService,
    ContactService,
    Doenca__cService,
    EventService,
    Hibrido__cService,
    Hibrido_de_Visita__cService,
    AttachmentComponent,
    HybridRelationshipService,
    Insomnia,
    Keyboard,
    MobileTextsService,
    MunicipioService,
    Network,
    Praga__cService,
    RecordTypeService,
    SafraService,
    ScreenOrientation,
    ServiceProvider,
    SplashScreen,
    StatusBar,
    UserService,
    Visita__cService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    DatabaseProvider,
    DataDaoProvider,
    DataErrorDaoProvider,
    ErrorLogProvider,
  ],
  exports: [
    CropClientComponent,
    HeaderComponent,
    ReportHeaderComponent,
    ReportImageComponent,
    ReportInformationComponent,
    ReportPlagueComponent,
    ReportTextComponent,
    ReportWeedComponent,
  ],
})
export class AppModule { }
