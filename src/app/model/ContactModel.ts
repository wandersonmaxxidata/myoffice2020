import { ClientsModel } from "./ClientsModel";
import { BrandModel } from "./BrandModel";
import { RecordTypeModel } from "./RecordTypeModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { RecordTypeService } from "../../providers/recordtype/recordtype-service-salesforce";
import { BrandService } from "../../providers/brand/brand-service-salesforce";
import { ClientsService } from "../../providers/clients/clients-service-salesforce";

/**
 * Classe model da entidade Contact
 */

export class ContactModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'Contact';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "Name", type: "string" },{ path: "FirstName", type: "string" },{ path: "LastName", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "AccountId", type: "string" },{ path: "RecordTypeId", type: "string" },{ path: "Profissao__c", type: "string" },{ path: "Phone", type: "string" },{ path: "MobilePhone", type: "string" },{ path: "Email", type: "string" },{ path: "BR_Brand__c", type: "string" },{ path: "BR_Marca__c", type: "string" },{ path: "CreatedDate", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'Contact' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Name:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    FirstName:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    LastName:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    AccountId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RecordTypeId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Profissao__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Phone:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    MobilePhone:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Email:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Brand__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Marca__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    CreatedDate:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    RecordType:RecordTypeModel = new RecordTypeModel(null);

    setRecordType(provider: RecordTypeService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.RecordTypeId, 1, 'ascending', null,
                (response) => {
                    this.RecordType = new RecordTypeModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    Marca:BrandModel = new BrandModel(null);

    setMarca(provider: BrandService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.BR_Brand__c, 1, 'ascending', null,
                (response) => {
                    this.Marca = new BrandModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    Account:ClientsModel = new ClientsModel(null);

    setAccount(provider: ClientsService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.AccountId, 1, 'ascending', null,
                (response) => {
                    this.Account = new ClientsModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdContact').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdContact', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdContact', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdContact', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
