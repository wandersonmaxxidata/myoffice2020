
export class DataModel {

    private id: number;
    private dat_data_type: string;
    private dat_sop_id: string;
    private dat_event_sop_id: string;
    private dat_data: string;

    constructor(
        id?: number,
        dat_data_type?: string,
        dat_sop_id?: string,
        dat_event_sop_id?: string,
        dat_data?: string
    ){
        this.setId(id);
        this.setDataType(dat_data_type);
        this.setSopId(dat_sop_id);
        this.setEventId(dat_event_sop_id);
        this.setData(dat_data);
    }

    public setId(id: number): void{
        this.id = id;
    }
    public setDataType(dat_data_type: string): void{
        this.dat_data_type = dat_data_type;
    }
    public setSopId(dat_sop_id: string): void{
        this.dat_sop_id = dat_sop_id;
    }
    public setEventId(dat_event_sop_id: string): void{
        this.dat_event_sop_id = dat_event_sop_id;
    }
    public setData(dat_data: string): void{
        this.dat_data = dat_data;
    }


    public getId(){
        return this.id;
    }
    public getDataType(){
        return this.dat_data_type;
    }
    public getSopId(){
        return this.dat_sop_id;
    }
    public getEventId(){
        return this.dat_event_sop_id;
    }
    public getData(){
        return this.dat_data;
    }




}