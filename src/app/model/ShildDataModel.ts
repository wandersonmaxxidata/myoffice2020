
export class ShildDataModel {

    private id: number;
    private sda_type: string;
    private sda_sop_id: number;
    private sda_id_data: number;
    private sda_data:any;

    constructor(
        id?: number,
        sda_type?: string,
        sda_sop_id?: number,
        sda_id_data?: number,
        sda_data?: any,
    ){
        this.setId(id);
        this.setType(sda_type);
        this.setSopId(sda_sop_id);
        this.setDataId(sda_id_data);
        this.setData(sda_data);
    }

    public setId(id: number): void{
        this.id = id;
    }
    public setType(sda_type: string): void{
        this.sda_type = sda_type;
    }
    public setSopId(sda_sop_id: number): void{
        this.sda_sop_id = sda_sop_id;
    }
    public setDataId(sda_id_data: number): void{
        this.sda_id_data = sda_id_data;
    }
    public setData(sda_data: any): void{
        this.sda_data = sda_data;
    }

    public getId(){
        return this.id;
    }
    public getType(){
        return this.sda_type;
    }
    public getSopId(){
        return this.sda_sop_id;
    }
    public getData(){
        return this.sda_data;
    }
    
    public getDataId(){
        return this.sda_id_data;
    }




}