
export class DataErrorModel {

    private id: number;
    private dat_data_type: string;
    private dat_sop_id: number;

    constructor(
        id?: number,
        dat_data_type?: string,
        dat_sop_id?: number
    ){
        this.setId(id);
        this.setDataType(dat_data_type);
        this.setSopId(dat_sop_id);
    }

    public setId(id: number): void{
        this.id = id;
    }
    public setDataType(dat_data_type: string): void{
        this.dat_data_type = dat_data_type;
    }
    public setSopId(dat_sop_id: number): void{
        this.dat_sop_id = dat_sop_id;
    }


    public getId(){
        return this.id;
    }
    public getDataType(){
        return this.dat_data_type;
    }
    public getSopId(){
        return this.dat_sop_id;
    }




}