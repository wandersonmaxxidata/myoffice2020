import { ClientsModel } from "./ClientsModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { ClientsService } from "../../providers/clients/clients-service-salesforce";

/**
 * Classe model da entidade Event
 */

export class EventModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'Event';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "OwnerId", type: "string" },{ path: "BR_MobileAppFlag__c", type: "string" },{ path: "BR_SalesCoachingEvent__c", type: "string" },{ path: "ClienteId__c", type: "string" },{ path: "RecordTypeId", type: "string" },{ path: "Observacoes__c", type: "string" },{ path: "Subject", type: "string" },{ path: "Tipo_Compromisso__c", type: "string" },{ path: "Task_Status__c", type: "string" },{ path: "ActivityDateTime", type: "string" },{ path: "DurationInMinutes", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "WhatId", type: "string" },{ path: "WhoId", type: "string" },{ path: "Safra_ID__c", type: "string" },{ path: "Status__c", type: "string" },{ path: "BR_Division__c", type: "string" },{ path: "BR_Tipo_de_Visita__c", type: "string" },{ path: "Geracao_de_Demanda__c", type: "string" },{ path: "Tipo_de_Geracao_de_Demanda__c", type: "string" },{ path: "Data_do_Plantio__c", type: "string" },{ path: "Safra__c", type: "string" },{ path: "Fazenda__c", type: "string" },{ path: "StartDateTime", type: "string" },{ path: "EndDateTime", type: "string" },{ path: "BR_StartTime__c", type: "string" },{ path: "BR_EndTime__c", type: "string" },{ path: "IsAllDayEvent", type: "string" },{ path: "Lembrete__c", type: "string" },{ path: "Location", type: "string" },{ path: "IsOutlook__c", type: "string" },{ path: "Description", type: "string" },{ path: "CreatedDate", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'Event' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    OwnerId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MobileAppFlag__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_SalesCoachingEvent__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ClienteId__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RecordTypeId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Observacoes__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Subject:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Tipo_Compromisso__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Task_Status__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ActivityDateTime:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    DurationInMinutes:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    WhatId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    WhoId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Safra_ID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Status__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Division__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Tipo_de_Visita__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Geracao_de_Demanda__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Tipo_de_Geracao_de_Demanda__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Data_do_Plantio__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Safra__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Fazenda__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    StartDateTime:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    EndDateTime:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_StartTime__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_EndTime__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    IsAllDayEvent:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Lembrete__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Location:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    IsOutlook__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Description:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    CreatedDate:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
    */
    
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    Account:ClientsModel = new ClientsModel(null);
    

    setAccount(provider: ClientsService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('Id', this.WhatId, 1, 'ascending', null,
                (response) => {
                    this.Account = new ClientsModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdEvent').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdEvent', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdEvent', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdEvent', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
