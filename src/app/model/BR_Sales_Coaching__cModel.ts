import { EventModel } from "./EventModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { EventService } from "../../providers/event/event-service-salesforce";

/**
 * Classe model da entidade BR_Sales_Coaching__c
 */

export class BR_Sales_Coaching__cModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'BR_Sales_Coaching__c';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "Name", type: "string" },{ path: "BR_Status__c", type: "string" },{ path: "BR_Cancel_Motivation__c", type: "string" },{ path: "RecordTypeId", type: "string" },{ path: "BR_RTV__c", type: "string" },{ path: "BR_Stage__c", type: "string" },{ path: "BR_Stage_1_Completed__c", type: "string" },{ path: "BR_Stage_2_Completed__c", type: "string" },{ path: "BR_Stage_3_Completed__c", type: "string" },{ path: "BR_Stage_4_Completed__c", type: "string" },{ path: "BR_Stage_5_Completed__c", type: "string" },{ path: "BR_Stage_6_Completed__c", type: "string" },{ path: "BR_StartDate__c", type: "string" },{ path: "BR_EndDate__c", type: "string" },{ path: "BR_Location__c", type: "string" },{ path: "BR_AllDayEvent__c", type: "string" },{ path: "BR_MobileAppFlag__c", type: "string" },{ path: "BR_Notes_Prepare_1__c", type: "string" },{ path: "BR_Notes_Prepare_2__c", type: "string" },{ path: "BR_Notes_Execute_1__c", type: "string" },{ path: "BR_Notes_Execute_2__c", type: "string" },{ path: "BR_Notes_Observe_1__c", type: "string" },{ path: "BR_Notes_Observe_2__c", type: "string" },{ path: "BR_Notes_Observe_3__c", type: "string" },{ path: "BR_Notes_Observe_4__c", type: "string" },{ path: "BR_Notes_Observe_5__c", type: "string" },{ path: "BR_Notes_Share_1__c", type: "string" },{ path: "BR_Strong_Point_1__c", type: "string" },{ path: "BR_Strong_Point_1_Description__c", type: "string" },{ path: "BR_Strong_Point_2__c", type: "string" },{ path: "BR_Strong_Point_2_Description__c", type: "string" },{ path: "BR_Strong_Point_3__c", type: "string" },{ path: "BR_Strong_Point_3_Description__c", type: "string" },{ path: "BR_Opportunity_1__c", type: "string" },{ path: "BR_Opportunity_2__c", type: "string" },{ path: "BR_Opportunity_1_Description__c", type: "string" },{ path: "BR_Opportunity_1_Time__c", type: "string" },{ path: "BR_Opportunity_2_Description__c", type: "string" },{ path: "BR_Opportunity_2_Time__c", type: "string" },{ path: "BR_Opportunity_3__c", type: "string" },{ path: "BR_Opportunity_3_Description__c", type: "string" },{ path: "BR_Opportunity_3_Time__c", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'BR_Sales_Coaching__c' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Name:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Status__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Cancel_Motivation__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RecordTypeId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_RTV__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stage__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stage_1_Completed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stage_2_Completed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stage_3_Completed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stage_4_Completed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stage_5_Completed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stage_6_Completed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_StartDate__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_EndDate__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Location__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_AllDayEvent__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MobileAppFlag__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Prepare_1__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Prepare_2__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Execute_1__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Execute_2__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Observe_1__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Observe_2__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Observe_3__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Observe_4__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Observe_5__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Notes_Share_1__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Strong_Point_1__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Strong_Point_1_Description__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Strong_Point_2__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Strong_Point_2_Description__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Strong_Point_3__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Strong_Point_3_Description__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_1__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_2__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_1_Description__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_1_Time__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_2_Description__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_2_Time__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_3__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_3_Description__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Opportunity_3_Time__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    Event:Array<EventModel> = [];

    setEvent(provider: EventService): Promise<any> {
        this.Event = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('WhatId', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.Event.push(new EventModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdBR_Sales_Coaching__c').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdBR_Sales_Coaching__c', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdBR_Sales_Coaching__c', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdBR_Sales_Coaching__c', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
