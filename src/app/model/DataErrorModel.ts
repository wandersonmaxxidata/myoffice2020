
export class DataErrorModel {

    private id: number;
    private der_data_id: string;
    private der_error_id: number;

    constructor(
        id?: number,
        der_data_id?: string,
        der_error_id?: number
    ){
        this.setId(id);
        this.setDataId(der_data_id);
        this.setErrorId(der_error_id);
    }

    public setId(id: number): void{
        this.id = id;
    }
    public setDataId(der_data_id: string): void{
        this.der_data_id = der_data_id;
    }
    public setErrorId(der_error_id: number): void{
        this.der_error_id = der_error_id;
    }


    public getId(){
        return this.id;
    }
    public getDataId(){
        return this.der_data_id;
    }
    public getErrorId(){
        return this.der_error_id;
    }




}