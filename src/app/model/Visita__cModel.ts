import { BR_ProductUsed__cModel } from "./BR_ProductUsed__cModel";
import { BR_WeedHandling__cModel } from "./BR_WeedHandling__cModel";
import { Hibrido_de_Visita__cModel } from "./Hibrido_de_Visita__cModel";
import { BR_CropProducts__cModel } from "./BR_CropProducts__cModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { BR_CropProducts__cService } from "../../providers/br_cropproducts__c/br_cropproducts__c-service-salesforce";
import { Hibrido_de_Visita__cService } from "../../providers/hibrido_de_visita__c/hibrido_de_visita__c-service-salesforce";
import { BR_WeedHandling__cService } from "../../providers/br_weedhandling__c/br_weedhandling__c-service-salesforce";
import { BR_ProductUsed__cService } from "../../providers/br_productused__c/br_productused__c-service-salesforce";

/**
 * Classe model da entidade Visita__c
 */

export class Visita__cModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'Visita__c';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "BR_MobileAppFlag__c", type: "string" },{ path: "BR_EmailContactList__c", type: "string" },{ path: "BR_has_disease_control_fungicide__c", type: "string" },{ path: "BR_UsedHerbicideInTheWinter__c", type: "string" },{ path: "BR_GenerateReport__c", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "BR_stand_well_established__c", type: "string" },{ path: "BR_failed_planting__c", type: "string" },{ path: "BR_DesiccationMadeInPlanting__c", type: "string" },{ path: "divulgacao_de_resultado__c", type: "string" },{ path: "BR_MadeEarlyDesiccation__c", type: "string" },{ path: "BR_infestation_weed__c", type: "string" },{ path: "BR_plagues_remaning_previous_culture__c", type: "string" },{ path: "BR_handling_held_harvest_postharvest__c", type: "string" },{ path: "BR_reason__c", type: "string" },{ path: "BR_Stand_Final_ha__c", type: "string" },{ path: "BR_ProductUsed__c", type: "string" },{ path: "Status__c", type: "string" },{ path: "BR_plants_weeds_resistant__c", type: "string" },{ path: "BR_Altitude__c", type: "string" },{ path: "RecordTypeId", type: "string" },{ path: "BR_BaseFertilizationK__c", type: "string" },{ path: "BR_BaseFertilizationN__c", type: "string" },{ path: "BR_BaseFertilizationP__c", type: "string" },{ path: "BR_FertilizationCoveringK__c", type: "string" },{ path: "BR_FertilizationCoveringN__c", type: "string" },{ path: "BR_FertilizationCoveringP__c", type: "string" },{ path: "Cliente__c", type: "string" },{ path: "Data_de_Execucao__c", type: "string" },{ path: "EventId__c", type: "string" },{ path: "GerDemandaId__c", type: "string" },{ path: "BR_Division__c", type: "string" },{ path: "Final__c", type: "string" },{ path: "BR_EventVisitType__c", type: "string" },{ path: "Safra__c", type: "string" },{ path: "Matriz__c", type: "string" },{ path: "Hibrido__c", type: "string" },{ path: "BR_Silage__c", type: "string" },{ path: "Latitude__c", type: "string" },{ path: "Longitude__c", type: "string" },{ path: "Observacao__c", type: "string" },{ path: "BR_plant_area_refuge__c", type: "string" },{ path: "BR_percentage_refuge_areas__c", type: "string" },{ path: "Porcentagem_de_Produtividade__c", type: "string" },{ path: "Area_Plantada__c", type: "string" },{ path: "BR_PlantingDepth__c", type: "string" },{ path: "BR_Product__c", type: "string" },{ path: "Espacamento__c", type: "string" },{ path: "Tipo_de_Geracao_de_Demanda__c", type: "string" },{ path: "Name", type: "string" },{ path: "Data_do_Plantio__c", type: "string" },{ path: "BR_PragueWhichAttackedArea__c", type: "string" },{ path: "BR_fungicide_product__c", type: "string" },{ path: "BR_PresentVisit__c", type: "string" },{ path: "BR_CultureForCrop__c", type: "string" },{ path: "BR_CropObjective__c", type: "string" },{ path: "BR_Dealed__c", type: "string" },{ path: "BR_Closed__c", type: "string" },{ path: "BR_CropClimateVisitType__c", type: "string" },{ path: "BR_EventTypeCrop__c", type: "string" },{ path: "BR_CropSubject__c", type: "string" },{ path: "Numero_de_participantes__c", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'Visita__c' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MobileAppFlag__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_EmailContactList__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_has_disease_control_fungicide__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_UsedHerbicideInTheWinter__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_GenerateReport__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_stand_well_established__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_failed_planting__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_DesiccationMadeInPlanting__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    divulgacao_de_resultado__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MadeEarlyDesiccation__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_infestation_weed__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_plagues_remaning_previous_culture__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_handling_held_harvest_postharvest__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_reason__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Stand_Final_ha__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_ProductUsed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Status__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_plants_weeds_resistant__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Altitude__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RecordTypeId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_BaseFertilizationK__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_BaseFertilizationN__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_BaseFertilizationP__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_FertilizationCoveringK__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_FertilizationCoveringN__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_FertilizationCoveringP__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Cliente__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Data_de_Execucao__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    EventId__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    GerDemandaId__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Division__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Final__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_EventVisitType__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Safra__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Matriz__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Hibrido__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Silage__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Latitude__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Longitude__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Observacao__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_plant_area_refuge__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_percentage_refuge_areas__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Porcentagem_de_Produtividade__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Area_Plantada__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PlantingDepth__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Product__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Espacamento__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Tipo_de_Geracao_de_Demanda__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Name:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Data_do_Plantio__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PragueWhichAttackedArea__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_fungicide_product__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PresentVisit__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_CultureForCrop__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_CropObjective__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Dealed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Closed__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_CropClimateVisitType__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_EventTypeCrop__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_CropSubject__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Numero_de_participantes__c:string = undefined;
    
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    CropProducts:Array<BR_CropProducts__cModel> = [];
    

    setCropProducts(provider: BR_CropProducts__cService): Promise<any> {
        this.CropProducts = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('BR_VisitCrop__c', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.CropProducts.push(new BR_CropProducts__cModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    Hibrido_de_Visita__c:Array<Hibrido_de_Visita__cModel> = [];

    setHibrido_de_Visita__c(provider: Hibrido_de_Visita__cService): Promise<any> {
        this.Hibrido_de_Visita__c = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('BR_VisitMobileId__c', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.Hibrido_de_Visita__c.push(new Hibrido_de_Visita__cModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    BR_WeedHandling__c:Array<BR_WeedHandling__cModel> = [];

    setBR_WeedHandling__c(provider: BR_WeedHandling__cService): Promise<any> {
        this.BR_WeedHandling__c = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('BR_Visit__c', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.BR_WeedHandling__c.push(new BR_WeedHandling__cModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    ProductUsed:Array<BR_ProductUsed__cModel> = [];

    setProductUsed(provider: BR_ProductUsed__cService): Promise<any> {
        this.ProductUsed = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('BR_VisitUsedProduct__c', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.ProductUsed.push(new BR_ProductUsed__cModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdVisita__c').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdVisita__c', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdVisita__c', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdVisita__c', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    geCurenttId() {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
