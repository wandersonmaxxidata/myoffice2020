import { BrandModel } from "./BrandModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { BrandService } from "../../providers/brand/brand-service-salesforce";

/**
 * Classe model da entidade User
 */

export class UserModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'User';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "BR_UserId__c", type: "string" },{ path: "LanguageLocaleKey", type: "string" },{ path: "BR_ProfileName__c", type: "string" },{ path: "BR_User_Type__c", type: "string" },{ path: "BR_KAMUser__c", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "Name", type: "string" },{ path: "BR_DistrictName__c", type: "string" },{ path: "BR_Culture__c", type: "string" },{ path: "BR_Regional__c", type: "string" },{ path: "Marca__c", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'User' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_UserId__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    LanguageLocaleKey:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_ProfileName__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_User_Type__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_KAMUser__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Name:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_DistrictName__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Culture__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Regional__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Marca__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    Marca:BrandModel = new BrandModel(null);

    setMarca(provider: BrandService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('Name', this.Marca__c, 1, 'ascending', null,
                (response) => {
                    this.Marca = new BrandModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdUser').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdUser', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdUser', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdUser', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
