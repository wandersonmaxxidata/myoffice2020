import { ContactModel } from "./ContactModel";
import { MunicipioModel } from "./MunicipioModel";
import { BR_ProfileMapping__cModel } from "./BR_ProfileMapping__cModel";
import { RecordTypeModel } from "./RecordTypeModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { RecordTypeService } from "../../providers/recordtype/recordtype-service-salesforce";
import { BR_ProfileMapping__cService } from "../../providers/br_profilemapping__c/br_profilemapping__c-service-salesforce";
import { MunicipioService } from "../../providers/municipio/municipio-service-salesforce";
import { ContactService } from "../../providers/contact/contact-service-salesforce";

/**
 * Classe model da entidade Clients
 */

export class ClientsModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'Clients';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "OwnerId", type: "string" },{ path: "ParentId", type: "string" },{ path: "BR_Parent_Name__c", type: "string" },{ path: "RADL__c", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "Name", type: "string" },{ path: "RecordTypeId", type: "string" },{ path: "BR_MobileSearchFilter__c", type: "string" },{ path: "Municipio__c", type: "string" },{ path: "UF__c", type: "string" },{ path: "Rua__c", type: "string" },{ path: "Inscricao_Estadual_Produtor__c", type: "string" },{ path: "GC_VATIN__c", type: "string" },{ path: "GC_Latitude__c", type: "string" },{ path: "GC_Longitude__c", type: "string" },{ path: "Phone", type: "string" },{ path: "GC_Email_Address__c", type: "string" },{ path: "GC_SAP_ID__c", type: "string" },{ path: "BR_FamilyGroup__c", type: "string" },{ path: "BR_CreditExpirationDate__c", type: "string" },{ path: "BR_RenewForm__c", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'Clients' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    OwnerId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ParentId:string = undefined;
    	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Parent_Name__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RADL__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Name:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RecordTypeId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MobileSearchFilter__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Municipio__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    UF__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Rua__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Inscricao_Estadual_Produtor__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    GC_VATIN__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    GC_Latitude__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    GC_Longitude__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Phone:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    GC_Email_Address__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    GC_SAP_ID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_FamilyGroup__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_CreditExpirationDate__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_RenewForm__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    RecordType:RecordTypeModel = new RecordTypeModel(null);

    setRecordType(provider: RecordTypeService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.RecordTypeId, 1, 'ascending', null,
                (response) => {
                    this.RecordType = new RecordTypeModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    BR_ProfileMapping__c:Array<BR_ProfileMapping__cModel> = [];

    setBR_ProfileMapping__c(provider: BR_ProfileMapping__cService): Promise<any> {
        this.BR_ProfileMapping__c = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('BR_Account__c', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.BR_ProfileMapping__c.push(new BR_ProfileMapping__cModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    Municipio:MunicipioModel = new MunicipioModel(null);

    setMunicipio(provider: MunicipioService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.Municipio__c, 1, 'ascending', null,
                (response) => {
                    this.Municipio = new MunicipioModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    Contact:Array<ContactModel> = [];

    setContact(provider: ContactService): Promise<any> {
        this.Contact = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('AccountId', this.getId(), 1000, 'ascending', "FirstName",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.Contact.push(new ContactModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdClients').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdClients', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdClients', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdClients', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
