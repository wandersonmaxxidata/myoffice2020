import { AppPreferences } from '@ionic-native/app-preferences';

/**
 * Classe model da entidade Hibrido__c
 */

export class Hibrido__cModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'Hibrido__c';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "Name", type: "string" },{ path: "BR_BrandMobile__c", type: "string" },{ path: "BR_ComercialHybrid__c", type: "string" },{ path: "Tipo_de_Produto__c", type: "string" },{ path: "BR_ProductGroup__c", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'Hibrido__c' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Name:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_BrandMobile__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_ComercialHybrid__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Tipo_de_Produto__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_ProductGroup__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdHibrido__c').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdHibrido__c', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdHibrido__c', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdHibrido__c', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
