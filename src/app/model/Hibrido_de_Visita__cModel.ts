import { Hibrido__cModel } from "./Hibrido__cModel";
import { Doenca__cModel } from "./Doenca__cModel";
import { Praga__cModel } from "./Praga__cModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { Praga__cService } from "../../providers/praga__c/praga__c-service-salesforce";
import { Doenca__cService } from "../../providers/doenca__c/doenca__c-service-salesforce";
import { Hibrido__cService } from "../../providers/hibrido__c/hibrido__c-service-salesforce";

/**
 * Classe model da entidade Hibrido_de_Visita__c
 */

export class Hibrido_de_Visita__cModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'Hibrido_de_Visita__c';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "BR_HybridName__c", type: "string" },{ path: "BR_VisitMobileId__c", type: "string" },{ path: "BR_MobileAppFlag__c", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "BR_Lodging__c", type: "string" },{ path: "Quebramento__c", type: "string" },{ path: "BR_DayCycle__c", type: "string" },{ path: "BR_PlantationDate__c", type: "string" },{ path: "Ambiente_Produtivo_T_ha__c", type: "string" },{ path: "BR_HarvestDate__c", type: "string" },{ path: "Populacao_Final__c", type: "string" },{ path: "Area_Colhida__c", type: "string" },{ path: "Hibrido_Visita__c", type: "string" },{ path: "Hibrido__c", type: "string" },{ path: "Marca__c", type: "string" },{ path: "Umidade__c", type: "string" },{ path: "PRODUTIVIDADE_LIQ_13__c", type: "string" },{ path: "Percentual_de_Grao_Ardido__c", type: "string" },{ path: "Area_Plantada__c", type: "string" },{ path: "BR_PlantedArea__c", type: "string" },{ path: "BR_PlantationSeedsM__c", type: "string" },{ path: "Populacao_de_Plantio__c", type: "string" },{ path: "Populacao_de_Emergencia__c", type: "string" },{ path: "Produto__c", type: "string" },{ path: "Tratamento_de_Sementes__c", type: "string" },{ path: "BR_THa__c", type: "string" },{ path: "Tombamento__c", type: "string" },{ path: "RecordTypeId", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'Hibrido_de_Visita__c' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_HybridName__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_VisitMobileId__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MobileAppFlag__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Lodging__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Quebramento__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_DayCycle__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PlantationDate__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Ambiente_Produtivo_T_ha__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_HarvestDate__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Populacao_Final__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Area_Colhida__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Hibrido_Visita__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Hibrido__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Marca__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Umidade__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    PRODUTIVIDADE_LIQ_13__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Percentual_de_Grao_Ardido__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Area_Plantada__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PlantedArea__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PlantationSeedsM__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Populacao_de_Plantio__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Populacao_de_Emergencia__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Produto__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Tratamento_de_Sementes__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_THa__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Tombamento__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RecordTypeId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    Plagues:Array<Praga__cModel> = [];

    setPlagues(provider: Praga__cService): Promise<any> {
        this.Plagues = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('Hibrido_de_Visita__c', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.Plagues.push(new Praga__cModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    Diseases:Array<Doenca__cModel> = [];

    setDiseases(provider: Doenca__cService): Promise<any> {
        this.Diseases = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup('Hibrido_de_Visita__c', this.getId(), 1000, 'ascending', "",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.Diseases.push(new Doenca__cModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    AllHybrids:Hibrido__cModel = new Hibrido__cModel(null);

    setAllHybrids(provider: Hibrido__cService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.Hibrido_Visita__c, 1, 'ascending', null,
                (response) => {
                    this.AllHybrids = new Hibrido__cModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdHibrido_de_Visita__c').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdHibrido_de_Visita__c', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdHibrido_de_Visita__c', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdHibrido_de_Visita__c', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
