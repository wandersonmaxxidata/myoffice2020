import { RecordTypeModel } from "./RecordTypeModel";
import { ClientsModel } from "./ClientsModel";
import { AppPreferences } from '@ionic-native/app-preferences';
import { ClientsService } from "../../providers/clients/clients-service-salesforce";
import { RecordTypeService } from "../../providers/recordtype/recordtype-service-salesforce";

/**
 * Classe model da entidade BR_ProfileMapping__c
 */

export class BR_ProfileMapping__cModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'BR_ProfileMapping__c';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "RecordTypeId", type: "string" },{ path: "RADL__c", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "BR_Rating__c", type: "string" },{ path: "BR_MarketingInitiative__c", type: "string" },{ path: "BR_HasQiOnInstalled__c", type: "string" },{ path: "BR_IsInfluencer__c", type: "string" },{ path: "BR_SalesSplit__c", type: "string" },{ path: "BR_Description__c", type: "string" },{ path: "BR_RefugeHave__c", type: "string" },{ path: "BR_WhatMotive__c", type: "string" },{ path: "BR_IsLeasehold__c", type: "string" },{ path: "BR_PlantedArea__c", type: "string" },{ path: "BR_TotalArea__c", type: "string" },{ path: "BR_Account__c", type: "string" },{ path: "BR_Income__c", type: "string" },{ path: "BR_Culture__c", type: "string" },{ path: "BR_LarggestSupplyCrop__c", type: "string" },{ path: "BR_EmployeeAmount__c", type: "string" },{ path: "BR_ProgramCategoryGTM__c", type: "string" },{ path: "BR_OthersLoyalProgram__c", type: "string" },{ path: "BR_PreviousCropComment__c", type: "string" },{ path: "BR_CurrentCropComment__c", type: "string" },{ path: "OwnerId", type: "string" },{ path: "CreatedDate", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'BR_ProfileMapping__c' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RecordTypeId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    RADL__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Rating__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MarketingInitiative__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_HasQiOnInstalled__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_IsInfluencer__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_SalesSplit__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Description__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_RefugeHave__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_WhatMotive__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_IsLeasehold__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PlantedArea__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_TotalArea__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Account__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Income__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Culture__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_LarggestSupplyCrop__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_EmployeeAmount__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_ProgramCategoryGTM__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_OthersLoyalProgram__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_PreviousCropComment__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_CurrentCropComment__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    OwnerId:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    CreatedDate:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    Account:ClientsModel = new ClientsModel(null);

    setAccount(provider: ClientsService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.BR_Account__c, 1, 'ascending', null,
                (response) => {
                    this.Account = new ClientsModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    RecordType:RecordTypeModel = new RecordTypeModel(null);

    setRecordType(provider: RecordTypeService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(this.getIdFieldName(), this.RecordTypeId, 1, 'ascending', null,
                (response) => {
                    this.RecordType = new RecordTypeModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }
    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdBR_ProfileMapping__c').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdBR_ProfileMapping__c', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdBR_ProfileMapping__c', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdBR_ProfileMapping__c', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
