import { AppPreferences } from '@ionic-native/app-preferences';

/**
 * Classe model da entidade BR_CropProducts__c
 */

export class BR_CropProducts__cModel implements BaseModel {

    /**
     * Nome de nossa tabela
     */
    static soupName = 'BR_CropProducts__c';

    /**
     * Colunas de nossa tabela seguidamente de seu tipo.
     */
    static indexes = [
        { path: "Id", type: "string" },{ path: "ExternalID__c", type: "string" },{ path: "BR_MobileAppFlag__c", type: "string" },{ path: "BR_VisitCrop__c", type: "string" },{ path: "BR_InformationType__c", type: "string" },{ path: "BR_ProductConcorrente__c", type: "string" },{ path: "BR_VolumeConcorrent__c", type: "string" },{ path: "BR_MoneyConcorrente__c", type: "string" },{ path: "BR_VencimentoDoConcorrente__c", type: "string" },{ path: "BR_Tax__c", type: "string" },{ path: "BR_ForecastInvoice__c", type: "string" },{ path: "BR_VolNoForecast__c", type: "string" },{ path: "__local__", type: "string" },{ path: "__locally_created__", type: "string" },{ path: "__locally_updated__", type: "string" },{ path: "__locally_deleted__", type: "string" },
    ];
    /**
     * Identificação tipo do registro no banco de dados local
     */
    attributes = { type: 'BR_CropProducts__c' };
	/**
	* Atributo da entidade no saleforce.
    
	*/
    Id:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    ExternalID__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MobileAppFlag__c:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_VisitCrop__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_InformationType__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_ProductConcorrente__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_VolumeConcorrent__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_MoneyConcorrente__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_VencimentoDoConcorrente__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_Tax__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_ForecastInvoice__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    BR_VolNoForecast__c:string = undefined;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __local__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_created__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_updated__:boolean = false;
	/**
	* Atributo da entidade no saleforce.
    
	*/
    __locally_deleted__:boolean = false;


    constructor(object: Object) {
        if (object) {
            for (const key of Object.keys(object)) {
                this[key] = (object[key] === null || object[key] === undefined) ? this[key] : this.ifNumberConvertToString(object, key);
            }
        } else {
            this.Id = this.ExternalID__c = 'local_' + new Date().getTime();
            this.__local__ = true;
            this.__locally_created__ = true;
            this.__locally_updated__ = true;
            this.__locally_deleted__ = false;
        }
    }

    private ifNumberConvertToString(obj: any, key: string): string {
      return (typeof obj[key] === 'number' && !key.startsWith('_')) ? obj[key].toString() : obj[key];
    }

    static getLastSyncDownId(appPreferences: AppPreferences): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.fetch('lastSyncDownIdBR_CropProducts__c').then((res) => {
                console.log('getLastSyncDownId lastSyncDownIdBR_CropProducts__c', (Number(res) + 1).toString());
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    static setLastSyncDownId(appPreferences: AppPreferences, lastSyncDownId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            appPreferences.store('lastSyncDownIdBR_CropProducts__c', lastSyncDownId).then((res) => {
                console.log('setLastSyncDownId lastSyncDownIdBR_CropProducts__c', res);
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    validate():Promise<any>{
        let invalidFields = [];
        return new Promise((resolve, reject) => {
            
            if(invalidFields.length === 0){
                resolve();
            }else{
                reject(invalidFields);
            }

        });
    }

    getId():string {
        let id = this.Id ? this.Id : this.ExternalID__c
        return id
    }

    getIdFieldName():string {
        let idFieldName = this.Id ? 'Id' : 'ExternalID__c'
        return idFieldName
    }

}
