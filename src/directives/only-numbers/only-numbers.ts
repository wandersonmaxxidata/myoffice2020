import { Directive, HostListener } from '@angular/core';

/**
 * Diretiva que permite apenas números.
 */
@Directive({
  selector: '[only-numbers]', // Attribute selector
})
export class OnlyNumbersDirective {

  @HostListener('keyup', ['$event'])
  public onKeyup($event: any) {
    this.disabledKey($event);
  }

  @HostListener('ionBlur', ['$event'])
  public onBlur($event: any) {
    this.disabledKey($event);
  }

  @HostListener('keydown', ['$event'])
  public onKeyDown($event: any) {
    this.disabledKey($event);
  }

  private disabledKey(e: any) {

    if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && e.keyCode !== 8) {
      e.returnValue = false;
    }
  }

}
