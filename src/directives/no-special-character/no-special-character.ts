import { Directive , HostListener } from '@angular/core';

/*Diretiva que desativa qualquer teclar que não seja letras ou números.*/

@Directive({
  selector: '[no-special-character]',
})
export class NoSpecialCharacterDirective {

  @HostListener('keyup', ['$event'])
  public onKeyup($event: any) {
    this.disabledKey($event);
  }

  @HostListener('blur', ['$event'])
  public onBlur($event: any) {
    this.disabledKey($event);
  }

  @HostListener('keydown', ['$event'])
  public onKeyDown($event: any) {
    this.disabledKey($event);
  }

  private disabledKey(e: any) {

    if ((
      e.keyCode < 48 || e.keyCode > 57 &&
      e.keyCode < 65 || e.keyCode > 90 &&
      e.keyCode < 97 || e.keyCode > 122 &&
      e.keyCode < 128 || e.keyCode > 144 &&
      e.keyCode < 147 || e.keyCode > 154 &&
      e.keyCode < 160 || e.keyCode > 166 &&
      e.keyCode < 181 || e.keyCode > 183 &&
      e.keyCode < 198 || e.keyCode > 199 &&
      e.keyCode < 210 || e.keyCode > 216 &&
      e.keyCode < 222 || e.keyCode > 222)
      && e.keyCode !== 8 && e.keyCode !== 32) {
      e.returnValue = false;
    }
  }
}

