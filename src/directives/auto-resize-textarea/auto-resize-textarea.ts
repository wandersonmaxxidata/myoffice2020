import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[auto-resize-textarea]',
})
export class AutoResizeTextareaDirective implements OnInit {

  @HostListener('input', ['$event.target'])
  public onInput(textArea: HTMLTextAreaElement): void {
    this.adjust();
  }

  @Input('auto-resize-textarea') 
  public maxHeight: number;

  constructor(public element: ElementRef) {}

  public ngOnInit(): void {
    setTimeout(() => { this.adjust(); }, 750);
  }

  public adjust(): void {
    const ta = this.element.nativeElement.querySelector('textarea');
    if (ta) {
      ta.style.overflow = 'scroll';
      ta.style.height = null;
      ta.style.height = Math.min(ta.scrollHeight, this.maxHeight) + 'px';
    } else {
      this.element.nativeElement.style.overflow = 'scroll';
      this.element.nativeElement.height = null;
      this.element.nativeElement.style.height = Math.min(this.element.nativeElement.scrollHeight, this.maxHeight) + 'px';
    }
  }
}
