import { Directive, HostListener, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[mask]',
})
export class MaskDirective {

  private consts = [
    '+55',
  ];

  @Input()
  public mask: string;

  constructor(private control: NgControl) {}

  @HostListener('change')
  public ngOnChanges() {
    const value = this.control.control.value;
    if (value) {
      this.control.control.setValue(this.format(value));
    }
  }

  @HostListener('keyup', ['$event'])
  public onKeyUp($event: any) {
    if (
      $event.keyCode !== 8      // backspace
      && $event.keyCode !== 9   // tab
      && $event.keyCode !== 13  // enter
      && $event.keyCode !== 46  // delete
    ) {
      let selectionStart = $event.target.selectionStart;
      const value = this.control.control.value.slice();
      const newValue = this.format(value);

      selectionStart += (newValue.length - value.length);

      this.control.control.setValue(newValue);
      $event.target.selectionStart = selectionStart;
      $event.target.selectionEnd = selectionStart;
    }
  }

  private format(v: string): string {
    let s = '';
    let newString = v.slice();

    this.consts.forEach((item) => {
      newString = newString.replace(item, '');
    });

    const matches = newString.match(/[a-zA-Z0-9]+/g);

    if (matches !== null && matches !== undefined) {
      let value = matches.join('').split('');
      const chars = this.mask.split('');

      for (const c of chars) {
        if (value.length === 0) {
          break;
        }

        switch (c) {
          case '#':
          if (value[0].match(/\d/) !== null) {
            s += value[0];
            value = value.slice(1);
          }
          break;

          case '@':
          if (value[0].match(/[a-zA-Z]/) !== null) {
            s += value[0];
            value = value.slice(1);
          }
          break;

          case '*':
            s += value[0];
            value = value.slice(1);
          break;

          default:
            s += c;
        }
      }

      newString = s.slice();
      this.consts.forEach((item) => {
        newString = newString.replace(item, '');
      });

      const after = newString.match(/[a-zA-Z0-9]+/g);
      if (!after) { s = ''; }
    }
    return s;
  }
}
