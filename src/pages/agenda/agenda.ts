import { AgendaComponent } from '../../components/agenda/agenda';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { HeaderComponent } from '../../components/header/header';
import { Moment } from 'moment';
import { TabHelper } from '../../shared/Helpers/TabHelper';
import * as moment from 'moment';

@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})
export class AgendaPage {

  public currentDate: Moment;

  @ViewChild(AgendaComponent)
  private agendaComponent: AgendaComponent;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.currentDate = moment();
  }

  public ionViewDidEnter() {
    if (this.header) {
      this.header.updatePageTitle('Agenda');
    }

    if (this.agendaComponent) {
      if (this.agendaComponent.isMonthView) {
        this.agendaComponent.eventSource = undefined;
      }
      this.agendaComponent.loadSelectedDate(this.agendaComponent.current, this.agendaComponent.currentIndex);
    }

    TabHelper.enableAll(this.navCtrl);
  }

}
