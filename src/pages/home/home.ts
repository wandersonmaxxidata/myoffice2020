import { AlertController } from 'ionic-angular';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { AppPreferences } from '@ionic-native/app-preferences';
import { ArchitectureValidationManager } from '../../shared/Managers/ArchitectureValidationManager';
import { Component, NgZone, ViewChild, forwardRef } from '@angular/core';
import { Content } from 'ionic-angular';
import { Events, LoadingController, NavController, Platform, Tabs, NavParams } from 'ionic-angular';
import { HeaderComponent } from '../../components/header/header';
import { LoadingManager } from '../../shared/Managers/LoadingManager';
import { NotificationBO } from './../../shared/BO/NotificationBO';
import { NotificationKeys } from './../../shared/Constants/Keys';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { ServiceProvider } from './../../providers/service-salesforce';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabHelper } from '../../shared/Helpers/TabHelper';
import { WelcomeHomeComponent } from './../../components/welcome-home/welcome-home';
import { Network } from '@ionic-native/network';


/**
 * Página com listagem dos registros do salesforce
 */
@Component({
  selector: 'home',
  templateUrl: 'home.html',
})
export class HomePage {

  public syncError: boolean = false;
  public pageReturn: any = false;

  public notificationBO: NotificationBO;
  public countSyncBadge = 0;

  public geradorDeErroVar;
  public showSyncButton;

  public hiddenSyncHeader:boolean = false;

  public blockScroll: boolean;
  public tab: Tabs;
  public filterErro: any = false;

  @ViewChild(WelcomeHomeComponent)
  private welcomeHomeComponent: WelcomeHomeComponent;

  @ViewChild(Content)
  public content: Content;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public events: Events,
    private platform: Platform,
    private loadingController: LoadingController,
    private zone: NgZone,
    public network: Network,
    private splashScreen: SplashScreen,
    private screenOrientation: ScreenOrientation,
    private serviceProvider: ServiceProvider,
    public appPreferences: AppPreferences,
    public alertCtrl: AlertController,
  ) {
    this.notificationBO = new NotificationBO(this.serviceProvider);
    this.blockScroll = false;
    this.tab = this.navCtrl.parent;
    LoadingManager.createInstance(this.loadingController);
    ArchitectureValidationManager.createInstance(this.platform);

    //MAXXIDATA
    if (sessionStorage.getItem('alertSnycy') !== 'ok') {
      this.notificationBO.getCountSyncVisitsSnycy().then((count: number) => {
        this.countSyncBadge = count;
        sessionStorage.setItem('alertSnycy', 'ok');
        console.log(count)
        if (count > 0) {
          AlertHelper.showDialogSncyNow(this.alertCtrl, () => {
            this.syncProcess();
          }, () => {
          });
        } else {
        }
      });
    }
    //FIM MAXXIDATA
  }

  geradorDeErro($event) {
    sessionStorage.setItem('geraErro', String(this.geradorDeErroVar));
  }

  public syncProcess() {
    let message = 'Você está sem conexão com internet no momento. Conecte-se a internet e tente novamente.';
    console.log(this.network.type);
    if (this.network.type === 'none') {
      AlertHelper.showNoConnectionAlert(this.alertCtrl, message);
    } else {
      if (this.network.type !== 'wifi') {
        message = 'Você está utilizando <strong>' + this.network.type.toUpperCase() + '</strong>, deseja continuar o sincronismo?';
        console.log(message);
        AlertHelper.showStartSyncProcessAlert(this.alertCtrl, message, () => {
          if (!this.hiddenSyncHeader) {
          this.hiddenSyncHeader = true;
          this.events.publish(NotificationKeys.startSync);
          }
        }, () => {
          console.log('Sync cancelled');
        });
      } else {
        if (!this.hiddenSyncHeader) {
        this.hiddenSyncHeader = true;
        this.events.publish(NotificationKeys.startSync);
        }
      }
    }
  }

  public ngOnInit(): void {
    console.log('------| Events: home : !!! unsubscribe !!!');

    this.events.unsubscribe(NotificationKeys.startSyncFromHome);
    this.events.unsubscribe(NotificationKeys.closeSync);
  }

  public ngAfterContentInit() {

    // this.events.subscribe(`HomePage:${NotificationKeys.firstSync}`, () => {
    //   this.showSyncButton = false;
    // });

    console.log('------| Events: home : ### subscribe ###');

    if (this.header) {
      this.header.updatePageTitle('MyOffice');
    }

    this.events.subscribe(NotificationKeys.startSyncFromHome, () => {
      this.zone.run(() => {
        this.scrollToTop();
        this.blockScroll = true;
        this.syncError = false;
      });
    });

    // this.events.subscribe(NotificationKeys.startSync, () => {
    //   this.zone.run(() => {
    //     this.scrollToTop();
    //     this.blockScroll = true;
    //     this.syncError = false;
    //   });
    // });

    this.events.subscribe(NotificationKeys.closeSync, () => {
      this.zone.run(() => {
        this.blockScroll = false;
      });
    });
  }

  showError() {
    this.syncError = true;
    this.events.publish('openSyncy');
  }

  public scrollToTop() {
    setTimeout(() => {
      if (this.content) {
        this.content.scrollToTop();
      }
    }, 400);
  }

  public ionViewWillEnter() {

    if (sessionStorage.getItem('filter')) {
      this.syncError = true;
      this.filterErro = sessionStorage.getItem('filter');
    }

    if (this.syncError) {
      this.events.publish('openSyncErr');
    }

    if (this.navParams.get('syncError')) {

      this.syncError = this.navParams.get('syncError');
      this.events.publish('openSyncErr');

    }


    //DEBUG


    // this.serviceProvider.br_profilemapping__cService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('PROFILE MAPPING =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });



    // this.serviceProvider.br_sincronizedvisit__cService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('VISITA SINCROMIZADAS =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });

    // this.serviceProvider.clientsService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('CLIENTES TODOS =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });


    // this.serviceProvider.safraService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('SAFRAS =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });


    // this.serviceProvider.eventService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('EVENTOS =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });



    // this.serviceProvider.recordtypeService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('TYPES RECORD =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });

    // this.serviceProvider.visita__cService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('VISITAS =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });


    // // this.serviceProvider.praga__cService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    // // this.serviceProvider.praga__cService.returnEventId('a0M2a0000037ToWEAU').then((data) => {
    // //   console.log('PRAGAS =======================================================================')
    // //   console.log(data)
    // // }, (error) => {
    // //   console.log('=======================================================================')
    // //   console.log(error)

    // // });


    // this.serviceProvider.praga__cService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('PRAGAS =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });


    // this.serviceProvider.br_weedhandling__cService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('PRAGAS WW =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });


    // this.serviceProvider.clientsService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('USUARIOS WW =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });


    // this.serviceProvider.hibrido_de_visita__cService.queryAllFromSoup('Id', 'ascending', 1000, (data) => {
    //   console.log('HIBRIDO  =======================================================================')
    //   console.log(data)
    // }, (error) => {
    //   console.log('=======================================================================')
    //   console.log(error)

    // });
    //DEBUG

    this.blockScroll = false;
    this.getCountSyncBadge();
    this.events.publish(NotificationKeys.openHome);
  }

  public ionViewDidEnter() {
    TabHelper.enableAll(this.navCtrl);
    this.welcomeHomeComponent.handleCurrentDatePresentation();
    this.verifySyncState();
    this.header.syncButtonManager();
    this.hideSplashScreen();
  }

  public verifySyncState() {
    this.appPreferences.fetch('syncOk').then((value) => {
      if (value !== 'success') {
        console.log('-----| syncOk APP PREFERENCES', value);
        this.zone.run(() => {
          AlertHelper.showFirstOpenAppAlert(this.alertCtrl, () => {
            this.events.publish(`HomePage:${NotificationKeys.firstSync}`);
            this.events.publish(NotificationKeys.startSync);
          }, () => {
            console.log('cancel');
          });
        });
      }
    });
  }

  public hideSplashScreen() {
    if (SplashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 1000);
    }
  }

  public goToAgendaPage() {
    this.tab.select(1);
  }

  private getCountSyncBadge() {
    return new Promise((resolve) => {
      this.notificationBO.getCountSyncBadge().then((count: number) => {
        this.countSyncBadge = count;
        resolve();
      });
    });
  }

}
