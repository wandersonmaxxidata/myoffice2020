import { ClientItemsComponent } from '../../components/client-items/client-items';
import { ClientsService } from '../../providers/clients/clients-service-salesforce';
import { Component, QueryList, ViewChild, ViewChildren, forwardRef } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { FilterList } from '../../shared/Constants/Types';
import { HeaderComponent } from '../../components/header/header';
import { NotificationKeys } from '../../shared/Constants/Keys';
import { ServiceProvider } from './../../providers/service-salesforce';
import { TabHelper } from '../../shared/Helpers/TabHelper';

@Component({
  selector: 'clients-list',
  templateUrl: 'clients-list.html',
  providers: [ClientsService],
})
export class ClientsListPage {

  public namePage = 'ClientsListPage';
  public clientsCursor: any = { currentPageOrderedEntries: [] };
  public clientsListSegment = 'farmer';
  public context: ClientsListPage = this;
  public sortBy = { field: 'Name', label: 'Name' };
  public space = ' ';

  public searchInputType = {
    farmer: 'farmerSearchType',
    farm: 'farmSearchType',
    channel: 'channelSearchType',
  };

  @ViewChildren(ClientItemsComponent)
  private clientItemComponents: QueryList<ClientItemsComponent>;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public events: Events,
  ) {}

  public ionViewWillEnter() {
    TabHelper.enableAll(this.navCtrl);

    if (this.header) {
      this.header.updatePageTitle('Clientes');
      this.header.closeSearchBar();
    }
  }

  public reloadClients() {
    setTimeout(() => {
      if (this.clientItemComponents) {
        sessionStorage.removeItem('fone');
        this.events.publish(`${this.namePage}:${NotificationKeys.changeTab}`, FilterList.profileMapping);

        this.clientItemComponents.forEach((clientItemComponent) => {
          clientItemComponent.loadClients('', this.clientsListSegment);
        });
      }
    }, 100);
  }

}
