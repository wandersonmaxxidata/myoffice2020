import { Component } from '@angular/core';
import { AlertController, ModalController, NavController } from 'ionic-angular';
import * as moment from 'moment';

@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {
  public eventSource = [];
  public viewTitle: string;
  public selectedDay = new Date();

  public calendar = {
    mode: 'month',
    currentDate: new Date(),
  };

  constructor(public navCtrl: NavController, private modalCtrl: ModalController, private alertCtrl: AlertController) { }

  public addEvent() {
    const modal = this.modalCtrl.create('EventModalPage', { selectedDay: this.selectedDay });
    modal.present();
    modal.onDidDismiss((data) => {
      if (data) {
        const eventData = data;

        eventData.startTime = new Date(data.startTime);
        eventData.endTime = new Date(data.endTime);

        const events = this.eventSource;
        events.push(eventData);
        this.eventSource = [];
        setTimeout(() => {
          this.eventSource = events;
        });
      }
    });
  }

  public changeMode(mode) {
    this.calendar.mode = mode;
  }

  public onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  public onEventSelected(event) {
    const start = moment(event.startTime).format('LLLL');
    const end = moment(event.endTime).format('LLLL');

    const alert = this.alertCtrl.create({
      title: '' + event.title,
      subTitle: 'From: ' + start + '<br>To: ' + end,
      buttons: ['OK'],
    });
    alert.present();
  }

  public onTimeSelected(ev) {
    this.selectedDay = ev.selectedTime;
  }
}
