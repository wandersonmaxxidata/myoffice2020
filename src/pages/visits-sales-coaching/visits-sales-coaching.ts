import { AlertController, Content, Events, NavController, NavParams, ToastController } from 'ionic-angular';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { BR_Sales_Coaching__cModel } from './../../app/model/BR_Sales_Coaching__cModel';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { EventModel } from './../../app/model/EventModel';
import { HeaderComponent } from './../../components/header/header';
import { Keyboard } from '@ionic-native/keyboard';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { MustEditRegisterHelper } from '../../shared/Helpers/MustEditRegisterHelper';
import { NavigationHelper } from '../../shared/Helpers/NavigationHelper';
import { NotificationKeys } from './../../shared/Constants/Keys';
import { SalesCoachingBO, SalesCoachingStatus } from './../../shared/BO/SalesCoachingBO';
import { SalesCoachingRecordTypes } from '../../shared/Constants/Types';
import { SalesCoachingTexts } from '../../shared/Constants/CoachingTexts';
import { ServiceProvider } from './../../providers/service-salesforce';
import { ToastHelper } from '../../shared/Helpers/ToastHelper';

@Component({
  selector: 'visits-sales-coaching',
  templateUrl: 'visits-sales-coaching.html',
})

export class VisitSalesCoachingPage {

  public isPreview: boolean;
  public isFields: any[];
  public titleStep: string;
  private event: EventModel;
  public salesCoaching: BR_Sales_Coaching__cModel = new BR_Sales_Coaching__cModel(null);
  public salesCoachingBO: SalesCoachingBO;
  public salesCoachingDeveloperName: string;
  private cacheVisit;
  private title: string;
  private isReadOnly: boolean;
  private showHelp = true;
  private showHelpPre = true;
  private showHelpPos = true;
  private showHelpExec = true;
  private showHelpShare1 = true;
  private showHelpShare2 = true;
  private showHelpWatch1 = true;
  private showHelpWatch2 = true;
  private showHelpWatch3 = true;
  private showHelpWatch4 = true;
  private showHelpWatch5 = true;
  private showHelpPosVisit = true;
  private showHelpFeedback = true;

  private showHelpFeed = true;
  private stepComplete = '';
  private onlineObject = [
    { obj: 'BR_Notes_Prepare_2__c', name: 'Fase Prepare: Aprenda para começar a visita' },
    { obj: 'BR_Notes_Execute_1__c', name: 'Fase Execute: Processo de vendas' },
    { obj: 'BR_Strong_Point_1__c', name: 'Fase Compartilhe: Ponto Forte (1)' },
    { obj: 'BR_Strong_Point_1_Description__c', name: 'Fase Compartilhe: Exemplo (1)' },
    { obj: 'BR_Opportunity_1__c', name: 'Fase Pense no Futuro: Áreas de Desenvolvimento (1)' },
    { obj: 'BR_Opportunity_1_Description__c', name: 'Fase Pense no Futuro: Tarefas de Aprendizado (1)' },
    { obj: 'BR_Opportunity_1_Time__c', name: 'Fase Pense no Futuro: Prazo (1)' },
  ];

  private offlineObject = [
    { obj: 'BR_Notes_Observe_1__c', name: 'Fase Observe: Fase Abra com valor' },
    { obj: 'BR_Notes_Observe_2__c', name: 'Fase Observe: Descubra necessidades do cliente' },
    { obj: 'BR_Notes_Observe_3__c', name: 'Fase Observe: Apresente Soluções' },
    { obj: 'BR_Notes_Observe_4__c', name: 'Fase Observe: Lidar com objeções' },
    { obj: 'BR_Notes_Observe_5__c', name: 'Fase Observe: Encerramento e Próximos Passos' },
  ];

  private textHelpVisit = {};

  private statusSteps = {
    step1: { status: 'enabled', isComplete: false },
    step2: { status: 'disabled', isComplete: false },
    step3: { status: 'disabled', isComplete: false },
    step4: { status: 'disabled', isComplete: false },
    step5: { status: 'disabled', isComplete: false },
    step6: { status: 'disabled', isComplete: false },
  };

  private isOnline: boolean;

  @ViewChild(Content) public content: Content;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public alertCtrl: AlertController,
    public events: Events,
    public toastCtrl: ToastController,
    public keyboard: Keyboard,
  ) {
    document.body.classList.add('hiddenTabs');
    this.salesCoachingBO = new SalesCoachingBO(this.service);
    this.event = new EventModel(this.navParams.data);
    this.loadVisit(this.event);
  }

  private handleInitialState() {
    if (this.salesCoaching.BR_Stage__c === 'Prepare') {
      this.setStep1();
    } else if (this.salesCoaching.BR_Stage__c === 'Execute') {
      this.setStep2();
    } else if (this.salesCoaching.BR_Stage__c === 'Compartilhe') {
      this.setStep3();
    } else if (this.salesCoaching.BR_Stage__c === 'Pense no futuro e Planeje') {
      this.setStep4();
    } else if (this.salesCoaching.BR_Stage__c === 'Observe') {
      this.setStep5();
    } else if (this.salesCoaching.BR_Stage__c === 'Pós-visita') {
      this.setStep6();
    } else {
      this.setStep1();
    }

    if (this.salesCoaching.BR_Status__c === SalesCoachingStatus.completed || this.salesCoaching.BR_Status__c === SalesCoachingStatus.canceled) {
      this.isReadOnly = true;
      if (this.salesCoaching.Name) {
        this.title = this.salesCoaching.Name;
      }
    } else {
      this.isReadOnly = false;
      if (this.salesCoaching.Name) {
        this.title = 'Novo' + this.salesCoaching.Name;
      }
    }
    if (MustEditRegisterHelper.isObjectTreeWithError(this.salesCoaching)) { this.isReadOnly = false; }
  }

  public loadVisit(event) {
    this.salesCoachingBO.loadSalesCoaching(event).then((salesCoaching: BR_Sales_Coaching__cModel) => {
      this.salesCoaching = new BR_Sales_Coaching__cModel(salesCoaching);
      this.salesCoachingBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
        if (this.header) {
          this.header.updatePageTitle(this.salesCoaching.Name);
        }
        this.textHelpVisit = SalesCoachingTexts[developerName];
        this.salesCoachingDeveloperName = developerName;
        this.verifyRecordType(developerName);
        if (this.salesCoaching.BR_Status__c === SalesCoachingStatus.completed) {
          this.salesCoaching.BR_Stage_1_Completed__c = 'done';
          this.salesCoaching.BR_Stage_2_Completed__c = 'done';
          this.salesCoaching.BR_Stage_3_Completed__c = 'done';
          this.salesCoaching.BR_Stage_4_Completed__c = 'done';
          this.salesCoaching.BR_Stage_5_Completed__c = 'done';
          this.salesCoaching.BR_Stage_6_Completed__c = 'done';
        }
        this.checkPhaseToComplete();
        this.handleInitialState();
      });
    });
  }

  private updateStatus(step: string) {
    Object.keys(this.statusSteps).forEach((i) => {
      if (step === i) {
        this.statusSteps[i].status = 'enabled';
        this.stepComplete = this.statusSteps[i];
      } else {
        this.statusSteps[i].status = 'disabled';
      }
    });
  }

  private checkPhaseToComplete() {
    let lastIndex;
    let index;
    if (this.salesCoaching.BR_MobileAppFlag__c === false || !this.salesCoaching.Id.includes('local')) {
      if (this.salesCoaching.BR_Stage__c) {
        if (this.isOnline) {
          if (this.salesCoaching.BR_Stage__c === 'Prepare-se') {
            index = 0;
          } else if (this.salesCoaching.BR_Stage__c === 'Execute') {
            index = 1;
          } else if (this.salesCoaching.BR_Stage__c === 'Compartilhe') {
            index = 2;
          } else if (this.salesCoaching.BR_Stage__c === 'Pense no futuro e Planeje') {
            index = 3;
          }
        } else {
          if (this.salesCoaching.BR_Stage__c === 'Prepare-se') {
            index = 0;
          } else if (this.salesCoaching.BR_Stage__c === 'Execute') {
            index = 1;
          } else if (this.salesCoaching.BR_Stage__c === 'Observe') {
            index = 2;
          } else if (this.salesCoaching.BR_Stage__c === 'Pós-visita') {
            index = 3;
          } else if (this.salesCoaching.BR_Stage__c === 'Compartilhe') {
            index = 4;
          } else if (this.salesCoaching.BR_Stage__c === 'Pense no futuro e Planeje') {
            index = 5;
          }
        }
      }
    }
    // if (index === 3) {
    //   lastIndex = index + 2;
    // } else if (index === 2) {
    //   lastIndex = index + 3;
    // } else {
      lastIndex = index;
    // }

    Object.keys(this.statusSteps).forEach((step, beforeIndex) => {
      if (beforeIndex < lastIndex) {
        if (this.isOnline) {
          if (beforeIndex === 0) {
            this.salesCoaching.BR_Stage_1_Completed__c = 'done';
          } else if (beforeIndex === 1) {
            this.salesCoaching.BR_Stage_2_Completed__c = 'done';
          } else if (beforeIndex === 2) {
            this.salesCoaching.BR_Stage_3_Completed__c = 'done';
          } else if (beforeIndex === 3) {
            this.salesCoaching.BR_Stage_4_Completed__c = 'done';
          }
        } else {
          if (beforeIndex === 0) {
            this.salesCoaching.BR_Stage_1_Completed__c = 'done';
          } else if (beforeIndex === 1) {
            this.salesCoaching.BR_Stage_2_Completed__c = 'done';
          } else if (beforeIndex === 2) {
            this.salesCoaching.BR_Stage_5_Completed__c = 'done';
          } else if (beforeIndex === 3) {
            this.salesCoaching.BR_Stage_6_Completed__c = 'done';
          } else if (beforeIndex === 4) {
            this.salesCoaching.BR_Stage_3_Completed__c = 'done';
          } else if (beforeIndex === 5) {
            this.salesCoaching.BR_Stage_4_Completed__c = 'done';
          }
        }
      }
    });
  }

  private completeStep() {
    let stepX;
    Object.keys(this.statusSteps).forEach((i, index) => {
      if (this.stepComplete === this.statusSteps[i]) {
        this.statusSteps[i].isComplete = true;
        this.setCompletedStep(i);
        stepX = i;
      }
    });
    this.setNextStep(stepX);
  }

  private loadCompletedStep() {
    if (this.salesCoaching.BR_Stage_1_Completed__c === 'done') {
      this.statusSteps['step1'].isComplete = true;
    }
    if (this.salesCoaching.BR_Stage_2_Completed__c === 'done') {
      this.statusSteps['step2'].isComplete = true;
    }
    if (this.salesCoaching.BR_Stage_3_Completed__c === 'done') {
      this.statusSteps['step3'].isComplete = true;
    }
    if (this.salesCoaching.BR_Stage_4_Completed__c === 'done') {
      this.statusSteps['step4'].isComplete = true;
    }
    if (this.salesCoaching.BR_Stage_5_Completed__c === 'done') {
      this.statusSteps['step5'].isComplete = true;
    }
    if (this.salesCoaching.BR_Stage_6_Completed__c === 'done') {
      this.statusSteps['step6'].isComplete = true;
    }
  }

  private setCompletedStep(step) {
    if (step === 'step1') {
      this.salesCoaching.BR_Stage_1_Completed__c = 'done';
    }
    if (step === 'step2') {
      this.salesCoaching.BR_Stage_2_Completed__c = 'done';
    }
    if (step === 'step3') {
      this.salesCoaching.BR_Stage_3_Completed__c = 'done';
    }
    if (step === 'step4') {
      this.salesCoaching.BR_Stage_4_Completed__c = 'done';
    }
    if (step === 'step5') {
      this.salesCoaching.BR_Stage_5_Completed__c = 'done';
    }
    if (step === 'step6') {
      this.salesCoaching.BR_Stage_6_Completed__c = 'done';
    }
  }

  private checkAllStepsCompleted() {
    if (this.isOnline) {
      return (
        this.salesCoaching.BR_Stage_1_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_2_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_3_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_4_Completed__c === 'done'
      );
    } else {
      return (
        this.salesCoaching.BR_Stage_1_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_2_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_3_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_4_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_5_Completed__c === 'done'
        && this.salesCoaching.BR_Stage_6_Completed__c === 'done'
      );
    }
  }

  private showPreview() {
    this.isPreview = true;
  }

  private setNextStep(step) {
    switch (step) {
      case 'step1':
        this.setStep2();
        break;
      case 'step2':
        {
          if (this.isOnline) {
            this.setStep3();
          } else {
            this.setStep5();
          }
        }
        break;
      case 'step3':
        this.setStep4();
        break;
      case 'step5':
        this.setStep6();
        break;
      case 'step6':
        this.setStep3();
        break;
    }
  }

  private resetSteps() {
    Object.keys(this.statusSteps).forEach((i) => {
      this.statusSteps[i].isComplete = false;
    });
  }

  public ionViewDidEnter() {
    this.cacheVisit = JSON.stringify(this.salesCoaching);
  }

  public ionViewWillLeave() {
    if (!this.isReadOnly && this.cacheVisit !== JSON.stringify(this.salesCoaching)) {
      this.saveVisit();
    }
    document.body.classList.remove('hiddenTabs');
  }

  private saveVisit(): Promise<any> {
    return new Promise((resolve, reject) => {

      if (this.salesCoaching.BR_Status__c === SalesCoachingStatus.open) {
        this.salesCoaching.BR_Status__c = SalesCoachingStatus.inProgress;
      }

      if (this.isOnline) {
        if (this.salesCoaching.BR_Stage_4_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Pense no futuro e Planeje';
        } else if (this.salesCoaching.BR_Stage_3_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Pense no futuro e Planeje';
        } else if (this.salesCoaching.BR_Stage_2_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Compartilhe';
        } else if (this.salesCoaching.BR_Stage_1_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Execute';
        }
      } else {
        if (this.salesCoaching.BR_Stage_4_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Pense no futuro e Planeje';
        } else if (this.salesCoaching.BR_Stage_3_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Pense no futuro e Planeje';
        } else if (this.salesCoaching.BR_Stage_6_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Compartilhe';
        } else if (this.salesCoaching.BR_Stage_5_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Pós-visita';
        } else if (this.salesCoaching.BR_Stage_2_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Observe';
        } else if (this.salesCoaching.BR_Stage_1_Completed__c === 'done') {
          this.salesCoaching.BR_Stage__c = 'Execute';
        }
      }

      LoadingManager.getInstance().show().then(() => {
        this.salesCoachingBO.saveSalesCoaching(this.event, this.salesCoaching).then((savedVisit: BR_Sales_Coaching__cModel) => {
          this.salesCoaching = savedVisit;

          this.cacheVisit = JSON.stringify(this.salesCoaching);
          this.events.publish(NotificationKeys.statusEventSave, this.salesCoaching);

          setTimeout(() => {
            LoadingManager.getInstance().hide();
            ToastHelper.showSuccessMessage(this.toastCtrl);
            console.log('---| Success:saveVisit: ', this.salesCoaching);
            resolve();
          }, 500);

        }, (err) => {
          console.log('-*-| visitDAO.saveVisit:error: ', err);
          LoadingManager.getInstance().hide();
        });
      });
    });
  }

  public checkEmptyField(fields) {
    const emptyFields = [];
    fields.forEach((field) => {
      if (!this.salesCoaching[field.obj]) {
        emptyFields.push(field.name);
      }
    });
    return emptyFields;
  }

  public completeVisit() {
    if (this.isSaveDisabled()) {
      const fields = this.isFields.join(',');
      const message = '<b>Favor preencher os campos obrigatórios para salvar a visita:</b><br><br>' + fields.replace(/,/g, '<br>');
      AlertHelper.showRequiredFieldsSalesAlert(this.alertCtrl, message);
    } else {
      AlertHelper.showConfirmDialogForSalesCompletion(this.alertCtrl, () => {
        this.isReadOnly = true;
        this.salesCoaching.BR_Status__c = SalesCoachingStatus.completed;
        this.saveVisit().then(() => {
          NavigationHelper.goBackToOrigin(this.navCtrl);
        });
      }, () => {
        console.log('cancel');
      });
    }
  }

  public verifyRecordType(developerName) {
    if (developerName) {
      this.isOnline =
        developerName.includes(SalesCoachingRecordTypes.needs)
          || developerName.includes(SalesCoachingRecordTypes.planArticulate)
          || developerName.includes(SalesCoachingRecordTypes.negotiateCloseOnline)
          || developerName.includes(SalesCoachingRecordTypes.assistPurchaseOnline)
          || developerName.includes(SalesCoachingRecordTypes.trackDeliveryOTC)
          || developerName.includes(SalesCoachingRecordTypes.demandGenerationOnline)
          || developerName.includes(SalesCoachingRecordTypes.endSeasonOnline)
          ? true : false;
    }
  }

  public scrollToTop() {
    setTimeout(() => {
      this.content.scrollToTop();
    }, 300);
  }

  public setStep1() {
    this.updateStatus('step1');
    this.titleStep = 'Prepare-se';
    if (this.salesCoaching.BR_Status__c !== SalesCoachingStatus.completed) {
      this.statusSteps['step1'].isComplete = false;
      this.statusSteps['step2'].isComplete = false;
      this.statusSteps['step3'].isComplete = false;
      this.statusSteps['step4'].isComplete = false;
      this.statusSteps['step5'].isComplete = false;
      this.statusSteps['step6'].isComplete = false;
    }
    this.loadCompletedStep();
    this.salesCoaching.BR_Stage__c = 'Prepare-se';
    this.isPreview = false;
    this.scrollToTop();
    this.keyboard.close();
    setTimeout(() => {
      this.keyboard.close();
    }, 1000);    
  }

  public setStep2() {
    this.updateStatus('step2');
    this.titleStep = 'Execute';
    if (this.salesCoaching.BR_Status__c !== SalesCoachingStatus.completed) {
      this.statusSteps['step2'].isComplete = false;
      this.statusSteps['step3'].isComplete = false;
      this.statusSteps['step4'].isComplete = false;
      this.statusSteps['step5'].isComplete = false;
      this.statusSteps['step6'].isComplete = false;
    }
    this.loadCompletedStep();
    this.salesCoaching.BR_Stage__c = 'Execute';
    this.isPreview = false;
    this.scrollToTop();
    this.keyboard.close();
    setTimeout(() => {
      this.keyboard.close();
    }, 1000);    
  }

  public setStep3() {
    this.updateStatus('step3');
    this.titleStep = 'Compartilhe';
    if (this.salesCoaching.BR_Status__c !== SalesCoachingStatus.completed) {
      this.statusSteps['step3'].isComplete = false;
      this.statusSteps['step4'].isComplete = false;
    }
    this.loadCompletedStep();
    this.salesCoaching.BR_Stage__c = 'Compartilhe';
    this.isPreview = false;
    this.scrollToTop();
    this.keyboard.close();
    setTimeout(() => {
      this.keyboard.close();
    }, 1000);    
  }

  public setStep4() {
    this.updateStatus('step4');
    this.titleStep = 'Pense no futuro e planeje';
    if (this.salesCoaching.BR_Status__c !== SalesCoachingStatus.completed) {
      this.statusSteps['step4'].isComplete = false;
    }
    this.loadCompletedStep();
    this.salesCoaching.BR_Stage__c = 'Pense no futuro e Planeje';
    this.isPreview = false;
    this.scrollToTop();
    this.keyboard.close();
    setTimeout(() => {
      this.keyboard.close();
    }, 1000);    
  }

  private setStep5() {
    this.updateStatus('step5');
    this.titleStep = 'Observe';
    if (this.salesCoaching.BR_Status__c !== SalesCoachingStatus.completed) {
      this.statusSteps['step3'].isComplete = false;
      this.statusSteps['step4'].isComplete = false;
      this.statusSteps['step5'].isComplete = false;
      this.statusSteps['step6'].isComplete = false;
    }
    this.loadCompletedStep();
    this.salesCoaching.BR_Stage__c = 'Observe';
    this.isPreview = false;
    this.scrollToTop();
    this.keyboard.close();
    setTimeout(() => {
      this.keyboard.close();
    }, 1000);    
  }

  private setStep6() {
    this.updateStatus('step6');
    this.titleStep = 'Pós-Visita';
    if (this.salesCoaching.BR_Status__c !== SalesCoachingStatus.completed) {
      this.statusSteps['step3'].isComplete = false;
      this.statusSteps['step4'].isComplete = false;
      this.statusSteps['step6'].isComplete = false;
    }
    this.loadCompletedStep();
    this.salesCoaching.BR_Stage__c = 'Pós-visita';
    this.isPreview = false;
    this.scrollToTop();
    this.keyboard.close();
    setTimeout(() => {
      this.keyboard.close();
    }, 1000);    
  }

  private isSaveDisabled() {
    if (this.isOnline) {
      this.isFields = [];
      this.isFields.push(this.checkEmptyField(this.onlineObject));
      return !(
        this.salesCoaching.BR_Notes_Prepare_2__c
        && this.salesCoaching.BR_Notes_Execute_1__c
        && this.salesCoaching.BR_Strong_Point_1__c
        && this.salesCoaching.BR_Strong_Point_1_Description__c
        && this.salesCoaching.BR_Opportunity_1__c
        && this.salesCoaching.BR_Opportunity_1_Description__c
        && this.salesCoaching.BR_Opportunity_1_Time__c
      );
    } else {
      this.isFields = [];
      this.isFields.push(this.checkEmptyField(this.onlineObject));
      this.isFields.push(this.checkEmptyField(this.offlineObject));
      return !(
        this.salesCoaching.BR_Notes_Prepare_2__c
        && this.salesCoaching.BR_Notes_Execute_1__c
        && this.salesCoaching.BR_Strong_Point_1__c
        && this.salesCoaching.BR_Strong_Point_1_Description__c
        && this.salesCoaching.BR_Opportunity_1__c
        && this.salesCoaching.BR_Opportunity_1_Description__c
        && this.salesCoaching.BR_Opportunity_1_Time__c
        && this.salesCoaching.BR_Notes_Observe_1__c
        && this.salesCoaching.BR_Notes_Observe_2__c
        && this.salesCoaching.BR_Notes_Observe_3__c
        && this.salesCoaching.BR_Notes_Observe_4__c
        && this.salesCoaching.BR_Notes_Observe_5__c
      );
    }
  }
}
