import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AppPreferences } from '@ionic-native/app-preferences';

@Component({
  selector: 'page-sync',
  templateUrl: 'sync.html',
})
export class SyncPage {

  public value: number;

  public itemClients: string;
  public itemProperties: string;
  public itemParcers: string;
  public itemContacts: string;

  public entidade = '';
  public progressStyle: any;
  public syncCurrentIconName: string;
  public erroSync: boolean;
  public showSpinner: any;

  constructor(public navCtrl: NavController, public appPreferences: AppPreferences, public navParams: NavParams, public service: ServiceProvider, private zone: NgZone, private splashScreen: SplashScreen ) {
    this.erroSync = false;
    this.showSpinner = true;
    this.syncProcess();
  }

  public hideSplashScreen() {
    if (SplashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 100);
    }
  }

  public handleProgress(progress: number) {
    console.log('CURRENT PROGRESS ----- ', progress);
    this.zone.run(() => {
      if (progress !== 0) {
        this.showSpinner = false;
      }
      this.value = progress;
    });
  }

  public setErrorPreferencesTags(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.appPreferences.store('syncOk', 'error').then((value) => {
        resolve(value);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  public setSuccessPreferencesTags(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.appPreferences.store('syncOk', 'success').then((value) => {
        resolve();
      })
        .catch((err) => {
          reject(err);
        });
    });
  }

  public handleZone(syncCurrentIconName: string, entidade: string) {
    this.zone.run(() => {
      this.syncCurrentIconName = syncCurrentIconName;
      this.entidade = entidade;
    });
  }

  public resync() {
    this.itemParcers = '';
    this.itemProperties = '';
    this.itemContacts = '';
    this.erroSync = false;
    this.syncProcess();
  }

  public syncProcess() {
    this.startSyncDown();
  }

  public startSyncDown() {

    console.log('---| Down autenticateService');
    this.service.autenticateService.getCurrentUser().then((success) => {
      this.service.userService.targetSync.query = this.service.userService.targetSync.query + ' where Id = \'' + success.userId + '\'';
      return this.service.getUserService().syncDown(this.handleProgress, this);
    }).then(() => {
      this.zone.run(() => {
        this.hideSplashScreen();
      });
      this.itemClients = 'current';
      this.entidade = 'Clientes';
      this.syncCurrentIconName = 'account';
      console.log('---| Down getClientsService');
      return this.service.getClientsService().syncDown(this.handleProgress, this);
    }).then(() => {
      this.itemClients = 'complete';
      this.itemContacts = 'current';
      this.entidade = 'Contatos';
      this.syncCurrentIconName = 'contact';
      console.log('---| Down getContactsService');
      return this.service.getContactService().syncDown(this.handleProgress, this);
    }).then(() => {
      this.itemContacts = 'complete';
      console.log('---| SyncDown Finished');
      this.startSyncUp();
    }).catch((error) => {
      console.log(error);
      this.erroSync = true;
      this.setErrorPreferencesTags().catch((err) => {
        console.log('Preferences Properties Err', err);
      });
    });

  }

  public startSyncUp() {

    this.value = 100;
    this.itemProperties = 'complete';

    console.log('---| Up getClientsService');
    this.service.getClientsService().syncUp(true, this.handleProgress, this).then(() => {
      console.log('---| Up getContactsService');
      return this.service.getContactService().syncUp(true, this.handleProgress, this);
    }).then(() => {
      console.log('---| setSuccessPreferencesTags');
      return this.setSuccessPreferencesTags();
    }).then(() => {
      console.log('---| Synchronization Finished');
      setTimeout(() => { this.navCtrl.popToRoot(); }, 1000);
    }).catch((err) => {
      console.log(err);
      this.erroSync = true;
    });

  }

  public getUser(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getUserService().queryAllFromSoup('Id', 'descending', 10, (response) => {
        resolve(response);
      }, (err) => {
        console.log(err);
      });
    });
  }

  public queryFromSoup(service: any, object: string, orderBy: string): Promise<any> {
    return new Promise((resolve, reject) => {
      service.queryExactFromSoup('APIObjectName__c', object, 25, 'descending', orderBy, (response) => {
        resolve(response);
      }, (err) => {
        console.log(err);
      });
    });
  }
}
