import { AgendaPage } from '../agenda/agenda';
import { App } from 'ionic-angular';
import { AppPreferences } from '@ionic-native/app-preferences';
import { ClientsListPage } from './../clients-list/clients-list';
import { Component } from '@angular/core';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { Events, Platform } from 'ionic-angular';
import { HomePage } from './../home/home';
import { Keyboard } from '@ionic-native/keyboard';
import { NavController } from 'ionic-angular';
import { NotificationBO } from './../../shared/BO/NotificationBO';
import { NotificationKeys } from './../../shared/Constants/Keys';
import { NotificationPage } from './../notification/notification';
import { ServiceProvider } from './../../providers/service-salesforce';
import { SettingsPage } from './../settings/settings';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  public numBadges: any;
  public notificationBO: NotificationBO;
  public home = HomePage;
  public agenda = AgendaPage;
  public clients = ClientsListPage;
  public notifications = NotificationPage;
  public settings = SettingsPage;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public platform: Platform,
    public appPreferences: AppPreferences,
    public serviceProvider: ServiceProvider,
    public keyboard: Keyboard,
    public appCtrl: App,
    public storage: Storage,
  ) {
    platform.ready().then(() => {
      this.notificationBO = new NotificationBO(this.serviceProvider);
    });
  }

  public ngOnInit(): void {
    console.log('------| Events: tabs : !!! unsubscribe !!!');

    this.events.unsubscribe(NotificationKeys.afterEndSync);
    this.events.unsubscribe(NotificationKeys.totalNotifications);
  }

  public ngAfterContentInit() {
    console.log('------| Events: tabs : ### subscribe ###');

    this.keyboard.onKeyboardShow().subscribe(() => {
      document.body.classList.add('keyboard-is-open');
    });

    this.keyboard.onKeyboardHide().subscribe(() => {
      document.body.classList.remove('keyboard-is-open');
    });

    this.events.subscribe(NotificationKeys.afterEndSync, () => {
      this.events.publish(NotificationKeys.totalNotifications);
      this.loadTabs();
    });

    this.events.subscribe(NotificationKeys.totalNotifications, (total?) => {
      if (total) {
        this.numBadges = total;
      } else {
        this.appPreferences.fetch('user_id').then((user_id) => {
          this.notificationBO.totalNotifications(user_id).then((data: any) => {
            this.appPreferences.store('totalNotifications', data.toString());
            this.numBadges = Number(data);
            console.log('------| numBadges value: ', this.numBadges);
          });
        });
      }
    });
  }

  public loadTabs() {
    this.storage.get('user_profile').then((user_profile: any) => {
      const elementTabs = Array.from(document.getElementById('tabs').getElementsByTagName('a'));
      const perfil = DataHelper.checkBooleanValuesObj(user_profile);

      if (perfil) {
        if (user_profile.isGR) {
          this.hideTabs(elementTabs, 'clientes');
        } else {
          this.hideTabs(elementTabs, 'showAll');
        }
      } else {
        this.hideTabs(elementTabs);
      }
    });
  }

  private hideTabs(elementTabs: HTMLAnchorElement[], filter?: string) {
    Array.from(elementTabs).forEach((element) => {
      if (filter) {
        if (element.text.toLowerCase() === filter) {
          element.classList.add('hidden');
        } else {
          element.classList.remove('hidden');
        }
      } else {
        if (element.text.toLowerCase() !== 'home' && element.text.toLowerCase() !== 'ajustes') {
          element.classList.add('hidden');
        }
      }
    });
  }

}
