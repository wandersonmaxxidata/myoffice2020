import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { IPicklistItem } from './../../interfaces/IPicklistItem';
import { BrandModel } from './../../app/model/BrandModel';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { AlertController, Events, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
import { AppPreferences } from '@ionic-native/app-preferences';
import { CallNumber } from '@ionic-native/call-number';
import { EmailComposer } from '@ionic-native/email-composer';
import { ServiceProvider } from '../../providers/service-salesforce';
import { ContactModel } from './../../app/model/ContactModel';
import { ClientsModel } from '../../app/model/ClientsModel';
import { RecordTypeModel } from '../../app/model/RecordTypeModel';
import { NotificationKeys } from '../../shared/Constants/Keys';
import { ToastHelper } from '../../shared/Helpers/ToastHelper';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { SaveHelper } from '../../shared/Helpers/SaveHelper';
import { WindowStorageHelper } from '../../shared/Helpers/WindowStorageHelper';
import { HeaderComponent } from '../../components/header/header';
import { TabHelper } from '../../shared/Helpers/TabHelper';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';

/**
 * Página de detalhe da aplicação.
 */
@Component({
  selector: 'contacts-detail',
  templateUrl: 'contacts-detail.html',
  providers: [CallNumber, EmailComposer],
})
export class ContactsDetailPage {
  /**
   * Objeto com uma determinada conta, trazida via parametro
   */
  public error: any;
  public contact: ContactModel;
  public contacts = [];
  public isReadOnly: boolean;
  public isCreateMode: boolean;
  public isInvalid: boolean;
  public isEditMode: boolean;
  public isDeleteMode = false;
  public user: any[] = [];
  public context: ContactsDetailPage = this;
  public sortBy = { field: 'Name', label: 'Name' };
  public callback;

  @ViewChild(forwardRef(() => HeaderComponent))
  public header: HeaderComponent;

  private pageTitle = 'Adicionar Contato';

  private iProfissao__c: IPicklistItem[] = [];

  constructor(
    public navCtrl: NavController,
    private callNumber: CallNumber,
    private emailComposer: EmailComposer,
    public navParams: NavParams,
    public service: ServiceProvider,
    private modal: ModalController,
    public alertCtrl: AlertController,
    public events: Events,
    public toastCtrl: ToastController,
    public appPreferences: AppPreferences,
  ) {
    const client: ClientsModel = this.navParams.data.client;
    const contact = this.navParams.data.contact;
    this.callback = this.navParams.data.callback ? this.navParams.data.callback : undefined;

    this.contacts = (client && client.Contact) ? client.Contact : [];
    this.iProfissao__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('Profissao__c');

    if (!contact) {
      this.isCreateMode = true;
      this.isEditMode = false;
      this.isReadOnly = false;

      this.contact = new ContactModel(null);
      this.contact.Account = new ClientsModel(client);
      this.contact.AccountId = this.contact.Account.Id;
      this.mountContact(this.contact);
      TabHelper.disableNonSelectedTabs(this.navCtrl);
    } else {
      this.isCreateMode = false;
      this.isEditMode = false;
      this.isReadOnly = true;

      this.contact = new ContactModel(contact);
      this.contact.Account = new ClientsModel(this.navParams.data.client);
      this.contact.AccountId = this.contact.Account.Id;
      this.mountContact(this.contact);
      console.log(this.contact.Phone, this.contact.MobilePhone);
    }
  }

  public ngOnInit() {
    if (this.header) {
      this.header.updatePageTitle(this.pageTitle);
    }
  }

  public ionViewWillLeave() {
    LoadingManager.getInstance().hide();
  }

  public err(err) {
    console.log(err);
  }

  public setBrand() {
    return new Promise((resolve, reject) => {
      this.appPreferences.fetch('user_brand').then((value) => {
        this.service.getBrandService().queryExactFromSoup('Name', value, 1,
          'ascending', null, (data) => {
            if (data.currentPageOrderedEntries && data.currentPageOrderedEntries[0]) {
              const brand = data.currentPageOrderedEntries[0];
              resolve(brand);
              LoadingManager.getInstance().hide();
            } else {
              resolve();
              LoadingManager.getInstance().hide();
            }
          }, (err) => {
            reject(err);
            console.log(err);
            LoadingManager.getInstance().hide();
          });
      }).catch((err) => {
        reject(err);
        console.log(err, 'Error appPreferences');
      });
    });
  }

  public setRecordType() {
    return new Promise((resolve, reject) => {
      if (this.contact.RecordTypeId == null) {
        this.service.getRecordTypeService().queryExactFromSoup('DeveloperName', 'BR_Contact', 1,
          'ascending', null, (data) => {
            if (data.currentPageOrderedEntries && data.currentPageOrderedEntries[0]) {
              const type = data.currentPageOrderedEntries[0];
              resolve(type);
              LoadingManager.getInstance().hide();
            } else {
              resolve();
              LoadingManager.getInstance().hide();
            }
          }, (err) => {
            console.log(err);
            reject(err);
            LoadingManager.getInstance().hide();
          });
      } else {
        resolve();
        LoadingManager.getInstance().hide();
      }
    });
  }

  public async mountContact(contact: ContactModel) {
    await this.setBrand().then((brand: BrandModel) => {
      if (brand) {
        contact.BR_Brand__c = brand.Id;
        contact.Marca.Id = brand.Id;
        contact.Marca.Name = brand.Name;
        contact.BR_Marca__c = brand.Name;
      }
    }).catch((error) => {
      console.log(error);
    });
    await this.setRecordType().then((type: RecordTypeModel) => {
      if (type) {
        contact.RecordTypeId = type.Id;
        contact.RecordType.Id = type.Id;
        contact.RecordType.Name = type.Name;
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  public phoneNumber(phone) {
    this.callNumber.callNumber(phone, true)
      .then(() => console.log('Launched dialer'))
      .catch((err) => console.log('Error launching dialer', err));
  }

  public mailTo(userEmail) {
    this.emailComposer.open({ app: 'mailto', to: userEmail })
      .then(() => console.log('Launched email'))
      .catch((err) => console.log('Error launching email', err));
  }

  public editContact() {
    this.isCreateMode = false;
    this.isEditMode = true;
    this.isReadOnly = false;
    LoadingManager.getInstance().hide();
    TabHelper.disableNonSelectedTabs(this.navCtrl);
  }

  public async getBrand(response) {
    for (const contactsBrand of this.contacts) {
      if (this.contact.BR_Brand__c === contactsBrand.BR_Brand__c) {
        this.isInvalid = true;
      }
    }
  }

  public completedContact() {
    AlertHelper.showDialogForSave(this.alertCtrl, () => {
      this.saveContact().then((contact) => {
        if (this.callback) {
          this.callback(contact).then(() => {
            this.navCtrl.pop();
          });
        }
      });
    }, () => {
      console.log('cancel');
    });
  }

  public canceledContact() {
    AlertHelper.showDialogForCancel(this.alertCtrl, () => {
      this.cancelContact();
    }, () => {
      console.log('cancel');
    });
  }

  public saveContact(): Promise<any> {
    return new Promise((resolve, reject) => {

      LoadingManager.getInstance().show().then(() => {
        const entries = [];

        this.contact.Name = `${this.contact.FirstName} ${this.contact.LastName}`;
        if (this.contact.Marca.Id) {
          this.contact.BR_Brand__c = this.contact.Marca.Id;
          this.contact.BR_Marca__c = this.contact.Marca.Name;
        }

        SaveHelper.update_Local_(this.contact);

        entries.push(this.contact);
        this.service.getContactService().upsertSoupEntries(entries,
          (response) => {
            LoadingManager.getInstance().hide();
            this.contact = new ContactModel(response[0]);
            console.log('-----------| success save this.contact: ', this.contact);
            console.log('-----------| success save contact: ', response);
            this.isReadOnly = true;
            this.isEditMode = false;
            this.events.publish(NotificationKeys.contacsUpdate);
            ToastHelper.showSuccessMessage(this.toastCtrl);
            TabHelper.enableAll(this.navCtrl);
            setTimeout(() => {
              resolve(this.contact);
            }, 500);
          },
          (err) => {
            TabHelper.enableAll(this.navCtrl);
            console.log('-----------| erro save contact: ', err);
          });
      });
    });
  }

  public cancelContact() {
    if (this.isEditMode) {
      this.isReadOnly = true;
      this.isEditMode = false;
      this.contact = new ContactModel(this.navParams.data.contact);
      TabHelper.enableAll(this.navCtrl);
    } else {
      this.contact = new ContactModel({});
      this.navCtrl.pop();
    }
  }


  public checkEmailError(): boolean {
    if (this.contact.Email === '') { this.contact.Email = undefined; }
    if (DataHelper.emailMatch(this.contact.Email)|| this.contact.Email === undefined) {
      return true;
    }
    return false;
  }


  public checkEmail(): boolean {
    return DataHelper.emailMatch(this.contact.Email) && this.contact.Email.length > 0;
  }

  public checkFirstName() {
    return this.contact.FirstName && DataHelper.onlyLetters(this.contact.FirstName) && this.contact.FirstName.length >= 3;
  }

  public checkLastName(): boolean {
    return this.contact.LastName && DataHelper.onlyLetters(this.contact.LastName) && this.contact.LastName.length >= 3;
  }

  public checkPhone(): boolean {
    if (this.contact.Phone === '') { this.contact.Phone = undefined; }

    if (DataHelper.telMatch(this.contact.Phone) || this.contact.Phone === undefined) {
      return true;
    }

    return false;
  }

  public checkMobilePhone(): boolean {
    if (this.contact.MobilePhone === '') { this.contact.MobilePhone = undefined; }

    if (DataHelper.telMatch(this.contact.MobilePhone) || this.contact.MobilePhone === undefined) {
      return true;
    }

    return false;
  }



  public isSaveDisabled() {
    if (this.contact.Email != ''){
      sessionStorage.setItem('currentAddEmail',this.contact.Email)
    }
    
    return !(
      this.checkFirstName() &&
      this.checkLastName() &&
      this.checkEmail() &&
      this.checkPhone() &&
      this.checkMobilePhone()
    );
  }
}
