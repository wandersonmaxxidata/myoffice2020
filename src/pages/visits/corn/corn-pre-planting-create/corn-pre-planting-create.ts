import { Hibrido_de_Visita__cModel } from './../../../../app/model/Hibrido_de_Visita__cModel';
import { EventBO } from './../../../../shared/BO/EventBO';
import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { PrePlantingAddHybridPage } from './../../modals/pre-planting-add-hybrid-modal/pre-planting-add-hybrid-modal';
import { VisitHybridRecordTypes } from '../../../../shared/Constants/Types';
import { ReportPage } from './../../report/report';
import { NotificationKeys } from './../../../../shared/Constants/Keys';
import { ToastController } from 'ionic-angular';
import { ServiceProvider } from './../../../../providers/service-salesforce';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { AlertController, Events, ModalController, ModalOptions } from 'ionic-angular';
import { LoadingManager } from './../../../../shared/Managers/LoadingManager';
import { AttachmentComponent } from '../../../../components/attachment/home-attachment';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { HeaderComponent } from './../../../../components/header/header';
import { IGeolocation } from './../../../../components/geolocation/IGeolocation';
import { VisitHybridBO } from './../../../../shared/BO/VisitHybridBO';
import { VisitBO, VisitStatus } from './../../../../shared/BO/VisitBO';
import { Visita__cModel } from './../../../../app/model/Visita__cModel';
import { EventModel } from './../../../../app/model/EventModel';
import { GeolocationComponent } from './../../../../components/geolocation/geolocation';
import { AlertHelper } from '../../../../shared/Helpers/AlertHelper';
import { NavigationHelper } from '../../../../shared/Helpers/NavigationHelper';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import { ToastHelper } from '../../../../shared/Helpers/ToastHelper';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';
import { MustEditRegisterHelper } from '../../../../shared/Helpers/MustEditRegisterHelper';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-corn-pre-planting-create',
  templateUrl: 'corn-pre-planting-create.html',
})
export class CornPrePlantingCreatePage {
  public event: EventModel;
  public eventBO: EventBO;
  public visit: Visita__cModel = new Visita__cModel(null);
  public cacheVisit;
  public cacheAttachment;

  //MAXXIDATA
  public editStatus: boolean = false;
  public editTxt: string;
  //FIM MAXXIDATA

  public visitBO: VisitBO;
  public visitHybridBO: VisitHybridBO;

  public title: string;
  public isReadOnly: boolean;

  public geolocation: IGeolocation;

  public showHybrid = true;
  public showProductUsed = true;
  public showPlague = true;
  public userProfile: any;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  @ViewChild(AttachmentComponent)
  private pageHomeAttachment: AttachmentComponent;

  @ViewChild(GeolocationComponent)
  private geolocationComponent: GeolocationComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public events: Events,
    public storage: Storage,
  ) {
    //MAXXIDATA
    if (this.navParams.data.editVisitError) {
      this.event = this.navParams.data.event;
    } else {
      this.event = this.navParams.data;
    }
    //FIM MAXXIDATA
    this.eventBO = new EventBO(this.service);
    this.visitBO = new VisitBO(this.service);
    this.visitHybridBO = new VisitHybridBO(this.service);

    LoadingManager.getInstance()
      .show()
      .then(() => {
        this.visitBO.loadVisit(this.event).then((visit: Visita__cModel) => {
          this.storage.get('user_profile').then((user_profile: any) => {
            this.userProfile = user_profile;
          });
          LoadingManager.getInstance().hide();
          this.handleInitialState(visit);
        });
      });
  }

  private ionViewDidEnter() {
    this.cacheVisit = JSON.stringify(this.visit);
    this.cacheAttachment = JSON.stringify(this.pageHomeAttachment.attachments);
  }

  private ionViewCanLeave() {
    //MAXXIDATA
    if (this.navParams.data.editVisitError) {

    } else {

    return new Promise((resolve, reject) => {
      if (
        (!this.isReadOnly &&
          !this.isSaveDisabled() &&
          this.cacheVisit !== JSON.stringify(this.visit)) ||
        this.cacheAttachment !==
          JSON.stringify(this.pageHomeAttachment.attachments)
      ) {
        this.saveVisit().then(() => {
          resolve();
        });
      } else if (
        this.isSaveDisabled() &&
        this.visit.Status__c !== VisitStatus.completed
      ) {
        AlertHelper.showDialogForVisitAutoSave(
          this.alertCtrl,
          () => {
            resolve();
          },
          () => {
            reject();
            console.log('cancel');
          },
        );
      } else {
        resolve();
      }
    });
    
  }
  //FIM MAXXIDATA
  }

  private updatePageTitle() {
    if (this.header) {
      this.header.updatePageTitle(this.title);
    }
  }

  private setParentIdToAttachmentView() {
    this.pageHomeAttachment.setParentId(this.visit.getId());
  }

  private handleInitialState(visit: Visita__cModel) {
    this.visit = visit;
    this.visit.Name = 'Pré-Plantio';

    if (this.event.BR_Division__c) {
      this.visit.BR_Division__c = this.event.BR_Division__c;
    }

    if (
      this.visit.Status__c === VisitStatus.completed ||
      this.visit.Status__c === VisitStatus.canceled
    ) {
       //MAXXIDATA
       console.log('VISITA STATUS EDITAR')
       console.log(this.visit)
       console.log(this.visit.Id.indexOf("Local_") )
       if (this.visit.Id.indexOf("local_") && this.visit.GerDemandaId__c == null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === false) {
        console.log('VISITA STATUS EDITAR')
         this.isReadOnly = true;
       } else if(this.visit.GerDemandaId__c != null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === true ) {
          if (this.visit.Status__c == VisitStatus.completed) {
            this.editStatus = true;
            this.editTxt = 'Visita com possibilidade de edição.';
          }
          this.isReadOnly = false;
        }else if(this.visit.GerDemandaId__c != null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === false ) {
          this.isReadOnly = true;
       }else {
         if (this.visit.Status__c == VisitStatus.completed) {
           this.editStatus = true;
           this.editTxt = 'Visita com possibilidade de edição.';
         }
         this.isReadOnly = false;
       }
       //FIM MAXXIDATA
      this.title = 'Pré-Plantio';
    } else {
      this.isReadOnly = false;
      this.title = 'Novo Pré-Plantio';
    }

    this.updatePageTitle();
    this.setParentIdToAttachmentView();
    this.setLocation();

    if (MustEditRegisterHelper.isObjectTreeWithError(this.visit)) {
      this.isReadOnly = false;
    }
  }

  private setLocation() {
    this.geolocation = {
      altitude: this.visit.BR_Altitude__c,
      latitude: this.visit.Latitude__c,
      longitude: this.visit.Longitude__c,
    };

    if (this.geolocationComponent) {
      this.geolocationComponent.setLocation(this.geolocation);
    }
  }

  private updateGeolocation(location) {
    if (location) {
      this.visit.BR_Altitude__c = location.altitude;
      this.visit.Latitude__c = location.latitude;
      this.visit.Longitude__c = location.longitude;
    }
  }

  public cancelVisit() {
    AlertHelper.showConfirmDialogForVisitCancellation(
      this.alertCtrl,
      () => {
        this.isReadOnly = true;
        this.visit.Status__c = VisitStatus.canceled;
        this.saveVisit().then(() => {
          NavigationHelper.goBackToOrigin(this.navCtrl);
        });
      },
      () => {
        console.log('cancel');
      },
    );
  }

  public completeVisit() {
    AlertHelper.showConfirmDialogForVisitCompletion(
      this.alertCtrl,
      () => {
        if (this.willOpenReport()) {
          this.openReport();
        } else {
          this.isReadOnly = true;
          this.visit.Status__c = VisitStatus.completed;
          this.visit.Data_de_Execucao__c = DateHelper.getCurrentDateString();
          this.saveVisit().then(() => {
            NavigationHelper.goBackToOrigin(this.navCtrl);
          });
        }
      },
      () => {
        console.log('cancel');
      },
    );
  }

  private saveVisit(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.visit.EventId__c = this.event.getId();
      let statusChange = false;
      let statusProgress = false;

      if (this.visit.Status__c === VisitStatus.open) {
        this.visit.Status__c = VisitStatus.inProgress;
        statusProgress = true;
      }

      if (statusProgress || this.isReadOnly) {
        statusChange = true;
      }

      LoadingManager.getInstance()
        .show()
        .then(() => {
          this.visitBO.saveVisit(this.event, this.visit).then(
            (savedVisit: Visita__cModel) => {
              this.visit = savedVisit;
              this.cacheVisit = JSON.stringify(this.visit);
              this.cacheAttachment = JSON.stringify(
                this.pageHomeAttachment.attachments,
              );

              if (statusChange) {
                this.events.publish(
                  NotificationKeys.statusEventSave,
                  this.visit,
                );
              }

              setTimeout(() => {
                LoadingManager.getInstance().hide();
                ToastHelper.showSuccessMessage(this.toastCtrl);
                console.log('---| Success:saveVisit: ', this.visit);
                //MAXXIDATA
                if (this.navParams.data.editVisitError) {
                  this.service.visita__cService.deletLogErrorEntities(this.navParams.data.idDataError, this.navParams.data.idData, this.navParams.data.shildId);
                }
                //FIM MAXXIDATA
                resolve();
              }, 500);
            },
            err => {
              console.log('-*-| visitDAO.saveVisit:error: ', err);
              LoadingManager.getInstance().hide();
            },
          );
        });
    });
  }

  private willOpenReport() {
    return this.eventBO.willOpenReport(this.event, this.userProfile);
  }

  private openReport() {
    LoadingManager.getInstance().hide();

    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(
      ReportPage,
      { event: this.event, visit: this.visit },
      options,
    );
    modal.present();

    modal.onDidDismiss((finish: boolean) => {
      if (finish) {
        this.isReadOnly = true;
        this.visit.Status__c = VisitStatus.completed;
        this.visit.BR_GenerateReport__c = true;
        this.visit.Data_de_Execucao__c = DateHelper.getCurrentDateString();
        this.saveVisit().then(() => {
          NavigationHelper.goBackToOrigin(this.navCtrl);
        });
      }
    });
  }

  // ************************************************************ //
  // ******************Begin pre planting custom***************** //
  // ************************************************************ //
  private isSaveDisabled() {
    return !(
      DataHelper.checkToggleObj(
        this.visit.BR_plagues_remaning_previous_culture__c,
        this.visit.BR_PragueWhichAttackedArea__c,
      ) &&
      DataHelper.checkToggleObj(
        this.visit.BR_handling_held_harvest_postharvest__c,
        this.visit.BR_ProductUsed__c,
      )
    );
  }

  private showProductsUsedSelection() {
    this.visit.BR_ProductUsed__c = undefined;
  }

  private showPestSelection() {
    this.visit.BR_PragueWhichAttackedArea__c = undefined;
  }

  // ************************************************************ //
  // ************************Begin Hybrid************************ //
  // ************************************************************ //
  private openPrePlantingAddHybridModal(index) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    this.visitHybridBO
      .create(VisitHybridRecordTypes.prePlanting)
      .then(createdHybrid => {
        const hyb = {
          hybrid:
            index != null
              ? this.visit.Hibrido_de_Visita__c[index]
              : createdHybrid,
          isReadOnly: this.isReadOnly,
          division: this.event.BR_Division__c,
        };

        const modal = this.modalCtrl.create(
          PrePlantingAddHybridPage,
          hyb,
          options,
        );
        modal.present();

        modal.onWillDismiss((newHybrid: Hibrido_de_Visita__cModel) => {
          if (newHybrid) {
            newHybrid.BR_VisitMobileId__c = this.visit.getId();

            if (index !== undefined && index !== null) {
              this.visit.Hibrido_de_Visita__c[index] = newHybrid;
            } else {
              this.visit.Hibrido_de_Visita__c.push(newHybrid);
            }
            console.log('------| Hybrid added: ', this.visit);
          }
        });
      });
  }

  private removeHybrid(hybridIndex: number) {
    this.visitHybridBO
      .delete(this.visit.Hibrido_de_Visita__c, hybridIndex)
      .then(newHybrids => {
        this.visit.Hibrido_de_Visita__c = newHybrids;
        console.log('------| Hybrid removed: ', this.visit);
      });
  }

  // ************************************************************ //
  // **********************Begin ProductUsed********************* //
  // ************************************************************ //
  private openProductUsedModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(
      MultipicklistModal,
      {
        title: 'Adicionar Herbicidas',
        items: this.visit.BR_ProductUsed__c,
        list: 'BR_ProductUsed__c',
        limit: 3,
      },
      options,
    );
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.visit.BR_ProductUsed__c = data.join(';');
      }
    });
  }

  // ************************************************************ //
  // ************************Begin Plague************************ //
  // ************************************************************ //
  private openPlagueModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(
      MultipicklistModal,
      {
        title: 'Adicionar Praga',
        items: this.visit.BR_PragueWhichAttackedArea__c,
        list: 'BR_PragueWhichAttackedArea__c',
        limit: 5,
      },
      options,
    );
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.visit.BR_PragueWhichAttackedArea__c = data.join(';');
      }
    });
  }
}
