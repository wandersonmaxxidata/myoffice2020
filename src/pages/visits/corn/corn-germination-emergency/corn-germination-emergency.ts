import { EventBO } from './../../../../shared/BO/EventBO';
import { ProductUsedBO } from './../../../../shared/BO/ProductUsedBO';
import { ReportPage } from './../../report/report';
import { BR_ProductUsed__cModel } from './../../../../app/model/BR_ProductUsed__cModel';
import { ProductsUsedModal } from './../../modals/add-products-used/add-products-used';
import { LoadingManager } from './../../../../shared/Managers/LoadingManager';
import { Events } from 'ionic-angular/util/events';
import { NotificationKeys } from './../../../../shared/Constants/Keys';
import { AlertController, ToastController } from 'ionic-angular';
import { BR_WeedHandling__cModel } from './../../../../app/model/BR_WeedHandling__cModel';
import { Praga__cModel } from './../../../../app/model/Praga__cModel';
import { PlagueModal } from './../../modals/add-plague/add-plague';
import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { VisitHybridRecordTypes } from '../../../../shared/Constants/Types';
import { VisitBO, VisitStatus } from './../../../../shared/BO/VisitBO';
import { VisitHybridBO } from './../../../../shared/BO/VisitHybridBO';
import { WeedHandlingBO } from './../../../../shared/BO/WeedHandlingBO';
import { PlagueBO } from './../../../../shared/BO/PlagueBO';
import { ServiceProvider } from './../../../../providers/service-salesforce';
import { Visita__cModel } from './../../../../app/model/Visita__cModel';
import { EventModel } from './../../../../app/model/EventModel';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { ModalController, ModalOptions, NavController, NavParams } from 'ionic-angular';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import { IGeolocation } from './../../../../components/geolocation/IGeolocation';
import { HeaderComponent } from '../../../../components/header/header';
import { AttachmentComponent } from '../../../../components/attachment/home-attachment';
import { GeolocationComponent } from '../../../../components/geolocation/geolocation';
import { AlertHelper } from '../../../../shared/Helpers/AlertHelper';
import { NavigationHelper } from '../../../../shared/Helpers/NavigationHelper';
import { WeedHandlingModal } from '../../modals/add-weed-handling/add-weed-handling';
import { ToastHelper } from '../../../../shared/Helpers/ToastHelper';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';
import { MustEditRegisterHelper } from '../../../../shared/Helpers/MustEditRegisterHelper';
import { Storage } from "@ionic/storage";

@Component({
    selector: 'page-corn-germination-emergency',
    templateUrl: 'corn-germination-emergency.html',
})
export class CornGerminationEmergencyPage {

  public event: EventModel;
  public eventBO: EventBO;
  public visit: Visita__cModel = new Visita__cModel(null);
  public cacheVisit;
  public cacheAttachment;

  //MAXXIDATA
  public editStatus: boolean = false;
  public editTxt: string;
  //FIM MAXXIDATA

  public visitBO: VisitBO;
  public visitHybridBO: VisitHybridBO;
  public plagueBO: PlagueBO;
  public productUsedBO: ProductUsedBO;
  public weedHandlingBO: WeedHandlingBO;

  public title: string;
  public isReadOnly: boolean;

  public showHybridData = true;
  public showInformationData = true;
  public showWeedHandlingData = true;

  public geolocation: IGeolocation;
  public userProfile: any;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  @ViewChild(AttachmentComponent)
  private pageHomeAttachment: AttachmentComponent;

  @ViewChild(GeolocationComponent)
  private geolocationComponent: GeolocationComponent;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public events: Events,
    public storage: Storage,
  ) {
    //MAXXIDATA
    if (this.navParams.data.editVisitError) {
      this.event = this.navParams.data.event;
    } else {
      this.event = this.navParams.data;
    }
    //FIM MAXXIDATA
    this.eventBO = new EventBO(this.service);
    this.visitBO = new VisitBO(this.service);
    this.visitHybridBO = new VisitHybridBO(this.service);
    this.plagueBO = new PlagueBO(this.service);
    this.productUsedBO = new ProductUsedBO(this.service);
    this.weedHandlingBO = new WeedHandlingBO(this.service);

    LoadingManager.getInstance().show().then(() => {
      this.visitBO.loadVisit(this.event).then((visit: Visita__cModel) => {
        this.storage.get('user_profile').then((user_profile: any) => {
          this.userProfile = user_profile;
        });
        LoadingManager.getInstance().hide();
        this.handleInitialState(visit);
      });
    });
  }

  private ionViewDidEnter() {
    this.cacheVisit = JSON.stringify(this.visit);
    this.cacheAttachment = JSON.stringify(this.pageHomeAttachment.attachments);
  }

  private ionViewCanLeave() {
    //MAXXIDATA
    if (this.navParams.data.editVisitError) {

    } else {

    return new Promise((resolve, reject) => {
      if (!this.isReadOnly && !this.isSaveDisabled() && this.cacheVisit !== JSON.stringify(this.visit) || this.cacheAttachment !== JSON.stringify(this.pageHomeAttachment.attachments)) {
        this.saveVisit().then(() => {
          resolve();
        });
      } else if (this.isSaveDisabled() && this.visit.Status__c !== VisitStatus.completed) {
        AlertHelper.showDialogForVisitAutoSave(this.alertCtrl, () => {
          resolve();
        }, () => {
          reject();
          console.log('cancel');
        });
      } else {
        resolve();
      }
    });
       
  }
  //FIM MAXXIDATA
  }

  

  private updatePageTitle() {
    if (this.header) {
      this.header.updatePageTitle(this.title);
    }
  }

  private setParentIdToAttachmentView() {
    this.pageHomeAttachment.setParentId(this.visit.getId());
  }

  private handleInitialState(visit: Visita__cModel) {
    this.visit = visit;
    this.visit.Name = 'Germinação e Emergência';

    if (this.event.BR_Division__c) {
      this.visit.BR_Division__c = this.event.BR_Division__c;
    }

    if (this.visit.Status__c === VisitStatus.completed || this.visit.Status__c === VisitStatus.canceled) {
       //MAXXIDATA
       if (this.visit.Id.indexOf("local_") && this.visit.GerDemandaId__c == null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === false) {
        console.log('VISITA STATUS EDITAR')
         this.isReadOnly = true;
       } else if(this.visit.GerDemandaId__c != null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === true ) {
          if (this.visit.Status__c == VisitStatus.completed) {
            this.editStatus = true;
            this.editTxt = 'Visita com possibilidade de edição.';
          }
          this.isReadOnly = false;
        }else if(this.visit.GerDemandaId__c != null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === false ) {
          this.isReadOnly = true;
       }else {
         if (this.visit.Status__c == VisitStatus.completed) {
           this.editStatus = true;
           this.editTxt = 'Visita com possibilidade de edição.';
         }
         this.isReadOnly = false;
       }
       //FIM MAXXIDATA
      this.title = 'Germinação e Emergência';
    } else {
      this.isReadOnly = false;
      this.title = 'Nova Germinação e Emergência';
    }

    this.updatePageTitle();
    this.setParentIdToAttachmentView();
    this.setLocation();

    if (MustEditRegisterHelper.isObjectTreeWithError(this.visit)) { this.isReadOnly = false; }
  }

  private setLocation() {
    this.geolocation = {
      altitude: this.visit.BR_Altitude__c,
      latitude: this.visit.Latitude__c,
      longitude: this.visit.Longitude__c,
    };

    if (this.geolocationComponent) {
      this.geolocationComponent.setLocation(this.geolocation);
    }
  }

  private updateGeolocation(location) {
    if (location) {
      this.visit.BR_Altitude__c = location.altitude;
      this.visit.Latitude__c = location.latitude;
      this.visit.Longitude__c = location.longitude;
    }
  }

  public cancelVisit() {
    AlertHelper.showConfirmDialogForVisitCancellation(this.alertCtrl, () => {
      this.isReadOnly = true;
      this.visit.Status__c = VisitStatus.canceled;
      this.saveVisit().then(() => {
        NavigationHelper.goBackToOrigin(this.navCtrl);
      });
    }, () => {
      console.log('cancel');
    });
  }

  public completeVisit() {
    AlertHelper.showConfirmDialogForVisitCompletion(this.alertCtrl, () => {
      if (this.willOpenReport()) {
        this.openReport();
      } else {
        this.isReadOnly = true;
        this.visit.Status__c = VisitStatus.completed;
        this.visit.Data_de_Execucao__c = DateHelper.getCurrentDateString();
        this.saveVisit().then(() => {
          NavigationHelper.goBackToOrigin(this.navCtrl);
        });
      }
    }, () => {
      console.log('cancel');
    });
  }

  private saveVisit(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.visit.EventId__c = this.event.getId();
      let statusChange = false;
      let statusProgress = false;

      if (this.visit.Status__c === VisitStatus.open) {
        this.visit.Status__c = VisitStatus.inProgress;
        statusProgress = true;
      }

      if (statusProgress || this.isReadOnly) {
        statusChange = true;
      }

      LoadingManager.getInstance().show().then(() => {
        this.visitBO.saveVisit(this.event, this.visit).then((savedVisit: Visita__cModel) => {
          this.visit = savedVisit;
          this.cacheVisit = JSON.stringify(this.visit);
          this.cacheAttachment = JSON.stringify(this.pageHomeAttachment.attachments);


          if (statusChange) {
            this.events.publish(NotificationKeys.statusEventSave, this.visit);
          }

          setTimeout(() => {
            LoadingManager.getInstance().hide();
            ToastHelper.showSuccessMessage(this.toastCtrl);
            console.log('---| Success:saveVisit: ', this.visit);
            //MAXXIDATA
            if (this.navParams.data.editVisitError) {
              this.service.visita__cService.deletLogErrorEntities(this.navParams.data.idDataError, this.navParams.data.idData, this.navParams.data.shildId);
            }
            //FIM MAXXIDATA
            resolve();
          }, 500);

        }, (err) => {
          console.log('-*-| visitDAO.saveVisit:error: ', err);
          LoadingManager.getInstance().hide();
        });
      });
    });
  }

  private willOpenReport() {
    return this.eventBO.willOpenReport(this.event, this.userProfile);
  }

  private openReport() {
    LoadingManager.getInstance().hide();

    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(ReportPage, { event: this.event, visit: this.visit }, options);
    modal.present();

    modal.onDidDismiss((finish: boolean) => {
      if (finish) {
        this.isReadOnly = true;
        this.visit.Status__c = VisitStatus.completed;
        this.visit.BR_GenerateReport__c = true;
        this.visit.Data_de_Execucao__c = DateHelper.getCurrentDateString();
        this.saveVisit().then(() => {
          NavigationHelper.goBackToOrigin(this.navCtrl);
        });
      }
    });
  }

  // ************************************************************ //
  // ****************Begin corn germination custom*************** //
  // ************************************************************ //
  private isSaveDisabled() {
    return !(
      DataHelper.checkToggleObj(this.visit.BR_UsedHerbicideInTheWinter__c, this.visit.ProductUsed)
      && DataHelper.checkToggleObj(this.visit.BR_MadeEarlyDesiccation__c, this.visit.BR_WeedHandling__c)
      && DataHelper.checkToggleObj(this.visit.BR_failed_planting__c, this.visit.BR_reason__c)
      && (!this.visit.BR_Stand_Final_ha__c || (this.visit.BR_Stand_Final_ha__c && this.standFinalRange()))
    );
  }

  private clearFailedPlanting() {
    this.visit.BR_reason__c = undefined;
  }

  private standFinalRange() {
    return DataHelper.validationForRange(this.visit.BR_Stand_Final_ha__c, 20000, 100000);
  }

  // ************************************************************ //
  // ************************Begin Hybrid************************ //
  // ************************************************************ //
  private openHybridModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Híbrido',
      sql: true,
      list: 'HybridList',
      limit: 1,
      division: this.event.BR_Division__c,
    }, options);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        this.visitHybridBO.create(VisitHybridRecordTypes.germinationAndEmergency).then((visitHybrid) => {
          visitHybrid.BR_VisitMobileId__c = this.visit.getId();
          visitHybrid.Hibrido_Visita__c = data[0].value;
          visitHybrid.BR_HybridName__c = data[0].label;

          this.visit.Hibrido_de_Visita__c.push(visitHybrid);
          console.log('------| Hybrid added: ', this.visit);
        });
      }
    });
  }

  private removeHybrid(hybridIndex: number) {
    this.visitHybridBO.delete(this.visit.Hibrido_de_Visita__c, hybridIndex).then((newHybrids) => {
      this.visit.Hibrido_de_Visita__c = newHybrids;
      console.log('------| Hybrid removed: ', this.visit);
    });
  }

  // ************************************************************ //
  // ************************Begin Plague************************ //
  // ************************************************************ //
  private openPlagueModal(h_index: number, p_index: number) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const plagueObj = this.visit.Hibrido_de_Visita__c[h_index].Plagues[p_index]
      ? this.visit.Hibrido_de_Visita__c[h_index].Plagues[p_index]
      : null;

    const modal = this.modalCtrl.create(PlagueModal, {
      plague: plagueObj,
      isReadOnly: this.isReadOnly,
    }, options);
    modal.present();

    modal.onDidDismiss((plague: Praga__cModel) => {
      if (plague) {
        plague.Hibrido_de_Visita__c = this.visit.Hibrido_de_Visita__c[h_index].getId();
        this.visit.Hibrido_de_Visita__c[h_index].Plagues.push(plague);
        console.log('------| Plague added: ', this.visit);
      }
    });
  }

  private removePlague(h_index: number, p_index: number) {
    this.plagueBO.delete(this.visit.Hibrido_de_Visita__c[h_index].Plagues, p_index).then((newPlagues) => {
      this.visit.Hibrido_de_Visita__c[h_index].Plagues = newPlagues;
      console.log('------| Plague removed: ', this.visit);
    });
  }

  // ************************************************************ //
  // **********************Begin ProductUsed********************* //
  // ************************************************************ //
  private openProductUsedModal() {
    const modal = this.modalCtrl.create(ProductsUsedModal);
    modal.present();

    modal.onDidDismiss((newProductsUsed: BR_ProductUsed__cModel) => {
      console.log('---|onDidDismiss:ProductsUsedModalReturn: ', newProductsUsed);
      if (newProductsUsed) {
        newProductsUsed.BR_VisitUsedProduct__c = this.visit.getId();
        this.visit.ProductUsed.push(newProductsUsed);
      }
    });
  }

  private removeProductUsed(p_index: number) {
    this.productUsedBO.delete(this.visit.ProductUsed, p_index).then((newProductUsed) => {
      this.visit.ProductUsed = newProductUsed;
      console.log('------| ProductUsed removed: ', this.visit);
    });
  }

  private removeAllProductUsed() {
    for (let i = 0; i < this.visit.ProductUsed.length; i++) {
      this.productUsedBO.delete(this.visit.ProductUsed, i).then((newProductUsed) => {
        this.visit.ProductUsed = newProductUsed;
        console.log('------| ProductUsed removed: ', this.visit);
      });
    }
  }

  // ************************************************************ //
  // *********************Begin WeedHandling********************* //
  // ************************************************************ //
  private openWeedHandlingModal(w_index: number) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(WeedHandlingModal, {
      weedHandling: this.visit.BR_WeedHandling__c[w_index],
      isReadOnly: this.isReadOnly,
    }, options);
    modal.present();

    modal.onDidDismiss((weedHandling: BR_WeedHandling__cModel) => {
      if (weedHandling) {
        weedHandling.BR_Visit__c = this.visit.getId();

        if (w_index !== null && w_index !== undefined) {
          this.visit.BR_WeedHandling__c[w_index] = weedHandling;
        } else {
          this.visit.BR_WeedHandling__c.push(weedHandling);
        }
        console.log('------| WeedHandling added: ', this.visit);
      }
    });
  }

  private removeWeedHandling(w_index: number) {
    this.weedHandlingBO.delete(this.visit.BR_WeedHandling__c, w_index).then((newWeedHandling) => {
      this.visit.BR_WeedHandling__c = newWeedHandling;
      console.log('------| WeedHandling removed: ', this.visit);
    });
  }

  private removeAllWeedHandling() {
    for (let i = 0; i < this.visit.BR_WeedHandling__c.length; i++) {
      this.weedHandlingBO.delete(this.visit.BR_WeedHandling__c, i).then((newWeedHandling) => {
        this.visit.BR_WeedHandling__c = newWeedHandling;
        console.log('------| WeedHandling removed: ', this.visit);
      });
    }
  }

}
