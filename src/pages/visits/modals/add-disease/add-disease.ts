import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { ModalController, ModalOptions, NavController, NavParams, ViewController } from 'ionic-angular';
import { Component } from '@angular/core';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';
import { Doenca__cModel } from './../../../../app/model/Doenca__cModel';

@Component({
  selector: 'add-disease',
  templateUrl: 'add-disease.html',
})
export class DiseaseModal {

  public disease: Doenca__cModel;
  public page: string;
  public isReadOnly: false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
  ) {
    this.disease = this.navParams.data.disease
      ? new Doenca__cModel(this.navParams.data.disease)
      : new Doenca__cModel(null);

    this.page = this.navParams.data.page ? this.navParams.data.page : null;
    this.isReadOnly = this.navParams.data.isReadOnly;
  }

  private closeModal() {
    this.viewCtrl.dismiss();
  }

  private applyDisease() {
    this.viewCtrl.dismiss(this.disease);
  }

  private openPhaseModal() {
    const phase = {
      title: 'Adicionar Fase',
      items: this.disease.BR_Phase__c,
      list: 'BR_Phase__c',
    };
    this.openMultipickList(phase);
  }

  private openDiseaseModal() {
    const disease = {
      title: 'Adicionar Doença',
      items: this.disease.Tipo_de_Doenca__c,
      list: 'Tipo_de_Doenca__c',
    };
    this.openMultipickList(disease);
  }

  private openProductModal() {
    const product = {
      title: 'Adicionar Produto',
      items: this.disease.Produto__c,
      list: 'Produto__c',
    };
    this.openMultipickList(product);
  }

  private openMultipickList(obj: any) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, obj, options);
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) { this.disease[obj.list] = data.join(';'); }
    });
  }

  private isSaveDisabled() {
    return !(
      this.disease.BR_Phase__c
      && this.disease.Tipo_de_Doenca__c
      && this.disease.Produto__c
    );
  }

}
