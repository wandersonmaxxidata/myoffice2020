import { Component } from '@angular/core';
import { ModalController, NavParams, ViewController } from 'ionic-angular';
import { Hibrido_de_Visita__cModel } from '../../../../app/model/Hibrido_de_Visita__cModel';
import { IPicklistItem } from '../../../../interfaces/IPicklistItem';
import { ServiceProvider } from '../../../../providers/service-salesforce';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';
import * as moment from 'moment';

@Component({
  selector: 'soy-harvest-info-hybrid-modal',
  templateUrl: 'soy-harvest-info-hybrid-modal.html',
})
export class SoyHarvestInfoHybridModal {

  public hybridVisit: Hibrido_de_Visita__cModel;

  public isSilage: boolean;
  public isReadOnly: boolean;

  public plantingArea;
  public iPopulacaoFinal: IPicklistItem[] = [];

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public serviceProvider: ServiceProvider,
    public modal: ModalController,
  ) {
    this.hybridVisit = new Hibrido_de_Visita__cModel(this.navParams.data.hybrid);
    this.isReadOnly = this.navParams.data.isReadOnly;

    this.plantingArea = this.hybridVisit.Area_Plantada__c;
    const today = moment().format('YYYY-MM-DD');

    if (!this.hybridVisit.BR_HarvestDate__c) {
      this.hybridVisit.BR_HarvestDate__c = today.toString();
    }
    if (!this.hybridVisit.BR_PlantationDate__c) {
      this.hybridVisit.BR_PlantationDate__c = today.toString();
    }

  }

  private dismiss() {
    this.viewCtrl.dismiss();
  }

  private checkLodging(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.BR_Lodging__c, 0, 100));
  }

  private checkUmidade(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Umidade__c, 5, 45));
  }

  private checkLiquidProductivity(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.PRODUTIVIDADE_LIQ_13__c, 1000, 8500));
  }

  private checkAreaColhida(): boolean {
    return (
      Number(this.hybridVisit.Area_Colhida__c) >= 0
      && (Number(this.hybridVisit.Area_Colhida__c) <= this.plantingArea || !this.plantingArea)
    );
  }

  private closeModal() {
    this.viewCtrl.dismiss(this.hybridVisit);
  }

  private isSaveEnable() {
    return !(
      this.hybridVisit.BR_PlantationDate__c
      && this.hybridVisit.BR_HarvestDate__c
      && this.checkLodging()
      && this.checkUmidade()
      && this.checkLiquidProductivity()
      && (this.hybridVisit.Area_Colhida__c && this.checkAreaColhida())
    );
  }
}
