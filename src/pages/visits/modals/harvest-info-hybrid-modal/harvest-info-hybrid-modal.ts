import { Hibrido_de_Visita__cModel } from './../../../../app/model/Hibrido_de_Visita__cModel';
import { ServiceProvider } from './../../../../providers/service-salesforce';
import { Component } from '@angular/core';
import { ModalController, NavParams, ViewController } from 'ionic-angular';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';
import { IPicklistItem } from '../../../../interfaces/IPicklistItem';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';

@Component({
  selector: 'harvest-info-hybrid-modal',
  templateUrl: 'harvest-info-hybrid-modal.html',
})
export class HarvestInfoHybridModal {

  public hybridVisit: Hibrido_de_Visita__cModel;

  public isSilage: boolean;
  public isReadOnly: boolean;

  public plantingArea;
  public iPopulacaoFinal: IPicklistItem[] = [];

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public serviceProvider: ServiceProvider,
    public modal: ModalController,
  ) {
    this.hybridVisit = new Hibrido_de_Visita__cModel(this.navParams.data.hybrid);

    this.isSilage = this.navParams.data.isSilage;
    this.isReadOnly = this.navParams.data.isReadOnly;

    this.iPopulacaoFinal = WindowStorageHelper.retrievePicklistsFromLocalStorage('Populacao_Final__c');
    this.plantingArea = this.hybridVisit.Area_Plantada__c;

    if (!this.hybridVisit.BR_HarvestDate__c) {
      this.hybridVisit.BR_HarvestDate__c = DateHelper.dateISO(null);
    }

  }

  private dismiss() {
    this.viewCtrl.dismiss();
  }

  private checkPercentualGraoArdido(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Percentual_de_Grao_Ardido__c, 0, 100));
  }

  private checkLodging(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.BR_Lodging__c, 0, 100));
  }

  private checkUmidade(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Umidade__c, 10, 40));
  }

  private checkTHa(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.BR_THa__c, 1, 80));
  }

  private checkLiquidProductivity(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.PRODUTIVIDADE_LIQ_13__c, 2000, 25000));
  }

  private checkAreaColhida(): boolean {
    return (
      Number(this.hybridVisit.Area_Colhida__c) >= 0
      && (Number(this.hybridVisit.Area_Colhida__c) <= this.plantingArea || !this.plantingArea)
    );
  }

  private closeModal() {
    this.viewCtrl.dismiss(this.hybridVisit);
  }

  private savedEnable() {
    const constVariables = (
      this.hybridVisit.BR_HarvestDate__c
      && (this.checkPercentualGraoArdido() || !this.hybridVisit.Percentual_de_Grao_Ardido__c)
      && (this.checkLodging() || !this.hybridVisit.BR_Lodging__c)
      && this.checkUmidade()
      && (this.hybridVisit.Area_Colhida__c && this.checkAreaColhida())
    );

    if (this.isSilage) {
      return !(constVariables && this.checkTHa());
    } else {
      return !(constVariables && this.checkLiquidProductivity());
    }
  }

}
