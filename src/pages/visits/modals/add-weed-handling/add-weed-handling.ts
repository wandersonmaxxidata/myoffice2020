import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { BR_WeedHandling__cModel } from './../../../../app/model/BR_WeedHandling__cModel';
import { Component } from '@angular/core';
import { ModalController, ModalOptions, NavParams, ViewController } from 'ionic-angular';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';

@Component({
  selector: 'weed-handling-modal',
  templateUrl: 'add-weed-handling.html',
})
export class WeedHandlingModal {

  public weedHandling: BR_WeedHandling__cModel;
  public page: string;
  public isReadOnly: false;

  public type;
  public level;

  public showBR_Phase__c = false;
  public showBR_LevelOfInfestation__c = false;
  public showBR_Product__c = false;

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
  ) {

    this.weedHandling = this.navParams.data.weedHandling
      ? new BR_WeedHandling__cModel(this.navParams.data.weedHandling)
      : new BR_WeedHandling__cModel(null);

    this.page = this.navParams.data.page ? this.navParams.data.page : null;
    this.isReadOnly = this.navParams.data.isReadOnly;

    this.type = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_WeedType__c');
    this.level =  WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_LevelOfInfestation__c');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  public applyWeed() {
    this.viewCtrl.dismiss(this.weedHandling);
  }

  private openPhaseModal() {
    const phase = {
      title: 'Adicionar Fase',
      items: this.weedHandling.BR_Phase__c,
      list: 'BR_Phase__c',
    };
    this.openMultipickList(phase);
  }

  private openProductModal() {
    const product = {
      title: 'Adicionar Produto',
      items: this.weedHandling.BR_Product__c,
      list: 'BR_Product__c',
      limit: 1,
    };
    this.openMultipickList(product);
  }

  private openMultipickList(obj: any) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, obj, options);
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) { this.weedHandling[obj.list] = data.join(';'); }
    });
  }

  private isSaveDisabled() {
    if (this.page === 'soy-green-visit') {
      this.showBR_Phase__c = true;
      this.showBR_Product__c = true;

      return !(
        this.weedHandling.BR_Phase__c
        && this.weedHandling.BR_WeedType__c
        && this.weedHandling.BR_Product__c
      );

    } else {
      this.showBR_LevelOfInfestation__c = true;

      return !(
        this.weedHandling.BR_WeedType__c
        && this.weedHandling.BR_LevelOfInfestation__c
      );
    }
  }

}
