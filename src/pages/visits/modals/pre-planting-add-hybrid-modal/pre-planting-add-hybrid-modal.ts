import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { Hibrido_de_Visita__cModel } from './../../../../app/model/Hibrido_de_Visita__cModel';
import { Component } from '@angular/core';
import { ModalController, ModalOptions, NavParams, ViewController } from 'ionic-angular';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import * as moment from 'moment';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';

@Component({
  selector: 'pre-planting-add-hybrid-modal',
  templateUrl: 'pre-planting-add-hybrid-modal.html',
})
export class PrePlantingAddHybridPage {

  public hybridVisit: Hibrido_de_Visita__cModel;

  public isReadOnly = false;
  public division: string;

  public year = moment().year();

  private masks: any = {
    numberMask: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
  };

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private modalCtrl: ModalController,
  ) {
    this.hybridVisit = new Hibrido_de_Visita__cModel(this.navParams.data.hybrid);
    this.division = this.navParams.data['division'];
    this.isReadOnly = this.navParams.data.isReadOnly;

    if (!this.hybridVisit.BR_PlantationDate__c) {
      this.hybridVisit.BR_PlantationDate__c = DateHelper.dateISO(null);
    }
  }

  public openHybridModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Híbrido',
      sql: true,
      list: 'HybridList',
      limit: 1,
      division: this.division,
    }, options);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        console.log('---| Modal returned data: visitHybrid: ', data);
        this.hybridVisit.Hibrido_Visita__c = data[0].value;
        this.hybridVisit.BR_HybridName__c = data[0].label;
      }
    });
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  public closeModal() {
    this.viewCtrl.dismiss(this.hybridVisit);
  }

  private savedEnable() {
    return !(
      this.hybridVisit.BR_HybridName__c &&
      this.checkPopulation()
    );
  }

  private checkPopulation(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Populacao_de_Plantio__c, 40, 120));
  }

}
