import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { ModalController, ModalOptions, NavController, NavParams, ViewController } from 'ionic-angular';
import { Component } from '@angular/core';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';
import { Praga__cModel } from '../../../../app/model/Praga__cModel';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';

@Component({
  selector: 'add-plague',
  templateUrl: 'add-plague.html',
})
export class PlagueModal {

  public plague: Praga__cModel;
  public page: string;
  public isReadOnly: false;

  public type;
  public which;
  public nivel;

  public showBR_Phase__c = false;
  public showNivel_de_Infestacao__c = false;
  public Necessidade_de_Inseticida__c: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
  ) {
    this.plague = this.navParams.data.plague
      ? new Praga__cModel(this.navParams.data.plague)
      : new Praga__cModel(null);

    this.getVariableToSalesforceGambi();
    this.page = this.navParams.data.page ? this.navParams.data.page : null;
    this.isReadOnly = this.navParams.data.isReadOnly;

    this.type = WindowStorageHelper.retrievePicklistsFromLocalStorage('Tipo_de_Praga__c');
    this.which = WindowStorageHelper.retrievePicklistsFromLocalStorage('Qual__c');
    this.nivel = WindowStorageHelper.retrievePicklistsFromLocalStorage('Nivel_de_Infestacao__c');
  }

  private closeModal() {
    this.viewCtrl.dismiss();
  }

  private applyPlague() {
    this.viewCtrl.dismiss(this.plague);
  }

  private getVariableToSalesforceGambi() {
    this.Necessidade_de_Inseticida__c = this.plague.Necessidade_de_Inseticida__c ? true : false;
    this.setVariableToSalesforceGambi();
  }

  private setVariableToSalesforceGambi() {
    this.plague.Necessidade_de_Inseticida__c = this.Necessidade_de_Inseticida__c ? 'Sim' : 'Não';
    this.plague.Quimicos__c = undefined;
  }

  private openPhaseModal() {
    const phase = {
      title: 'Adicionar Fase',
      items: this.plague.BR_Phase__c,
      list: 'BR_Phase__c',
    };
    this.openMultipickList(phase);
  }

  private openChemicalModal() {
    const chemical = {
      title: 'Adicionar Químicos',
      items: this.plague.Quimicos__c,
      list: 'Quimicos__c',
      limit: 3,
    };
    this.openMultipickList(chemical);
  }

  private openMultipickList(obj: any) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, obj, options);
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) { this.plague[obj.list] = data.join(';'); }
    });
  }

  private isSaveDisabled() {
    const fieldConsts = (
      this.plague.Tipo_de_Praga__c
      && this.plague.Qual__c
      && DataHelper.checkToggleObj(this.Necessidade_de_Inseticida__c, this.plague.Quimicos__c)
    );

    if (this.page === 'soy-green-visit') {
      this.showBR_Phase__c = true;
      this.showNivel_de_Infestacao__c = true;

      return !(
        fieldConsts
        && this.plague.BR_Phase__c
        && this.plague.Nivel_de_Infestacao__c
      );
    }
    return !(fieldConsts);
  }
}
