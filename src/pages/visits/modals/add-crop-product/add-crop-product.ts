import { ServiceProvider } from './../../../../providers/service-salesforce';
import { Component } from '@angular/core';
import { ModalController, ModalOptions, NavParams, ViewController } from 'ionic-angular';
import { BR_CropProducts__cModel } from '../../../../app/model/BR_CropProducts__cModel';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';
import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import * as moment from 'moment';

@Component({
  selector: 'add-crop-product',
  templateUrl: 'add-crop-product.html',
})
export class CropProductModal {

  public cropProduct: BR_CropProducts__cModel = new BR_CropProducts__cModel(null);

  public BR_InformationType__cList = [];
  public BR_MoneyConcorrente__cList = [];
  public year = moment().year();

  public isLostTrade = false;
  public isReadOnly = false;

  public masks: any = {
    numberMask: [/\d/, /\d/, /\d/],
    numberMaskPlanting: [/\d/, /\d/, /\d/],
    numberMaskPercentage: [/\d/, /\d/, /\d/],
    // numberMask: [/^[0-9.,]{0,20}$/],
  };

  constructor(
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private service: ServiceProvider,
  ) {
    this.loadVariables();

    if (this.navParams.data && this.navParams.data.cropProduct) {
      this.isReadOnly = this.navParams.data.isReadOnly;
      this.cropProduct = new BR_CropProducts__cModel(this.navParams.data.cropProduct);

      this.onInformationTypeChange();
    } else {
      this.cropProduct.BR_MoneyConcorrente__c = 'Real';
      this.cropProduct.BR_VencimentoDoConcorrente__c = moment().format('YYYY-MM-DD');
    }

    this.cropProduct.BR_MobileAppFlag__c = true;
  }

  private loadVariables() {
    this.BR_InformationType__cList = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_InformationType__c');
    this.BR_MoneyConcorrente__cList = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_MoneyConcorrente__c');
  }

  private closeModal() {
    this.viewCtrl.dismiss();
  }

  private isSaveDisabled() {
    return !this.cropProduct.BR_InformationType__c ||
      !this.cropProduct.BR_ProductConcorrente__c;
  }

  private onInformationTypeChange() {
    this.isLostTrade = (this.cropProduct.BR_InformationType__c === 'Negócio Perdido');
  }

  private applyProduct() {
    this.viewCtrl.dismiss(this.cropProduct);
  }

  private openForecastInvoiceModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Previsão de Faturamento',
      items: this.cropProduct.BR_ForecastInvoice__c,
      list: 'BR_ForecastInvoice__c',
      // limit: 3,
    }, options);
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.cropProduct.BR_ForecastInvoice__c = data.join(';');
      }
    });
  }

  private openCropCompetingProduct() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Produto Concorrente',
      items: this.cropProduct.BR_ProductConcorrente__c,
      list: 'BR_ProductConcorrente__c',
      limit: 1,
    }, options);
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.cropProduct.BR_ProductConcorrente__c = data.join(';');
      }
    });
  }
}
