import { Hibrido_de_Visita__cModel } from './../../../../app/model/Hibrido_de_Visita__cModel';
import { ServiceProvider } from './../../../../providers/service-salesforce';
import { Component } from '@angular/core';
import { ModalController, ModalOptions, NavController, NavParams, ViewController } from 'ionic-angular';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';
import { IPicklistItem } from '../../../../interfaces/IPicklistItem';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';

@Component({
  selector: 'soy-visit-planting-modal',
  templateUrl: 'soy-visit-planting-modal.html',
})
export class SoyVisitPlantingModal {

  public hybridVisit: Hibrido_de_Visita__cModel;

  public isReadOnly: boolean;
  public division: string;

  public iBR_PlantationSeedsM__c: IPicklistItem [];

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public serviceProvider: ServiceProvider,
    public modal: ModalController,
  ) {
    this.hybridVisit = new Hibrido_de_Visita__cModel(this.navParams.data.hybrid);
    this.division = this.navParams.data['division'];
    this.isReadOnly = this.navParams.data.isReadOnly;

    if (!this.hybridVisit.BR_HarvestDate__c) {
      this.hybridVisit.BR_HarvestDate__c = DateHelper.dateISO(null);
    }

    this.iBR_PlantationSeedsM__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_PlantationSeedsM__c');
  }

  public openHybrid() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modal.create(MultipicklistModal, {
      title: 'Adicionar Variedades',
      sql: true,
      list: 'HybridList',
      limit: 1,
      division: this.division,
    }, options);
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        console.log('---| Modal returned data: visitHybrid: ', data);
        this.hybridVisit.Hibrido_Visita__c = data[0].value;
        this.hybridVisit.BR_HybridName__c = data[0].label;
      }
    });
  }

  private dismiss() {
    this.viewCtrl.dismiss();
  }

  private closeModal() {
    this.viewCtrl.dismiss(this.hybridVisit);
  }

  private isSaveDisabled() {
    return !(
      this.hybridVisit.BR_HybridName__c &&
      this.hybridVisit.Area_Plantada__c &&
      this.hybridVisit.BR_PlantationSeedsM__c
    );
  }

}
