import { IPicklistItem } from './../../../../interfaces/IPicklistItem';
import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { Hibrido_de_Visita__cModel } from './../../../../app/model/Hibrido_de_Visita__cModel';
import { Component } from '@angular/core';
import { ModalController, ModalOptions, NavParams, ViewController } from 'ionic-angular';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import * as moment from 'moment';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';

@Component({
  selector: 'planting-add-hybrid-modal',
  templateUrl: 'planting-add-hybrid-modal.html',
})
export class PlantingAddHybridPage {

  public hybridVisit: Hibrido_de_Visita__cModel;
  public isReadOnly = false;
  public division: string;
  public seedsTreatment: any[];
  public year = moment().year();
  public iAmbienteProdutivo: IPicklistItem[] = [];


  private masks: any = {
    numberMask: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
  };

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private modalCtrl: ModalController,
    private modal: ModalController,
  ) {
    this.hybridVisit = new Hibrido_de_Visita__cModel(this.navParams.data.hybrid);
    this.division = this.navParams.data['division'];
    this.isReadOnly = this.navParams.data.isReadOnly;

    if (!this.hybridVisit.BR_PlantationDate__c) {
      this.hybridVisit.BR_PlantationDate__c = DateHelper.dateISO(null);
    }

    this.seedsTreatment = WindowStorageHelper.retrievePicklistsFromLocalStorage('Tratamento_de_Sementes__c');
    this.iAmbienteProdutivo = WindowStorageHelper.retrievePicklistsFromLocalStorage('Ambiente_Produtivo_T_ha__c');
  }

  public openHybridModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Híbrido',
      sql: true,
      list: 'HybridList',
      limit: 1,
      division: this.division,
    }, options);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        console.log('---| Modal returned data: visitHybrid: ', data);
        this.hybridVisit.Hibrido_Visita__c = data[0].value;
        this.hybridVisit.BR_HybridName__c = data[0].label;
      }
    });
  }

  public openProductModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modal.create(MultipicklistModal, {
      title: 'Produto',
      items: this.hybridVisit.Produto__c,
      list: 'Produto__c',
      limit: 3,
    }, options);
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.hybridVisit.Produto__c = data.join(';');
      }
    });
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  public closeModal() {
    this.viewCtrl.dismiss(this.hybridVisit);
  }

  public checkProducts() {
    if (this.hybridVisit.Tratamento_de_Sementes__c === 'Nenhum') {
      this.hybridVisit.Produto__c = undefined;
    }
  }

  public checkTreatment() {
    if (this.hybridVisit.Tratamento_de_Sementes__c === 'Nenhum' || this.hybridVisit.Tratamento_de_Sementes__c === undefined || this.hybridVisit.Tratamento_de_Sementes__c === null) {
      return true;
    } else {
      return false;
    }
  }

  private savedEnable() {
    if (this.hybridVisit.Tratamento_de_Sementes__c === 'Nenhum' || this.hybridVisit.Tratamento_de_Sementes__c === undefined || this.hybridVisit.Tratamento_de_Sementes__c === null) {
      return !(
        this.hybridVisit.Area_Plantada__c &&
        this.hybridVisit.Populacao_de_Plantio__c &&
        this.hybridVisit.Ambiente_Produtivo_T_ha__c &&
        this.hybridVisit.BR_HybridName__c &&
        this.checkPopulation()
      );
    } else {
      return !(
        this.hybridVisit.Area_Plantada__c &&
        this.hybridVisit.Populacao_de_Plantio__c &&
        this.hybridVisit.Ambiente_Produtivo_T_ha__c &&
        this.hybridVisit.BR_HybridName__c &&
        this.hybridVisit.Produto__c &&
        this.checkPopulation()
      );
    }
  }

  private checkPopulation(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Populacao_de_Plantio__c, 40, 120));
  }


}
