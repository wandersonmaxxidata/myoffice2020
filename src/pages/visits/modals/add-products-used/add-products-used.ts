import { ServiceProvider } from './../../../../providers/service-salesforce';
import { BR_ProductUsed__cModel } from './../../../../app/model/BR_ProductUsed__cModel';
import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';

@Component({
  selector: 'products-used-modal',
  templateUrl: 'add-products-used.html',
})
export class ProductsUsedModal {

  public productsUsed: BR_ProductUsed__cModel;
  public productItems: any[];
  public dosageTypeItems: any[];

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public service: ServiceProvider,
  ) {
    this.productsUsed = new BR_ProductUsed__cModel(null);
    this.productItems = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_ProductsUsed__c');
    this.dosageTypeItems = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_DosageType__c');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  public applyProducts() {
    this.viewCtrl.dismiss(this.productsUsed);
  }

  private savedEnable() {
    return !(
      this.productsUsed.BR_ProductsUsed__c &&
      this.productsUsed.BR_DosageType__c &&
      this.productsUsed.BR_DosageAmount__c
    );
  }

}
