import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { Hibrido_de_Visita__cModel } from './../../../../app/model/Hibrido_de_Visita__cModel';
import { ServiceProvider } from './../../../../providers/service-salesforce';
import { Component } from '@angular/core';
import { ModalController, ModalOptions, NavParams, ViewController } from 'ionic-angular';
import { WindowStorageHelper } from '../../../../shared/Helpers/WindowStorageHelper';
import { DataHelper } from '../../../../shared/Helpers/DataHelper';

@Component({
  selector: 'corn-harvesting-modal',
  templateUrl: 'corn-harvesting-modal.html',
})
export class CornHarvestingModal {

  public hybridVisit: Hibrido_de_Visita__cModel;

  public division: string;
  public isReadOnly = false;

  public iPopulacao_de_Emergencia__c = [];
  public iPopulacao_Final__c = [];
  public iPopulacao_de_Plantio__c = [];

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public serviceProvider: ServiceProvider,
    public modalCtrl: ModalController,
  ) {
    this.hybridVisit = new Hibrido_de_Visita__cModel(this.navParams.data.hybrid);
    this.division = this.navParams.data['division'];
    this.isReadOnly = this.navParams.data.isReadOnly;

    this.iPopulacao_de_Emergencia__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('Populacao_de_Emergencia__c');
    this.iPopulacao_Final__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('Populacao_Final__c');
    this.iPopulacao_de_Plantio__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('Populacao_de_Plantio__c');
  }

  public openHybridModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Híbrido',
      sql: true,
      list: 'HybridList',
      limit: 1,
      division: this.division,
    }, options);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        console.log('---| Modal returned data: visitHybrid: ', data);
        this.hybridVisit.Hibrido_Visita__c = data[0].value;
        this.hybridVisit.BR_HybridName__c = data[0].label;
      }
    });
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  public closeModal() {
    this.viewCtrl.dismiss(this.hybridVisit);
  }

  private savedEnable() {
    return !(
      this.hybridVisit.Hibrido_Visita__c
      && this.hybridVisit.Populacao_de_Plantio__c
      && this.hybridVisit.Populacao_de_Emergencia__c
      && this.hybridVisit.Populacao_Final__c
      && this.checkPercentualGraoArdido()
      && this.checkUmidade()
      && this.checkTombamento()
      && this.checkQuebramento()
      && this.checkLiquidProductivity()
      && this.checkPlatingPopulation()
      && this.checkEmergencyPopulation()
      && this.checkFinalPopulation()
    );
  }

  private checkPercentualGraoArdido(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Percentual_de_Grao_Ardido__c, 0, 100));
  }

  private checkTombamento(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Tombamento__c, 0, 100));
  }

  private checkQuebramento(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Quebramento__c, 0, 100));
  }

  private checkLiquidProductivity(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.PRODUTIVIDADE_LIQ_13__c, 2000, 25000));
  }

  private checkUmidade(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Umidade__c, 10, 40));
  }

  private checkPlatingPopulation(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Populacao_de_Plantio__c, 40, 120));
  }

  private checkEmergencyPopulation(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Populacao_de_Emergencia__c, 40, 120));
  }

  private checkFinalPopulation(): boolean {
    return (DataHelper.validationForRange(this.hybridVisit.Populacao_Final__c, 40, 120));
  }

}
