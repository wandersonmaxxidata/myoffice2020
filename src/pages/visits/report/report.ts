import { Component, ViewChild, forwardRef } from '@angular/core';
import { AlertController, Events, ModalController, ModalOptions, NavParams, ToastController, ViewController } from 'ionic-angular';
import { ServiceProvider } from './../../../providers/service-salesforce';
import { ReportBO } from './../../../shared/BO/ReportBO';
import { ReportDAO } from '../../../shared/DAO/ReportDAO';
import { EventModel } from './../../../app/model/EventModel';
import { Visita__cModel } from './../../../app/model/Visita__cModel';
import { ReportModal } from './../../../components/report/report-modal/report-modal';
import { HeaderComponent } from '../../../components/header/header';
import { AlertHelper } from '../../../shared/Helpers/AlertHelper';

@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {

  public reportBO: ReportBO;
  public reportDAO: ReportDAO;

  public title: string;
  public event: EventModel;
  public visit: Visita__cModel;

  public headerObj: any = {};
  public weed: any = [];
  public plague: any = [];
  public visitType = '';
  public type = {
    pp: false, p: false, ge: false, dv: false, dr: false, c: false,
  };

  public principalMsgObj: any = [];
  public weedRecommendationObj: any = [];
  public plagueRecommendationObj: any = [];
  public handleGeralMsgObj: any = [{ body: [{ subtitle: '', text: '' }] }];

  public msg = {
    handleGeralMsgSession: 'Manejo Geral',
    principalMsgSession: 'Mensagem principal',
    weedMsgSession: 'Manejo de Plantas Daninhas',
    plagueMsgSession: 'Manejo de Pragas',
  };

  public informationObj: any = [];
  public infoWeedObj: any = {};
  public weedObj: any = {};
  public plagueObj: any = {};
  public imageObj: any = [];

  public showInformation = false;
  public showInfoWeed = false;
  public showWeed = false;
  public showTextWeed = false;
  public showPlague = false;
  public showTextPlague = false;
  public showObs = true;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public events: Events,
    public modalCtrl: ModalController,
    public service: ServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
  ) {
    this.event = new EventModel(navParams.data.event);
    this.visit = new Visita__cModel(navParams.data.visit);
    this.reportBO = new ReportBO(this.service);

    this.reportBO.loadTypes(this.event.BR_Tipo_de_Visita__c).then((currentType: any) => {
      this.title = `Relatório de ${currentType.visitType}`;
      this.visitType = currentType.visitType;
      this.type[currentType.type] = true;
    });
  }

  public ionViewDidLoad() {
    this.updateTitle();
    this.setValues().then(() => {
      this.openModal();
      this.setViewsComponents();
    });
  }

  public updateTitle() {
    if (this.header) {
      this.header.updatePageTitle(this.title);
    }
  }

  public openModal() {
    this.reportBO.getHybridsLaunch(this.visitType, this.visit).then((data: any) => {
      console.log('------| OBJ returned (to report modal): ', data);

      if (data && data.length > 0) {
        const options: ModalOptions = {
          enterAnimation: 'modal-md-slide-in',
          leaveAnimation: 'modal-md-slide-out',
        };

        const modal = this.modalCtrl.create(ReportModal, data, options);
        modal.present();
      }
    });
  }

  private setValues() {
    return new Promise((resolve, reject) => {
      this.reportBO.loadHeaderObj(this.event, this.visit).then((headerObj) => {
        console.log('------| headerObj: ', headerObj);
        this.headerObj = headerObj;
        return this.reportBO.loadMsgObj(this.visitType, this.headerObj.brand, this.msg.principalMsgSession);

      }).then((msg__principal) => {
        console.log('------| msg__principal: ', msg__principal);
        if (msg__principal) {
          msg__principal[0].body[0].subtitle = null;
        }
        this.principalMsgObj = msg__principal;
        return this.reportBO.loadMsgObj(this.visitType, this.headerObj.brand, this.msg.weedMsgSession);

      }).then((msg__weed) => {
        console.log('------| msg__weed: ', msg__weed);
        this.weedRecommendationObj = msg__weed;
        return this.reportBO.loadMsgObj(this.visitType, this.headerObj.brand, this.msg.plagueMsgSession);

      }).then((msg__plague) => {
        console.log('------| msg__plague: ', msg__plague);
        this.plagueRecommendationObj = msg__plague;
        return this.reportBO.loadMsgObj(this.visitType, this.headerObj.brand, this.msg.handleGeralMsgSession);

      }).then((msg__geral) => {
        console.log('------| msg__geral: ', msg__geral);
        this.handleGeralMsgObj = msg__geral;
        return this.reportBO.loadInformationObj(this.type, this.visit);

      }).then((informationObj) => {
        console.log('------| informationObj: ', informationObj);
        this.informationObj = informationObj;
        return this.reportBO.loadWeedObj(this.type, this.visit, this.handleGeralMsgObj);

      }).then((weedObj: any) => {
        console.log('------| weedObj: ', weedObj);
        this.infoWeedObj = weedObj.infoWeed;
        this.weedObj = weedObj.weed;
        return this.reportBO.loadPlagueObj(this.type, this.visit);

      }).then((plagueObj) => {
        console.log('------| plagueObj: ', plagueObj);
        this.plagueObj = plagueObj;
        return this.reportBO.loadImageObj(this.visit);

      }).then((imageObj) => {
        this.imageObj = imageObj;
        resolve();
      });
    });
  }

  private finishConfirm() {
    AlertHelper.showConfirmDialogForVisitCompletion(this.alertCtrl, () => {
      this.backPage(true);
    }, () => {
      console.log('cancel');
    });
  }

  private backPage(finished) {
    this.viewCtrl.dismiss(finished);
  }

  private setViewsComponents() {
    this.showInformation = (this.informationObj.length > 0) && (this.type.pp || this.type.p);
    this.showInfoWeed = (this.infoWeedObj && (this.type.ge || this.type.dv || this.type.dr));
    this.showWeed = (this.weedObj.visible && (!this.type.dv && !this.type.dr));
    this.showTextWeed = (this.weedRecommendationObj && !this.type.dv);
    this.showPlague = (this.plagueObj.visible && !this.type.c);
    this.showTextPlague = (this.plagueRecommendationObj && !this.type.c);
  }

}
