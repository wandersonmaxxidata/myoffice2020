import { SoyHarvestInfoHybridModal } from './../../modals/soy-harvest-info-hybrid-modal/soy-harvest-info-hybrid-modal';
import { VisitHybridRecordTypes } from '../../../../shared/Constants/Types';
import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { NotificationKeys } from './../../../../shared/Constants/Keys';
import { LoadingManager } from './../../../../shared/Managers/LoadingManager';
import { ServiceProvider } from './../../../../providers/service-salesforce';
import { AlertController, Events, ModalController, ModalOptions, NavController, NavParams, ToastController } from 'ionic-angular';
import { HeaderComponent } from './../../../../components/header/header';
import { AttachmentComponent } from '../../../../components/attachment/home-attachment';
import { GeolocationComponent } from './../../../../components/geolocation/geolocation';
import { IGeolocation } from './../../../../components/geolocation/IGeolocation';
import { VisitHybridBO } from './../../../../shared/BO/VisitHybridBO';
import { VisitBO, VisitStatus } from './../../../../shared/BO/VisitBO';
import { Visita__cModel } from './../../../../app/model/Visita__cModel';
import { EventModel } from './../../../../app/model/EventModel';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { AlertHelper } from '../../../../shared/Helpers/AlertHelper';
import { NavigationHelper } from '../../../../shared/Helpers/NavigationHelper';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import { ToastHelper } from '../../../../shared/Helpers/ToastHelper';
import { MustEditRegisterHelper } from '../../../../shared/Helpers/MustEditRegisterHelper';

@Component({
  selector: 'page-soy-harvest',
  templateUrl: 'soy-harvest.html',
})
export class SoyHarvestPage {

  public event: EventModel;
  public visit: Visita__cModel = new Visita__cModel(null);
  public cacheVisit;
  public cacheAttachment;

  //MAXXIDATA
  public editStatus: boolean = false;
  public editTxt: string;
  //FIM MAXXIDATA

  public visitBO: VisitBO;
  public visitHybridBO: VisitHybridBO;

  public title: string;
  public isReadOnly: boolean;

  public showInformationData = true;
  public showHybridData = true;

  public hybridIsOk = true;

  public geolocation: IGeolocation;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  @ViewChild(AttachmentComponent)
  private pageHomeAttachment: AttachmentComponent;

  @ViewChild(GeolocationComponent)
  private geolocationComponent: GeolocationComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public events: Events,
  ) {
    this.event = this.navParams.data;
    this.visitBO = new VisitBO(this.service);
    this.visitHybridBO = new VisitHybridBO(this.service);

    LoadingManager.getInstance().show().then(() => {
      this.visitBO.loadVisit(this.event).then((visit: Visita__cModel) => {
        LoadingManager.getInstance().hide();
        this.handleInitialState(visit);
      });
    });
  }

  private ionViewDidEnter() {
    this.cacheVisit = JSON.stringify(this.visit);
    this.cacheAttachment = JSON.stringify(this.pageHomeAttachment.attachments);
  }

  private ionViewCanLeave() {
    //MAXXIDATA
    if (this.navParams.data.editVisitError) {

    } else {

    return new Promise((resolve, reject) => {
      if (!this.isReadOnly && !this.isSaveDisabled() && this.cacheVisit !== JSON.stringify(this.visit) || this.cacheAttachment !== JSON.stringify(this.pageHomeAttachment.attachments)) {
        this.saveVisit().then(() => {
          resolve();
        });
      } else if (this.isSaveDisabled() && this.visit.Status__c !== VisitStatus.completed) {
        AlertHelper.showDialogForVisitAutoSave(this.alertCtrl, () => {
          resolve();
        }, () => {
          reject();
          console.log('cancel');
        });
      } else {
        resolve();
      }
    });
            
  }
  //FIM MAXXIDATA
  }

  private updatePageTitle() {
    if (this.header) {
      this.header.updatePageTitle(this.title);
    }
  }

  private setParentIdToAttachmentView() {
    this.pageHomeAttachment.setParentId(this.visit.getId());
  }

  private handleInitialState(visit: Visita__cModel) {
    this.visit = visit;
    this.visit.Name = 'Colheita - Soja';

    if (this.event.BR_Division__c) {
      this.visit.BR_Division__c = this.event.BR_Division__c;
    }

    if (this.visit.Status__c === VisitStatus.completed || this.visit.Status__c === VisitStatus.canceled) {
       //MAXXIDATA
       //console.log('VISITA STATUS EDITAR')
       //console.log(this.visit.Id)
       //console.log(this.visit.Id.indexOf("Local_"))
       if (this.visit.Id.indexOf("local_")) {
         this.isReadOnly = true;
       } else {
         if (this.visit.Status__c == "Concluída") {
           this.editStatus = true;
           this.editTxt = 'Visita com possibilidade de edição.';
         }
         this.isReadOnly = false;
       }
       //FIM MAXXIDATA
      this.title = 'Colheita - Soja';
    } else {
      this.isReadOnly = false;
      this.title = 'Nova Colheita - Soja';
    }

    this.updatePageTitle();
    this.setParentIdToAttachmentView();
    this.setLocation();

    if (MustEditRegisterHelper.isObjectTreeWithError(this.visit)) { this.isReadOnly = false; }
  }

  private setLocation() {
    this.geolocation = {
      altitude: this.visit.BR_Altitude__c,
      latitude: this.visit.Latitude__c,
      longitude: this.visit.Longitude__c,
    };

    if (this.geolocationComponent) {
      this.geolocationComponent.setLocation(this.geolocation);
    }
  }

  private updateGeolocation(location) {
    if (location) {
      this.visit.BR_Altitude__c = location.altitude;
      this.visit.Latitude__c = location.latitude;
      this.visit.Longitude__c = location.longitude;
    }
  }

  public cancelVisit() {
    AlertHelper.showConfirmDialogForVisitCancellation(this.alertCtrl, () => {
      this.isReadOnly = true;
      this.visit.Status__c = VisitStatus.canceled;
      this.saveVisit().then(() => {
        NavigationHelper.goBackToOrigin(this.navCtrl);
      });
    }, () => {
      console.log('cancel');
    });
  }

  public completeVisit() {
    AlertHelper.showConfirmDialogForVisitCompletion(this.alertCtrl, () => {
      this.isReadOnly = true;
      this.visit.Status__c = VisitStatus.completed;
      this.visit.Data_de_Execucao__c = DateHelper.getCurrentDateString();
      this.saveVisit().then(() => {
        NavigationHelper.goBackToOrigin(this.navCtrl);
      });
    }, () => {
      console.log('cancel');
    });
  }

  private saveVisit(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.visit.EventId__c = this.event.getId();
      let statusChange = false;
      let statusProgress = false;

      if (this.visit.Status__c === VisitStatus.open) {
        this.visit.Status__c = VisitStatus.inProgress;
        statusProgress = true;
      }

      if (statusProgress || this.isReadOnly) {
        statusChange = true;
      }

      LoadingManager.getInstance().show().then(() => {
        this.visitBO.saveVisit(this.event, this.visit).then((savedVisit: Visita__cModel) => {
          this.visit = savedVisit;
          this.cacheVisit = JSON.stringify(this.visit);
          this.cacheAttachment = JSON.stringify(this.pageHomeAttachment.attachments);

          if (statusChange) {
            this.events.publish(NotificationKeys.statusEventSave, this.visit);
          }

          setTimeout(() => {
            LoadingManager.getInstance().hide();
            ToastHelper.showSuccessMessage(this.toastCtrl);
            console.log('---| Success:saveVisit: ', this.visit);
            //MAXXIDATA
            if (this.navParams.data.editVisitError) {
              this.service.visita__cService.deletLogErrorEntities(this.navParams.data.idDataError, this.navParams.data.idData, this.navParams.data.shildId);
            }
            //FIM MAXXIDATA
            resolve();
          }, 500);

        }, (err) => {
          console.log('-*-| visitDAO.saveVisit:error: ', err);
          LoadingManager.getInstance().hide();
        });
      });
    });
  }

  // ************************************************************ //
  // *******************Begin soy harvest custom***************** //
  // ************************************************************ //
  private isSaveDisabled() {
    return !(
      this.hybridIsOk
    );
  }

  // ************************************************************ //
  // ************************Begin Hybrid************************ //
  // ************************************************************ //
  private openHybridModal(index) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Variedade',
      sql: true,
      list: 'HybridList',
      limit: 1,
      division: this.event.BR_Division__c,
    }, options);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        console.log('---| Modal returned data: visitHybrid: ', data);

        this.visitHybridBO.create(VisitHybridRecordTypes.cropSoy).then((visitHybrid) => {
          visitHybrid.BR_VisitMobileId__c = this.visit.getId();
          visitHybrid.Hibrido_Visita__c = data[0].value;
          visitHybrid.BR_HybridName__c = data[0].label;

          this.visit.Hibrido_de_Visita__c.push(visitHybrid);
        });
      }
    });
  }

  private removeHybrid(hybridIndex: number) {
    this.visitHybridBO.delete(this.visit.Hibrido_de_Visita__c, hybridIndex).then((newHybrids) => {
      this.visit.Hibrido_de_Visita__c = newHybrids;
      this.checkHybridsIntegrity();
      console.log('------| Hybrid removed: ', this.visit);
    });
  }

  private openInfoHybrid(index) {
    const visitInfo = {
      hybrid: this.visit.Hibrido_de_Visita__c[index],
      isReadOnly: this.isReadOnly,
    };

    const modal = this.modalCtrl.create(SoyHarvestInfoHybridModal, visitInfo);
    modal.present();

    modal.onWillDismiss((output) => {
      if (output != null) {
        this.visit.Hibrido_de_Visita__c[index] = output;
        this.visit.Hibrido_de_Visita__c[index].BR_MobileAppFlag__c = true;
        this.visit.Hibrido_de_Visita__c[index].BR_VisitMobileId__c = this.visit.getId();
      }
    });
  }

  private checkHybridsIntegrity(index?: number) {
    this.hybridIsOk = true;

    const item = this.visit.Hibrido_de_Visita__c;
    const length = (item && index) ? item.length : (index + 1);
    let i = index ? index : 0;

    const requiredFields = (item[i].Area_Colhida__c && item[i].PRODUTIVIDADE_LIQ_13__c && item[i].BR_Lodging__c && item[i].Umidade__c);

    for (i; i < length; i++) {
      if (!requiredFields) {
        this.hybridIsOk = false;
      } else {
        return this.hybridIsOk;
      }
    }
  }

}
