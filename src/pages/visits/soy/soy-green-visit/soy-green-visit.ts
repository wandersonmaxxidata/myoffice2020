import { LoadingManager } from './../../../../shared/Managers/LoadingManager';
import { Events } from 'ionic-angular/util/events';
import { NotificationKeys } from './../../../../shared/Constants/Keys';
import { ToastController } from 'ionic-angular';
import { DiseaseModal } from './../../modals/add-disease/add-disease';
import { Doenca__cModel } from './../../../../app/model/Doenca__cModel';
import { BR_WeedHandling__cModel } from './../../../../app/model/BR_WeedHandling__cModel';
import { Praga__cModel } from './../../../../app/model/Praga__cModel';
import { PlagueModal } from './../../modals/add-plague/add-plague';
import { MultipicklistModal } from '../../../../components/modals/modal-multipicklist/modal-multipicklist';
import { AlertController } from 'ionic-angular';
import { VisitHybridRecordTypes } from '../../../../shared/Constants/Types';
import { VisitBO, VisitStatus } from './../../../../shared/BO/VisitBO';
import { VisitHybridBO } from './../../../../shared/BO/VisitHybridBO';
import { WeedHandlingBO } from './../../../../shared/BO/WeedHandlingBO';
import { DiseaseBO } from './../../../../shared/BO/DiseaseBO';
import { PlagueBO } from './../../../../shared/BO/PlagueBO';
import { ServiceProvider } from './../../../../providers/service-salesforce';
import { Visita__cModel } from './../../../../app/model/Visita__cModel';
import { EventModel } from './../../../../app/model/EventModel';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { ModalController, ModalOptions, NavController, NavParams } from 'ionic-angular';
import { DateHelper } from '../../../../shared/Helpers/DateHelper';
import { IGeolocation } from './../../../../components/geolocation/IGeolocation';
import { HeaderComponent } from '../../../../components/header/header';
import { AttachmentComponent } from '../../../../components/attachment/home-attachment';
import { GeolocationComponent } from '../../../../components/geolocation/geolocation';
import { AlertHelper } from '../../../../shared/Helpers/AlertHelper';
import { NavigationHelper } from '../../../../shared/Helpers/NavigationHelper';
import { WeedHandlingModal } from '../../modals/add-weed-handling/add-weed-handling';
import { ToastHelper } from '../../../../shared/Helpers/ToastHelper';
import { MustEditRegisterHelper } from '../../../../shared/Helpers/MustEditRegisterHelper';

@Component({
  selector: 'soy-green-visit',
  templateUrl: 'soy-green-visit.html',
})
export class SoyGreenVisitPage {

  public event: EventModel;
  public visit: Visita__cModel = new Visita__cModel(null);
  public cacheVisit;
  public cacheAttachment;

  //MAXXIDATA
  public editStatus: boolean = false;
  public editTxt: string;
  //FIM MAXXIDATA

  public visitBO: VisitBO;
  public visitHybridBO: VisitHybridBO;
  public plagueBO:  PlagueBO;
  public diseaseBO: DiseaseBO;
  public weedHandlingBO: WeedHandlingBO;

  public title: string;
  public isReadOnly: boolean;

  public showHybrid       = true;
  public showWeedHandling = true;

  public geolocation: IGeolocation;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  @ViewChild(AttachmentComponent)
  private pageHomeAttachment: AttachmentComponent;

  @ViewChild(GeolocationComponent)
  private geolocationComponent: GeolocationComponent;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public events: Events,
  ) {
    //MAXXIDATA
    if (this.navParams.data.editVisitError) {
      this.event = this.navParams.data.event;
    } else {
      this.event = this.navParams.data;
    }
    //FIM MAXXIDATA
    this.visitBO = new VisitBO(this.service);
    this.visitHybridBO = new VisitHybridBO(this.service);
    this.plagueBO = new PlagueBO(this.service);
    this.diseaseBO = new DiseaseBO(this.service);
    this.weedHandlingBO = new WeedHandlingBO(this.service);

    LoadingManager.getInstance().show().then(() => {
      this.visitBO.loadVisit(this.event).then((visit: Visita__cModel) => {
        LoadingManager.getInstance().hide();
        this.handleInitialState(visit);
      });
    });
  }

  private ionViewDidEnter() {
    this.cacheVisit = JSON.stringify(this.visit);
    this.cacheAttachment = JSON.stringify(this.pageHomeAttachment.attachments);
  }

  private ionViewCanLeave() {
    //MAXXIDATA
    if (this.navParams.data.editVisitError) {

    } else {

    return new Promise((resolve, reject) => {
      if (!this.isReadOnly && !this.isSaveDisabled() && this.cacheVisit !== JSON.stringify(this.visit) || this.cacheAttachment !== JSON.stringify(this.pageHomeAttachment.attachments)) {
        this.saveVisit().then(() => {
          resolve();
        });
      } else if (this.isSaveDisabled() && this.visit.Status__c !== VisitStatus.completed) {
        AlertHelper.showDialogForVisitAutoSave(this.alertCtrl, () => {
          resolve();
        }, () => {
          reject();
          console.log('cancel');
        });
      } else {
        resolve();
      }
    });
        
  }
  //FIM MAXXIDATA
  }

  private updatePageTitle() {
    if (this.header) {
      this.header.updatePageTitle(this.title);
    }
  }

  private setParentIdToAttachmentView() {
    this.pageHomeAttachment.setParentId(this.visit.getId());
  }

  private handleInitialState(visit: Visita__cModel) {
    this.visit = visit;
    this.visit.Name = 'Visita Verde';

    if (this.event.BR_Division__c) {
      this.visit.BR_Division__c = this.event.BR_Division__c;
    }

    if (this.visit.Status__c === VisitStatus.completed || this.visit.Status__c === VisitStatus.canceled) {
       //MAXXIDATA
       if (this.visit.Id.indexOf("local_") && this.visit.GerDemandaId__c == null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === false) {
        console.log('VISITA STATUS EDITAR')
         this.isReadOnly = true;
       } else if(this.visit.GerDemandaId__c != null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === true ) {
          if (this.visit.Status__c == VisitStatus.completed) {
            this.editStatus = true;
            this.editTxt = 'Visita com possibilidade de edição.';
          }
          this.isReadOnly = false;
        }else if(this.visit.GerDemandaId__c != null && this.visit.__locally_created__ === false && this.visit.__locally_updated__ === false ) {
          this.isReadOnly = true;
       }else {
         if (this.visit.Status__c == VisitStatus.completed) {
           this.editStatus = true;
           this.editTxt = 'Visita com possibilidade de edição.';
         }
         this.isReadOnly = false;
       }
       //FIM MAXXIDATA
      this.title = 'Visita Verde';
    } else {
      this.isReadOnly = false;
      this.title = 'Nova Visita Verde';
    }

    this.updatePageTitle();
    this.setParentIdToAttachmentView();
    this.setLocation();

    if (MustEditRegisterHelper.isObjectTreeWithError(this.visit)) { this.isReadOnly = false; }
  }

  private setLocation() {
    this.geolocation = {
      altitude: this.visit.BR_Altitude__c,
      latitude: this.visit.Latitude__c,
      longitude: this.visit.Longitude__c,
    };

    if (this.geolocationComponent) {
      this.geolocationComponent.setLocation(this.geolocation);
    }
  }

  private updateGeolocation(location) {
    if (location) {
      this.visit.BR_Altitude__c = location.altitude;
      this.visit.Latitude__c = location.latitude;
      this.visit.Longitude__c = location.longitude;
    }
  }

  public cancelVisit() {
    AlertHelper.showConfirmDialogForVisitCancellation(this.alertCtrl, () => {
      this.isReadOnly = true;
      this.visit.Status__c = VisitStatus.canceled;
      this.saveVisit().then(() => {
        NavigationHelper.goBackToOrigin(this.navCtrl);
      });
    }, () => {
      console.log('cancel');
    });
  }

  public completeVisit() {
    AlertHelper.showConfirmDialogForVisitCompletion(this.alertCtrl, () => {
      this.isReadOnly = true;
      this.visit.Status__c = VisitStatus.completed;
      this.visit.Data_de_Execucao__c = DateHelper.getCurrentDateString();
      this.saveVisit().then(() => {
        NavigationHelper.goBackToOrigin(this.navCtrl);
      });
    }, () => {
      console.log('cancel');
    });
  }

  private saveVisit(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.visit.EventId__c = this.event.getId();
      let statusChange = false;
      let statusProgress = false;

      if (this.visit.Status__c === VisitStatus.open) {
        this.visit.Status__c = VisitStatus.inProgress;
        statusProgress = true;
      }

      if (statusProgress || this.isReadOnly) {
        statusChange = true;
      }

      LoadingManager.getInstance().show().then(() => {
        this.visitBO.saveVisit(this.event, this.visit).then((savedVisit: Visita__cModel) => {
          this.visit = savedVisit;
          this.cacheVisit = JSON.stringify(this.visit);
          this.cacheAttachment = JSON.stringify(this.pageHomeAttachment.attachments);


          if (statusChange) {
            this.events.publish(NotificationKeys.statusEventSave, this.visit);
          }

          setTimeout(() => {
            LoadingManager.getInstance().hide();
            ToastHelper.showSuccessMessage(this.toastCtrl);
            console.log('---| Success:saveVisit: ', this.visit);
            //MAXXIDATA
            if (this.navParams.data.editVisitError) {
              this.service.visita__cService.deletLogErrorEntities(this.navParams.data.idDataError, this.navParams.data.idData, this.navParams.data.shildId);
            }
            //FIM MAXXIDATA
            resolve();
          }, 500);

        }, (err) => {
          console.log('-*-| visitDAO.saveVisit:error: ', err);
          LoadingManager.getInstance().hide();
        });
      });
    });
  }

  // ************************************************************ //
  // ******************Begin green visit custom****************** //
  // ************************************************************ //
  private isSaveDisabled() {
    return !(
      true // campos obrigatórios (true == não há)
    );
  }

  // ************************************************************ //
  // ************************Begin Hybrid************************ //
  // ************************************************************ //
  private openHybridModal() {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(MultipicklistModal, {
      title: 'Adicionar Variedade',
      sql: true,
      list: 'HybridList',
      limit: 1,
      division: this.event.BR_Division__c,
    }, options);
    modal.present();

    modal.onDidDismiss((data: any) => {
      if (data) {
        this.visitHybridBO.create(VisitHybridRecordTypes.soyGreenVisit).then((visitHybrid) => {
          visitHybrid.BR_VisitMobileId__c = this.visit.getId();
          visitHybrid.Hibrido_Visita__c = data[0].value;
          visitHybrid.BR_HybridName__c = data[0].label;

          this.visit.Hibrido_de_Visita__c.push(visitHybrid);
          console.log('------| Hybrid added: ', this.visit);
        });
      }
    });
  }

  private removeHybrid(hybridIndex: number) {
    this.visitHybridBO.delete(this.visit.Hibrido_de_Visita__c, hybridIndex).then((newHybrids) => {
      this.visit.Hibrido_de_Visita__c = newHybrids;
      console.log('------| Hybrid removed: ', this.visit);
    });
  }

  // ************************************************************ //
  // ************************Begin Plague************************ //
  // ************************************************************ //
  private openPlagueModal(h_index: number, p_index: number) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const plagueObj = this.visit.Hibrido_de_Visita__c[h_index].Plagues[p_index]
      ? this.visit.Hibrido_de_Visita__c[h_index].Plagues[p_index]
      : null;

    const modal = this.modalCtrl.create(PlagueModal, {
      page: 'soy-green-visit',
      plague: plagueObj,
      isReadOnly: this.isReadOnly,
    }, options);
    modal.present();

    modal.onDidDismiss((plague: Praga__cModel) => {
      if (plague) {
        plague.Hibrido_de_Visita__c = this.visit.Hibrido_de_Visita__c[h_index].getId();
        this.visit.Hibrido_de_Visita__c[h_index].Plagues.push(plague);
        console.log('------| Plague added: ', this.visit);
      }
    });
  }

  private removePlague(h_index: number, p_index: number) {
    this.plagueBO.delete(this.visit.Hibrido_de_Visita__c[h_index].Plagues, p_index).then((newPlagues) => {
      this.visit.Hibrido_de_Visita__c[h_index].Plagues = newPlagues;
      console.log('------| Plague removed: ', this.visit);
    });
  }

  // ************************************************************ //
  // ************************Begin Disease*********************** //
  // ************************************************************ //
  private openDiseaseModal(h_index: number, d_index: number) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const diseaseObj = this.visit.Hibrido_de_Visita__c[h_index].Diseases[d_index]
      ? this.visit.Hibrido_de_Visita__c[h_index].Diseases[d_index]
      : null;

    const modal = this.modalCtrl.create(DiseaseModal, {
      disease: diseaseObj,
      isReadOnly: this.isReadOnly,
    }, options);
    modal.present();

    modal.onDidDismiss((disease: Doenca__cModel) => {
      if (disease) {
        disease.Hibrido_de_Visita__c = this.visit.Hibrido_de_Visita__c[h_index].getId();
        this.visit.Hibrido_de_Visita__c[h_index].Diseases.push(disease);
        console.log('------| Disease added: ', this.visit);
      }
    });
  }

  private removeDisease(h_index: number, d_index: number) {
    this.diseaseBO.delete(this.visit.Hibrido_de_Visita__c[h_index].Diseases, d_index).then((newDisease) => {
      this.visit.Hibrido_de_Visita__c[d_index].Diseases = newDisease;
      console.log('------| Disease removed: ', this.visit);
    });
  }

  // ************************************************************ //
  // *********************Begin WeedHandling********************* //
  // ************************************************************ //
  private openWeedHandlingModal(w_index: number) {
    const options: ModalOptions = {
      enterAnimation: 'modal-md-slide-in',
      leaveAnimation: 'modal-md-slide-out',
    };

    const modal = this.modalCtrl.create(WeedHandlingModal, {
      page: 'soy-green-visit',
      weedHandling: this.visit.BR_WeedHandling__c[w_index],
      isReadOnly: this.isReadOnly,
    }, options);
    modal.present();

    modal.onDidDismiss((weedHandling: BR_WeedHandling__cModel) => {
      if (weedHandling) {
        weedHandling.BR_Visit__c = this.visit.getId();

        if (w_index !== null && w_index !== undefined) {
          this.visit.BR_WeedHandling__c[w_index] = weedHandling;
        } else {
          this.visit.BR_WeedHandling__c.push(weedHandling);
        }
        console.log('------| WeedHandling added: ', this.visit);
      }
    });
  }

  private removeWeedHandling(w_index: number) {
    this.weedHandlingBO.delete(this.visit.BR_WeedHandling__c, w_index).then((newWeedHandling) => {
      this.visit.BR_WeedHandling__c = newWeedHandling;
      console.log('------| WeedHandling removed: ', this.visit);
    });
  }

}
