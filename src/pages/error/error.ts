import { Component } from '@angular/core';
import { Events, NavController } from 'ionic-angular';

/**
 * Página com listagem dos registros do salesforce
 */
@Component({
    selector: 'error',
    templateUrl: 'error.html',
})
export class ErrorPage {

    /**
     * Constructor com a chamada dos métodos de registro da tabela da base de dados, sincronização com salesforce e chamada dos registros locais.
     * @param navCtrl
     * Recurso do próprio Angular que permite a navegação entre páginas
     */

    constructor(public navCtrl: NavController,
        public events: Events,
    ) {
    }

}
