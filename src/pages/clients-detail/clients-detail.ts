import { ClientBO } from '../../shared/BO/ClientBO';
import { ClientsModel } from '../../app/model/ClientsModel';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { HeaderComponent } from '../../components/header/header';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { RecordTypeDeveloperName } from '../../shared/Constants/Types';
import { ServiceProvider } from '../../providers/service-salesforce';

@Component({
  selector: 'clients-detail',
  templateUrl: 'clients-detail.html',
})
export class ClientsDetailPage {

  public client: ClientsModel;
  public tabType = 'information';
  public clientBO: ClientBO;

  public isFromAssociated = false;
  public associatedClientIdToReturn = '';
  public hasAssociatedFarmer: boolean;

  @ViewChild(forwardRef(() => HeaderComponent))
  public header: HeaderComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public events: Events,
  ) {
    this.clientBO = new ClientBO(this.service);
  }

  public ionViewWillEnter() {
    LoadingManager.getInstance().show().then(() => {
      this.client = new ClientsModel(this.navParams.data);
      console.log('---| ClientsDetailPage:client: ', this.client);

      this.loadProfileMapping().then(() => {
        this.loadValues(this.navParams.data).then(() => {
          LoadingManager.getInstance().hide();
        });
      });
    });
  }

  public loadValues(data: any) {
    return new Promise((resolve) => {
      if (this.client.RecordType.Name === 'Fazenda') {
        this.clientBO.farmerByParentId(data.ParentId).then((result: any) => {
          this.hasAssociatedFarmer = (result && result.length > 0) ? false : true;
        });
      } else {
        this.hasAssociatedFarmer = false;
      }
      if (data) {
        this.isFromAssociated = data.isFromAssociated;
      }
      if (this.isFromAssociated === true) {
        this.associatedClientIdToReturn = data.associatedClientIdToReturn;
      }

      this.updatePageTitle(this.tabType);
      resolve();
    });
  }

  public swipe(event) {
    if (event.direction === 2 && this.tabType === 'information') {
      this.tabType = 'contacts';
    } else if (event.direction === 2 && this.tabType === 'contacts') {
      this.tabType = 'profile_mapping';
    } else if (event.direction === 2 && this.tabType === 'profile_mapping') {
      this.tabType = 'visits';
    } else if (event.direction === 4 && this.tabType === 'visits') {
      this.tabType = 'profile_mapping';
    } else if (event.direction === 4 && this.tabType === 'profile_mapping') {
      this.tabType = 'contacts';
    } else if (event.direction === 4 && this.tabType === 'contacts') {
      this.tabType = 'information';
    }
  }

  private updatePageTitle(tabType: string) {
    let title = '';
    switch (tabType) {
      case 'information':
        title = this.getPageTitleBasedOnRecordTypeName();
        break;
      case 'contacts':
        title = this.client.Name;
        break;
      case 'profile_mapping':
        title = 'Mapeamento de Perfil';
        break;
      case 'visits':
        title = 'Visitas';
        break;
      default:
        title = '';
        break;
    }
    if (title) {
      this.header.updatePageTitle(title);
    }
  }

  private getPageTitleBasedOnRecordTypeName(): string {
    let title = 'Detalhes ';
    switch (this.client.RecordType.DeveloperName) {
      case RecordTypeDeveloperName.farmer:
        title += 'do Agricultor';
        break;
      case RecordTypeDeveloperName.farm:
        title += 'da Fazenda';
        break;
      case RecordTypeDeveloperName.channel:
        title += 'do Canal';
        break;
      default:
        title = '';
        break;
    }
    return title;
  }

  public loadProfileMapping() {
    return new Promise((resolve, reject) => {
      this.client.setBR_ProfileMapping__c(this.service.getBR_ProfileMapping__cService()).then(() => {
        resolve();
      }).catch((err) => {
        resolve();
      });
    });
  }

}
