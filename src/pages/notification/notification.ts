import { AppPreferences } from '@ionic-native/app-preferences';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { EventBO } from '../../shared/BO/EventBO';
import { Events, NavController, Tabs } from 'ionic-angular';
import { EventsPage } from '../events/events';
import { HeaderComponent } from '../../components/header/header';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { NotificationBO } from './../../shared/BO/NotificationBO';
import { NotificationKeys } from './../../shared/Constants/Keys';
import { ServiceProvider } from './../../providers/service-salesforce';
import { TabHelper } from '../../shared/Helpers/TabHelper';

@Component({
  selector: 'notification-page',
  templateUrl: 'notification.html',
  providers: [ServiceProvider],
})
export class NotificationPage {

  public tab: Tabs;

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  public eventBO: EventBO;
  public notificationBO: NotificationBO;
  public notifications: any[] = [];

  constructor(
    public serviceProvider: ServiceProvider,
    public navCtrl: NavController,
    public appPreferences: AppPreferences,
    public events: Events,
  ) {
    this.eventBO = new EventBO(this.serviceProvider);
    this.notificationBO = new NotificationBO(this.serviceProvider);
  }

  public ionViewWillEnter() {
    TabHelper.enableAll(this.navCtrl);
    this.loadNotifications().then(() => {
      this.events.publish(NotificationKeys.totalNotifications, this.notifications.length);
    });
  }

  public ngOnInit() {
    if (this.header) {
      this.header.updatePageTitle('Notificações');
    }
  }

  public loadNotifications() {
    return new Promise((resolve) => {
      LoadingManager.getInstance().show().then(() => {
        this.appPreferences.fetch('user_id').then((user_id) => {
          this.notificationBO.load(user_id).then((data: any) => {
            this.notifications = data;
            LoadingManager.getInstance().hide();
            resolve();
          });
        });
      });
    });
  }

  public openEventDetail(index: number) {
    LoadingManager.getInstance().show().then(() => {
      setTimeout(() => {
        this.eventBO.eventDetail(this.notifications[index]).then((event) => {
          this.navCtrl.push(EventsPage, event).then(() => {
            LoadingManager.getInstance().hide();
          });
        });
      }, 1500);
    });
  }

}
