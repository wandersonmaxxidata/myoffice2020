import { HeaderComponent } from './../../components/header/header';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { DateHelper } from '../../shared/Helpers/DateHelper';

@Component({
  selector: 'page-outlook',
  templateUrl: 'outlook.html',
})
export class OutlookPage {

  public showEventDuration = true;
  public isEditMode = {
    geral: false,
    client: false,
    harvest: false,
    duration: false,
  };

  public EndDateTime = '';
  public StartDateTime = '';
  public subject = '';
  public description = '';
  public location = '';
  public lembrete = '';

  public saveNew;
  public saveEdit;
  public isAllDay: boolean;
  public year = moment().year();

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(public navCtrl: NavController, public navParams: NavParams, evt: Events) {
    if (this.navParams.data) {
      this.loadVariables(this.navParams.data);
    }
  }

  public ngOnInit() {
    if (this.header) {
      this.header.updatePageTitle('Evento - Outlook');
    }
  }

  private checkConditionEdit(option) {
    return (this.saveEdit || this.saveNew) && this.isEditMode[option];
  }

  private loadVariables(data) {

    if (DateHelper.isAllDayEventNotAdjusted(data.event)) {
      data.event.StartDateTime = DateHelper.adjustAllDayEventTimezone(data.event.StartDateTime);
      data.event.EndDateTime = DateHelper.adjustAllDayEventTimezone(data.event.EndDateTime);
    }

    this.lembrete = data.event.Lembrete__c;
    this.isAllDay = data.event.IsAllDayEvent;
    this.EndDateTime = moment(data.event.EndDateTime).format();
    this.StartDateTime = moment(data.event.StartDateTime).format();
    this.subject = data.event.Subject;
    this.description = data.event.Observacoes__c;
    this.location = data.event.Location;

  }

  private showEventDurationButton() {
    this.showEventDuration = !this.showEventDuration;
  }

}
