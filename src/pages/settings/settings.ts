import { AlertController, Events, NavController, Platform } from 'ionic-angular';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { AppPreferences } from '@ionic-native/app-preferences';
import { AppVersion } from '@ionic-native/app-version';
import { AutenticateService } from '../../providers/autenticate/autenticate-service-salesforce';
import { Component, ViewChild, forwardRef } from '@angular/core';
import { DateHelper } from '../../shared/Helpers/DateHelper';
import { Device } from '@ionic-native/device';
import { HeaderComponent } from '../../components/header/header';
import { LoadingManager } from './../../shared/Managers/LoadingManager';
import { LocalStorageKeys } from '../../shared/Constants/Keys';
import { NotificationBO } from './../../shared/BO/NotificationBO';
import { ServiceProvider } from './../../providers/service-salesforce';
import { Storage } from '@ionic/storage';
import { TabHelper } from '../../shared/Helpers/TabHelper';
import { UserModel } from './../../app/model/UserModel';

@Component({
  selector: 'settings-page',
  templateUrl: 'settings.html',
  providers: [ServiceProvider, Device],
})
export class SettingsPage {
  public settingsMarca: any;
  public settingsRegional: any;
  public settingsDistrict: any;
  public settingsName: any;

  public notificationBO: NotificationBO;
  public error: any;
  public platform: any;
  public versionNumber: any;
  public platformName: any;
  public platformVersion: any;

  public lastSynchronismDateMessage: string;
  public currentDateMessage: string;

  public user: any[] = [];
  public context: SettingsPage = this;
  public sortBy = { field: 'Name', label: 'Name' };
  public space = ' ';
  public currentUserId;

  public userId = {
    user: this.currentUserId,
  };

  @ViewChild(forwardRef(() => HeaderComponent))
  private header: HeaderComponent;

  constructor(
    public service: ServiceProvider,
    public navCtrl: NavController,
    public appVersion: AppVersion,
    public plt: Platform,
    public auth: AutenticateService,
    public appPreferences: AppPreferences,
    public alertCtrl: AlertController,
    public events: Events,
    public device: Device,
    public storage: Storage,
  ) {
    this.notificationBO = new NotificationBO(this.service);

    this.appVersion.getVersionNumber().then((version) => {
      this.versionNumber = version;
    });

    this.handleCurrentDatePresentation();
    this.handleLastSynchronismPresentation();
    this.service.autenticateService.getCurrentUser()
      .then((sucess) => {
        this.currentUserId = sucess.userId;
        console.log(sucess, ' ------------- autenticateService');
      }).
      catch((error) => {
        console.log(error);
      });

    console.log(this.plt.version());

    if (this.plt.is('android')) {
      this.platformName = 'Android';
      this.platformVersion = this.plt.version().str;
    }
    if (this.plt.is('ios')) {
      this.platformName = 'iOS';
      this.platformVersion = this.device.version;
    }

  }

  public ngOnInit() {
    if (this.header) {
      this.header.updatePageTitle('Ajustes');
    }
  }

  public ionViewDidEnter() {
    this.appPreferences.fetch('user_name').then((userName) => {
      this.settingsName = userName;
    });
    this.appPreferences.fetch('user_district').then((userDistrict) => {
      this.settingsDistrict = userDistrict;
    });
    this.appPreferences.fetch('user_regional').then((userRegional) => {
      this.settingsRegional = userRegional;
    });
    this.appPreferences.fetch('user_brand').then((userBrand) => {
      this.settingsMarca = userBrand;
    });
    TabHelper.enableAll(this.navCtrl);
  }

  public showAlertCancelSync() {
    this.notificationBO.getCountSyncBadge().then((countSyncBadge: number) => {
      AlertHelper.showConfirmLogoutAlert(this.alertCtrl, () => {
        this.logout();
      }, () => {
        console.log('cancel');
      }, countSyncBadge);
    });
  }

  public logout() {
    TabHelper.disableAll(this.navCtrl);
    LoadingManager.getInstance().show(60000).then(() => {
      const promises: Array<Promise<any>> = [];

      Object.keys(this.service).forEach((i) => {
        const promise = (i !== 'autenticateService') ? this.service[i].clearSoup() : Promise.resolve();
        promises.push(promise);
      });

      Promise.all(promises).then(() => {
        this.appPreferences.clearAll().then(() => {
          return this.storage.clear();
        }).then(() => {
          return localStorage.clear();
        }).then(() => {
          return this.auth.logoutSf();
        }).then(() => {
          console.log('---| Success:SettingsPage:logout: finished');
        });
      }).catch((err) => {
        console.log('---| Error:SettingsPage:logout: ', err);
        LoadingManager.getInstance().hide();
        TabHelper.enableAll(this.navCtrl);
        AlertHelper.showErrorLogout(this.alertCtrl);
      });
    });
  }

  public success(value) {
    console.log('---| SettingsPage:logout:value: ', value);
    console.log(value);
  }

  public err(error) {
    console.log('-*-| SettingsPage:logout:error: ', error);
    console.log(error);
  }

  public handleProgress(progress: number) {
    console.log('CURRENT PROGRESS ----- ', progress);
  }

  public ionViewWillLeave() {
    LoadingManager.getInstance().hide();
  }

  public execStandardQuery() {
    this.queryExactFromSoupUser(this.currentUserId).then((response) => {
      this.user = [];
      response.currentPageOrderedEntries.forEach((item) => {
        const push = new UserModel(item);
        this.user.push(push);
      });
      console.log(response);
      LoadingManager.getInstance().hide();
    }).catch((err) => {
      this.error(err);
      LoadingManager.getInstance().hide();
    });
  }

  public queryExactFromSoupUser(userId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.service.getUserService().queryExactFromSoup('BR_UserId__c', userId, 1, 'ascending', 'BR_UserId__c', (response) => {
        resolve(response);
      }, (err) => {
        this.error(err);
      });
    });
  }

  private handleCurrentDatePresentation() {
    console.log('---| CurrentDate toLocaleString: ', new Date().toLocaleString());
    console.log('---| CurrentDate toLocaleDateString: ', new Date().toLocaleDateString());
    console.log('---| CurrentDate toLocaleTimeString: ', new Date().toLocaleTimeString());

    const currentDate = new Date();
    const formattedDate = DateHelper.formattedCurrentDate(currentDate);
    this.currentDateMessage = formattedDate;
  }

  private handleLastSynchronismPresentation() {
    const lastSyncDateString = localStorage.getItem(LocalStorageKeys.lastSyncDateKey);
    const lastSyncDate: Date = new Date(lastSyncDateString);
    const formattedDate = DateHelper.formattedLastSyncDate(lastSyncDate);
    this.lastSynchronismDateMessage = formattedDate;
  }

}
