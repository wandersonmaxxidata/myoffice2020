import { ClientsModel } from './../../app/model/ClientsModel';
import { AlertController, ModalOptions, NavController, NavParams, Platform } from 'ionic-angular';
import { AlertHelper } from '../../shared/Helpers/AlertHelper';
import { AppPreferences } from '@ionic-native/app-preferences';
import { BR_Sales_Coaching__cModel } from '../../app/model/BR_Sales_Coaching__cModel';
import { ClientsDetailPage } from '../clients-detail/clients-detail';
import { Component, NgZone, ViewChild, forwardRef } from '@angular/core';
import { CornDayDPage } from '../visits/corn/corn-day-d/corn-day-d';
import { CornGerminationEmergencyPage } from '../visits/corn/corn-germination-emergency/corn-germination-emergency';
import { CornHarvestPage } from '../visits/corn/corn-harvest/corn-harvest';
import { CornPlantingCreatePage } from '../visits/corn/corn-planting-create/corn-planting-create';
import { CornPrePlantingCreatePage } from '../visits/corn/corn-pre-planting-create/corn-pre-planting-create';
import { CornReproductiveDevelopmentPage } from '../visits/corn/corn-reproductive-development/corn-reproductive-development';
import { CornVegetativeDevelopmentPage } from '../visits/corn/corn-vegetative-development/corn-vegetative-development';
import { CornVisitEventPage } from '../visits/corn/corn-visit-event/corn-visit-event';
import { CropClimatePage } from '../visits/crop/crop-climate/crop-climate';
import { CropComercialPage } from '../visits/crop/crop-comercial/crop-comercial';
import { CropEventPage } from '../visits/crop/crop-event/crop-event';
import { DataHelper } from '../../shared/Helpers/DataHelper';
import { DateHelper } from '../../shared/Helpers/DateHelper';
import { EventBO } from '../../shared/BO/EventBO';
import { EventDeveloperName, EventDivision, RecordTypeDeveloperName, SalesCoachingRecordTypes } from '../../shared/Constants/Types';
import { EventModel } from '../../app/model/EventModel';
import { Events, ModalController } from 'ionic-angular';
import { EventStatus } from '../../shared/BO/EventBO';
import { HeaderComponent } from '../../components/header/header';
import { LoadingManager } from '../../shared/Managers/LoadingManager';
import { ManagerEmailsModal } from '../../components/modals/modal-manager-emails/modal-manager-emails';
import { MustEditRegisterHelper } from '../../shared/Helpers/MustEditRegisterHelper';
import { NotificationKeys } from '../../shared/Constants/Keys';
import { SafraModel } from '../../app/model/SafraModel';
import { SalesCoachingBO, SalesCoachingStatus } from '../../shared/BO/SalesCoachingBO';
import { SearchClientModal } from '../../components/modals/modal-search-client/modal-search-client';
import { ServiceProvider } from '../../providers/service-salesforce';
import { SoyGreenVisitPage } from '../visits/soy/soy-green-visit/soy-green-visit';
import { SoyHarvestPage } from '../visits/soy/soy-harvest/soy-harvest';
import { SoyPlantingVisitPage } from '../visits/soy/soy-planting-visit/soy-planting-visit';
import { Storage } from '@ionic/storage';
import { TabHelper } from '../../shared/Helpers/TabHelper';
import { ToastController } from 'ionic-angular';
import { ToastHelper } from '../../shared/Helpers/ToastHelper';
import { UserBO } from '../../shared/BO/UserBO';
import { Visita__cModel } from '../../app/model/Visita__cModel';
import { VisitBO, VisitStatus } from '../../shared/BO/VisitBO';
import { VisitRecordTypes } from '../../shared/Constants/Types';
import { VisitSalesCoachingPage } from '../visits-sales-coaching/visits-sales-coaching';
import { WindowStorageHelper } from '../../shared/Helpers/WindowStorageHelper';
import * as moment from 'moment';

@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

  public visit: Visita__cModel;
  public selectedsContacts = [];
  public contacts: any[];
  public isRC: boolean;
  public filter_Division = [];

  public event: EventModel = new EventModel(null);
  public userBO: UserBO;
  public eventBO: EventBO;
  public visitBO: VisitBO;
  public salesCoaching: BR_Sales_Coaching__cModel;
  public salesCoachingBO: SalesCoachingBO;
  public userProfile: any;

  public isRTV: boolean;
  public sortBy = { field: 'Status__c', label: 'Status__c' };
  public client: ClientsModel;
  public clientsName: string;
  public farmName: string;

  public isGR: boolean;
  public rtvName: string;
  public visitTitle: string;
  public visitCoaching: string;
  public isOnline = true;
  public clientTitle: string;
  public durationTitle: string;

  public Data_do_Plantio__c = '';
  public StartDateTime = '';
  public EndDateTime = '';
  public year = moment().year();

  public isStart: boolean;
  public isCancel: boolean;
  public isReadOnly: boolean;
  public isEditMode = {
    geral: false,
    client: false,
    harvest: false,
    duration: false,
  };
  public saveNew: boolean;
  public saveEdit: boolean;
  public saveEventFromVisit;
  public isAllDay = false;

  public objWithError = false;
  public BR_EmailContactList__c;

  public cropOption = false;
  public contactList = [];
  public contactSelected;

  public farmToVisit = false;
  public farmNameToVisit = '';
  public farmerNameToVisit = '';

  public isFarmer = false;
  public farmList = [];
  public farmSelected;

  public farmerSelected: boolean;
  public isChannel = false;

  public title = '';
  public updateTitle = true;
  public buttonVisit = '';

  public showEventVisit = true;
  public showSubjectEvent = true;
  public showEventClient = true;
  public showEventHarvest = true;
  public showEventDuration = true;

  public BR_Tipo_de_Visita__c = [];
  public Tipo_Visita_Filter = [];
  public BR_Division__c = [];
  public Tipo_de_Geracao_de_Demanda__c = [];
  public filter_Geracao_de_Demanda = [];
  public Lembrete__c = [];
  public cancelOptions = [];
  public safras: SafraModel[] = [];

  private cacheEvent;
  private cacheStartTime;
  private cacheEndTime;

  public editTxt: string;
  public editEventTemp:boolean = false;

  @ViewChild(forwardRef(() => HeaderComponent))
  public header: HeaderComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    public modalCtrl: ModalController,
    public events: Events,
    public platform: Platform,
    public zone: NgZone,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public appPreferences: AppPreferences,
    public storage: Storage,
  ) {
    this.userBO = new UserBO(this.service);
    this.eventBO = new EventBO(this.service);
    this.visitBO = new VisitBO(this.service);
    this.salesCoachingBO = new SalesCoachingBO(this.service);

    this.event = new EventModel(this.navParams.data.event);
    this.isFarmer = this.navParams.data.isFarmer;
    this.storage.get("user_profile").then((user_profile: any) => {
      this.userProfile = user_profile;
    });
    //MAXXIDATA
    if (this.navParams.data.farmToVisit) {
      this.farmToVisit = true;
      console.log('ASSOCIADO ');
      if(this.navParams.data.associateFarm.BR_Parent_Name__c != undefined){
        
      console.log(this.navParams.data.associateFarm.BR_Parent_Name__c);
      this.farmerNameToVisit = this.navParams.data.associateFarm.BR_Parent_Name__c.toUpperCase();
      // this.event.Fazenda__c = this.navParams.data.associateFarm.Id;
      // this.farmList.push([this.navParams.data.associateFarm.Id, this.navParams.data.associateFarm.Name]);
      this.farmNameToVisit = this.navParams.data.associateFarm.Name.toUpperCase();
      // this.farmName = this.navParams.data.associateFarm.Name;
      this.clientsName = this.navParams.data.associateFarm.BR_Parent_Name__c;
      }else{
        this.farmNameToVisit = this.navParams.data.associateFarm.Name.toUpperCase();
      }

      // this.event.WhatId = this.navParams.data.associateFarm.Id;
      // this.event.ClienteId__c = undefined;


      this.loadObjects().then((obj: any) => {
        this.objWithError = MustEditRegisterHelper.isObjectTreeWithError(obj);
        this.verifyStatus();
      });
    } else {
      this.loadObjects().then((obj: any) => {
        this.objWithError = MustEditRegisterHelper.isObjectTreeWithError(obj);
        this.verifyStatus();
      });
    }
    //FIM MAXXIDATA
  }

  public ngOnInit() {
    console.log('------| Events: events : !!! unsubscribe !!!');

    this.events.unsubscribe(NotificationKeys.statusEventSave);
  }

  public ngAfterContentInit() {
    console.log('------| Events: events : ### subscribe ###');

    this.events.subscribe(NotificationKeys.statusEventSave, (visit: any) => {
      let status;
      if (visit.Status__c === VisitStatus.completed || visit.BR_Status__c === SalesCoachingStatus.completed) {
        status = EventStatus.executed;
      } else if (visit.Status__c === VisitStatus.canceled || visit.BR_Status__c === SalesCoachingStatus.canceled) {
        status = EventStatus.canceled;
      } else if (visit.Status__c === VisitStatus.inProgress || visit.BR_Status__c === SalesCoachingStatus.inProgress) {
        status = EventStatus.inProgress;
      }

      if (status && (this.event.Status__c !== status)) {
        if (((this.event.BR_Tipo_de_Visita__c === visit.Name) && (this.event.getId() === visit.EventId__c)) || (this.event.WhatId === visit.Id)) {
          this.updateEventStatus(status);
        }
      }
    });
  }

  public ionViewDidEnter() {
    console.log('======USER PROFILE');
    console.log(this.userProfile);
    this.cacheEvent = JSON.stringify(this.event);
  }

  public ionViewWillUnload() {
    TabHelper.enableAll(this.navCtrl);
  }

  public ionViewCanLeave() {
    return new Promise((resolve, reject) => {
      if (!this.isCancel && this.hasCacheChanged()) {
        AlertHelper.showConfirmDialog(this.alertCtrl, resolve, reject);
      } else {
        resolve();
      }
    });
  }

  private updatePageTitle() {
    if (this.header) {
      this.header.updatePageTitle(this.title);
    }
  }

  public loadObjects() {
    return new Promise((resolve) => {

      this.storage.get('user_profile').then((user_profile: any) => {
        this.userProfile = user_profile;

        this.eventBO.getRecordTypeName(this.event.RecordTypeId).then((developerName: string) => {
          this.isGR = this.event.Status__c
            ? (developerName && developerName.includes(EventDeveloperName.coaching))
            : (this.userProfile && this.userProfile.isGR);

          this.visitTitle = this.isGR ? 'Informações da visita' : 'Visita';
          this.visitCoaching = this.isGR ? 'Etapa' : 'Tipo de Visita';
          this.clientTitle = this.isGR ? 'RTV' : 'Cliente';
          this.durationTitle = this.isGR ? 'Detalhes do compromisso' : 'Duração';

          this.eventBO.loadEventToSalesforceGambi(this.event).then((newEvent: EventModel) => {
            this.event = new EventModel(newEvent);

            this.loadEvent(this.event).then(() => {
              // console.log(this.event.Id.indexOf("local_"))
              // if(this.event.Id.indexOf("local_") >= 0){
              //   console.log(this.isEditMode)
              //   this.isReadOnly = false;
              //   this.editTxt = 'Evento com possibilidade de edição.';
              //   this.isEditMode.duration = false;
              //   this.isEditMode.client = false;
              //   this.isEditMode.geral= true;
              //   this.isEditMode.harvest = true;
              // }else{
                this.isReadOnly = (this.event.Status__c && !this.event.WhatId);
              // }
              

              if (this.isGR) {
                this.salesCoachingBO.loadSalesCoaching(this.event).then((salesCoaching: BR_Sales_Coaching__cModel) => {
                  console.log('------| SALESCOACHING: ', salesCoaching);
                  this.salesCoaching = salesCoaching;

                  if (this.salesCoaching.BR_RTV__c) {
                    this.userBO.getUserNameById(this.salesCoaching.BR_RTV__c).then((userName: string) => {
                      this.rtvName = userName.toUpperCase();
                    });
                  }
                  resolve(this.salesCoaching);

                }).catch((error) => {
                  console.log('-*-| loadEvent:error: ', error);
                });
              } else {
                this.visitBO.loadVisit(this.event).then((visit: Visita__cModel) => {
                  console.log('------| VISIT: ', visit);
                  this.visit = visit;
                  this.loadContacts(this.visit).then(() => {
                    resolve(this.visit);
                  });
                }).catch((error) => {
                  console.log('-*-| loadEvent:error: ', error);
                });
              }
            });
          });
        });
      });
    });
  }

  public loadContacts(visit) {
    return new Promise((resolve) => {
      this.zone.run(() => {
        this.eventBO.contactList(this.event).then((contacts: any) => {
          this.BR_EmailContactList__c = visit.BR_EmailContactList__c ? visit.BR_EmailContactList__c : '';
          this.selectedsContacts = [];

          this.BR_EmailContactList__c.split(';').forEach((item) => {
            for (const contact of contacts) {
              if (contact && item === contact[3]) {
                this.selectedsContacts.push(contact[3]);
                break;
              }
            }
          });

          console.log('---| selectedsContacts: ', this.selectedsContacts);
          this.BR_EmailContactList__c = this.selectedsContacts.join(';');
          resolve();
        });
      });
    });
  }

  public showPlantingDate() {
    return this.event.Tipo_de_Geracao_de_Demanda__c ||
      (this.event.BR_Tipo_de_Visita__c === 'Colheita' && this.event.BR_Division__c === 'Milho');
  }

  private loadEvent(event) {
    return new Promise((resolve) => {
      if (!this.event.Status__c || this.event.Status__c === EventStatus.open || this.event.Status__c === EventStatus.inProgress) {
        TabHelper.disableNonSelectedTabs(this.navCtrl);
      }

      if (!this.event.IsAllDayEvent) {
        this.event.IsAllDayEvent = false;
        this.cacheStartTime = moment(this.event.StartDateTime).get('hour');
        this.cacheEndTime = this.event.EndDateTime
          ? moment(this.event.EndDateTime).get('hour')
          : moment(this.event.StartDateTime).add(1, 'hours').get('hour');
      }

      this.appPreferences.fetch('user_id').then((value) => {
        console.log('USER ID APPPREFERENCES', value);
        this.event.OwnerId = value;
      }).catch((err) => {
        console.log(err, 'USER ID APPPREFERENCES ERROR');
      });

      // if (!this.navParams.data.farmToVisit) {
      if (!this.isGR && this.event.WhatId) {
        this.event.setAccount(this.service.getClientsService()).then(() => {
          this.clientsName = this.event.Account.Name.toUpperCase();
          this.farmerSelected = true;
          this.isChannel = this.navParams.data.isChannel ? true : false;

          this.client = new ClientsModel(this.event.Account);
          this.client.setRecordType(this.service.getRecordTypeService()).then(() => {
            this.client.setMunicipio(this.service.getMunicipioService()).then(() => {
              console.log('Client loaded: ', this.client);
            });
          });
        }).catch((err) => {
          console.log(err);
        });
      }
      // }
      if (!this.navParams.data.farmToVisit) {
        if (this.event.Fazenda__c || (!this.event.ClienteId__c && this.event.WhatId)) {
          const farmToGambi = (!this.event.ClienteId__c && this.event.WhatId) ? this.event.WhatId : this.event.Fazenda__c;

          this.eventBO.getFarmById(farmToGambi).then((farm: any) => {
            if (farm) {
              console.log('FAZENDA')
              this.farmName = farm.Name;
            }
            if (!this.event.WhatId) {
              if (farm.ParentId) {
                this.eventBO.getFarmerWithParentId(farm.ParentId).then((farmer: any) => {
                  if (farmer) {
                    console.log('FAZENDEIRO')
                    this.clientsName = farmer.Name.toUpperCase();
                    this.event.WhatId = farmer.Id;
                    this.client = farmer;
                    this.getPickLists();
                  }
                }, (err) => {
                  console.log(err);
                });
              }
            } else {
              this.getPickLists();
            }
          });
        }
      }


      // if (this.event.Fazenda__c || (!this.event.ClienteId__c && this.event.WhatId)) {
      //   const farmToGambi = (!this.event.ClienteId__c && this.event.WhatId) ? this.event.WhatId : this.event.Fazenda__c;

      //   this.eventBO.getFarmById(farmToGambi).then((farm: any) => {
      //     if (farm) {
      //       console.log(farm)
      //       this.farmName = this.navParams.data.associateFarm.Name;
      //       this.clientsName = this.farmNameToVisit.toUpperCase();
      //       this.event.WhatId = this.navParams.data.associateFarm.Id;
      //       this.event.ClienteId__c = this.navParams.data.associateFarm.Id;
      //       this.client = this.navParams.data.associateFarm;
      //       this.getPickListsFromFarm();
      //     }
      //     if (!this.event.WhatId) {

      //       if (farm.ParentId) {

      //         this.eventBO.getFarmerWithParentId(farm.ParentId).then((farmer: any) => {
      //           console.log(farmer)
      //         if (farmer) {
      //           this.clientsName = farmer.Name.toUpperCase();
      //           this.event.WhatId = farmer.Id;
      //           this.client = farmer;
      //           this.getPickLists();
      //         }
      //         }, (err) => {
      //           console.log(err);
      //         });
      //       }
      //     } else {
      //       //MAXXIDATA
      //       if (this.navParams.data.farmToVisit) {
      //         this.farmName = this.navParams.data.associateFarm.Name;
      //         this.clientsName = this.farmNameToVisit.toUpperCase();
      //         this.event.WhatId = this.navParams.data.associateFarm.Id;
      //         this.event.ClienteId__c = this.navParams.data.associateFarm.Id;
      //         this.client = this.navParams.data.associateFarm;
      //         this.getPickListsFromFarm();
      //       }else{
      //       //FIM MAXXIDATA
      //       this.getPickLists();
      //       }
      //     }
      //   });
      // }

      this.eventBO.cropVisitChange(this.event).then((data: EventModel) => {
        this.event = new EventModel(data);

        if (this.event.Lembrete__c === null || this.event.Lembrete__c === undefined) {
          this.event.Lembrete__c = '1 dia';
        }

        if (this.userProfile.isRTV && this.event.BR_Division__c && this.event.BR_Division__c.length > 0) {
          this.isRTV = true;
        } else if (this.userProfile.isRC) {
          this.isRC = true;
          this.filter_Division.push('Milho', 'Soja');
        }

        this.setDateTimes();
        this.loadVariables();
        this.divisionOption();
        console.log('---| EVENT: ', this.event);
      });

      resolve();
    });
  }

  private updateEventStatus(status: string) {
    this.event.Status__c = status;
    this.updateTitle = true;
    this.saveNew = false;
    this.saveEdit = false;
    this.saveEventFromVisit = true;
    this.saveEvent();
  }

  private hasCacheChanged() {
    const format = 'YYYY-MM-DD HH:mm';
    return moment(this.StartDateTime).format(format) !== moment(this.event.StartDateTime).format(format) ||
      moment(this.EndDateTime).format(format) !== moment(this.event.EndDateTime).format(format) ||
      this.cacheEvent !== JSON.stringify(this.event);
  }

  public willOpenReport() {
    return this.eventBO.willOpenReport(this.event, this.userProfile);
  }

  public openClientsDetail() {
    this.navCtrl.push(ClientsDetailPage, this.client);
  }

  private setValuesObj(obj, newValue) {
    Object.keys(obj).forEach((i: string, index: number) => {
      obj[i] = newValue;
    });
  }

  public checkValuesObj(obj) {
    let check = false;
    Object.keys(obj).forEach((i: string, index: number) => {
      if (obj[i]) { check = true; }
    });
    return check;
  }

  private editEvent() {
    this.isStart = false;
    this.isCancel = false;
    this.saveEdit = true;
    this.saveNew = false;
    if (this.event.BR_Division__c && this.event.BR_Division__c.length > 0) {
      this.isRTV = true;
    }
  }

  public editClient(e) {
    e.stopPropagation();
    this.editEvent();
    this.isEditMode.client = true;
  }

  public editHarvest(e) {
    e.stopPropagation();
    this.editEvent();
    this.isEditMode.harvest = true;
  }

  public editDuration(e) {
    e.stopPropagation();
    this.editEvent();
    this.isEditMode.duration = true;
  }

  public enableToAction() {
    // console.log('LEITURA')
    // console.log(this.isReadOnly)
    if(this.event.Id.indexOf("local_") >= 0){
      
      if(this.event.Status__c == EventStatus.executed){
        this.editTxt = 'Email com possibilidade de edição.';
        this.editEventTemp = true;
      }
      return true;
    }else{
      return (
        this.event.Status__c !== EventStatus.executed
        && this.event.Status__c !== EventStatus.canceled
      );
    }
  }

  private verifyStatus() {
    this.title = 'Detalhe do evento';

    if (this.event.Status__c === null || this.event.Status__c === undefined) {
      this.title = 'Novo evento';
      this.isStart = false;
      this.isCancel = false;
      this.saveEdit = false;
      this.saveNew = true;

      this.setValuesObj(this.isEditMode, true);

    } else if (this.event.Status__c === EventStatus.open) {
      this.buttonVisit = 'INICIAR VISITA';
      this.isStart = true;
      this.isCancel = true;
      this.setValuesObj(this.isEditMode, false);

    } else if (this.event.Status__c === EventStatus.inProgress) {
      this.buttonVisit = 'CONTINUAR VISITA';
      this.isStart = true;
      this.isCancel = true;
      this.setValuesObj(this.isEditMode, false);

    } else if (this.event.Status__c === EventStatus.executed) {
      this.buttonVisit = 'VISUALIZAR VISITA';
      this.isStart = true;
      this.isCancel = false;
      this.setValuesObj(this.isEditMode, true);

    } else if (this.event.Status__c === EventStatus.canceled) {
      this.buttonVisit = 'VISUALIZAR VISITA';
      this.isStart = false;
      this.isCancel = true;
      this.setValuesObj(this.isEditMode, true);
    }

    this.updatePageTitle();
  }

  private loadVariables() {
    // this.BR_Division__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_Division__c');
    this.BR_Division__c = EventDivision;

    this.BR_Tipo_de_Visita__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_Tipo_de_Visita__c');
    this.Lembrete__c = WindowStorageHelper.retrievePicklistsFromLocalStorage('Lembrete__c');
    this.cancelOptions = WindowStorageHelper.retrievePicklistsFromLocalStorage('BR_Cancel_Motivation__c');
    this.safras = [];

    this.eventBO.getSafra().then((response: any) => {
      response.forEach((element) => {
        this.safras.push(new SafraModel(element));
      });

      if (!this.event.Status__c) {
        this.filterOnlyPossibleSafraValues();
      }
    });
  }

  private filterOnlyPossibleSafraValues() {
    this.safras = this.safras.filter((safra) => {
      return safra.Safra_atual__c.toLowerCase() === 'sim';
    });
  }

  private loadFilterPicklist() {
    if (this.event.BR_Division__c === 'Milho') {
      //SASALES-315
      this.filter_Geracao_de_Demanda = [
        'Lado a Lado',
        'Acompanhamento Comercial',
        'Lavoura Demonstrativa',
      ];

    } else if (this.event.BR_Division__c === 'Soja') {
      this.filter_Geracao_de_Demanda = [
        'Lado a Lado',
        'E5',
        'Lavoura Demostrativa',
      ];
    }
  }

  private setDateTimes() {
    if (DateHelper.isAllDayEventNotAdjusted(this.event)) {
      this.event.StartDateTime = DateHelper.adjustAllDayEventTimezone(this.event.StartDateTime);
      this.event.EndDateTime = DateHelper.adjustAllDayEventTimezone(this.event.EndDateTime);
    }
    if (this.event.StartDateTime && !this.event.EndDateTime) {
      this.Data_do_Plantio__c = moment(this.event.StartDateTime).add(30, 'day').format();
      this.StartDateTime = moment(this.event.StartDateTime).add(1, 'hours').set('minute', 0).set('second', 0).format();
      this.EndDateTime = moment(this.event.StartDateTime).add(2, 'hours').set('minute', 0).set('second', 0).format();
    } else if (this.event.BR_Tipo_de_Visita__c || this.isGR) {
      this.Data_do_Plantio__c = DateHelper.dateISO(this.event.Data_do_Plantio__c);
      this.StartDateTime = DateHelper.dateISO(this.event.StartDateTime);
      this.EndDateTime = DateHelper.dateISO(this.event.EndDateTime);
    } else {
      this.Data_do_Plantio__c = moment(DateHelper.dateISO(null)).add(30, 'day').format();
      this.StartDateTime = DateHelper.dateISO(null);
      this.EndDateTime = DateHelper.dateISOwithOneMoreHour(null);
    }
    this.dateChange();
  }

  private dateChange() {
    return new Promise((resolve) => {
      this.event.Data_do_Plantio__c = DateHelper.parseISOString(this.Data_do_Plantio__c);
      this.event.StartDateTime = DateHelper.parseISOString(this.StartDateTime);
      this.event.EndDateTime = DateHelper.parseISOString(this.EndDateTime);

      this.event.BR_StartTime__c = String(DateHelper.getUTCTimestamp(this.event.StartDateTime));
      this.event.BR_EndTime__c = String(DateHelper.getUTCTimestamp(this.event.EndDateTime));

      this.event.ActivityDateTime = this.event.StartDateTime;
      resolve();
    });
  }

  public updateEndDate() {
    const eventStartDate = this.event.StartDateTime.substr(0, 10);
    const startDate = this.StartDateTime.substr(0, 10);
    if (eventStartDate !== startDate) {
      this.EndDateTime = DateHelper.updateEndDate(this.StartDateTime, this.EndDateTime);
      this.dateChange();
    }
  }

  public isAllDayEvent() {
    let startDateTime = moment(this.StartDateTime);
    let endDateTime = moment(this.EndDateTime);

    if (this.event.IsAllDayEvent) {
      this.isAllDay = true;

      startDateTime = moment(startDateTime).set({ hour: 0, minute: 0, second: 0 });
      endDateTime = moment(startDateTime).add(1, 'day');
    } else {
      this.isAllDay = false;
      startDateTime = (this.event.Status__c && this.cacheStartTime)
        ? startDateTime.set('hour', this.cacheStartTime)
        : startDateTime.set('hour', moment().add(1, 'hours').get('hour'));

      endDateTime = (this.event.Status__c && this.cacheEndTime)
        ? endDateTime.set('hour', this.cacheEndTime)
        : endDateTime.set('hour', moment().add(2, 'hours').get('hour'));

      startDateTime.set('minute', 0);

      endDateTime.set('date', startDateTime.get('date'));
      endDateTime.set('month', startDateTime.get('month'));
      endDateTime.set('year', startDateTime.get('year'));
      endDateTime.set('minute', 0);
    }
    this.StartDateTime = startDateTime.format();
    this.EndDateTime = endDateTime.format();
  }

  private count = 0;
  public typeChange() {
    this.event.Tipo_de_Geracao_de_Demanda__c = null;
    this.event.Geracao_de_Demanda__c = false;
    this.BR_EmailContactList__c = undefined;

    if (this.count === 0) {
      this.loadFilterPicklist();
      this.count++;
    }
  }

  public showGenerationDemand() {
    if (this.event.BR_Tipo_de_Visita__c) {
      if (!this.event.BR_Tipo_de_Visita__c.includes('Pré-Plantio')
        && !this.event.BR_Tipo_de_Visita__c.includes('Plantio')) {
        return (this.event.Tipo_de_Geracao_de_Demanda__c);
      }
      return true;
    }
    return false;
  }

  public showIfGRWithStatus(status?: boolean) {
    return status
      ? (this.isGR && this.event.Status__c)
      : (!this.isGR || (this.isGR && !this.event.Status__c));
  }

  public isSaveDisabled() {
    const validationFields = this.isGR
      ? (this.event.Status__c ? true : (this.event.BR_Tipo_de_Visita__c && this.clientsName))
      : (this.event.Safra__c && this.event.BR_Division__c && this.event.BR_Tipo_de_Visita__c && this.clientsName);

    return !validationFields;
  }

  private validationTimeSave() {
    return new Promise((resolve, reject) => {
      const diffDate = DateHelper.diffDate(this.event.StartDateTime, this.event.EndDateTime);

      if (diffDate < 30) {
        reject(false);
      } else {
        resolve(true);
      }
    });
  }

  private saveEvent() {
    return new Promise((resolve, reject) => {
      this.dateChange().then(() => {
        this.validationTimeSave().then((data) => {

          if (this.isGR && !this.event.Status__c) {
            this.event.BR_SalesCoachingEvent__c = true;
            this.event.Subject = `Sessão de Coaching de Vendas - ${this.event.BR_Tipo_de_Visita__c}`;
          } else if (!this.isGR) {
            //MAXXIDATA
            if (this.navParams.data.farmToVisit) {
              this.event.Location = this.navParams.data.associateFarm.Name;
            } else {
              this.event.Location = this.farmSelected ? this.farmList[this.farmSelected][1] === undefined ? this.clientsName : this.farmList[this.farmSelected][1] : this.clientsName;
            }

            //FIM MAXXIDATA
            this.event.Subject = `${this.event.BR_Tipo_de_Visita__c} - ${this.event.Location}`;
          }

          if (!this.event.IsAllDayEvent) {
            const durationInMinutes = DateHelper.diffInMinutes(this.event.StartDateTime, this.event.EndDateTime);
            this.event.DurationInMinutes = durationInMinutes.toString();
          } else {
            this.event.DurationInMinutes = null;
          }

          for (const safra of this.safras) {
            if (safra.Name === this.event.Safra__c) {
              this.event.Safra_ID__c = safra.Id;
            }
          }

          if (this.saveNew) {
            this.saveNew = false;
            this.saveEdit = true;

            this.getRecordInfoPage().then((options) => {
              if (this.isGR) {
                this.salesCoachingBO.createIfNotExistsWithEvent(this.event, options.recordTypeName).then((savedSalesCoaching: BR_Sales_Coaching__cModel) => {
                  this.eventBO.create(this.event, options.eventRecordTypeName).then(async (event) => {
                    this.afterEventSave(event);
                  }).catch((error) => {
                    reject(error);
                  });
                });
              } else {
                this.visitBO.createIfNotExistsWithEvent(this.event, options.recordTypeName, this.BR_EmailContactList__c).then((savedVisit: Visita__cModel) => {
                  this.eventBO.create(this.event, options.eventRecordTypeName).then(async (event) => {
                    this.afterEventSave(event);
                  }).catch((error) => {
                    reject(error);
                  });
                });
              }
            });

          } else {
            this.eventBO.update(this.event).then(async (event) => {
              if (this.isGR && !this.saveEventFromVisit) {
                this.salesCoachingBO.saveSalesCoaching(event, this.salesCoaching);
              }
              this.afterEventSave(event);
            }).catch((error) => {
              reject(error);
            });
          }
        }, (err) => {
          AlertHelper.showTimeLimitAlert(this.alertCtrl);
        });
      });
    });
  }

  private afterEventSave(eventSaved: EventModel) {
    return new Promise((resolve) => {
      this.events.publish(NotificationKeys.loadClientVisits);
      this.events.publish(NotificationKeys.totalNotifications);

      this.setValuesObj(this.isEditMode, false);
      this.event = new EventModel(eventSaved);

      if (this.updateTitle) {
        this.verifyStatus();
      } else {
        this.updateTitle = true;
      }

      setTimeout(() => {
        this.cacheEvent = JSON.stringify(this.event);
        LoadingManager.getInstance().hide();
        if (this.event.Status__c === EventStatus.open) {
          this.navCtrl.pop();
        }
        resolve();
      }, 750);
    });
  }

  private updateEmailsVisit(BR_EmailContactList__c) {
    return new Promise((resolve) => {
      this.visitBO.updateEmails(this.event, BR_EmailContactList__c).then(() => {
        LoadingManager.getInstance().hide();
        resolve();
      });
    });
  }

  public promptSaveEvent() {

    //MAXXIDATA
    console.log(this.selectedsContacts)
    console.log(this.selectedsContacts)
    if (this.willOpenReport() == true && this.selectedsContacts.length === 0) {
      if (this.isGR) {

        AlertHelper.showDialogForSave(this.alertCtrl, () => {
          LoadingManager.getInstance().show().then(() => {

            this.saveEvent().then(() => {
              LoadingManager.getInstance().hide();
              ToastHelper.showSuccessMessage(this.toastCtrl);
            });

          });
        }, () => {
          console.log('cancel');
        });

      } else {

        AlertHelper.showDialogContactNull(this.alertCtrl, () => {
          AlertHelper.showDialogForSave(this.alertCtrl, () => {
            LoadingManager.getInstance().show().then(() => {

              this.saveEvent().then(() => {
                LoadingManager.getInstance().hide();
                ToastHelper.showSuccessMessage(this.toastCtrl);
              });

            });
          }, () => {
            console.log('cancel');
          });
        }, () => {
          console.log('cancel');
        });

      }
    } else {

      AlertHelper.showDialogForSave(this.alertCtrl, () => {
        LoadingManager.getInstance().show().then(() => {

          this.saveEvent().then(() => {
            LoadingManager.getInstance().hide();
            ToastHelper.showSuccessMessage(this.toastCtrl);
          });

        });
      }, () => {
        console.log('cancel');
      });

    }
    //FIM MAXXIDATA

  }

  public promptCancelEvent() {
    const options = this.isGR ? this.cancelOptions : undefined;

    AlertHelper.showDialogForCancel(this.alertCtrl, (motivation) => {
      LoadingManager.getInstance().show().then(() => {
        this.cancelEvent(motivation);
      });
    }, () => {
      console.log('cancel');
    }, options);
  }

  private cancelEvent(motivation?: string) {
    this.eventBO.cancel(this.event, motivation).then(() => {
      this.isCancel = true;
      this.events.publish(NotificationKeys.loadClientVisits);
      this.events.publish(NotificationKeys.totalNotifications);

      setTimeout(() => {
        LoadingManager.getInstance().hide();
        ToastHelper.showSuccessMessage(this.toastCtrl);
        this.navCtrl.pop();
      }, 1000);

    }).catch((error) => {
      console.log('-*-| cancelEvent:error: ', error);
    });
  }

  private checkConditionEdit(option) {
    return (this.saveEdit || this.saveNew) && this.isEditMode[option];
  }

  public openEventsSearchModal() {
    this.eventBO.getRecordTypeToClients().then((Ids: any) => {
      const options: ModalOptions = {
        enterAnimation: 'modal-md-slide-in',
        leaveAnimation: 'modal-md-slide-out',
      };

      const modalParams = {
        ParentId: (this.event.ClienteId__c ? this.event.WhatId : undefined),
        RecordTypeId: Ids.join(','),
        isGR: this.isGR,
      };

      const modal = this.modalCtrl.create(SearchClientModal, modalParams, options);
      modal.present();

      modal.onWillDismiss((accountSelected: ClientsModel) => {
        if (accountSelected && accountSelected.Name !== this.clientsName) {
          this.eventBO.getTipo_Compromisso__c(accountSelected.RecordTypeId).then((Tipo_Compromisso__c: string) => {
            this.event.Tipo_Compromisso__c = Tipo_Compromisso__c;
            this.farmerSelected = !(Tipo_Compromisso__c === RecordTypeDeveloperName.channel);
            this.farmName = '';

            this.client = accountSelected;
            this.clientsName = accountSelected.Name.toUpperCase();
            this.event.WhatId = accountSelected.Id;
            this.event.Fazenda__c = undefined;

            this.BR_EmailContactList__c = '';
            this.getPickLists();
          });
        }
      });
    });
  }

  public openVisit() {
    this.validationTimeSave().then(() => {
      this.getRecordInfoPage().then((options) => {
        this.navCtrl.push(options.page, this.event);
      });
    }, (err) => {
      AlertHelper.showTimeLimitAlert(this.alertCtrl);
    });
  }

  public openManagerEmailsModal() {
    const list = [];
    this.eventBO.contactList(this.event).then((contactList: any) => {
      this.contactList = contactList;
      console.log('------| Contacts list returned: ', this.contactList);

      this.contactList.forEach((item) => {
        if (DataHelper.emailMatch(item[3])) {
          list.push(item[3]);
        }
      });

      const options = {
        contactsList: list,
        emailsSelected: this.BR_EmailContactList__c ? this.BR_EmailContactList__c.split(';') : [],
        client: this.client,
      };

      const modal = this.modalCtrl.create(ManagerEmailsModal, options);
      modal.present();

      modal.onWillDismiss((data) => {
        this.getPickLists();
        if (data && data.emailsSelected) {
          console.log('------| Emails selected: ', data.emailsSelected);
          this.selectedsContacts = data.emailsSelected;
          this.BR_EmailContactList__c = data.emailsSelected.join(';');

          if (!this.checkConditionEdit('geral')) {
            this.updateEmailsVisit(this.BR_EmailContactList__c);
          }
        }
      });
    });
  }

  public generationSelected() {
    this.event.Geracao_de_Demanda__c = this.event.Tipo_de_Geracao_de_Demanda__c ? true : false;
  }

  private getPickLists() {
    this.eventBO.contactList(this.event).then((contactList: any) => {
      console.log('------| Contacts list returned: ', contactList);

      this.contactList = contactList;
      this.contactSelected = null;

      this.contactList.forEach((item, index) => {
        if (item[0] === this.event.WhoId) {
          this.contactSelected = index;
        }
      });
      this.setContact();
    });

    this.eventBO.farmList(this.event).then((farmList: any) => {
      console.log('------| Farms list returned: ', farmList);

      this.farmList = DataHelper.removeDuplicatesFromObject(farmList, 1);
      if (this.farmSelected == null || this.farmSelected === undefined) {
        this.farmSelected = null;
      }

      this.farmList.forEach((item, index) => {
        if (item[0] === this.event.Fazenda__c || item[0] === this.event.WhatId) {
          this.farmSelected = index;
        }
      });
      this.setFarm();
    });
  }


  //MAXXIDATA
  private getPickListsFromFarm() {
    this.eventBO.contactList(this.event).then((contactList: any) => {
      console.log('------| Contacts list returned: ', contactList);

      this.contactList = contactList;
      this.contactSelected = null;

      this.contactList.forEach((item, index) => {
        if (item[0] === this.event.WhoId) {
          this.contactSelected = index;
        }
      });
      this.setContact();
    });

  }
  //FIM MAXXIDATA


  private divisionOption() {
    this.appPreferences.fetch('user_brand').then((user_brand: string) => {
      const userBrand = user_brand.toUpperCase();
      const brandToDivision = (userBrand === 'AGROCERES' || userBrand === 'ATS AG' || userBrand === 'ATCAG' || userBrand === 'AG' || userBrand === 'DEKALB' || userBrand === 'ATS DKB' || userBrand === 'ATCDK' || userBrand === 'DK')
        ? 'COACHING_AGROCERES_DEKALB'
        : 'COACHING_AGROESTE';

      const division = this.isGR ? brandToDivision : this.event.BR_Division__c;

      this.eventBO.listVisitType(division).then((result: any) => {
        this.Tipo_Visita_Filter = result;

        if (!this.event.Status__c) {
          this.event.BR_Tipo_de_Visita__c = null;
        }
      });

      if (this.event.BR_Division__c && this.event.BR_Division__c === 'Crop') {
        this.cropOption = true;
      } else {
        this.cropOption = false;
        this.contactSelected = null;
        this.event.WhoId = null;
      }
      this.loadFilterPicklist();
    });
  }

  private setContact() {
    if (this.contactSelected && this.contactList.length > 0) {
      this.event.WhoId = this.contactSelected >= 0 ? this.contactList[this.contactSelected][0] : null;
    }
  }

  private setFarm() {
    if (this.farmSelected && this.farmList.length > 0) {
      this.event.Fazenda__c = this.farmSelected >= 0 ? this.farmList[this.farmSelected][0] : null;
    }
  }

  private getRecordInfoPage(): any {
    return new Promise((resolve) => {
      const options = { eventRecordTypeName: '', recordTypeName: '', page: {} };
      let online;

      if (this.isGR) {
        options.eventRecordTypeName = EventDeveloperName.coaching;
        options.page = VisitSalesCoachingPage;

        if (this.event.Subject.includes('Identificação')) {
          options.recordTypeName = SalesCoachingRecordTypes.needs;

        } else if (this.event.Subject.includes('Plano e Articular')) {
          options.recordTypeName = SalesCoachingRecordTypes.planArticulate;

        } else if (this.event.Subject.includes('Negociar e Fechar')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.negotiateCloseOnline : SalesCoachingRecordTypes.negotiateCloseRideAlong;
          });

        } else if (this.event.Subject.includes('Assistência com a Compra')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.assistPurchaseOnline : SalesCoachingRecordTypes.assistPurchaseRideAlong;
          });

        } else if (this.event.Subject.includes('Entrega & Order-To-Cash')) {
          options.recordTypeName = SalesCoachingRecordTypes.trackDeliveryOTC;

        } else if (this.event.Subject.includes('Geração de Demanda')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.demandGenerationOnline : SalesCoachingRecordTypes.demandGenerationRideAlong;
          });

        } else if (this.event.Subject.includes('Final da Safra')) {
          this.eventBO.getRecordTypeName(this.salesCoaching.RecordTypeId).then((developerName: string) => {
            online = developerName ? developerName.includes('Online') : this.isOnline;
            options.recordTypeName = DataHelper.getBoolean(online) ? SalesCoachingRecordTypes.endSeasonOnline : SalesCoachingRecordTypes.endSeasonRideAlong;
          });
        }

      } else {
        options.eventRecordTypeName = EventDeveloperName.default;

        if (this.event.BR_Division__c.includes('Crop')) {
          if (this.event.BR_Tipo_de_Visita__c.includes('Comercial')) {
            options.recordTypeName = VisitRecordTypes.comercial;
            options.page = CropComercialPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Climate')) {
            options.recordTypeName = VisitRecordTypes.climate;
            options.page = CropClimatePage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Evento')) {
            options.recordTypeName = VisitRecordTypes.eventCrop;
            options.page = CropEventPage;
          }
        } else if (this.event.BR_Division__c.includes('Soja')) {
          if (this.event.BR_Tipo_de_Visita__c.includes('Plantio')) {
            options.recordTypeName = VisitRecordTypes.soyPlanting;
            options.page = SoyPlantingVisitPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Colheita')) {
            options.recordTypeName = VisitRecordTypes.cropSoy;
            options.page = SoyHarvestPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Visita Verde')) {
            options.recordTypeName = VisitRecordTypes.soyGreenVisit;
            options.page = SoyGreenVisitPage;
          }
        } else if (this.event.BR_Division__c.includes('Milho')) {
          if (this.event.BR_Tipo_de_Visita__c.includes('Pré-Plantio')) {
            options.recordTypeName = VisitRecordTypes.prePlanting;
            options.page = CornPrePlantingCreatePage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Plantio')) {
            options.recordTypeName = VisitRecordTypes.planting;
            options.page = CornPlantingCreatePage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Germinação e Emergência')) {
            options.recordTypeName = VisitRecordTypes.germinationAndEmergency;
            options.page = CornGerminationEmergencyPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Desenvolvimento Vegetativo')) {
            options.recordTypeName = VisitRecordTypes.vegetativeDevelopment;
            options.page = CornVegetativeDevelopmentPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Desenvolvimento Reprodutivo')) {
            options.recordTypeName = VisitRecordTypes.reproductiveDevelopment;
            options.page = CornReproductiveDevelopmentPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Colheita')) {
            options.recordTypeName = VisitRecordTypes.harvest;
            options.page = CornHarvestPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Dia D')) {
            options.recordTypeName = VisitRecordTypes.dDay;
            options.page = CornDayDPage;

          } else if (this.event.BR_Tipo_de_Visita__c.includes('Evento')) {
            options.recordTypeName = VisitRecordTypes.cornEvent;
            options.page = CornVisitEventPage;
          }
        }
      }
      resolve(options);
    });
  }

}
