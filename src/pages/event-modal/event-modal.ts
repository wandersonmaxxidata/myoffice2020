import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import * as moment from 'moment';

@Component({
  selector: 'page-event-modal',
  templateUrl: 'event-modal.html',
})
export class EventModalPage {

  public event = { startTime: new Date().toISOString(), endTime: new Date().toISOString(), allDay: false };

  constructor(public navCtrl: NavController, private navParams: NavParams, public viewCtrl: ViewController) {
    const preselectedDate = moment(this.navParams.get('selectedDay')).format();
    this.event.startTime = preselectedDate;
    this.event.endTime = preselectedDate;
  }

  public cancel() {
    this.viewCtrl.dismiss();
  }

  public save() {
    this.viewCtrl.dismiss(this.event);
  }

}
