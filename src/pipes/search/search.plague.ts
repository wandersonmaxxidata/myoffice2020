import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPlague',
})
export class SearchPlaguePipe implements PipeTransform {

  public transform(value: any, input: string) {
    if (input) {
      input = input.toLowerCase();
      return value.filter((el: any) => {
        return el.label.toLowerCase().indexOf(input) > -1;
      });
    }
    return value;
  }
}