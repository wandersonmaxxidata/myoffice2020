import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchWeed',
})
export class SearchWeedPipe implements PipeTransform {

  public transform(value: any, input: string) {
    if (input) {
      input = input.toLowerCase();
      return value.filter((el: any) => {
        return el.label.toLowerCase().indexOf(input) > -1;
      });
    }
    return value;
  }
}