var fs = require('fs');
var classContentBuffer = "";
var importTemplateModule = 'import { &importService } from "../providers/&entity/&importPath";\n';
var importTemplateProvider = 'import { &EntityNameService } from "./&entity/&providerPath";\n';
var importTemplateModel = 'import { &EntityNameService } from "../../providers/&entity/&providerPath";\n';
var constructorTemplate = 'public &entityService: &EntityNameService';
var getServiceTemplate = `
    /**
     * provider dos serviços da entidade &EntityName
     */
    get&EntityNameService(){
        return this.&entityService;
    }`
var imports = [];
var constructorParameters = [];
var gets = [];
fs.readFile('EntitiesConfig.json', 'utf8', function (err, data) {
    var object;
    if (err) {
        console.log(err);
        throw err;
    }
    object = JSON.parse(data);

    var entityObject = Object.keys(object);
    asyncLoop(entityObject.length,
        function (loop) {
            var currentEntityObject = entityObject[loop.iteration()];
            makeModel(object, currentEntityObject);
            makeModelProvider(object, currentEntityObject);
            prepareImports(readEntity(object[currentEntityObject]), function () {
                prepareProviders(readEntity(object[currentEntityObject]), function () {
                    loop.next();
                });
            });
        },
        function () {
            makeAutenticateProvider(function () {
                prepareImports('Autenticate', function () {
                    prepareProviders('Autenticate', function () {
                        makeProvider();
                    });
                });
            });

        });
});

function makeModel(object, entityObject) {
    var classFileName = './src/app/model/';
    var soupName;
    var className;
    var classIndexes;
    var classProperties;
    var classCustomObjects;
    var classRequiredFields;
    checkDir(classFileName);

    className = readEntity(object[entityObject]);
    soupName = object[entityObject].SoupName;
    classIndexes = readFieldsIndexes(object[entityObject]);
    classCustomObjects = readFieldsRelated(object[entityObject]);
    classProperties = readFieldsProperties(object[entityObject]);
    classRequiredFields = readRequiredFields(object[entityObject]);
    classFileName = classFileName.concat(className).concat('Model.ts');
    fs.readFile('ClassTemplate.tmpl', 'utf8', function (err2, dataTmpl) {
        if (err2) {
            return console.log(err2);
        } else {
            dataTmpl = dataTmpl.replace(new RegExp('&SoupName', 'g'), soupName);
            dataTmpl = dataTmpl.replace(new RegExp('&EntityName', 'g'), className);
            dataTmpl = dataTmpl.replace(new RegExp('&EntityIndexes', 'g'), classIndexes);
            dataTmpl = dataTmpl.replace(new RegExp('&EntityField', 'g'), classProperties);
            dataTmpl = dataTmpl.replace(new RegExp('&EntityRelated', 'g'), classCustomObjects.classContentBuffer);
            dataTmpl = dataTmpl.replace(new RegExp('&RequiredFields', 'g'), classRequiredFields);
            dataTmpl = dataTmpl.replace(new RegExp('&ImportEntityRelated', 'g'), classCustomObjects.importsBuffer.join(''));

            for (var i = 0; i < object[entityObject].Related.length; i++) {
                let entity = object[entityObject].Related[i].ToEntity;
                let relatedImport = 'import { ' + entity + 'Model } from "./' + entity + 'Model";\n'
                dataTmpl = relatedImport.concat(dataTmpl);
            }

            fs.writeFile(classFileName, dataTmpl, function (err3) {
                if (err3) {
                    return console.log(err3);
                } else {
                    console.log('arquivo criado: ' + classFileName);
                }
            });
        }
    });
}

function makeModelProvider(object, entityObject) {
    var classFileName = './src/providers/';
    var className = readEntity(object[entityObject]);
    var soupName = object[entityObject].SoupName;
    checkDir(classFileName + className.toLowerCase() + '/');

    classFileName = classFileName.concat(className.toLowerCase() + '/').concat(className.toLowerCase()).concat('-service-salesforce.ts').toLowerCase();
    fs.readFile('ProviderModelTemplate.tmpl', 'utf8', function (err2, dataTmpl) {
        if (err2) {
            return console.log(err2);
        } else {
            dataTmpl = dataTmpl.replace(new RegExp('&targetSync', 'g'), readFieldsTarget(object[entityObject]));
            dataTmpl = dataTmpl.replace(new RegExp('&EntityName', 'g'), className);
            dataTmpl = dataTmpl.replace(new RegExp('&SoupName', 'g'), soupName);

            let syncUpFields = readFieldsSyncUp(object[entityObject]);
            dataTmpl = dataTmpl.replace(new RegExp('&AllSyncUpFields', 'g'), syncUpFields.allSyncUpFields);
            dataTmpl = dataTmpl.replace(new RegExp('&SecondaryIdFields', 'g'), syncUpFields.secondaryIdFields);

            fs.writeFile(classFileName, dataTmpl, function (err3) {
                if (err3) {
                    return console.log(err3);
                } else {
                    console.log('arquivo criado: ' + classFileName);
                }
            });
        }
    });

}

function makeProvider() {
    var classFileName = './src/providers/';
    checkDir(classFileName);

    classFileName = classFileName.concat('service-salesforce.ts');

    fs.readFile('ProviderTemplate.tmpl', 'utf8', function (err2, dataTmpl) {
        if (err2) {
            return console.log(err2);
        } else {
            dataTmpl = dataTmpl.replace(new RegExp('&imports', 'g'), imports.join(''));
            dataTmpl = dataTmpl.replace(new RegExp('&constructorParameters', 'g'), constructorParameters.join(', '));
            dataTmpl = dataTmpl.replace(new RegExp('&gets', 'g'), gets.join('\n'));

            fs.writeFile(classFileName, dataTmpl, function (err3) {
                if (err3) {
                    return console.log(err3);
                } else {
                    console.log('arquivo criado: ' + classFileName);
                }
            });
        }
    });
}

function readEntity(entity) {
    return entity.Name;
}

function readFieldsIndexes(entity) {

    classContentBuffer = '';
    var indexTmpl = '{ path: "&Field", type: "string" },';
    for (var i = 0; i < entity.Fields.length; i++) {
        if (entity.Fields[i].IsIndex === true) {
            classContentBuffer = classContentBuffer.concat(indexTmpl.replace(new RegExp('&Field', 'g'), entity.Fields[i].Name));
        }
    }
    return classContentBuffer;
}

function checkDir(path) {
    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }
}

function readFieldsProperties(entity) {

    classContentBuffer = '';
    var propertieTmpl = `\t/**\n\t* Atributo da entidade no saleforce.
    \n\t*/
    &Field:string&Default;\n`;
    for (var i = 0; i < entity.Fields.length; i++) {
        var temp = propertieTmpl.replace(new RegExp('string', 'g'),
          (entity.Fields[i].Type === 'boolean')
            ? entity.Fields[i].Type + ' = false'
            : entity.Fields[i].Type + ' = undefined'
        );

        var defaultValue = '';
        if (entity.Fields[i].DefaultValue !== undefined && entity.Fields[i].DefaultValue !== null) {
            defaultValue = ' = ' + entity.Fields[i].DefaultValue;
        }

        temp = temp.replace(new RegExp('&Default', 'g'), defaultValue);
        var propertieName = entity.Fields[i].Alias ? entity.Fields[i].Alias : entity.Fields[i].Name;
        classContentBuffer = classContentBuffer.concat(temp.replace(new RegExp('&Field', 'g'), propertieName));
    }
    return classContentBuffer;
}

function importFilter(contentStr) {
    return checkTerms(contentStr, 'import ');
}

function checkTerms(contentStr, termStr) {
    return (contentStr.search(termStr) != -1);
}

function prepareImports(nameProvider, callback) {
    var importLinesCount = -1;
    var providerPath = nameProvider.concat('-service-salesforce').toLowerCase();
    var providerService = nameProvider.concat('Service');
    fs.readFile('./src/app/app.module.ts', 'utf8', function (err, dataFile) {
        if (err) {
            console.log(err);
            throw err;
        }
        var lines = dataFile.split('\n');
        var importLines = lines.filter(importFilter);
        importLinesCount = importLines.length;
        var hasImported = false;
        for (var line in importLines) {
            if (checkTerms(importLines[line], providerPath)) {
                hasImported = true;
                break;
            }
        }

        imports.push(importTemplateProvider.replace(new RegExp('&EntityName', 'g'), nameProvider).replace(new RegExp('&entity', 'g'), nameProvider.toLowerCase()).replace(new RegExp('&providerPath', 'g'), providerPath));
        constructorParameters.push(constructorTemplate.replace(new RegExp('&EntityName', 'g'), nameProvider).replace(new RegExp('&entity', 'g'), nameProvider.toLowerCase()));
        gets.push(getServiceTemplate.replace(new RegExp('&EntityName', 'g'), nameProvider).replace(new RegExp('&entity', 'g'), nameProvider.toLowerCase()));

        if (!hasImported) {
            dataFile = importTemplateModule.replace(new RegExp('&importPath', 'g'), providerPath).replace(new RegExp('&entity', 'g'), nameProvider.toLowerCase()).concat(dataFile);
            dataFile = dataFile.replace(new RegExp('&importService', 'g'), providerService);

            fs.writeFile('./src/app/app.module.ts', dataFile, (err2) => {
                if (err2) {
                    console.log(err2);
                    throw er2;
                }
                console.log('Import line added!');
                callback.call();
            });
        } else {
            callback.call();
        }
    });
}

function prepareProviders(nameProvider, callback) {
    nameProvider = nameProvider.concat('Service');
    fs.readFile('./src/app/app.module.ts', 'utf8', function (err, dataFile) {
        var object;
        if (err) {
            console.log(err);
            throw err;
        }
        var lines = dataFile.split('\n');
        var importLines = lines.filter(importFilter);

        var indexStart = providersLineIndexStart(lines);
        var indexEnd = providersLineIndexEnd(lines, indexStart);
        var hasProvider = false;
        for (var line = indexStart + 1; line < indexEnd; line++) {
            if (checkTerms(lines[line], nameProvider)) {
                hasProvider = true;
                break;
            }
        }
        if (!hasProvider) {
            lines.splice(indexStart + 1, 0, '    ' + nameProvider + ',');
            dataFile = lines.join('\n');
            fs.writeFile('./src/app/app.module.ts', dataFile, (err) => {
                if (err) {
                    console.log(err);
                    throw err;
                }
                console.log('Provider added!');
                callback.call();
            });
        } else {
            callback.call();
        }
    });
}

function providersLineIndexStart(lines) {
    var index = -1;
    var n = lineIndexStart(lines, '@');
    for (var line = n; line < lines.length; line++) {
        if (lines[line] === undefined)
            continue;
        if (checkTerms(lines[line], 'providers:')) {
            index = line;
            break;
        }
    }
    return index;
}

function lineIndexStart(lines, termStr) {
    var index = -1;
    for (var line in lines) {
        if (lines[line].startsWith(termStr) === true) {
            index = line;
            break;
        }
    }
    return index;
}

function providersLineIndexEnd(lines, start) {
    var index = -1;

    for (var line = start; line < lines.length; line++) {
        if (checkTerms(lines[line], ']')) {

            index = line;
            break;
        }
    }
    return index;
}


function asyncLoop(iterations, func, callback) {
    let index = 0;
    let done = false;
    let loop = {
        next: function () {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function () {
            return index - 1;
        },

        break: function () {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
}

function readFieldsTarget(entity) {

    classContentBuffer = '';
    var indexTmpl = 'type: "soql", query: "SELECT &targetFields FROM &SoupName &whereClause"';
    var targetFilds = [];
    for (var i = 0; i < entity.Fields.length; i++) {
        if (entity.Fields[i].IsNotQuery !== true) {
            targetFilds.push(entity.Fields[i].Name);
        }
    }
    classContentBuffer = targetFilds.join(", ");
    classContentBuffer = indexTmpl.replace(new RegExp('&targetFields', 'g'), classContentBuffer);
    var whereClause = "";
    if (entity.hasOwnProperty("Restrictions")) {
      whereClause = "WHERE &targetWhere";

      var targetWhere = "";
      for (var i = 0; i < entity.Restrictions.length; i++) {
        var replace = "&field IN (&values)";
        let field = entity.Restrictions[i].Field;
        var values = "";
        for (var j = 0; j < entity.Restrictions[i].PossibleValues.length; j++) {
          values += "'"+entity.Restrictions[i].PossibleValues[j]+"'";
          if (j < (entity.Restrictions[i].PossibleValues.length-1)) {
            values += ",";
          }
        }
        replace = replace.replace(new RegExp('&field', 'g'), field);
        replace = replace.replace(new RegExp('&values', 'g'), values);
        targetWhere = targetWhere + replace;
        if (i < (entity.Restrictions.length-1)) {
          targetWhere += " AND ";
        }
      }

      whereClause = whereClause.replace(new RegExp('&targetWhere', 'g'), targetWhere);
    }
    classContentBuffer = classContentBuffer.replace(new RegExp('&whereClause', 'g'), whereClause);

    return classContentBuffer;
}

function readFieldsSyncUp(entity) {
    let syncUpFields = {
        allSyncUpFields: "",
        secondaryIdFields: ""
    };
    var allSyncUpFields = [];
    var secondaryIdFields = [];
    for (var i = 0; i < entity.Fields.length; i++) {
        if (entity.Fields[i].IsNotQueryUP !== true) {
            allSyncUpFields.push('"' + entity.Fields[i].Name + '"');
        }
        if (entity.Fields[i].IsSecondaryId === true) {
            secondaryIdFields.push('"' + entity.Fields[i].Name + '"');
        }
    }

    syncUpFields.allSyncUpFields = allSyncUpFields;
    syncUpFields.secondaryIdFields = secondaryIdFields;

    return syncUpFields;
}

function makeAutenticateProvider(callback) {
    var classFileName = './src/providers/';
    var className = 'Autenticate';
    checkDir(classFileName + className.toLowerCase() + '/');

    classFileName = classFileName.concat(className.toLowerCase() + '/').concat(className.toLowerCase()).concat('-service-salesforce.ts').toLowerCase();

    fs.writeFile(classFileName, `import { Injectable } from '@angular/core';

    @Injectable()
    export class AutenticateService {

      constructor() {
      }

      logoutSf() {
        if (window.hasOwnProperty('logout')) {
              window['logout'].call();
        }else{
            console.log("propertie \'logout\' not found!");
        }
      }

      getCurrentUser():Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('getCurrentUser')) {
                window['getCurrentUser'].call(null, resolve, reject);
            }else{
                reject("propertie \'getCurrentUser\' not found!");
            }
        });
      }

      authenticate(sucess: Function, error: Function):Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('authenticate')) {
                window['authenticate'].call(null, resolve, reject);
            }else{
                reject("propertie \'authenticate\' not found!");
            }
        });
      }

      removeAllStores(sucess: Function, error: Function):Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('removeAllStores')) {
                window['removeAllStores'].call(null, resolve, reject);
            }else{
                reject("propertie 'removeAllStores' not found!");
            }
        });
      }

      removeAllGlobalStores(sucess: Function, error: Function):Promise<any>{
        return new Promise((resolve, reject) => {
            if (window.hasOwnProperty('removeAllGlobalStores')) {
                window['removeAllGlobalStores'].call(null, resolve, reject);
            }else{
                reject("propertie 'removeAllGlobalStores' not found!");
            }
        });
      }

    }
    `, function (err3) {
            if (err3) {
                return console.log(err3);
            } else {
                //imports.push(importTemplateProvider.replace(new RegExp('&EntityName', 'g'), 'Autenticate').replace(new RegExp('&entity', 'g'), 'autenticate').replace(new RegExp('&providerPath', 'g'), 'autenticate-service-salesforce'));
                //constructorParameters.push(constructorTemplate.replace(new RegExp('&EntityName', 'g'), 'Autenticate').replace(new RegExp('&entity', 'g'), 'autenticate'));
                //gets.push(getServiceTemplate.replace(new RegExp('&EntityName', 'g'), 'Autenticate').replace(new RegExp('&entity', 'g'), 'autenticate'));
                console.log('arquivo criado: ' + classFileName);
                callback();
            }
        });
}

function getRelatedTemplate(type) {
    if (type === 'Model') {
        return `
    &Name:&ToEntityModel = new &ToEntityModel(null);

    set&Name(provider: &ToEntityService): Promise<any> {
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(&ByColumn, this.&OnColumn, 1, 'ascending', null,
                (response) => {
                    this.&Name = new &ToEntityModel(response.currentPageOrderedEntries[0]);
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }`;
    } else {
        return `
    &Name:Array<&ToEntityModel> = [];

    set&Name(provider: &ToEntityService): Promise<any> {
        this.&Name = [];
        return new Promise((resolve, reject) => {
            provider.queryExactFromSoup(&ByColumn, this.&OnColumn, 1000, 'ascending', "&OrderPath",
                (response) => {
                    for(var i in response.currentPageOrderedEntries){
                        this.&Name.push(new &ToEntityModel(response.currentPageOrderedEntries[i]));
                    }
                    resolve();
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }`;
    }
}

function readFieldsRelated(entity) {

    var response = {};
    var importsBuffer = [];
    classContentBuffer = '';

    for (var i = 0; i < entity.Related.length; i++) {
        var getTmpl = getRelatedTemplate(entity.Related[i].Type);

        classContentBuffer = classContentBuffer.concat(getTmpl.replace(new RegExp('&Name', 'g'), entity.Related[i].Name)
            .replace(new RegExp('&ToEntity', 'g'),  entity.Related[i].ToEntity)
            .replace(new RegExp('&OnColumn', 'g'),  entity.Related[i].OnColumn)
            .replace(new RegExp('&ByColumn', 'g'),  entity.Related[i].ByColumn)
            .replace(new RegExp('&OrderPath', 'g'), entity.Related[i].OrderPath || ''));

        var providerPath = entity.Related[i].ToEntity.concat('-service-salesforce').toLowerCase();
        importsBuffer.push(importTemplateModel.replace(new RegExp('&EntityName', 'g'), entity.Related[i].ToEntity).replace(new RegExp('&entity', 'g'), entity.Related[i].ToEntity.toLowerCase()).replace(new RegExp('&providerPath', 'g'), providerPath));
    }
    response.importsBuffer = importsBuffer;
    response.classContentBuffer = classContentBuffer;
    return response;
}

function readRequiredFields(entity) {

    classContentBuffer = '';
    var requiredTmpl = `
            if(this.&FieldName === null || this.&FieldName === undefined || this.&FieldName === ''){
                invalidFields.push({"Field":'&FieldName', "Reason": "This field can't be empty"});
            }
            `;
    var regexTmpl = `
            var regex&FieldName = new RegExp(\'&RegexField\', 'i');
            if(this.&FieldName !== null && this.&FieldName !== undefined){
                if(!regex&FieldName.test(this.&FieldName)){
                    invalidFields.push({"Field":'&FieldName', "Reason": "&ErrorMessage"});
                }
            }
            `;
    var propertieTmpl = `
            if(this.&FieldName !== null && this.&FieldName !== undefined){
                if(this.&FieldName.&Propertie &Operator &Value){
                    invalidFields.push({"Field":'&FieldName', "Reason": "&ErrorMessage"});
                }
            }
            `;
    for (var i = 0; i < entity.Fields.length; i++) {
        var currentField = entity.Fields[i];
        var tempRequired = requiredTmpl.replace(new RegExp('&FieldName', 'g'), currentField.Name);
        if (currentField.Required === true) {
            classContentBuffer = classContentBuffer.concat(tempRequired);
        }

        if (currentField.ValidationRules !== undefined && currentField.ValidationRules !== null) {
            for (var j = 0; j < currentField.ValidationRules.length; j++) {
                var currentValidation = currentField.ValidationRules[j];
                console.log('currentValidation ' + Object.keys(currentValidation))
                if (currentValidation.Regex !== null && currentValidation.Regex !== undefined) {
                    console.log('Regex ' + currentValidation.Regex)
                    var tempRegex = regexTmpl.replace(new RegExp('&FieldName', 'g'), currentField.Name);
                    tempRegex = tempRegex.replace(new RegExp('&RegexField', 'g'), currentValidation.Regex);
                    tempRegex = tempRegex.replace(new RegExp('&ErrorMessage', 'g'), currentValidation.ErrorMessage);
                    classContentBuffer = classContentBuffer.concat(tempRegex);
                } else {
                    var tempPropertie = propertieTmpl.replace(new RegExp('&FieldName', 'g'), currentField.Name);
                    tempPropertie = tempPropertie.replace(new RegExp('&Propertie', 'g'), currentValidation.Propertie);
                    tempPropertie = tempPropertie.replace(new RegExp('&Operator', 'g'), currentValidation.Operator);
                    tempPropertie = tempPropertie.replace(new RegExp('&Value', 'g'), currentValidation.Value);
                    tempPropertie = tempPropertie.replace(new RegExp('&ErrorMessage', 'g'), currentValidation.ErrorMessage);
                    classContentBuffer = classContentBuffer.concat(tempPropertie);
                }
            }
        }
    }
    return classContentBuffer;
}
