#!/usr/bin/env node

var shell = require('shelljs');
var path = require('path');
var exec = require('child_process').exec;
var cordovaLib = require('cordova-lib').cordova;
var fs = require('fs-extra');


//Organizar essa quantidade de variavel....

var rootcordova = cordovaLib.findProjectRoot();

var PlatformFile = "platforms/android/project.properties";
var customFile = "custom_plugins/project.properties"
var serverFolder= "platforms/android/app/src/main/res/xml"
var serverFolderFile= "platforms/android/app/src/main/res/xml/servers.xml"
var serverFile = "custom_plugins/servers.xml"

var platformAndroid = path.join(rootcordova, PlatformFile);
var customAndroid = path.join(rootcordova, customFile);
const serverPath = path.join(rootcordova, serverFolder);
const serverPathFile = path.join(rootcordova, serverFolderFile);
const serverXml = path.join(rootcordova, serverFile);


if(fs.existsSync(platformAndroid)){

  shell.echo('Copying project.properties...');

  fs.copy(customAndroid, platformAndroid)
    .then(() => console.log('project.properties Copied.'))
    .catch(err => console.error(err))

  shell.echo('Copying servers.xml...');

  fs.ensureDir(serverPath)
  .then(() => {
    console.log('res/xml folder for servers.xml exist')
    fs.ensureFile(serverPathFile)
    .then(() => {
      fs.copy(serverXml, serverPathFile)
        .then(() => console.log('servers.xml Copied.'))
        .catch(err => console.error(err))
    })
    .catch(err => {
      console.error(err)
    })

  })
  .catch(err => {
    console.error(err)
  })

}
