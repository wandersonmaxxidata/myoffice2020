/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.monsanto.brz.myOffice.Test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.monsanto.brz.myOffice.Test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 4000004;
  public static final String VERSION_NAME = "4.0.7";
}
