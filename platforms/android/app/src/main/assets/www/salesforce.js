var sfOAuthPlugin = function () { return cordova.require("com.salesforce.plugin.oauth"); };
var sfAccountManagerPlugin = function () { return cordova.require("com.salesforce.plugin.sfaccountmanager"); };
var sfSmartstore = function () { return cordova.require("com.salesforce.plugin.smartstore"); };
var logToConsole = function () { return cordova.require("com.salesforce.util.logger").logToConsole; };
var sfSmartsync = function () { return cordova.require("com.salesforce.plugin.smartsync"); };
var sfNetwork = function () { return cordova.require("com.salesforce.plugin.network"); };

var syncComplete = new Event('syncComplete');
var supportsPassive = false;

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function soupExists(storeConfig, soupName, sucessCb, errorCb) {
    console.log('soupExists soupName', soupName);
    sfSmartstore().soupExists(storeConfig,
        soupName,
        sucessCb,
        errorCb
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} indexes
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function registerSoup(storeConfig, soupName, indexes, sucessCb, errorCb) {
    console.log('registerSoup soupName', soupName);
    console.log('registerSoup indexes', indexes);

    sfSmartstore().registerSoup(storeConfig,
        soupName,
        indexes,
        sucessCb,
        errorCb
    );
}
/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} entries
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function upsertSoupEntries(storeConfig, soupName, entries, sucessCb, errorCb) {
    console.log('addEntriesToSoup soupName', soupName);
    console.log('addEntriesToSoup entries', entries);
    //TODO: adicionar aqui a verificação de existencia do {soupName}
    sfSmartstore().upsertSoupEntries(storeConfig, soupName, entries,
        function (items) {
            logToConsole()("added entries: " + items.length);
            console.log("Soup upsert OK");
            if (typeof sucessCb !== "undefined") {
                sucessCb(items);
            }
        },
        function (err) {
            logToConsole()("onErrorUpsert: " + err);
            console.log("Soup upsert ERROR");
            if (typeof errorCb !== "undefined") {
                errorCb(err);
            }
        }
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} indexPath
 * @param {*} beginKey
 * @param {*} endKey
 * @param {*} direction
 * @param {*} orderPath
 * @param {*} pageSize
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function queryRangeFromSoup(storeConfig, soupName, indexPath, beginKey, endKey, direction, pageSize, orderPath, sucessCb, errorCb) {
    var querySpec = sfSmartstore().buildRangeQuerySpec(indexPath, beginKey, endKey, direction, pageSize, orderPath, null);

    sfSmartstore().querySoup(storeConfig, soupName, querySpec,
        sucessCb,
        errorCb
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} indexPath
 * @param {*} likeKey
 * @param {*} orderPath
 * @param {*} pageSize
 * @param {*} direction
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function queryLikeFromSoup(storeConfig, soupName, indexPath, likeKey, direction, pageSize, orderPath, sucessCb, errorCb) {
    var querySpec = sfSmartstore().buildLikeQuerySpec(indexPath, likeKey, direction, pageSize, orderPath, null);

    sfSmartstore().querySoup(storeConfig, soupName, querySpec,
        sucessCb,
        errorCb
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} path
 * @param {*} direction
 * @param {*} pageSize
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function queryAllFromSoup(storeConfig, soupName, path, direction, pageSize, sucessCb, errorCb) {
    var querySpec = sfSmartstore().buildAllQuerySpec(path, direction, pageSize, null);

    sfSmartstore().querySoup(storeConfig, soupName, querySpec,
        sucessCb,
        errorCb
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} path
 * @param {*} matchKey
 * @param {*} pageSize
 * @param {*} direction
 * @param {*} orderPath
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function queryExactFromSoup(storeConfig, soupName, path, matchKey, pageSize, direction, orderPath, sucessCb, errorCb) {

    var querySpec = sfSmartstore().buildExactQuerySpec(path, matchKey, pageSize, direction, orderPath, null);

    sfSmartstore().querySoup(storeConfig, soupName, querySpec,
        sucessCb,
        errorCb
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} smartSql
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function querySmartFromSoup(storeConfig, smartSql, pageSize, sucessCb, errorCb) {

    var querySpec = sfSmartstore().buildSmartQuerySpec(smartSql, pageSize);

    sfSmartstore().runSmartQuery(storeConfig, querySpec,
        sucessCb,
        errorCb
    );
}

function moveCursorToPageIndex(storeConfig, cursor, newPageIndex, sucessCb, errorCb) {
    sfSmartstore().moveCursorToPageIndex(storeConfig, cursor, newPageIndex,
        sucessCb,
        errorCb
    );
}

function moveCursorToNextPage(storeConfig, cursor, sucessCb, errorCb) {
    sfSmartstore().moveCursorToNextPage(storeConfig, cursor,
        sucessCb,
        errorCb
    );
}

function moveCursorToPreviousPage(storeConfig, cursor, sucessCb, errorCb) {
    sfSmartstore().moveCursorToPreviousPage(storeConfig, cursor,
        sucessCb,
        errorCb
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} inputStr
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function retrieveEntries(storeConfig, soupName, inputStr, sucessCb, errorCb) {
    if (inputStr.length === 0) {
        inputStr = null;
    }

    logToConsole()("retrieveEntries soupName ", soupName);
    logToConsole()("retrieveEntries inputStr ", inputStr);
    var entryIds = eval(inputStr);

    sfSmartstore().retrieveSoupEntries(storeConfig, soupName,
        entryIds,
        sucessCb,
        errorCb
    );
}
/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} inputStr
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function removeEntries(storeConfig, soupName, inputStr, sucessCb, errorCb) {
    if (inputStr.length === 0) {
        inputStr = null;
    }

    logToConsole()("removeEntries soupName ", soupName);
    logToConsole()("removeEntries inputStr ", inputStr);
    var entryIds = eval(inputStr);

    sfSmartstore().removeFromSoup(storeConfig, soupName,
        entryIds,
        sucessCb,
        errorCb
    );
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} inputStr
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function removeAllStores(sucessCb, errorCb) {
    console.log("remove all stores");
    sfSmartstore().removeAllStores(sucessCb, errorCb);
}

function clearSoup(storeConfig, soupName, sucessCb, errorCb) {
    console.log("clear Soup");
    sfSmartstore().clearSoup(storeConfig, soupName, sucessCb, errorCb)
}

function authenticate(sucessCb, errorCb) {
    console.log("Authenticate");
    sfOAuthPlugin().authenticate(sucessCb, errorCb)
}


/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} inputStr
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function removeAllGlobalStores(sucessCb, errorCb) {
    console.log("remove all global stores");
    sfSmartstore().removeAllGlobalStores(sucessCb, errorCb);
}

/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} fieldlist
 * @param {*} sucessCb
 * @param {*} errorCb
 * 
 * 
 * 
 */


function syncUp02(entityName,storeConfig, soupName, fieldlist, handleProgress, context, successCB, errorCB) {
    sessionStorage.setItem('statusSync'+entityName,'RUNNING');
    sfSmartsync().syncUp(storeConfig, soupName, { mergeMode: sfSmartsync().MERGE_MODE.OVERWRITE, fieldlist: fieldlist },function (syncEvent) {
        document.addEventListener("sync", function (event) {
            handleSyncUpdate(event.detail, handleProgress, context);
            // sessionStorage.setItem('statusSync'+entityName,event.detail.status);
            if (event.detail.status !== "RUNNING") {
                document.removeEventListener('sync', arguments.callee, { passive: true });
            }
        }, { passive: true });
    }, handleSyncError.bind(this));
            document.addEventListener("syncComplete", function () {

                if (event.subType === 'syncUp') {
                    document.removeEventListener('syncComplete', arguments.callee, { passive: true });
                    if (event.status === 'sucess') {
                        
                        sessionStorage.setItem('statusSynclog',JSON.stringify(event));
                        console.log(event)
                        successCB('DONE');
                        // errorCB(event);
                    } else {
                        errorCB(event);
                    }
                }
    }, { passive: true });
}


function syncUpForce(storeConfig, soupName, fieldlist, successCB, errorCB) {
    
    sfSmartsync().syncUp(storeConfig, soupName, { mergeMode: sfSmartsync().MERGE_MODE.OVERWRITE, fieldlist: fieldlist },function (syncEvent) {
        document.addEventListener("sync", function (event) {
            sessionStorage.setItem('statusSync',event.detail.status);
            sessionStorage.setItem('statusSynclog',JSON.stringify(event));
            // handleSyncUpdate(event.detail, handleProgress, context);
            // sessionStorage.setItem('statusSync'+entityName,event.detail.status);
            if (event.detail.status !== "RUNNING") {
                sessionStorage.setItem('statusSync',event.detail.status);
                sessionStorage.setItem('statusSynclog',JSON.stringify(event));
                document.removeEventListener('sync', arguments.callee, { passive: true });
            }
        }, { passive: true });
    }, handleSyncError.bind(this));
            document.addEventListener("syncComplete", function () {

                if (event.subType === 'syncUp') {
                    document.removeEventListener('syncComplete', arguments.callee, { passive: true });
                    if (event.status === 'sucess') {
                        sessionStorage.setItem('statusSync','RUNNING');
                        sessionStorage.setItem('statusSynclog',JSON.stringify(event));
                        console.log(event)

                        successCB('DONE');
                        // errorCB(event);
                    } else {
                        sessionStorage.setItem('statusSynclog',JSON.stringify(event));
                        errorCB(event);
                    }
                }
    }, { passive: true });
}



function syncUp(storeConfig, soupName, fieldlist, handleProgress, context, sucessCb, errorCb) {
    sfSmartsync().syncUp(storeConfig, soupName, { mergeMode: sfSmartsync().MERGE_MODE.OVERWRITE, fieldlist: fieldlist },
        function (syncEvent) {
            document.addEventListener("sync", function (event) {
                        //W
                        console.log('=====================================================================================');
                        console.log(event)
                        //W
                handleSyncUpdate(event.detail, handleProgress, context);
                if (event.detail.status !== "RUNNING") {
                    document.removeEventListener('sync', arguments.callee, { passive: true });
                }
            }, { passive: true });
        }, handleSyncError.bind(this));
    document.addEventListener("syncComplete", function () {
                        //W
                        console.log('=====================================================================================');
                        console.log(event)
                         //W
        if (event.subType === 'syncUp') {
            document.removeEventListener('syncComplete', arguments.callee, { passive: true });
            if (event.status === 'sucess') {
                sucessCb(event);
            } else {
                errorCb(event);
            }
        }
    }, { passive: true });
}


/**
 *
 * @param {*} storeConfig
 * @param {*} soupName
 * @param {*} fieldlist
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function syncUpTarg(targ,storeConfig, soupName, fieldlist, handleProgress, context, sucessCb, errorCb) {
    console.log('=====================================================================================');
    console.log(targ)
    console.log('=====================================================================================');
    console.log(soupName)
    console.log('=====================================================================================');
    console.log(fieldlist)
    console.log('=====================================================================================');

    
    sfSmartsync().syncUp(storeConfig,targ, soupName, { mergeMode: sfSmartsync().MERGE_MODE.OVERWRITE, fieldlist: fieldlist },
        function (syncEvent) {
            document.addEventListener("sync", function (event) {
                        //W
                        
                        console.log('=====================================================================================');
                        console.log(event)
                        //W
                handleSyncUpdate(event.detail, handleProgress, context);
                if (event.detail.status !== "RUNNING") {
                    document.removeEventListener('sync', arguments.callee, { passive: true });
                }
            }, { passive: true });
        }, handleSyncError.bind(this));
    document.addEventListener("syncComplete", function () {
                        //W
                        console.log('=====================================================================================');
                        console.log(event)
                         //W
        if (event.subType === 'syncUp') {
            document.removeEventListener('syncComplete', arguments.callee, { passive: true });
            if (event.status === 'sucess') {
                sucessCb(event);
            } else {
                errorCb(event);
            }
        }
    }, { passive: true });
}
/**
 *
 * @param {*} storeConfig
 * @param {*} targetSync
 * @param {*} soupName
 * @param {*} lastSyncDownId
 * @param {*} sucessCb
 * @param {*} errorCb
 */
function syncDown(storeConfig, targetSync, soupName, lastSyncDownId, handleProgress, context, sucessCb, errorCb) {
    if (lastSyncDownId === undefined || lastSyncDownId === "" || lastSyncDownId === null) {
        sfSmartsync().syncDown(storeConfig, targetSync, soupName, { mergeMode: sfSmartsync().MERGE_MODE.LEAVE_IF_CHANGED },
            function (syncEvent) {

                document.addEventListener("sync", function () {
                    handleSyncUpdate(event.detail, handleProgress, context);
                    if (event.detail.status !== "RUNNING") {
                        document.removeEventListener('sync', arguments.callee, { passive: true });
                    }
                }, { passive: true });
            }, handleSyncError.bind(this));
    } else {
        sfSmartsync().reSync(storeConfig, lastSyncDownId, function (syncEvent) {
            document.addEventListener("sync", function () {
                handleSyncUpdate(event.detail, handleProgress, context);
                if (event.detail.status !== "RUNNING") {
                    document.removeEventListener('sync', arguments.callee, { passive: true });
                }
            }, { passive: true });
        }, handleSyncError.bind(this));
        console.log('***** reSync start *****');
    }

    document.addEventListener("syncComplete", function () {
        if (event.subType === 'syncDown') {
            document.removeEventListener('syncComplete', arguments.callee, { passive: true });
            if (event.status === 'sucess') {
                sucessCb(event);
            } else {
                errorCb(event);
            }
        }
    }, { passive: true });
}

// function syncDown(storeConfig, targetSync, soupName, lastSyncDownId, handleProgress, context, sucessCb, errorCb) {
//     if (lastSyncDownId === undefined || lastSyncDownId === "" || lastSyncDownId === null) {
//         sfSmartsync().syncDown(storeConfig, targetSync, soupName, { mergeMode: sfSmartsync().MERGE_MODE.LEAVE_IF_CHANGED },
//             function (syncEvent) {

//                 document.addEventListener("sync", function () {
//                     handleSyncUpdate(event.detail, handleProgress, context);
//                     if (event.detail.status !== "RUNNING") {
//                         document.removeEventListener('sync', arguments.callee, { passive: true });
//                     }
//                 }, { passive: true });
//             }, handleSyncError.bind(this));
//     } else {
//         sfSmartsync().syncDown(storeConfig, targetSync, soupName, { mergeMode: sfSmartsync().MERGE_MODE.LEAVE_IF_CHANGED },
//             function (syncEvent) {

//                 document.addEventListener("sync", function () {
//                     handleSyncUpdate(event.detail, handleProgress, context);
//                     if (event.detail.status !== "RUNNING") {
//                         document.removeEventListener('sync', arguments.callee, { passive: true });
//                     }
//                 }, { passive: true });
//             }, handleSyncError.bind(this));
//         console.log('***** reSync start *****');
//     }

//     document.addEventListener("syncComplete", function () {
//         if (event.subType === 'syncDown') {
//             document.removeEventListener('syncComplete', arguments.callee, { passive: true });
//             if (event.status === 'sucess') {
//                 sucessCb(event);
//             } else {
//                 errorCb(event);
//             }
//         }
//     }, { passive: true });
// }
/**
 *
 * @param {*} sync
 */
function handleSyncUpdate(sync, handleProgress, context) {
    syncComplete.subType = sync.type;
    handleProgress.call(context, sync.progress);
    if (sync.status === "RUNNING") {
        console.log("Sync - " + sync.type + " " + Math.round(sync.progress) + "%");
    } else if (sync.status === "FAILED") {
        syncComplete.status = 'error';
        document.dispatchEvent(syncComplete);
    } else if (sync.status === "DONE") {
        syncComplete.status = 'sucess';
        if (sync.type === "syncDown") {
            syncComplete.lastSyncDownId = sync._soupEntryId;
        }
        document.dispatchEvent(syncComplete);
    } else {
        /*syncComplete.status = 'sucess';
        document.dispatchEvent(syncComplete);*/
        console.log('handleSyncUpdate', sync.status);
    }
}
/**
 *
 * @param {*} error
 */
function handleSyncError(error) {
    console.log('error', error);
    syncComplete.status = 'error';
    document.dispatchEvent(syncComplete);
}


function logout(error) {
    console.log('logout...');
    sfOAuthPlugin().logout();
}

function getCurrentUser(success, fail) {
    sfAccountManagerPlugin().getCurrentUser(success, fail);
}

function getTableMetaData(instanceServer, tableName, context, sucessCb, errorCb) {
    var path = '/services/data/v41.0/sobjects/tableName/describe';
    path = path.replace('tableName', tableName);

    sfNetwork().sendRequest(instanceServer, path, function (res) {
        console.log(res);
        sucessCb.call(context, res);
    }, function (err) {
        console.log(err);
        errorCb.call(context, err);
    });

}

//Ghost
function clearGhostRecords(storeConfig, syncId, successCB, errorCB) {
    console.log('---| cleanResyncGhosts:ghost:syncId: ', syncId)
    sfSmartsync().cleanResyncGhosts(storeConfig, syncId, successCB, errorCB)
}

function syncStatus(storeConfig, syncId, successCB, errorCB) {
    console.log('---| getSyncStatus:syncId: ', syncId)
    sfSmartsync().getSyncStatus(storeConfig, syncId, successCB, errorCB)
}

function allStorages(){
    sfAccountManagerPlugin.setDevSupportEnabled(true);
    let success = (soupRet) => { return soupRet };
    
    let failure = (error) => console.error(`Registering soup fails with error: ${error}`);
    sfSmartstore().getAllStores(success, failure);
}

function allStoragesGlobal(){
    sfAccountManagerPlugin.setDevSupportEnabled(true);
    let success = (soupRet) => { return soupRet };
    let failure = (error) => console.error(`Registering soup fails with error: ${error}`);
    sfSmartstore().getAllGlobalStores(success, failure);
}
