# MyOffice Build Guide

# Installation
##### MyOffice Requirements:
> [Ionic 3.20.0](http://ionicframework.com/getting-started/)  
> [Node 8.12.0](https://nodejs.org/en/download/)  
> [Cordova 8.0.0](https://cordova.apache.org/#getstarted)  

Install Node with Homebrew or install from [here](https://nodejs.org/en/download/)
```
brew install node
```
Install Ionic and Cordova globally
```
npm install -g cordova ionic
```
Clone the Repo and install dependencies
```
git clone https://github.platforms.engineering/VVVR2/myOffice2.0.git
cd myOffice2.0
npm install
```

# Build
`ionic cordova prepare` will compile ionic project and generate platform configurations
Bundle id and Version number can be changed in `config.xml`

> Prepare iOS
```
ionic cordova prepare ios
```

> Prepare Android

```
ionic cordova prepare android
```

Open Xcode `myOffice2.0/platforms/ios/MyOffice.xcworkspace`  
Open Android Studio at `myOffice2.0/platforms/android`
#### NOTE: Build and emulate from Android Studio or Xcode

# Test Build
App Name `MyOffice-Test`  
Bundle id / Package id `com.monsanto.brz.myOffice.Test`
# Production Build
App Name `MyOffice`  
Bundle id / Package id `com.monsanto.brz.myOffice`
#### Android Prod
Comment out "Teste" and "Sandbox" urls from `android/SalesforceSDK/res/xml/servers.xml`
```xml
<!--<server name="Teste" url="https://monsanto-myoffice&#45;&#45;test.cs95.my.salesforce.com"/>-->
<server name="Production" url="https://monsanto-myoffice.my.salesforce.com"/>
<!--<server name="Sandbox" url="https://test.salesforce.com"/>-->
```
Comment out `getMenuInflater()`  
from `android/SalesforceSDK/src/com/salesforce/androidsdk/ui/LoginActivity.java`

```java
 public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.sf__login, menu);
        return super.onCreateOptionsMenu(menu);
    }
```

#### iOS Prod
Uncomment chunk that sets `loginViewConfigshowSettingsIcon = NO;`  
from `MyOffice/Plugins/AppDelegate+SalesforceHybridSDK.m`
```objc
    SFSDKLoginViewControllerConfig *loginViewConfig = [[SFSDKLoginViewControllerConfig  alloc] init];
    loginViewConfig.showSettingsIcon = NO;
    [SFUserAccountManager sharedInstance].loginViewControllerConfig = loginViewConfig;
```

#### NOTE: Going forward production flags will automate these changes
